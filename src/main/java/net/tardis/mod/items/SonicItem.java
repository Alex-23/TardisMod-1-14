package net.tardis.mod.items;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.artron.IArtronItemStackBattery;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.sonic.ISonic;
import net.tardis.mod.cap.items.sonic.SonicCapability;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.renderers.SonicRenderer;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.misc.ConsoleBoundItem;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.sonic.ISonicPart;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * Created by Swirtzly
 * on 22/08/2019 @ 20:15
 */
public class SonicItem extends ConsoleBoundItem implements IArtronItemStackBattery {
    private static final Integer COOLDOWN_TIME = 40;
    private static final Integer MAX_CHARGE = 500;

    public SonicItem() {
        super(new Item.Properties().maxStackSize(1).group(TItemGroups.MAIN).setISTER(() -> SonicRenderer::new));
    }

    public static void syncCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.getOrCreateTag().merge(stack.getShareTag());
        }
    }

    public static void readCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.readShareTag(stack.getOrCreateTag());
        }
    }

    public static AbstractSonicMode getCurrentMode(ItemStack stack) {
        ISonic data = SonicCapability.getForStack(stack).orElseGet(null);
        return data.getMode();
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
        ItemStack itemstack = playerIn.getHeldItem(handIn);
        if (playerIn.getActiveHand() != null && !worldIn.isRemote()) {
            if (!playerIn.getCooldownTracker().hasCooldown(itemstack.getItem())) {
            	if(!playerIn.isSneaking())
                    playerIn.setActiveHand(handIn);
            }
        }
        if (worldIn.isRemote() && playerIn.isSneaking()) {
    		ClientHelper.openGUI(TardisConstants.Gui.SONIC_MODE, new GuiContext());
    	}
        return new ActionResult<>(ActionResultType.SUCCESS, itemstack);
    }

    @Override
    public void onPlayerStoppedUsing(ItemStack stack, World world, LivingEntity livingEntity, int timeLeft) {
        if (world.isRemote) return;
        AbstractSonicMode mode = getCurrentMode(stack);
        if (livingEntity instanceof PlayerEntity) {
            PlayerEntity player = (PlayerEntity) livingEntity;

            RayTraceResult raytraceresult = PlayerHelper.getPosLookingAt(player, getCurrentMode(stack).getReachDistance());

            if (raytraceresult != null) {
                if (raytraceresult.getType() == RayTraceResult.Type.BLOCK) {
                    BlockRayTraceResult blockraytraceresult = (BlockRayTraceResult) raytraceresult;
                    BlockPos pos = blockraytraceresult.getPos();
                    boolean flag = mode.processBlock(player, world.getBlockState(pos), player.getActiveItemStack(), pos);
                    runPassFail(player, world, pos, flag);
                    SonicCapability.getForStack(stack).ifPresent(cap -> cap.sync(player, Hand.MAIN_HAND));
                } else if (raytraceresult.getType() == RayTraceResult.Type.ENTITY) {
                    EntityRayTraceResult entityRayTraceResult = (EntityRayTraceResult) raytraceresult;
                    boolean worked = mode.processEntity(player, entityRayTraceResult.getEntity(), stack);
                    runPassFail(player, world, entityRayTraceResult.getEntity().getPosition(), worked);
                    SonicCapability.getForStack(stack).ifPresent(cap -> cap.sync(player, Hand.MAIN_HAND));
                }

            }
        }
    }

    public void runPassFail(PlayerEntity playerEntity, World world, BlockPos pos, boolean worked) {
        world.playSound(null, pos.getX(), pos.getY(), pos.getZ(), worked ? TSounds.SONIC_GENERIC.get() : TSounds.SONIC_FAIL.get(), SoundCategory.PLAYERS, 0.25F, 1.0F);
        if (worked) {
            playerEntity.getCooldownTracker().setCooldown(this, COOLDOWN_TIME);
        }

    }

    @Override
    public void onUsingTick(ItemStack stack, LivingEntity player, int count) {
        getCurrentMode(stack).updateHeld((PlayerEntity) player, stack);
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
        syncCapability(stack);
        super.inventoryTick(stack, worldIn, entityIn, itemSlot, isSelected);

       if(entityIn instanceof PlayerEntity){
           stack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
               AbstractSonicMode mode = getCurrentMode(stack);
               if(mode != null)
                   mode.inventoryTick((PlayerEntity) entityIn, stack);
           });
       }

    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        if (worldIn == null) return;
        SonicCapability.getForStack(stack).ifPresent((data) -> {
            if (data.getTardis() != null && getTardisName(stack) != null) {
                tooltip.add(new TranslationTextComponent(TardisConstants.Translations.TOOLTIP_ATTUNED_OWNER).appendSibling(new StringTextComponent(getTardisName(stack)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
            }
        	else {
        		tooltip.add(TardisConstants.Translations.TOOLTIP_NO_ATTUNED);
        	}
            
            tooltip.add(TardisConstants.Translations.TOOLTIP_HOLD_SHIFT);
            tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
            
            if (Screen.hasShiftDown()) {
            	tooltip.clear();
            	tooltip.add(0, stack.getDisplayName());
        		AbstractSonicMode mode = getCurrentMode(stack);
                tooltip.add(new TranslationTextComponent("sonic.mode.type", mode.getTranslation().mergeStyle(TextFormatting.GRAY)));
                tooltip.add(new TranslationTextComponent("sonic.mode.type.desc", new TranslationTextComponent(mode.getDescriptionLangKey()).mergeStyle(TextFormatting.GRAY)));
                tooltip.add(new TranslationTextComponent("sonic.mode.charge", data.getCharge()));	
                tooltip.add(new TranslationTextComponent(TardisConstants.Translations.STORED_SCHEMATICS));
                List<ITextComponent> schematicTexts = TextHelper.getSchematicsInList(data.getSchematics());
                tooltip.addAll(schematicTexts);
            }
            if (Screen.hasControlDown()) {
        		tooltip.clear();
            	tooltip.add(0, stack.getDisplayName());
            	AbstractSonicMode mode = getCurrentMode(stack);
            	if (mode.hasAdditionalInfo()) {
                    tooltip.addAll(mode.getAdditionalInfo());
                }
            	else {
                    if (mode.hasAdditionalInfo()) {
                        tooltip.add(TardisConstants.Translations.TOOLTIP_HOLD_SHIFT);
                    }
            	}
//            	tooltip.add(new TranslationTextComponent("tooltip.sonic.parts"));
//                for (ISonicPart.SonicPart value : ISonicPart.SonicPart.values()) {
//                    SonicBasePart.SonicComponentTypes part = SonicCapability.getForStack(stack).orElse(null).getSonicPart(value);
//                    tooltip.add(part.getName());
//                }
        	}
        });
    }

    @Override
    public void onCreated(ItemStack stack, World worldIn, PlayerEntity playerIn) {
        super.onCreated(stack, worldIn, playerIn);
        SonicCapability.getForStack(stack).ifPresent(ISonic::randomiseParts);
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items) {

        if (group == ItemGroup.SEARCH) {
            ItemStack stack = new ItemStack(TItems.SONIC.get());
            items.add(stack);
            return;
        }
        if(this.getGroup() == group) {
            for (SonicBasePart.SonicComponentTypes componentTypes : SonicBasePart.SonicComponentTypes.values()) {
                ItemStack stack = new ItemStack(TItems.SONIC.get());
                for (ISonicPart.SonicPart value : ISonicPart.SonicPart.values()) {
                    SonicCapability.getForStack(stack).ifPresent((data) -> {
                        data.setSonicPart(componentTypes, value);
                    });
                }
                if (!items.contains(stack)) {
                    items.add(stack);
                }
            }
        }
    }

    @Override
    public ActionResultType itemInteractionForEntity(ItemStack sonic, PlayerEntity player, LivingEntity target, Hand hand) {
        AbstractSonicMode mode = getCurrentMode(sonic);
        if (player.getCooldownTracker().hasCooldown(sonic.getItem())) return ActionResultType.FAIL;

        boolean flag = mode.processEntity(player, target, sonic);
        runPassFail(player, player.world, target.getPosition(), flag);
        return ActionResultType.PASS;
    }

    @Override
    public int getUseDuration(ItemStack stack) {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean canPlayerBreakBlockWhileHolding(BlockState state, World worldIn, BlockPos pos, PlayerEntity player) {
        return true;
    }


    @Override
    public boolean shouldSyncTag() {
        return true;
    }

    @Nullable
    @Override
    public CompoundNBT getShareTag(ItemStack stack) {
        CompoundNBT tag = stack.getOrCreateTag();
        SonicCapability.getForStack(stack).ifPresent(handler -> tag.put("cap_sync", handler.serializeNBT()));
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundNBT nbt) {
        super.readShareTag(stack, nbt);
        if (nbt != null) {
            if (nbt.contains("cap_sync")) {
                SonicCapability.getForStack(stack).ifPresent(handler -> handler.deserializeNBT(nbt.getCompound("cap_sync")));
            }
        }
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return oldStack.getItem() != newStack.getItem();
    }

    @Override
    public float charge(ItemStack stack, float amount) {
        float charge = getCharge(stack);
        float maxCharge = this.getMaxCharge(stack);
        //If adding more will go over the max charge, return however much is needed to reach max.
        if (charge + amount >= maxCharge) {
            float chargeToAdd = maxCharge - charge;
            this.writeCharge(stack, chargeToAdd, false);
            return chargeToAdd;
        } else {
            this.writeCharge(stack, amount, false);
            return amount;
        }
    }

    @Override
    public float discharge(ItemStack stack, float amount) {
        float current = getCharge(stack);
        if (amount <= current) {
            this.writeCharge(stack, amount, true);
            return amount;
        }
        this.writeCharge(stack, 0, true);
        return current;
    }

    @Override
    public float getCharge(ItemStack stack) {
        ISonic data = SonicCapability.getForStack(stack).orElse(null);
        return data.getCharge();
    }

    @Override
    public float getMaxCharge(ItemStack stack) {
        ISonic data = SonicCapability.getForStack(stack).orElse(null);
        if (data != null)
            return data.getMaxCharge();
        return MAX_CHARGE;
    }

    /**
     * Set charge to take or add
     *
     * @param stack
     * @param charge
     * @param discharge - true if taking away charge, false if adding in charge
     */
    public void writeCharge(ItemStack stack, float charge, boolean discharge) {
        ISonic data = SonicCapability.getForStack(stack).orElse(null);
        data.setCharge(discharge ? getCharge(stack) - charge : getCharge(stack) + charge);
    }

    @Override
	public ItemStack onAttuned(ItemStack stack, ConsoleTile tile) {
		setTardis(stack, tile.getWorld().getDimensionKey().getLocation());
		tile.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> setTardisName(stack, data.getTARDISName()));
		return stack;
	}

	@Override
	public int getAttunementTime() {
		return TardisConstants.ATTUNEMENT_TIME_LESSER;
	}

	@Override
	public ResourceLocation getTardis(ItemStack stack) {
		ISonic remote = stack.getCapability(Capabilities.SONIC_CAPABILITY).orElse(null);
		if(remote == null)
			return null;
		return remote.getTardis();
	}

	@Override
	public void setTardis(ItemStack stack, ResourceLocation world) {
		stack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(sonic -> sonic.setTardis(world));
	}

	@Override
	public String getTardisName(ItemStack stack) {
		if(stack.getTag() != null && stack.getTag().contains("tardis_name")) {
			return stack.getTag().getString("tardis_name");
		}
		return null;
	}

	@Override
	public void setTardisName(ItemStack stack, String name) {
		stack.getOrCreateTag().putString("tardis_name", name);
	}

}
