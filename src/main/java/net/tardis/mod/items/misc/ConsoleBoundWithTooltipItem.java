package net.tardis.mod.items.misc;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.constants.TardisConstants;
/** Base class for non capability holding items
 * <br> Inherit this you don't want to reimplement tooltips for your attunable item*/
public class ConsoleBoundWithTooltipItem extends ConsoleBoundItem{
	
	public ConsoleBoundWithTooltipItem(Properties properties) {
		super(properties);
		this.setShowTooltips(true);
	}

	@Override
	public void createDefaultTooltips(ItemStack stack, World worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
		if (this.getTardis(stack) != null) {
            tooltip.add(new TranslationTextComponent(TardisConstants.Translations.TOOLTIP_ATTUNED_OWNER).appendSibling(new StringTextComponent(this.getTardisName(stack)).mergeStyle(TextFormatting.LIGHT_PURPLE)));
        }
        else {
        	tooltip.add(TardisConstants.Translations.TOOLTIP_NO_ATTUNED);
        }
	}
	
	

}
