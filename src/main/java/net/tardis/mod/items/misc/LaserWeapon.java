package net.tardis.mod.items.misc;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShieldItem;
import net.minecraft.util.SoundCategory;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.projectiles.LaserEntity;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.LaserGunItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.AbstractWeapon;
import net.tardis.mod.sounds.TSounds;

public class LaserWeapon extends AbstractWeapon<LivingEntity>{
    private LivingEntity user;

    public LaserWeapon(LivingEntity user) {
        this.user = user;
    }

    @Override
    public void onHitEntityPre(LaserEntity laserEntity, Entity hitEntity) {
        if (!this.user.world.isRemote()) {
        	if (hitEntity instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity)hitEntity;
                if (player.getHeldItemOffhand().getItem() instanceof ShieldItem || player.getHeldItemMainhand().getItem() instanceof ShieldItem) {
                    ItemStack heldStack = PlayerHelper.isInMainHand(user, TItems.EARTHSHOCK_GUN.get()) ? user.getHeldItemMainhand() : user.getHeldItemOffhand();
                    WorldHelper.maybeDisableShieldNoEnchant(player, user, heldStack, player.getHeldItemOffhand());
                }
            }
            ItemStack stack = !user.getHeldItemMainhand().isEmpty() ? user.getHeldItemMainhand() : user.getHeldItemOffhand();
            if (!stack.isEmpty()) {
                if (stack.getItem() instanceof LaserGunItem) {
                    LaserGunItem laserGun = (LaserGunItem)stack.getItem();
                    LaserPowerTier tier = laserGun.getCurrentTier(stack);
                    if (tier == LaserPowerTier.HIGH) {
                        if (user.world.rand.nextFloat() <= 0.25F)
                            hitEntity.setFire(5);
                    }
                }
            }
        }
    }

    @Override
    public boolean useWeapon(LivingEntity entity) {
        if (!entity.world.isRemote) {
            
            ItemStack stack = !entity.getHeldItemMainhand().isEmpty() ? entity.getHeldItemMainhand() : entity.getHeldItemOffhand();
            if (!stack.isEmpty()) {
                if (stack.getItem() instanceof LaserGunItem) {
                    float soundPitch = 1F;
                    
                    LaserEntity laser = new LaserEntity(TEntities.LASER.get(), entity, entity.world, damage(), TDamageSources.causeTardisMobDamage(TDamageSources.LASER, entity));
                    LaserGunItem laserGun = (LaserGunItem)stack.getItem();
                    LaserPowerTier tier = laserGun.getCurrentTier(stack);
                    //Setup laser fire direction, velocity and inaccuracy
                    laser.setDirectionAndMovement(laser, entity.rotationPitch, entity.rotationYawHead, 0.0F, tier.getVelocity(), 0.1F);
                    laser.setRayLength(tier.getLaserLength());
                    laser.setColor(tier.getLaserColour());
                    soundPitch = tier.getSoundPitch();
                    laser.setWeaponType(this);
                    entity.world.addEntity(laser);
                    laserGun.removeCharge(entity, stack, tier.getChargeRequired());
                    entity.world.playSound(null, entity.getPosition(), TSounds.LASER_GUN_FIRE.get(), SoundCategory.HOSTILE, 1F, soundPitch);
                    if (entity instanceof PlayerEntity) {
                        PlayerEntity player = (PlayerEntity)entity;
                        if (!player.abilities.isCreativeMode)
                            player.getCooldownTracker().setCooldown(laserGun.getItem(), tier.getCooldownTicks());
                    }
                    return true;
                }
            }
            return false;
        }
        else return false;
    }
    
    
}
