package net.tardis.mod.misc;

public interface BiFunctionVoid<U, V> {

	void run(U first, V second);
}
