package net.tardis.mod.misc;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.InteriorEffectsMessage;
import net.tardis.mod.registries.InteriorHumRegistry;
import net.tardis.mod.sounds.InteriorHum;

public class InteriorEffectsHandler implements INBTSerializable<CompoundNBT> {

    private World world;

    private InteriorHum hum = InteriorHumRegistry.EIGHTY.get();
    private boolean hasHumChanged = false;

    public InteriorEffectsHandler(World world){
        this.world = world;
    }

    public void setHum(InteriorHum hum){
        this.hum = hum;
        this.hasHumChanged = true;
        this.updateClients();
    }

    public void tick(){

        if (world.isRemote && shouldPlayNextHumLoop() && hum.getLoopedSoundEvent() != null){
            ClientHelper.playMovingSound(ClientHelper.getClientPlayer(), hum.getLoopedSoundEvent(), SoundCategory.AMBIENT, 1.0F, false);
            this.hasHumChanged = false;
        }
    }

    public boolean shouldPlayNextHumLoop(){
        return hum != null && (this.hasHumChanged || world.getGameTime() % hum.getDurationInTicks() == 0);
    }

    public void updateClients(){
        if(!this.world.isRemote)
            Network.sendToAllInWorld(new InteriorEffectsMessage(this.hum), (ServerWorld) world);
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();

        if(hum != null)
            tag.putString("hum", hum.getRegistryName().toString());
        tag.putBoolean("hum_changed", this.hasHumChanged);

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        if(nbt.contains("hum"))
            this.hum = InteriorHumRegistry.HUM_REGISTRY.get().getValue(new ResourceLocation(nbt.getString("hum")));
        this.hasHumChanged = nbt.getBoolean("hum_changed");
    }
}
