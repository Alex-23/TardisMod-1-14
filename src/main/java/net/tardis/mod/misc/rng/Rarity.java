package net.tardis.mod.misc.rng;

public class Rarity<T> {

    private T object;
    private int weight;

    public Rarity(T object, int weight){
        this.object = object;
        this.weight = weight;
    }

    public int getWeight(){
        return this.weight;
    }

    public T getObject(){
        return this.object;
    }

}
