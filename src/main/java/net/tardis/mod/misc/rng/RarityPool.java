package net.tardis.mod.misc.rng;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Random;

public class RarityPool<T> {

    private List<Rarity<T>> objects = Lists.newArrayList();

    public T chooseRandom(Random rand){

        int chance = rand.nextInt(this.getTotalWeight());

        int counter = 0;

        for(Rarity<T> object : this.objects){
            counter += object.getWeight();

            if(chance <= counter){
                return object.getObject();
            }

        }
        return null;
    }

    public int getTotalWeight(){
        int total = 0;

        for(Rarity<T> object : this.objects){
            total += object.getWeight();
        }

        return total;
    }

    public void addChance(int weight, T object){
        this.objects.add(new Rarity<>(object, weight));
    }

}
