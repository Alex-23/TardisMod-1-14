package net.tardis.mod.misc;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.util.math.RayTraceResult;
import net.tardis.mod.entity.projectiles.LaserEntity;
/** Wrapper class to define behaviour when an entity attacks something*/
public abstract class AbstractWeapon<T extends LivingEntity> {
	private float damageAmount = 1F;
	
	/** Define extra behaviour to occur if a laser entity hits something. Occurs after default entity and block hit behaviour has occurred.*/
    public void onHit(LaserEntity laserEntity, RayTraceResult result) {}
    /** Define behaviour to occur for entity hits right before we damage the entity*/
    public void onHitEntityPre(LaserEntity laserEntity, Entity result) {}
    /** Define behaviour to occur for block hits right before we execute default behaviour with block hits*/
    public void onHitBlockPre(LaserEntity laserEntity, BlockState result) {}
    
    public float damage() {return damageAmount;}
    
    public void setDamage(float damage) {this.damageAmount = damage;}
    
    /** Logic for when the entity uses this weapon*/
    public boolean useWeapon(T entity) {return true;}

}
