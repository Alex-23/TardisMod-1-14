package net.tardis.mod.boti;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.Entity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.DimensionType;
import net.minecraft.world.IBlockDisplayReader;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.level.ColorResolver;
import net.minecraft.world.lighting.WorldLightManager;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.tardis.mod.boti.stores.BlockStore;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.constants.TardisConstants;

import javax.annotation.Nullable;
import java.util.*;
import java.util.Map.Entry;

/** Data holder class. Probably doesn't need to be a world 
 * @author Spectre0987*/
public class WorldShell implements IBlockDisplayReader {

    //Object type to not make servers sad -- on client will be a BotiWorld
    private static Object botiWorld;
    private final int maxRadiusRange = TardisConstants.BOTI_SHELL_RADIUS;
    private HashMap<BlockPos, BlockStore> blocks = new HashMap<>();
    private HashMap<BlockPos, TileEntity> tileEntities = new HashMap<>();
    private HashMap<UUID, EntityStorage> entities = Maps.newHashMap();
    private boolean update = true;
    private BlockPos offset = BlockPos.ZERO;
    private RegistryKey<Biome> biome = Biomes.WARPED_FOREST;
//    private BiomeManager biomeManager;
    private RegistryKey<DimensionType> type;
    private Direction portalFacing = Direction.NORTH;


    //Cached bits for the Client
    private Biome cachedBiome;


    public WorldShell(BlockPos offset, RegistryKey<DimensionType> type, RegistryKey<Biome> biome) {
        this.offset = offset.toImmutable();
        this.type = type;
        this.biome = biome;
    }
    
    public WorldShell(BlockPos offset, World world) {
        this.offset = offset.toImmutable();
        this.type = world.func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY)
                .getOptionalKey(world.getDimensionType()).orElse(DimensionType.OVERWORLD);
        this.biome = world.func_241828_r().getRegistry(Registry.BIOME_KEY).getOptionalKey(world.getBiome(offset)).orElse(Biomes.FOREST);
    }

    public static WorldShell readFromBuffer(PacketBuffer buffer) {
        WorldShell shell = new WorldShell(buffer.readBlockPos(), RegistryKey.getOrCreateKey(Registry.DIMENSION_TYPE_KEY, buffer.readResourceLocation()), RegistryKey.getOrCreateKey(Registry.BIOME_KEY, buffer.readResourceLocation()));
        shell.setPortalDirection(Direction.byHorizontalIndex(buffer.readInt()));
        int size = buffer.readInt();
        for (int i = 0; i < size; ++i) {
            shell.put(buffer.readBlockPos(), new BlockStore(buffer));
        }
        shell.sortBlocks();
        return shell;
    }

    public void writeToBuffer(PacketBuffer buf) {
        buf.writeBlockPos(offset);
        buf.writeResourceLocation(this.type.getLocation());
        buf.writeResourceLocation(this.biome.getLocation());
        buf.writeInt(this.portalFacing.getHorizontalIndex());
        buf.writeInt(this.blocks.size());
        for (Entry<BlockPos, BlockStore> entry : this.getBlockMap().entrySet()) {
            buf.writeBlockPos(entry.getKey());
            entry.getValue().encode(buf);
        }
    }

    /**
     * MUST call {@link #sortBlocks()} After!
     * @param pos
     * @param store
     */
    public void put(BlockPos pos, BlockStore store) {
        this.blocks.put(pos, store);
    }

    public BlockStore get(BlockPos pos) {
        return this.blocks.get(pos);
    }

    public Map<BlockPos, BlockStore> getBlockMap() {
        return this.blocks;
    }

    public void setMap(HashMap<BlockPos, BlockStore> newShell) {
        this.blocks.clear();
        this.blocks.putAll(newShell);
        this.sortBlocks();
    }

    public void setOffset(BlockPos offset) {
        this.offset = offset;
    }

    @Override
    public TileEntity getTileEntity(BlockPos pos) {
        return tileEntities.get(pos);
    }

    @Override
    public BlockState getBlockState(BlockPos pos) {
        return get(pos) != null ? get(pos).getState() : Blocks.AIR.getDefaultState();
    }

    @Override
    public FluidState getFluidState(BlockPos pos) {
        return get(pos) != null ? get(pos).getState().getFluidState() : Fluids.EMPTY.getDefaultState();
    }

    public void setBiome(RegistryKey<Biome> b) {
        this.biome = b;
    }

    public Biome getBiome(World world){
        if(this.cachedBiome == null){
            this.cachedBiome = world.func_241828_r().getRegistry(Registry.BIOME_KEY).getValueForKey(this.biome);
        }
        return this.cachedBiome;
    }
    
    /** Must only be called on the client side*/
    @OnlyIn(Dist.CLIENT)
    @Nullable
    public BotiWorld getWorld() {

       DimensionType shellDimType = ClientHelper.getClientWorldCasted().func_241828_r().func_230520_a_().getValueForKey(this.type);

       if(botiWorld == null || shellDimType != ((BotiWorld)botiWorld).getDimensionType()) {
           botiWorld = BotiWorld.copy(ClientHelper.getClientWorldCasted(), shellDimType, BOTIRenderer.worldRenderer);
       }

        return (BotiWorld) botiWorld;
    }

    /** Must only be called on the client side*/
    @OnlyIn(Dist.CLIENT)
    public void setWorld(BotiWorld world) {
        botiWorld = world;

        for (TileEntity te : this.tileEntities.values())
            te.setWorldAndPos(world, te.getPos());

        for(EntityStorage e : this.getEntities())
            e.create(world);
    }

    public Map<BlockPos, TileEntity> getTiles() {
        return tileEntities;
    }

    public boolean needsUpdate() {
        return this.update;
    }

    public void setNeedsUpdate(boolean update) {
        this.update = update;
    }

    public BlockPos getOffset() {
        return this.offset;
    }

    @Nullable
    public DimensionType getDimensionType() {
        return this.getWorld().func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY).getOrThrow(type);
    }

    public void setDimensionType(RegistryKey<DimensionType> type) {
        this.type = type;
    }

    /** Must only be called on the client side*/
    @OnlyIn(Dist.CLIENT)
    public Collection<EntityStorage> getEntities() {
        return this.entities.values();
    }

    public void tick(boolean isClientSide) {
        if (isClientSide)
            this.tickWorld();
    }

    /** Must only be called on the client side*/
    @OnlyIn(Dist.CLIENT)
    private void tickWorld() {
        try {
            /**if (this.getWorld() != null) {
                this.getWorld().tickEntities();
                this.getWorld().tickBlockEntities();
            }*/

            List<UUID> removedEntities = new ArrayList<>();
            for(Entry<UUID, EntityStorage> entry : this.entities.entrySet()){
                entry.getValue().tick();
                if(entry.getValue().isRemoved())
                    removedEntities.add(entry.getKey());
            }

            //Removed Entites
            for(UUID id : removedEntities){
                this.entities.remove(id);
            }

        }
        catch (Exception e) {}
    }

    public void combine(WorldShell shell) {
        if (!shell.getBlockMap().isEmpty() && !shell.getBlockMap().equals(this.getBlockMap())) {
            this.blocks.putAll(shell.getBlockMap());
            this.tileEntities.putAll(shell.tileEntities);
            this.entities.putAll(shell.entities);
            this.setNeedsUpdate(true);
            this.sortBlocks();
        }
    }

    @Override
    public float func_230487_a_(Direction p_230487_1_, boolean p_230487_2_) {
        return 0;
    }

    @Override
    public WorldLightManager getLightManager() {
        return this.getWorld().getLightManager();
    }

    @Override
    public int getBlockColor(BlockPos blockPosIn, ColorResolver colorResolverIn) {
        return 0;
    }

    public void setPortalDirection(Direction dir) {
        this.portalFacing = dir;
    }
    
    public Direction getPortalDirection() {
        return this.portalFacing;
    }
    
    public void cullOutOfRangeBlocksInterior() {
        cullOutOfRangeBlocks(true);
    }
    
    public void cullOutOfRangeBlocksExterior() {
        cullOutOfRangeBlocks(false);
    }

    //Done the way it is to prevent CMEs
    public void cullOutOfRangeBlocks(boolean isTardisInteriorWorld) {
        List<BlockPos> positionsToRemove = Lists.newArrayList();
        Direction cullBehindPortalDirection = isTardisInteriorWorld ? this.portalFacing : this.portalFacing.getOpposite();
        BlockPos cullToLeftPortalPosition = this.offset.offset(portalFacing.rotateYCCW(), this.maxRadiusRange).down(8);
        int cullBehindAmount = maxRadiusRange;
        BlockPos cullToRightAndBehindPortalPosition = this.offset.offset(portalFacing.rotateY(), this.maxRadiusRange).offset(cullBehindPortalDirection, cullBehindAmount).up(8);
        AxisAlignedBB area = new AxisAlignedBB(cullToLeftPortalPosition, cullToRightAndBehindPortalPosition);
//        System.out.println(area.toString().replace("[", "").replace("]", "").replace("->", "").replace("AABB", "").replace(",", ""));
        //Gather positions out of range
        for(BlockPos pos : this.blocks.keySet()){

            Vector3d vec = new Vector3d(pos.getX(), pos.getY(), pos.getZ());
            if(area.contains(vec))
                positionsToRemove.add(pos);
        }

        //Remove Gathered Positions
        for(BlockPos pos : positionsToRemove){
            this.getBlockMap().remove(pos);
        }

        //Re-sort into layers
        this.sortBlocks();
    }

    public void buildLighting(){}

    @OnlyIn(Dist.CLIENT)
    public void updateEntities(List<EntityStorage> entities) {



        for (EntityStorage storage : entities) {
            if (this.entities.containsKey(storage.id)) {

                if(!storage.isAlive) {
                    this.entities.remove(storage.id);
                    return;
                }

                this.entities.get(storage.id).updateEntity(storage);
                Entity entity = storage.getEntity();
                if (entity != null && (!entity.isAlive() || !entity.getPosition().withinDistance(this.getOffset(), 32)))
                    this.entities.remove(storage.id);
            }
            else this.entities.put(storage.id, storage);
        }

    }

    public void addTile(BlockPos pos, TileEntity entity) {
        this.tileEntities.put(pos, entity);
        entity.updateContainingBlockInfo();
    }

    public void sortBlocks(){
        //TODO: Re-implement
    }
    
    public int getMaxRadiusRange() {
        return this.maxRadiusRange;
    }

    public void addChunk(BotiChunk chunk) {

        System.out.println("Adding chunk " + chunk);

        for(Entry<BlockPos, BlockStore> store : chunk.getMap().entrySet()){
            this.blocks.put(store.getKey(), store.getValue());
            System.out.println("Adding block " + store.getValue() + " at " + store.getKey());
        }

        this.setNeedsUpdate(true);

    }
}
