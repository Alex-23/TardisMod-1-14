package net.tardis.mod.flight;

import java.util.List;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.tileentities.ConsoleTile;

public class BulkheadFlightEvent extends FlightEvent{

	public BulkheadFlightEvent(FlightEventFactory entry, List<ResourceLocation> controls) {
		super(entry, controls);
	}
	
	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		tile.getDoor().ifPresent(door -> door.setOpenState(EnumDoorState.BOTH));
	}

}
