package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.misc.CrashType;
import net.tardis.mod.misc.CrashTypes;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.registries.FlightEventRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

public class TardisCollideInstigate extends FlightEvent{

	private ConsoleTile other;
	
	public static final Supplier<ArrayList<ResourceLocation>> CONTROLS = () -> Lists.newArrayList(
			ControlRegistry.COMMUNICATOR.get().getRegistryName(),
			ControlRegistry.THROTTLE.get().getRegistryName(),
			ControlRegistry.RANDOM.get().getRegistryName()
	);
	
	public TardisCollideInstigate(FlightEventFactory entry, ArrayList<ResourceLocation> list) {
		super(entry, list);
	}

	@Override
	public boolean onComplete(ConsoleTile tile) {
		boolean complete = super.onComplete(tile);
		
		if(complete && other != null) {
			other.setFlightEvent(((TardisCollideRecieve)FlightEventRegistry.COLLIDE_RECIEVE.get().create(other)).setOtherTARDIS(tile));
			other.updateClient();
		}
		
		return complete;
	}
	
	public TardisCollideInstigate setOtherTARDIS(ConsoleTile tile) {
		this.other = tile;
		return this;
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		
		if(other != null)
			other.damage(40F);
		
		tile.damage(150F);
		tile.crash(CrashTypes.DEFAULT);
		tile.updateClient();
	}
	
	@Override
	public int calcTime(ConsoleTile console) {
		return this.timeUntilMiss + console.flightTicks + 200;
	}
}
