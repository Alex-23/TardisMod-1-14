package net.tardis.mod.commands.subcommands;

import java.util.Map.Entry;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.commands.argument.DisguiseArgument;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.Disguise;
/** Debug command used to get a disguise and generate it*/
public class DisguiseCommand extends TCommand{
	
	private static int placeDisguiseBlocks(CommandSource source, Disguise disguise, ServerWorld world, BlockPos pos) {
		if (disguise != null) {
			if (!disguise.getOtherBlocks().isEmpty()) {
				for(Entry<BlockPos, BlockState> entry : disguise.getOtherBlocks().entrySet()) {
					world.setBlockState(pos.add(entry.getKey()), entry.getValue(), 3);
				}
			}
			world.setBlockState(pos, disguise.getBottomState(), 3);
			world.setBlockState(pos.up(), disguise.getTopState(), 3);
			TextComponent spawnedPosText = TextHelper.createTextWithoutTooltip("/tp @p " + WorldHelper.formatBlockPosDebug(pos).toString());
			source.sendFeedback(new TranslationTextComponent("command.tardis.disguise.spawn.success", disguise.getRegistryName(), spawnedPosText), false);
			return Command.SINGLE_SUCCESS;
		}
		else {
            source.sendErrorMessage(new TranslationTextComponent("command.tardis.disguise.invalid_disguise"));
            return 0;    
        }
	}
	
    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("disguise")
        		.requires(context -> context.hasPermissionLevel(2))
        		.then(Commands.literal("spawn")
        		    .then(Commands.argument("type", DisguiseArgument.getDisguiseArgument())
        		        .executes(context -> placeDisguiseBlocks(context.getSource(), DisguiseArgument.getDisguise(context, "type"), context.getSource().asPlayer().getServerWorld(), context.getSource().asPlayer().getPosition())
        		        		)
        		        )
        		 );

    }
}
