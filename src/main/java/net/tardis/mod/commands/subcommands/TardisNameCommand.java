package net.tardis.mod.commands.subcommands;

import java.util.Collection;
import java.util.Set;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.suggestion.SuggestionProvider;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.DimensionArgument;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.util.text.event.ClickEvent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.helper.TardisHelper;

public class TardisNameCommand extends TCommand {
    
    private static final SuggestionProvider<CommandSource> SUGGEST_TARDISES = (context, suggestionBuilder) -> {
          Collection<RegistryKey<World>> collection = TardisHelper.getTardisWorldKeys(context.getSource().getServer());
          return ISuggestionProvider.func_212476_a(collection.stream().map(RegistryKey::getLocation), suggestionBuilder);
    };

    private static int findDimForTardisName(CommandContext<CommandSource> context, MinecraftServer server, String tardisName) {
        Set<RegistryKey<World>> matchingWorlds = TardisHelper.getTardisWorldKeyByName(server, tardisName);
        for (RegistryKey<World> worldKey : matchingWorlds) {
            final String dimName = worldKey.getLocation().toString();
            ITextComponent suggestion = new TranslationTextComponent("\n==== Tardis Name ===\n%s\n=== Dimension ID ===\n%s", tardisName, dimName).appendSibling(new StringTextComponent("\n[Click to Copy]").mergeStyle(TextFormatting.RED))
                    .modifyStyle((style) -> style.setFormatting(TextFormatting.GREEN).setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, dimName)));
            context.getSource().sendFeedback(new TranslationTextComponent("command.tardis.matching_dim", suggestion), true);
        }
        return Command.SINGLE_SUCCESS;
    }
    
    private static int findTardisNameForDim(CommandContext<CommandSource> context, MinecraftServer server, ServerWorld world) {
        Set<String> matchingNames = TardisHelper.getTardisNamesByWorldKey(server, world.getDimensionKey());
        for (String name : matchingNames) {
            final String dimName = world.getDimensionKey().getLocation().toString();
            ITextComponent suggestion = new TranslationTextComponent("\n==== Dimension ID ===\n%s\n=== Tardis Name ===\n%s", dimName, name).appendSibling(new StringTextComponent("\n[Click to Copy]").mergeStyle(TextFormatting.RED))
                    .modifyStyle((style) -> style.setFormatting(TextFormatting.GREEN).setClickEvent(new ClickEvent(ClickEvent.Action.COPY_TO_CLIPBOARD, name)));
            context.getSource().sendFeedback(new TranslationTextComponent("command.tardis.matching_name", suggestion), true);
        }
        return Command.SINGLE_SUCCESS;
    }
    
    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("find").requires(context -> context.hasPermissionLevel(2))
            .then(Commands.literal("tardis_name")
                .then(Commands.argument("dimension", DimensionArgument.getDimension()).suggests(SUGGEST_TARDISES)
                    .executes(context -> findTardisNameForDim(context, context.getSource().getServer(), DimensionArgument.getDimensionArgument(context, "dimension")))
                    )
                 )
            .then(Commands.literal("world_key")
                .then(Commands.argument("tardis_name", StringArgumentType.word()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(TardisHelper.getTardisNames(context.getSource().getServer()),suggestionBuilder))
                     .executes(context -> findDimForTardisName(context, context.getSource().getServer(), StringArgumentType.getString(context, "tardis_name")))
                )
            );
    }

}
