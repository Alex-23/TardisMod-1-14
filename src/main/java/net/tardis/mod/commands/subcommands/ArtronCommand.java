package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.artron.IArtronItemStackBattery;

public class ArtronCommand extends TCommand{

	private static int check(CommandContext<CommandSource> context, ServerPlayerEntity player) {
		ItemStack stack = player.getHeldItemMainhand();

		if (!stack.isEmpty() && stack.getItem() instanceof IArtronItemStackBattery) {
			context.getSource().sendFeedback(new TranslationTextComponent("command.tardis.artron.check", ((IArtronItemStackBattery) stack.getItem()).getCharge(stack)), true);
			return Command.SINGLE_SUCCESS;
		}
		else {
			context.getSource().sendErrorMessage(new TranslationTextComponent("command.tardis.artron.invalid_item", stack.getDisplayName()));
			return 0;
		}
		
	}

	private static int add(CommandContext<CommandSource> context, ServerPlayerEntity player, int value) {
		ItemStack stack = player.getHeldItemMainhand() == null || player.getHeldItemMainhand().isEmpty() ? player.getHeldItemOffhand() : player.getHeldItemMainhand();

		if (!stack.isEmpty() && stack.getItem() instanceof IArtronItemStackBattery) {
			IArtronItemStackBattery artronBattery = (IArtronItemStackBattery) stack.getItem();

			float chargedValue = artronBattery.charge(stack, value);
			float artronBatteryCharge = artronBattery.getCharge(stack);

			context.getSource().sendFeedback(new TranslationTextComponent("command.tardis.artron.add",value, chargedValue, artronBatteryCharge), true);
			return Command.SINGLE_SUCCESS;
		}
		else {
			context.getSource().sendErrorMessage(new TranslationTextComponent("command.tardis.artron.invalid_item", stack.getDisplayName()));
			return 0;
		}
		
	}

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("artron")
                .requires(context -> context.hasPermissionLevel(2))
                .then(Commands.literal("check")
                        .executes(context -> check(context, context.getSource().asPlayer()))
                ) // end check
				.then(Commands.literal("add")
						.then(Commands.argument("value", IntegerArgumentType.integer())
								.executes(context -> add(context, context.getSource().asPlayer(), IntegerArgumentType.getInteger(context, "value"))
								)//end execute
						)//end value
				); // end add
    }

}