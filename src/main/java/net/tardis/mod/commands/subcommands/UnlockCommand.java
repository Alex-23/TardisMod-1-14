package net.tardis.mod.commands.subcommands;

import java.util.Collections;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.command.arguments.DimensionArgument;
import net.minecraft.command.arguments.EntityArgument;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.commands.argument.ConsoleRoomArgument;
import net.tardis.mod.commands.argument.ConsoleUnitArgument;
import net.tardis.mod.commands.argument.ExteriorArgument;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.CommandHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.misc.Console;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

public class UnlockCommand extends TCommand {

    private static int unlockExterior(CommandContext<CommandSource> context, ServerPlayerEntity player, AbstractExterior exterior) {
        return unlockExterior(context, player.getServerWorld(), exterior);
    }

    private static int unlockExterior(CommandContext<CommandSource> context, ServerWorld world, AbstractExterior item) {
       CommandSource source = context.getSource();
       if (TardisHelper.getConsole(context.getSource().getServer(), world).isPresent()) {
	       ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), world).orElse(null);
	       console.getUnlockManager().addExterior(item);
	
	       TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world);
	
	       source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.exterior", item.getDisplayName(), tardisIdentifier), true);
	       console.updateClient();
	       return Command.SINGLE_SUCCESS;
       }
       else {
       	context.getSource().sendErrorMessage(new TranslationTextComponent(TardisConstants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
       	return 0;
       }
    }

    private static int unlockAllExteriors(CommandContext<CommandSource> context, ServerWorld world) {
        CommandSource source = context.getSource();
        if (TardisHelper.getConsole(context.getSource().getServer(), world).isPresent()) {
        	ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), world).orElse(null);
            for(AbstractExterior ext : ExteriorRegistry.EXTERIOR_REGISTRY.get().getValues()) {
                console.getUnlockManager().addExterior(ext);
            }

            TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world);

            source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.exterior_all", tardisIdentifier), true);
            return Command.SINGLE_SUCCESS;
        }
        else {
        	context.getSource().sendErrorMessage(new TranslationTextComponent(TardisConstants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
        	return 0;
        }
        
    }
    
    private static int unlockInterior(CommandContext<CommandSource> context, ServerPlayerEntity player, ConsoleRoom room) {
        return unlockInterior(context, player.getServerWorld(), room);
    }
    
    private static int unlockInterior(CommandContext<CommandSource> context, ServerWorld world, ConsoleRoom item) {
        CommandSource source = context.getSource();
        if (TardisHelper.getConsole(context.getSource().getServer(), world).isPresent()) {
	        ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), world).orElse(null);
	        console.getUnlockManager().addConsoleRoom(item);
	
	        TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world);
	
	        source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.interior", item.getDisplayName(), tardisIdentifier), true);
	    
	        console.updateClient();
	        return Command.SINGLE_SUCCESS;
        }
        else {
        	context.getSource().sendErrorMessage(new TranslationTextComponent(TardisConstants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
        	return 0;
        }
    }
    
    private static int unlockAllInteriors(CommandContext<CommandSource> context, ServerWorld world) {
        CommandSource source = context.getSource();
        if (TardisHelper.getConsole(context.getSource().getServer(), world).isPresent()) {
	        ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), world).orElse(null);
	        console.getUnlockManager().clearConsoleRoomList(); //Clear the list so we can add all consoles
	        for(ConsoleRoom room : ConsoleRoom.getRegistry().values()) {
	            console.getUnlockManager().addConsoleRoom(room);
	        }
	
	        TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world);
	
	        source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.interior_all", tardisIdentifier), true);
	        return Command.SINGLE_SUCCESS;
        }
        else {
        	context.getSource().sendErrorMessage(new TranslationTextComponent(TardisConstants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
        	return 0;
        }
    }
    
    private static int unlockConsoleUnit(CommandContext<CommandSource> context, ServerPlayerEntity player, Console console) {
        return unlockConsoleUnit(context, player.getServerWorld(), console);
    }
    
    private static int unlockConsoleUnit(CommandContext<CommandSource> context, ServerWorld world, Console item) {
    	CommandSource source = context.getSource();
    	if (TardisHelper.getConsole(context.getSource().getServer(), world).isPresent()) {
	    	ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), world).orElse(null);
	        console.getUnlockManager().addConsole(item);
	
	        TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world);
	
	        source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.console", item.getDisplayName(), tardisIdentifier), true);
	    
	        console.updateClient();
	    	return Command.SINGLE_SUCCESS;
    	}
    	else {
        	context.getSource().sendErrorMessage(new TranslationTextComponent(TardisConstants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
        	return 0;
        }
    }
    
    private static int unlockAllConsoles(CommandContext<CommandSource> context, ServerWorld world) {
        CommandSource source = context.getSource();
        if (TardisHelper.getConsole(context.getSource().getServer(), world).isPresent()) {
	        ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), world).orElse(null);
	        for(Console consoleUnit : ConsoleRegistry.CONSOLE_REGISTRY.get().getValues()) {
	            console.getUnlockManager().addConsole(consoleUnit);
	        }
	
	        TextComponent tardisIdentifier = TextHelper.getTardisDimObject(world);
	
	        source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.console_all", tardisIdentifier), true);
	        return Command.SINGLE_SUCCESS;
        }
        else {
        	context.getSource().sendErrorMessage(new TranslationTextComponent(TardisConstants.Translations.NO_TARDIS_FOUND, world.getDimensionKey().getLocation().toString()));
        	return 0;
        }
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("unlock").requires(context -> context.hasPermissionLevel(2))
                //unlock for the Tardis you are currently inside
                .then(Commands.literal("exterior")
                    .then(Commands.literal("*")
                        .executes(context -> unlockAllExteriors(context, context.getSource().asPlayer().getServerWorld()))
                    .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                         .executes(context -> unlockAllExteriors(context, DimensionArgument.getDimensionArgument(context, "tardis"))
                         ))//Add optional parameter for specific Tardis
                     )//Add option to unlock all
                    .then(Commands.argument("item", ExteriorArgument.getExteriorArgument())
                        .executes(context -> unlockExterior(context, context.getSource().asPlayer(), ExteriorArgument.getExterior(context, "item")))
                    .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                         .executes(context -> unlockExterior(context, DimensionArgument.getDimensionArgument(context, "tardis"), ExteriorArgument.getExterior(context, "item")))
                         )//Add optional parameter for specific Tardis
                    )//End exterior selection argument
               )
               .then(Commands.literal("interior")
                    .then(Commands.literal("*")
                         .executes(context -> unlockAllInteriors(context, context.getSource().asPlayer().getServerWorld()))
                    .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                         .executes(context -> unlockAllInteriors(context, DimensionArgument.getDimensionArgument(context, "tardis"))
                         ))//Add optional parameter for specific Tardis
                    )//Add option to unlock all
                    .then(Commands.argument("item", ConsoleRoomArgument.getConsoleRoomArgument())
                        .executes(context -> unlockInterior(context, context.getSource().asPlayer(), ConsoleRoomArgument.getConsoleRoom(context, "item")))
                    .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                         .executes(context -> unlockInterior(context, DimensionArgument.getDimensionArgument(context, "tardis"), ConsoleRoomArgument.getConsoleRoom(context, "item")))
                         )//Add optional parameter for specific Tardis
                    )//End Console Room selection argument
               )//End Interior subcommand
               .then(Commands.literal("console")
                       .then(Commands.literal("*")
                            .executes(context -> unlockAllConsoles(context, context.getSource().asPlayer().getServerWorld()))
                       .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                            .executes(context -> unlockAllConsoles(context, DimensionArgument.getDimensionArgument(context, "tardis"))
                            ))//Add optional parameter for specific Tardis
                       )//Add option to unlock all
                       .then(Commands.argument("item", ConsoleUnitArgument.getConsoleUnitArgument())
                           .executes(context -> unlockConsoleUnit(context, context.getSource().asPlayer(), ConsoleUnitArgument.getConsoleUnit(context, "item")))
                       .then(Commands.argument("tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltip(suggestionBuilder, context.getSource().getServer())))
                            .executes(context -> unlockConsoleUnit(context, DimensionArgument.getDimensionArgument(context, "tardis"), ConsoleUnitArgument.getConsoleUnit(context, "item")))
                            )//Add optional parameter for specific Tardis
                       )//End Console Unit selection argument
               )//End Console subcommand
               //Unlock for a tardis that a player is being tracked for loyalty. This is done to make it alot easier to filter out alot of Tardises when there are many on a server.
               .then(Commands.argument("player", EntityArgument.player())
                   .then(Commands.argument("connected_tardis", DimensionArgument.getDimension()).suggests((context, suggestionBuilder) -> ISuggestionProvider.suggest(Collections.emptyList(), CommandHelper.addTardisKeysWithNameTooltipForPlayer(suggestionBuilder, context.getSource().getServer(), EntityArgument.getPlayer(context, "player"))))
                       .then(Commands.literal("exterior")
                            .then(Commands.literal("*")
                                .executes(context -> unlockAllExteriors(context, DimensionArgument.getDimensionArgument(context, "connected_tardis")))
                                )//Add option to unlock all
                            .then(Commands.argument("item", ExteriorArgument.getExteriorArgument())
                               .executes(context -> unlockExterior(context, DimensionArgument.getDimensionArgument(context, "connected_tardis"), ExteriorArgument.getExterior(context, "item"))))
                            )//End Exterior subcommand
                       .then(Commands.literal("interior")
                            .then(Commands.literal("*").executes(context -> unlockAllInteriors(context, DimensionArgument.getDimensionArgument(context, "connected_tardis")))
                                 )//Add option to unlock all
                            .then(Commands.argument("item", ConsoleRoomArgument.getConsoleRoomArgument())
                               .executes(context -> unlockInterior(context, DimensionArgument.getDimensionArgument(context, "connected_tardis"), ConsoleRoomArgument.getConsoleRoom(context, "item")))
                                 )//End individual item selction
                            )//End Interior subcommand
                       .then(Commands.literal("console")
                            .then(Commands.literal("*").executes(context -> unlockAllConsoles(context, DimensionArgument.getDimensionArgument(context, "connected_tardis")))
                                 )//Add option to unlock all
                            .then(Commands.argument("item", ConsoleUnitArgument.getConsoleUnitArgument())
                               .executes(context -> unlockConsoleUnit(context, DimensionArgument.getDimensionArgument(context, "connected_tardis"), ConsoleUnitArgument.getConsoleUnit(context, "item")))
                                 )//End individual item selction
                             )//End Console Unit subcommand
                       )//End Tardis Name subcommand
                    ); //End Player argument
    }
}
