package net.tardis.mod.missions;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.structure.StructureManager;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.minecraft.world.gen.settings.StructureSeparationSettings;
import net.minecraft.world.server.ServerWorld;
import net.tardis.api.space.IOxygenSealer;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.humanoid.CrewmateEntity;
import net.tardis.mod.entity.humanoid.HumanoidEmotionalState;
import net.tardis.mod.entity.humanoid.ShipCaptainEntity;
import net.tardis.mod.entity.mission.HumanoidMissionEntity;
import net.tardis.mod.experimental.advancement.GenericTardisTrigger;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.loottables.TardisLootTables;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;
import net.tardis.mod.missions.misc.DialogOption.DialogAction;
import net.tardis.mod.missions.stages.MissionStage;
import net.tardis.mod.missions.stages.MissionStages;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.CompleteMissionMessage;
import net.tardis.mod.network.packets.MissionUpdateMessage;
import net.tardis.mod.network.packets.SetMissionObjectiveMessage;
import net.tardis.mod.registries.MissionRegistry;
import net.tardis.mod.tileentities.ShipComputerTile;
import net.tardis.mod.world.dimensions.TDimensions;

public class DroneStationMission extends KillMission{
    
    public DroneStationMission(World world, BlockPos pos, int range) {
        super(MissionRegistry.STATION_DRONE.get(), world, pos, range);
    }

    @Override
    public ItemStack getReward() {
        return new ItemStack(TItems.TELE_STRUCTURE_UPGRADE.get());
    }

    @Override
    public EntityType<?> getKillTargetEntityType() {
        return TEntities.SECURITY_DROID.get();
    }

    @Override
    public float getProgressBarPercent() {
        return this.getCurrentObjective() / (float)this.getMaxObjectivesForCurrentStage();
    }

    @Override
    public boolean shouldKillAdvance() {
        return this.getCurrentStage() == MissionStages.KILL_DRONES.get() && !this.getCurrentStage().isComplete();
    }

    @Override
    public boolean shouldSpawnMore() {
        return this.getCurrentStage() ==  MissionStages.KILL_DRONES.get() && !this.getCurrentStage().isComplete() && this.getValidEntitiesInArea() < 8;
    }

    @Override
    public Dialog getDialogForObjective(LivingEntity speaker, PlayerEntity player, int stage) {
        if(this.getCurrentStage() == MissionStages.NOT_STARTED.get()) {
            Dialog root = this.createDialogForCharacter("capt.confused_greet");
            
            //Be a smartass
            DialogOption smartass = this.createPlayerResponse(null, "smartass");
            
            //Relieved response - default
            Dialog helpResp = this.createDialogForCharacter("capt.relieved_request");
            helpResp.addDialogOption(this.createPlayerResponse(null, "understand_request"));

            
            //Agree to help with self introduction
            DialogOption helping = this.createPlayerResponseWithArguments(helpResp, "respond_sos_with_intro", player.getDisplayName().getString());
            helping.setOptionAction((speak, player2) -> {
                Network.sendToServer(new SetMissionObjectiveMessage(speak.getPosition(), 1, this.getCurrentStage().getRegistryName()));
            });
            
            //Add dialog options
            root.addDialogOption(smartass);
            root.addDialogOption(helping);
            
            /** Hostile Greet*/
            Dialog hostileGreet = this.createDialogForCharacter("capt.hostile_greet");
            
            //Frustrated response if player agree to help in the hostile path
            Dialog frustratedResp = this.createDialogForCharacter("capt.frustrated_request");
            
            
            //Agree to help to hostile greet
            DialogOption respondSosPacify = this.createPlayerResponse(frustratedResp, "agree_to_help");
            respondSosPacify.setOptionAction((speak, player1) -> {
                Network.sendToServer(new SetMissionObjectiveMessage(speak.getPosition(), 1, this.getCurrentStage().getRegistryName()));
            });
            
            //Ignore
            frustratedResp.addDialogOption(this.createPlayerResponse(null, "ignore_frustrated_request"));
            
            
            frustratedResp.addDialogOption(respondSosPacify);
            
            //Hostile dialog options
            hostileGreet.addDialogOption(this.createPlayerResponse(frustratedResp, "respond_sos_pacifying"));
            hostileGreet.addDialogOption(smartass);

            
            if (speaker instanceof HumanoidMissionEntity) {
            	HumanoidMissionEntity missionHumanoid = (HumanoidMissionEntity)speaker;
            	if (missionHumanoid.getEmotionalState() == HumanoidEmotionalState.HOSTILE) {
            	    return hostileGreet;
            	}
            }
            
            return root;
        }
        else if (this.getCurrentStage() == MissionStages.KILL_DRONES.get()){
        	if (this.getCurrentObjective() == 0) {
        		Dialog root = this.createDialogForCharacter("capt.followup_player");
                
                DialogAction action = (speak, play) -> {
                    speak.world.addParticle(ParticleTypes.ANGRY_VILLAGER, speak.getPosX(), speak.getEyeHeight() + 1D, speak.getPosZ(), 0, 0.05, 0);
                };
                
                root.addDialogOption(this.createPlayerResponse(null, "followup_polite"));
                root.addDialogOption(this.createPlayerResponse(null, "followup_rude").setOptionAction(action));
                root.addDialogOption(this.createPlayerResponse(null, "followup_frustrated").setOptionAction(action));

                return root;
        	}
        	if (this.getCurrentObjective() > 0 && !this.getCurrentStage().isComplete()) { //If the player has already killed some drones but has not completed the stage yet
        		Dialog moreDrones = this.createDialogForCharacter("capt.followup_player.more_drones");
        		
        		DialogAction action = (speak, play) -> {
                    speak.world.addParticle(ParticleTypes.ANGRY_VILLAGER, speak.getPosX(), speak.getEyeHeight() + 1D, speak.getPosZ(), 0, 0.05, 0);
                };
        		
                moreDrones.addDialogOption(this.createPlayerResponse(null, "acknowledge_polite"));
                moreDrones.addDialogOption(this.createPlayerResponse(null, "acknowledge_rude").setOptionAction(action));
                moreDrones.addDialogOption(this.createPlayerResponse(null, "acknowledge_frustrated").setOptionAction(action));
                return moreDrones;
        	}
            
        }
        else if(this.getCurrentStage() == MissionStages.RETURN_TO_MISSION_HOST.get()) {
            if(!getAwarded()) {
                DialogAction spawnReward = (speak, player1) -> {
                	Network.sendToServer(new SetMissionObjectiveMessage(speaker.getPosition(), 1, this.getCurrentStage().getRegistryName()));
                	Network.sendToServer(new CompleteMissionMessage(speaker.getPosition()));
                };
                
                Dialog dia = this.createDialogForCharacter("capt.thankful_reward");
                DialogOption accept = this.createPlayerResponse(null, "polite_accept_reward");
                accept.setOptionAction(spawnReward);
                DialogOption acceptRude = this.createPlayerResponse(null, "ungrateful_accept_reward");
                acceptRude.setOptionAction(spawnReward);
                
                dia.addDialogOption(accept);
                dia.addDialogOption(acceptRude);

                return dia;
            }
        }
        return null;
    }

    /** Deprecated, see {@link MiniMissionType#spawnAndFindNearestMission(ServerWorld, StructureManager, BlockPos, int, boolean, long, StructureSeparationSettings)}*/
    @Deprecated
    public static BlockPos setupMission(ServerWorld world, BlockPos start) {
        
        if(!WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.SPACE_TYPE))
            return BlockPos.ZERO;
        
        int radius = 10000 / 2;
        
        BlockPos test = new BlockPos(
                start.getX() + (-radius + (world.getRandom().nextInt(radius))),
                0,
                start.getX() + (-radius + (world.getRandom().nextInt(radius))));
        
        if(!world.getWorldBorder().contains(test) || !world.getWorldBorder().contains(test.add(128, 0, 128)))
            return BlockPos.ZERO;
        
        ChunkPos cPos = new ChunkPos(test);
        if(!world.chunkExists(cPos.x, cPos.z)) {
            for(int x = 0; x < 5; x++) {
                for(int z = 0; z < 5; z++) {
                    world.forceChunk(cPos.x + x, cPos.z + z, true);
                }
            }
            
            ArrayList<BlockPos> landingSpots = Lists.newArrayList();
            
            test = test.add(0, 64 + (world.rand.nextDouble() * 64), 0);
            PlacementSettings settings = new PlacementSettings();
            Template temp = world.getStructureTemplateManager().getTemplate(Helper.createRL("tardis/structures/worldgen/space/spacestation_drone"));
            temp.func_237144_a_(world, test, new PlacementSettings(), world.rand);
            
            //Process data blocks
            for(BlockInfo info : temp.func_215381_a(test, settings, Blocks.STRUCTURE_BLOCK)){
                if(info.nbt != null && info.nbt.contains("metadata")) {
                    String data = info.nbt.getString("metadata");
                    
                    if(data.contentEquals("captain_spawn")) {
                        final BlockPos entCap = info.pos.toImmutable();
                        ShipCaptainEntity cap = TEntities.SHIP_CAPTAIN.get().create(world);
                        cap.setPosition(entCap.getX() + 0.5, entCap.getY() + 1, entCap.getZ() + 0.5);
                        cap.onInitialSpawn(world.getWorld(), world.getDifficultyForLocation(entCap), SpawnReason.STRUCTURE, null, null);
                        world.addEntity(cap);
                    }
                    else if(data.contentEquals("mission_marker")) {
                        world.getServer().enqueue(new TickDelayedTask(0, () -> {
                            world.getCapability(Capabilities.MISSION).ifPresent(missions -> {
                                MiniMission mis = MissionRegistry.STATION_DRONE.get().create(world, info.pos, 64);
                                missions.addMission(mis);
                                Network.sendToAllInWorld(new MissionUpdateMessage(mis), world);
                            });
                        }));
                    }
                    else if(data.contentEquals("crewmates")) {
                        int num = 2 + world.getRandom().nextInt(3);
                        for(int i = 0 ; i < num; ++i) {
                            CrewmateEntity entity = TEntities.CREWMATE.get().create(world);
                            entity.setPosition(info.pos.getX() + 0.5, info.pos.getY() + 1, info.pos.getZ() + 0.5);
                            entity.onInitialSpawn(world.getWorld(), world.getDifficultyForLocation(info.pos), SpawnReason.STRUCTURE, null, null);
                            world.addEntity(entity);
                        }
                    }
                }
                world.setBlockState(info.pos, Blocks.AIR.getDefaultState(), 3);
            }
            
            //Process ship computers
            for(BlockInfo info : temp.func_215381_a(test, settings, TBlocks.ship_computer.get())){
                ShipComputerTile tile = (ShipComputerTile)world.getTileEntity(info.pos);
                tile.setLootTable(TardisLootTables.SPACESTATION_DRONE);
            }
            
            for(BlockInfo info : temp.func_215381_a(test, settings, TBlocks.landing_pad.get())){
                landingSpots.add(info.pos);
            }
            
            for(int x = 0; x < 5; x++) {
                for(int z = 0; z < 5; z++) {
                    world.forceChunk(cPos.x + x, cPos.z + z, false);
                }
            }
            return landingSpots.size() > 0 ? landingSpots.get(world.rand.nextInt(landingSpots.size())) : test.toImmutable();
        }
        return BlockPos.ZERO;
        
    }
    
    @Override
    public boolean canKillTargetEntitySpawnHere(World world, BlockPos pos) {
        List<TileEntity> tes = WorldHelper.getTEsInChunks((ServerWorld)world, new ChunkPos(pos), 3);
        for(TileEntity te : tes) {
            IOxygenSealer sealer = te.getCapability(Capabilities.OXYGEN_SEALER).orElse(null);
            if(sealer != null) {
                if(sealer.getSealedPositions().contains(pos)) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    @Override
    public GenericTardisTrigger getCompletedAdvancementTrigger() {
        return TTriggers.DRONE_MISSION_COMPLETE;
    }

    @Override
    public MissionStage getNextStage() {
        if (this.getCurrentStage() == MissionStages.NOT_STARTED.get() && this.getCurrentStage().isComplete()) {
            return MissionStages.KILL_DRONES.get();
        }
        if (this.getCurrentStage() == MissionStages.KILL_DRONES.get() && this.getCurrentStage().isComplete()) {
            return MissionStages.RETURN_TO_MISSION_HOST.get();
        }
        if (this.getCurrentStage() == MissionStages.RETURN_TO_MISSION_HOST.get() && this.getCurrentStage().isComplete()) {
        	return MissionStages.MISSION_COMPLETED.get();
        }
        else {
            return this.getCurrentStage();    
        }
    }
}
