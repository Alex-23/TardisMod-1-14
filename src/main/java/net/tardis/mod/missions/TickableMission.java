package net.tardis.mod.missions;

import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.cap.ITickableMission;
import net.tardis.mod.missions.stages.MissionStages;

public abstract class TickableMission extends MiniMission implements ITickableMission{
	
	public TickableMission(MiniMissionType type, World world, BlockPos pos, int range) {
		super(type, world, pos, range);
	}

	@Override
    public void tick(World world) {
        if (!world.isRemote()) {
            if (world.getGameTime() % 40 == 0) {
            	IFormattableTextComponent barName = null;
                if (this.getCurrentStage() == MissionStages.NOT_STARTED.get()) {
                	barName = this.originalBarName;
                }
                if (this.getCurrentStage() == MissionStages.RETURN_TO_MISSION_HOST.get()) {
                	IFormattableTextComponent currentStage = this.getCurrentStage().getDisplayNameForCurrentObjective().deepCopy().appendSibling(this.getMissionHostEntity().getName()).deepCopy();
                	barName = currentStage;
                }
                this.setMissionName(barName == null ? this.getCurrentStage().getDisplayNameForCurrentObjective().deepCopy() : barName);
            }
        }
    }

}
