package net.tardis.mod.subsystem;

import net.minecraft.item.Item;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.tileentities.ConsoleTile;

public class SubsystemEntry extends ForgeRegistryEntry<SubsystemEntry>{

	private IFactory<Subsystem> fact;
	private Item item;
	
	public SubsystemEntry(IFactory<Subsystem> fact, Item item){
		this.fact = fact;
		this.item = item;
	}
	
	public Subsystem create(ConsoleTile console) {
		Subsystem sub = this.fact.create(console, item);
		sub.setEntry(this);
		return sub;
	}
	
	public static interface IFactory<T extends Subsystem>{
		T create(ConsoleTile console, Item key);
	}

}
