package net.tardis.mod.subsystem;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.Chunk;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.ShipComputerTile;
import net.tardis.mod.tileentities.console.misc.DistressSignal;
import net.tardis.mod.tileentities.console.misc.SparkingLevel;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class AntennaSubsystem extends Subsystem{

	public AntennaSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}
	@Override
	public CompoundNBT serializeNBT() {
		return super.serializeNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		super.deserializeNBT(nbt);
	}


	@Override
	public boolean stopsFlight() {
		return false;
	}

	@Override
	public void onTakeoff() {}

	@Override
	public void onLand() {
		this.damage(null, 1);
		if(this.console != null && this.canBeUsed()) {
			ExteriorTile ext = this.console.getExteriorType().getExteriorTile(console);
			if(ext != null && ext.getWorld() != null) {
				ChunkPos startCP = ext.getWorld().getChunk(ext.getPos()).getPos();
				int chunkRad = 3;
				for(int x = -chunkRad; x < chunkRad; ++x) {
					for(int z = -chunkRad; z < chunkRad; ++z) {
						Chunk c = ext.getWorld().getChunk(startCP.x + x, startCP.z + z);
						for(TileEntity te : c.getTileEntityMap().values()) {
							if(te instanceof ShipComputerTile) {
								ShipComputerTile comp = (ShipComputerTile)te;
								if(comp.getSchematic() != null) {
									console.addDistressSignal(new DistressSignal("Automated SOS Detected!", new SpaceTimeCoord(ext.getWorld().getDimensionKey(), te.getPos())));
									this.damage((ServerPlayerEntity)this.console.getPilot(), 2);
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void onFlightSecond() {}

	@Override
	public void explode(boolean softCrash) {}
	
	@Override
	public SparkingLevel getSparkState() {
		return SparkingLevel.NONE;
	}

}
