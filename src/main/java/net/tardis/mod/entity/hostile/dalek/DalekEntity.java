package net.tardis.mod.entity.hostile.dalek;

import java.util.function.Predicate;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityPredicate;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.attributes.AttributeModifierMap;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.monster.AbstractRaiderEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShieldItem;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.pathfinding.GroundPathNavigator;
import net.minecraft.pathfinding.PathNavigator;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.Difficulty;
import net.minecraft.world.World;
import net.minecraft.world.raid.Raid;
import net.tardis.api.space.entities.ISpaceImmuneEntity;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.TDataSerializers;
import net.tardis.mod.entity.hostile.dalek.types.DalekType;
import net.tardis.mod.registries.DalekTypeRegistry;

public class DalekEntity extends AbstractRaiderEntity implements ISpaceImmuneEntity{

    public static final DataParameter<ResourceLocation> DALEK_TYPE = EntityDataManager.createKey(DalekEntity.class, TDataSerializers.RESOURCE_LOCATION);
    public static final DataParameter<String> TEXTURE = EntityDataManager.createKey(DalekEntity.class, DataSerializers.STRING);
    private static final Predicate<LivingEntity> DEFAULT_PREDICATE = (predicate) -> {
        return !(predicate instanceof AbstractRaiderEntity) && predicate.getType() != TEntities.DALEK.get();
    };
    public static final EntityPredicate DEFAULT_ENEMY_CONDITION = (new EntityPredicate()).setCustomPredicate(DEFAULT_PREDICATE).setDistance(20D);
    
    //Commander stuff
//    private static final AxisAlignedBB SCAN_RANGE = new AxisAlignedBB(0, 0, 0, 1, 1, 1).grow(20);
//    private final DalekEntity commander = null;
//    private static final DataParameter<Boolean> IS_COMMANDER = EntityDataManager.createKey(DalekEntity.class, DataSerializers.BOOLEAN);

    public DalekEntity(EntityType<? extends AbstractRaiderEntity> type, World worldIn) {
        super(type, worldIn);
    }

    public DalekEntity(World worldIn) {
        super(TEntities.DALEK.get(), worldIn);
    }

    public static AttributeModifierMap.MutableAttribute createAttributes() {
        return MobEntity.func_233666_p_()
                .createMutableAttribute(Attributes.MAX_HEALTH, 120D)
                .createMutableAttribute(Attributes.MOVEMENT_SPEED, 0.2D)
                .createMutableAttribute(Attributes.FOLLOW_RANGE, 30D)
                .createMutableAttribute(Attributes.ATTACK_DAMAGE, 5D)
                .createMutableAttribute(Attributes.ATTACK_KNOCKBACK, 0.5D)
                .createMutableAttribute(Attributes.ATTACK_SPEED, 0.1D)
                .createMutableAttribute(Attributes.FLYING_SPEED, 0.4D)
                .createMutableAttribute(Attributes.KNOCKBACK_RESISTANCE, 0.1D);
    }
    
    @Override
    protected void registerData() {
        super.registerData();
        DalekType dalek = DalekTypeRegistry.getRandom(rand);
        getDataManager().register(DALEK_TYPE, dalek.getRegistryName());
        getDataManager().register(TEXTURE, getDalekType().getRandomTexture(this));
    }
    
    @Override
    public void notifyDataManagerChange(DataParameter<?> key) {
        super.notifyDataManagerChange(key);
    }

    @Override
    protected void registerGoals() {
        super.registerGoals();
        getDalekType().setupDalek(this);
    }
    
    @Override
    protected PathNavigator createNavigator(World worldIn) {
        GroundPathNavigator navigator = new GroundPathNavigator(this, worldIn);
        navigator.getNodeProcessor().setCanEnterDoors(true);
        navigator.setBreakDoors(true);
        navigator.setCanSwim(true);
        navigator.setAvoidSun(false);
        return navigator;
    }

	@Override
	protected void dropSpecialItems(DamageSource source, int looting, boolean recentlyHitIn) {
		ItemStack headStack = this.getItemStackFromSlot(EquipmentSlotType.HEAD);
		if (!headStack.isEmpty()) {
		    if (!ItemStack.areItemStacksEqual(headStack, Raid.createIllagerBanner())) {
		    	super.dropSpecialItems(source, looting, recentlyHitIn); //Prevent dropping of ominous banners
		    }
		}
	}

	@Override
    public void tick() {
        super.tick();
        if (getDalekType() != null)
            getDalekType().tickSpecial(this);
        if (!this.onGround) {
            world.addParticle(ParticleTypes.COMPOSTER, this.getPosX(), this.getPosY() - 0.25, this.getPosZ(), 0, -0.25, 0);
        }
    }

    @Override
    public void readAdditional(CompoundNBT compound) {
        super.readAdditional(compound);

        if (compound.contains("dalek_type")) {
            getDataManager().set(DALEK_TYPE, new ResourceLocation(compound.getString("dalek_type")));
        }

        if (compound.contains("texture")) {
            getDataManager().set(TEXTURE, compound.getString("texture"));
        }
    }

    @Override
    public void writeAdditional(CompoundNBT compound) {
        super.writeAdditional(compound);

        compound.putString("dalek_type", getDataManager().get(DALEK_TYPE).toString());

        compound.putString("texture", getDataManager().get(TEXTURE));

    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return SoundEvents.ENTITY_IRON_GOLEM_HURT;
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return getDalekType().getAmbientSound(this);
    }

    @Override
    protected SoundEvent getDeathSound() {
        return getDalekType().getDeathSound(this);
    }

    public DalekType getDalekType() {
        return DalekTypeRegistry.DALEK_TYPE_REGISTRY.get().getValue(getDataManager().get(DALEK_TYPE));
    }

    /** Set the Dalek Type to be a specific one. By default the dalek is assigned a random Dalek Type.*/
    public void setDalekType(DalekType dalekType) {
        ResourceLocation oldType = getDataManager().get(DALEK_TYPE);

        boolean hasChanged = !oldType.equals(dalekType.getRegistryName());
        getDataManager().set(DALEK_TYPE, dalekType.getRegistryName());
        if (hasChanged) {
            getDataManager().set(TEXTURE, dalekType.getRandomTexture(this));
            getDalekType().setupDalek(this);
        }
    }

    public String getTexture() {
        return getDataManager().get(TEXTURE);
    }

    public double getTargetDistance() {
        return this.getAttributeValue(Attributes.FOLLOW_RANGE);
    }
    
    public AxisAlignedBB getTargetableArea(double targetDistance) {
        return this.getBoundingBox().grow(targetDistance, 4.0D, targetDistance);
    }


    @Override
    public boolean canBreatheUnderwater() {
        return true;
    }

    @Override
    public boolean isInvulnerableTo(DamageSource source) {
        return super.isInvulnerableTo(source) || 
                /*
                 *If the source is from a projectile, handle projectile damage based on world difficulty
                 *If this source is not a Dalek damage source allow damage to occur. Else cancel damage from other daleks
                 *If there is no true source (no entity involved), cancel it unless it is void damage generated by the kill command or Tardis Squash
                 */ 
                source.getTrueSource() != null ? 
                    (source.isProjectile() ? (TConfig.SERVER.dalekImmuneToProjectiles.get() ? true : (this.world.getDifficulty() == Difficulty.HARD ? true : false)) : (source.getTrueSource().getType() != this.getDalekType().getDamageSource(this).getTrueSource().getType() ? false : true))
                         : source != TDamageSources.TARDIS_SQUASH && source != DamageSource.OUT_OF_WORLD;
    }

    @Override
    public float getSoundVolume() {
        return getDalekType() == DalekTypeRegistry.RUSTY.get() ? 0.2F : 0.8F;
    }

    @Override
    public float getSoundPitch() {
        return getDalekType() == DalekTypeRegistry.RUSTY.get() ? 0.1F : 1F;
    }

    @Override
    public void applyWaveBonus(int wave, boolean p_213660_2_) {
        Raid raid = this.getRaid();
        int i = 2;
        if (wave > raid.getWaves(Difficulty.NORMAL)) {
            i = 4;
            this.addPotionEffect(new EffectInstance(Effects.RESISTANCE, 99999, 3));
        }
        this.addPotionEffect(new EffectInstance(Effects.REGENERATION, 99999, i));
    }

    /** Correct Name: getRaidCelebrateSound, the MCP name is wrong lol */
    @Override
    public SoundEvent getRaidLossSound() {
        return SoundEvents.ENTITY_PILLAGER_CELEBRATE;
    }

    @Override
    public ItemStack getPickedResult(RayTraceResult target) {
        return new ItemStack(getDalekType().getSpawnItem());
    }

    /** Custom implementation of MobEntity#maybeDisableShield.
     * <p> Accounts for modded shields*/
    public void maybeDisableShield(PlayerEntity player, ItemStack dalekHeldItem, ItemStack playerHeldItem) {
        if (!dalekHeldItem.isEmpty() && !playerHeldItem.isEmpty() && dalekHeldItem.getItem() instanceof AxeItem && playerHeldItem.getItem() instanceof ShieldItem) {
             float f = 0.25F + (float)EnchantmentHelper.getEfficiencyModifier(this) * 0.05F;
             if (this.rand.nextFloat() < f) {
                ShieldItem shield = (ShieldItem)playerHeldItem.getItem();
                player.getCooldownTracker().setCooldown(shield, 100);
                this.world.setEntityState(player, (byte)30);
             }
          }
    }

    @Override
    public boolean shouldTakeSpaceDamage() {
        return false;
    }
}
