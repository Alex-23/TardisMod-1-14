package net.tardis.mod.entity.hostile.dalek.types;

import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.entity.hostile.dalek.DalekEntity;

public class RustyType extends DalekType {
    
    @Override
	public double getMaxHealth() {
		return 60D;
	}

	@Override
	public double getMovementSpeed() {
		return 0.4D;
	}

	@Override
	public double getAttackDamage() {
		return 2D;
	}
	
	@Override
	public double getAttacksPerSecond() {
		return 0.5D;
	}

	@Override
	public boolean canFly() {
		return false;
	}
	
	@Override
	public Vector3d getLaserColour() {
		return new Vector3d(0.5D, 1D, 1D);
	}

	@Override
    public void tickSpecial(DalekEntity dalekEntity) {
        super.tickSpecial(dalekEntity);

        if (dalekEntity.ticksExisted % 25 == 0) {
            for (int i = 0; i < 2; ++i) {
                dalekEntity.world.addParticle(ParticleTypes.LARGE_SMOKE, dalekEntity.getPosXRandom(0.5D), dalekEntity.getPosYRandom(), dalekEntity.getPosZRandom(0.5D), 0.0D, 0.0D, 0.0D);
            }
        }
    }
}
