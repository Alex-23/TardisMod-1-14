package net.tardis.mod.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.PrioritizedGoal;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.network.play.server.SEntityVelocityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.api.space.IOxygenSealer;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.exteriors.ExteriorBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.entity.ai.FollowOutOfTardisGoal;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.events.LivingEvents.TardisLeaveEvent;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IDoorSoundScheme;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.packets.exterior.DoorData;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.world.dimensions.TDimensions;

public class DoorEntity extends Entity {

    public static final DataParameter<Integer> STATE = EntityDataManager.createKey(DoorEntity.class, DataSerializers.VARINT);
    public static final DataParameter<Boolean> SUCC = EntityDataManager.createKey(DoorEntity.class, DataSerializers.BOOLEAN);
    public IDoorType doorType = EnumDoorType.STEAM;
    private List<UUID> teleportImmune = new ArrayList<>();
    private boolean isLocked = false;
    private int additionalLockLevel = 0;
    private float health = 10;

    public DoorEntity(EntityType<?> entityTypeIn, World worldIn) {
        super(entityTypeIn, worldIn);
    }

    public DoorEntity(World worldIn) {
        this(TEntities.DOOR.get(), worldIn);
    }

    @Override
    protected void registerData() {
        this.dataManager.register(STATE, EnumDoorState.CLOSED.ordinal());
        this.dataManager.register(SUCC, false);
    }

    @Override
    protected void readAdditional(CompoundNBT compound) {
        this.getDataManager().set(STATE, compound.getInt("door_state"));
        this.getDataManager().set(SUCC, compound.getBoolean("succ"));
        this.isLocked = compound.getBoolean("locked");
        this.additionalLockLevel = compound.getInt("additional_lock_level");
    }

    @Override
    protected void writeAdditional(CompoundNBT compound) {
        compound.putInt("door_state", this.dataManager.get(STATE));
        compound.putBoolean("locked", this.isLocked);
        compound.putBoolean("succ", this.getDataManager().get(SUCC));
        compound.putInt("additional_lock_level", this.additionalLockLevel);
    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public void onKillCommand() {
        return;
    }

    @Override
    public boolean isInvulnerableTo(DamageSource source) {
        return super.isInvulnerableTo(source) || source.isFireDamage();
    }

    @Override
    public ItemStack getPickedResult(RayTraceResult target) {
        return new ItemStack(TItems.INT_DOOR.get());
    }

    @Override
    public void tick() {
        super.tick();

        //If console is null, none of this should happen
        ConsoleTile console = this.getConsole();
        if (console == null)
            return;

        if (!world.isRemote) {
            //Teleport entities

            if (this.getOpenState() != EnumDoorState.CLOSED && this.ticksExisted> 5) {
                List<Entity> entities = world.getEntitiesWithinAABB(Entity.class, this.getDoorBB().offset(this.getPositionVec()));
                this.teleportEntity(entities);
                List<UUID> list = new ArrayList<UUID>();
                for (Entity e : entities) {
                    if (this.teleportImmune.contains(e.getUniqueID()))
                        list.add(e.getUniqueID());
                }
                this.teleportImmune = list;
            }

            //Suck if in a space dim
            if (world.getGameTime() % 100 == 0)
                this.handleSucking();

            //Interdimensional Fluids
            if (world.getGameTime() % 20 == 0) {
                ServerWorld otherW = world.getServer().getWorld(console.getCurrentDimension());
                ShieldGeneratorSubsystem shield = console.getSubsystem(ShieldGeneratorSubsystem.class).orElse(null);
                if (this.getOpenState() != EnumDoorState.CLOSED && shield != null && !shield.isActivated()) {
                    WorldHelper.setFluidStateIfNot(otherW.getFluidState(console.getCurrentLocation().offset(console.getTrueExteriorFacingDirection())), world, this.getPosition());
                    WorldHelper.setFluidStateIfNot(otherW.getFluidState(console.getCurrentLocation().up().offset(console.getTrueExteriorFacingDirection())), world, this.getPosition().up());
                } else {
                    WorldHelper.setFluidStateIfNot(Fluids.EMPTY.getDefaultState(), world, this.getPosition());
                    WorldHelper.setFluidStateIfNot(Fluids.EMPTY.getDefaultState(), world, this.getPosition().up());
                }
            }

        }
        this.doorType = console.getExteriorType().getDoorType();

        if ((console.isInFlight() || this.getDataManager().get(SUCC)) && this.getOpenState() != EnumDoorState.CLOSED) {
            this.suckIntoVoid();
        }
    }

    @Override
    public ActionResultType processInitialInteract(PlayerEntity player, Hand hand) {
        if (!world.isRemote) {
            if (this.getConsole() != null) {
                ExteriorTile ext = this.getConsole().getOrFindExteriorTile().orElse(null);
                if (ext != null) {
                    if (player.isSneaking()) {
                        if (!ext.isExteriorDeadLocked() && !ext.isInteriorRegenerating()) {
                            if (this.isLocked) {
                                this.isLocked = false;
                                world.playSound(null, this.getPosition(), TSounds.DOOR_UNLOCK.get(), SoundCategory.BLOCKS, 1F, 1F);
                                this.playSoundAtExterior(ext, TSounds.DOOR_UNLOCK.get(), SoundCategory.BLOCKS, 1F);
                                if (ext != null)
                                    ext.setLocked(false);
                                player.sendStatusMessage(ExteriorBlock.UNLOCKED, true);
                                return ActionResultType.SUCCESS;
                            } else {
                                //If the exterior state is deadlocked, make the exterior respect the door's data if the door's data is updated
                                if (ext.isExteriorDeadLocked()) {
                                    //If the door entity data was updated, update the exterior tile's data with this new data
                                    if (this.additionalLockLevel != ext.getAdditionalLockLevel()) {
                                        ext.setAdditionalLockLevel(this.additionalLockLevel);
                                        ext.updateSpecific(DoorData.create(ext));
                                    }
                                    if (ext.isInteriorRegenerating()) {
                                        player.sendStatusMessage(new TranslationTextComponent(ExteriorBlock.INTERIOR_CHANGE, ext.getRemainingInteriorChangeTime()), true);
                                    }
                                    else {
                                        player.sendStatusMessage(ExteriorBlock.DEADLOCKED, true);
                                    }
                                    return ActionResultType.SUCCESS;
                                }
                                else {
                                    this.isLocked = true;
                                    this.setOpenState(EnumDoorState.CLOSED);
                                    this.openAndUpdateExteriorDoor();
                                    if (ext != null) {
                                        ext.setLocked(isLocked);
                                    }
                                    world.playSound(null, this.getPosition(), TSounds.DOOR_LOCK.get(), SoundCategory.BLOCKS, 1F, 1F);
                                    this.playSoundAtExterior(ext, TSounds.DOOR_LOCK.get(), SoundCategory.BLOCKS, 1F);
                                    player.sendStatusMessage(ExteriorBlock.LOCKED, true);
                                    return ActionResultType.SUCCESS;
                                }
                            }
                        }
                    }
                    else {
                        //Same logic as when player is sneaking, if the door entity data was updated, make the exterior use the door entity's new data
                        if (this.additionalLockLevel != ext.getAdditionalLockLevel()) {
                            ext.setAdditionalLockLevel(this.additionalLockLevel);
                            ext.updateSpecific(DoorData.create(ext));
                        }
                        if (ext.isInteriorRegenerating()) {
                            player.sendStatusMessage(new TranslationTextComponent(ExteriorBlock.INTERIOR_CHANGE, ext.getRemainingInteriorChangeTime()), true);
                        }
                        
                        //Don't open the door if the door is locked or deadlocked
                        if (!this.isLocked && !this.isDeadLocked()) {
                            EnumDoorState[] valid = this.doorType.getValidStates();
                            int index = this.getOpenState().ordinal() + 1;
                            if (index>= valid.length)
                                index = 0;
                            this.setOpenState(valid[index]);
                            this.openAndUpdateExteriorDoor();

                            IDoorSoundScheme scheme = this.getConsole().getExteriorType().getDoorSounds();

                            if (this.getOpenState() == EnumDoorState.CLOSED) {
                                world.playSound(null, player.getPosition(), scheme.getClosedSound(), SoundCategory.BLOCKS, 1F, 1F);
                                this.playSoundAtExterior(ext, scheme.getClosedSound(), SoundCategory.BLOCKS, 1F);
                            } else {
                                world.playSound(null, player.getPosition(), scheme.getOpenSound(), SoundCategory.BLOCKS, 1F, 1F);
                                this.playSoundAtExterior(ext, scheme.getOpenSound(), SoundCategory.BLOCKS, 1F);
                            }
                        }
                        else {
                            if (this.isDeadLocked()) {
                                player.sendStatusMessage(ExteriorBlock.DEADLOCKED, true);
                                return ActionResultType.SUCCESS;
                            }
                            if (this.isLocked) {
                                player.sendStatusMessage(ExteriorBlock.LOCKED, true);
                                return ActionResultType.SUCCESS;
                            }
                        }
                    }
                }
                else {
                    Tardis.LOGGER.error("Cannot find Exterior Tile! Was the exterior block removed whilst it was still rematerialising?");
                }
            }

          /*  if (!player.getHeldItemMainhand().isEmpty()) {
                float f = player.rotationPitch;
                ConsoleTile console = this.getConsole();

                float f1 = console.getTrueExteriorFacingDirection().getHorizontalAngle() + player.rotationYaw;
                BlockPos gpos = console.getCurrentLocation().offset(console.getTrueExteriorFacingDirection(), 2);
                Vector3d vector3d = new Vector3d(gpos.getX(), gpos.getY() + player.getEyeHeight(), gpos.getZ());
                float f2 = MathHelper.cos(-f1 * ((float) Math.PI / 180F) - (float) Math.PI);
                float f3 = MathHelper.sin(-f1 * ((float) Math.PI / 180F) - (float) Math.PI);
                float f4 = -MathHelper.cos(-f * ((float) Math.PI / 180F));
                float f5 = MathHelper.sin(-f * ((float) Math.PI / 180F));
                float f6 = f3 * f4;

                float f7 = f2 * f4;
                double d0 = player.getAttribute(net.minecraftforge.common.ForgeMod.REACH_DISTANCE.get()).getValue();

                Vector3d vector3d1 = vector3d.add((double) f6 * d0, (double) f5 * d0, (double) f7 * d0);
                RegistryKey<World> type = console.getCurrentDimension();
                ServerWorld desWorld = ServerLifecycleHooks.getCurrentServer().getWorld(type);
                BlockRayTraceResult result = desWorld.rayTraceBlocks(new RayTraceContext(vector3d, vector3d1, RayTraceContext.BlockMode.OUTLINE, RayTraceContext.FluidMode.ANY, player));
                BlockState block = desWorld.getBlockState(result.getPos());

                player.world = desWorld;
                player.getHeldItemMainhand().getItem().onItemUse(new ItemUseContext(player, Hand.MAIN_HAND, result));
                System.out.println("Attempting to Activate: " + block.getBlock().getRegistryName());
                block.getBlock().onBlockActivated(block, desWorld, result.getPos(), player, Hand.MAIN_HAND, result);
                player.world = world;


                System.out.println(result.getPos());
                return ActionResultType.SUCCESS;
            }
*/          

        }
        return ActionResultType.SUCCESS;
    }
    
    @Override
    public boolean attackEntityFrom(DamageSource source, float amount) {
        if (!world.isRemote) {
            if (source.getTrueSource() instanceof PlayerEntity) {
                PlayerEntity playerIn = (PlayerEntity) source.getTrueSource();
                if (playerIn.abilities.isCreativeMode) {
                    this.health = 0;
                } else {
                    this.health -= amount;
                }
                this.world.playSound(null, getPosition(), SoundEvents.ITEM_SHIELD_BLOCK, SoundCategory.NEUTRAL, 1F, 1F);
            }
            if (this.health <= 0) {
                ItemEntity entity = new ItemEntity(world, this.getPosX(), this.getPosY(), this.getPosZ(), this.getPickedResult(null));
                world.addEntity(entity);
                remove();
            }
        }
        return true;
    }

    @Override
    public boolean canBeCollidedWith() {
        return true;
    }

    @Override
    public boolean canBePushed() {
        return false;
    }

    @Override
    public boolean canRenderOnFire() {
        return false;
    }

    @Nullable
    public ConsoleTile getConsole() {
        TileEntity te = this.world.getTileEntity(TardisHelper.TARDIS_POS);
        if (te instanceof ConsoleTile)
            return (ConsoleTile) te;
        return null;
    }

    public void setLocked(boolean locked) {
        this.isLocked = locked;
    }

    public boolean isLocked() {
        return this.isLocked;
    }

    public void playSoundAtExterior(@Nullable ExteriorTile tile, SoundEvent event, SoundCategory cat, float vol) {
        if (tile != null && tile.hasWorld())
            tile.getWorld().playSound(null, tile.getPos(), event, cat, vol, 1F);
    }

    public AxisAlignedBB getDoorBB() {
        switch (this.getHorizontalFacing()) {
            case EAST:
                return new AxisAlignedBB(0.4, 0, -0.5, 0.5, 2, 0.5);
            case WEST:
                return new AxisAlignedBB(-0.5, 0, -0.5, -0.4, 2, 0.5);
            case SOUTH:
                return new AxisAlignedBB(-0.5, 0, 0.4, 0.5, 2, 0.5);
            default:
                return new AxisAlignedBB(-0.5, 0, -0.5, 0.5, 2, -0.4);
        }
    }

    public void addEntityToTeleportImmuneList(UUID id) {
        this.teleportImmune.add(id);
    }

    public void teleportEntities(List<Entity> entities) {
        this.teleportEntity(entities);
    }

    private void teleportEntity(List<Entity> entity) {
        ConsoleTile console = this.getConsole();
        if (console == null)
            return;

        world.getServer().enqueue(new TickDelayedTask(1, () -> {

            for (Entity e : entity) {
                if (!(e instanceof DoorEntity)) {

                    if (console.isInFlight()) {

                        if (e instanceof PlayerEntity) {
                            if (console.getEmotionHandler().getLoyalty(e.getUniqueID())> 50) {
                                this.setOpenState(EnumDoorState.CLOSED);
                                continue;
                            } else if (console.getEmotionHandler().getLoyalty(e.getUniqueID())> 25) {
                                console.initLand();
                            }
                        }

                        BlockPos diff = console.getDestinationPosition().subtract(console.getCurrentLocation());
                        double scale = console.getPercentageJourney();
                        RegistryKey<World> type = scale> 0.5 ? console.getDestinationDimension() : console.getCurrentDimension();
                        ChunkPos cPos = new ChunkPos(console.getCurrentLocation());
                        ServerWorld sWorld = world.getServer().getWorld(type);               
                        
                        WorldHelper.forceChunkIfNotLoaded(sWorld, cPos, console.getCurrentLocation());
                        BlockPos targetPos = LandingSystem.getTopBlock(sWorld, console.getCurrentLocation().add(new BlockPos(diff.getX() * scale, diff.getY() * scale, diff.getZ() * scale)));
                        SpaceTimeCoord target = new SpaceTimeCoord(type, targetPos);
                        WorldHelper.unForceChunkIfLoaded(sWorld, cPos, console.getCurrentLocation());
                        
                        if (e instanceof LivingEntity)
                            MinecraftForge.EVENT_BUS.post(new TardisLeaveEvent((LivingEntity) e, this, world.getServer().getWorld(type)));

                        //The big Succ
                        e.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
                            cap.setDestination(target);
                            WorldHelper.teleportEntities(e, world.getServer().getWorld(TDimensions.VORTEX_DIM), 0, 128, 0, e.rotationYaw, e.rotationPitch);
                        });

                        //Suckout non- players too
                        if (!e.getCapability(Capabilities.PLAYER_DATA).isPresent()) {
                            WorldHelper.teleportEntities(e, world.getServer().getWorld(WorldHelper.getWorldKeyFromRL(target.getDimRL())), target.getPos().getX() + 0.5, target.getPos().getY() + 1, target.getPos().getZ() + 0.5, e.rotationYaw, e.rotationPitch);
                        }

                        return;
                    }

                    if (this.teleportImmune.contains(e.getUniqueID()))
                        continue;

                    ExteriorTile ext = console.getExteriorType().getExteriorTile(console);
                    if (ext != null)
                        ext.addTeleportedEntity(e.getUniqueID());

                    Vector3d oldMotion = e.getMotion();

                    ServerWorld destWorld = world.getServer().getWorld(console.getCurrentDimension());

                    Direction dir = console.getTrueExteriorFacingDirection();

                    float diff = WorldHelper.getFixedRotation(e.rotationYaw) - WorldHelper.getFixedRotation(this.rotationYaw);//(e.rotationYaw % 360.0F) - this.rotationYaw % 360.0F;

                    float realFacing = WorldHelper.getAngleFromFacing(dir.getOpposite()) + diff;

                    BlockPos pos = console.getCurrentLocation().offset(dir);
                    e.rotationYaw = WorldHelper.getAngleFromFacing(dir.getOpposite());

                    if (e instanceof LivingEntity)
                        MinecraftForge.EVENT_BUS.post(new TardisLeaveEvent((LivingEntity) e, this, world.getServer().getWorld(console.getCurrentDimension())));

                    WorldHelper.teleportEntities(e, destWorld, pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5, realFacing, e.rotationPitch);
                    console.startInteriorChangeProcess(destWorld);
                    //Follow out of TARDIS
                    if (e instanceof PlayerEntity) {
                        for (MonsterEntity ent : world.getEntitiesWithinAABB(MonsterEntity.class, new AxisAlignedBB(this.getPosition()).grow(20))) {
                            if (ent.getAttackTarget() == e) {
                                for (PrioritizedGoal goal : ent.goalSelector.goals) {
                                    if (goal.getGoal() instanceof FollowOutOfTardisGoal) {
                                        ((FollowOutOfTardisGoal) goal.getGoal()).setTarget(this.getPosition());
                                    }
                                }
                            }
                        }
                    }

                    if (e instanceof PlayerEntity && console.getEntity() != null) {
                        e.startRiding(console.getEntity());
                    }

                    Vector3d setMot = oldMotion.rotateYaw(-(float) Math.toRadians(realFacing));

                    world.getServer().enqueue(new TickDelayedTask(2, () -> {
                        Entity ent = destWorld.getEntityByUuid(e.getUniqueID());
                        if (ent != null)
                            ent.setMotion(setMot);
                        if (ent instanceof ServerPlayerEntity) {
                            ((ServerPlayerEntity) ent).connection.sendPacket(new SEntityVelocityPacket(ent));
                        }
                    }));

                }
            }

        }));
    }

    public void suckIntoVoid() {
        for (LivingEntity e : world.getEntitiesWithinAABB(LivingEntity.class, this.getBoundingBox().grow(40))) {
            if (e.canEntityBeSeen(this)) {
                e.stopRiding();
                if (e.isSleeping())
                    e.wakeUp();
                Vector3d motion = this.getPositionVec().subtract(e.getPositionVec()).normalize().scale(0.1);
                e.setMotion(e.getMotion().add(motion));
                e.isAirBorne = true;
                e.setOnGround(false);
            }
        }
    }

    public void setOpenState(EnumDoorState open) {
        this.dataManager.set(STATE, open.ordinal());
        if (open != EnumDoorState.CLOSED)
            this.setLocked(false);
    }

    public EnumDoorState getOpenState() {
        return EnumDoorState.values()[this.dataManager.get(STATE)];
    }
    
    public boolean getSucking() {
        return this.getDataManager().get(SUCC);
    }

    public void setSucking(boolean succ) {
        this.getDataManager().set(SUCC, succ);
    }
    
    public void handleSucking() {
        if (!world.isRemote) {
            ServerWorld ws = world.getServer().getWorld(this.getConsole().getCurrentDimension());
            //Check for the capability as custom Dimensions can no longer extend dimension, so no dimensions will be able to implement ISpaceDimProperties
            //Hence we cannot check for the raw interface, only the capability
            if (ws.getCapability(Capabilities.SPACE_DIM_PROPERTIES).isPresent()) {
                ws.getCapability(Capabilities.SPACE_DIM_PROPERTIES).ifPresent(properties -> {
                    //If would get sucked out
                    ObjectWrapper<Boolean> suck = new ObjectWrapper<Boolean>(!properties.hasAir());

                    //Account for Oxygen sealers

                    ExteriorTile ext = this.getConsole().getExteriorType().getExteriorTile(this.getConsole());

                    //If the exterior doesn't exist, it can't be in space
                    if (ext == null)
                        return;

                    Direction dir = ext.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING);

                    for (TileEntity te : ws.loadedTileEntityList) {
                        IOxygenSealer oxy = te.getCapability(Capabilities.OXYGEN_SEALER).orElse(null);
                        if (oxy != null) {
                            if (oxy.getSealedPositions().contains(ext.getPos().offset(dir))) {
                                suck.setValue(false);
                                break;
                            }
                        }
                    }

                    this.getConsole().getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(shield -> {
                        if (shield.isForceFieldActivated())
                            suck.setValue(false);
                    });

                    //Actually set the suck value
                    if (this.getDataManager().get(SUCC) != suck.getValue())
                        this.getDataManager().set(SUCC, suck.getValue());
                });

            } else if (this.getDataManager().get(SUCC))
                this.getDataManager().set(SUCC, false);
        }
    }

    public int getAdditionalLockLevel() {
        return this.additionalLockLevel;
    }

    /** Set the level of additional locks on top of normal locking for the exterior
     * <p> 0 - No additional lock levels. Exterior only has 1 lock level via the Tardis Key
     * <br> 1 - Double Locked. Prevents player from entering during interior changing
     * <br> 2 - Triple Locked. Other mods cannot bypass this unless specified.*/
    public void setAdditionalLockLevel(int level) {
        this.additionalLockLevel = level;
    }
    
    public boolean isDeadLocked() {
        return this.additionalLockLevel > 0;
    }
    /** Logic to update the exterior's door states with data from this door entity when the door entity is interacted with*/
    public void openAndUpdateExteriorDoor() {
        if (!world.isRemote) {
            ConsoleTile console = this.getConsole();
            if (console != null) {
                ExteriorTile ext = console.getExteriorType().getExteriorTile(console);
                if (ext != null) {
                    ext.setDoorState(this.getOpenState());
                    if (this.getOpenState() != EnumDoorState.CLOSED) {
                        ext.setLocked(false);
                    }
                    if (ext.getAdditionalLockLevel() != this.additionalLockLevel) {
                        ext.setAdditionalLockLevel(this.additionalLockLevel);
                    }
                    ext.updateSpecific(DoorData.create(ext));
                    
                }
            }
        }
    }
    /** Raw method to update the exterior's data to that of this door entity*/
    public void updateExteriorDoorData() {
        ConsoleTile tile = this.getConsole();
        if (tile != null) {
            ExteriorTile ext = tile.getExteriorType().getExteriorTile(tile);
            if (ext != null) {
                ext.setDoorState(this.getOpenState());
                ext.setLocked(this.isLocked);
                ext.setAdditionalLockLevel(this.additionalLockLevel);
                ext.updateSpecific(DoorData.create(ext));
            }
        }
    }
}
