package net.tardis.mod.entity.ai.humanoids;

import net.minecraft.block.BlockState;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.goal.MoveToBlockGoal;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.server.SAnimateHandPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.server.ServerChunkProvider;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tags.TardisBlockTags;

public class DoCrewWorkGoal extends MoveToBlockGoal{

	private int work;
	private boolean working = false;

	public DoCrewWorkGoal(CreatureEntity creature, double speedIn, int length) {
		super(creature, speedIn, length, 1);
	}

	@Override
	protected boolean shouldMoveTo(IWorldReader worldIn, BlockPos pos) {
		return worldIn.getBlockState(pos).isIn(TardisBlockTags.CREW_TASK);
	}

	@Override
	public boolean shouldContinueExecuting() {
		if(working){
			++work;
			if(work > 200) {
				return false;
			}
			return true;
		}
		return super.shouldContinueExecuting();
	}

	@Override
	public void resetTask() {
		super.resetTask();
		this.creature.setHeldItem(Hand.MAIN_HAND, ItemStack.EMPTY);
		this.working = false;
		this.work = 0;
	}

	@Override
	public void startExecuting() {
		super.startExecuting();
		this.creature.setHeldItem(Hand.MAIN_HAND, new ItemStack(TItems.PLASMIC_SHELL_GENERATOR.get()));
	}

	@Override
	public void tick() {
		super.tick();
		if(this.getIsAboveDestination()){
			if(this.creature.ticksExisted % 20 == 0) {
				if (!creature.world.isRemote())
				    swingArm(Hand.MAIN_HAND);
			}
			this.working = true;
		}
	}

	public void swingArm(Hand hand){
		if (!creature.isSwingInProgress) {
			creature.swingProgressInt = -1;
			creature.isSwingInProgress = true;
			creature.swingingHand = hand;
			if (creature.world instanceof ServerWorld) {
				SAnimateHandPacket sanimatehandpacket = new SAnimateHandPacket(creature, hand == Hand.MAIN_HAND ? 0 : 3);
				ServerChunkProvider serverchunkprovider = ((ServerWorld)creature.world).getChunkProvider();
				serverchunkprovider.sendToAllTracking(creature, sanimatehandpacket);
			}
		}
	}

	@Override
	protected int getRunDelay(CreatureEntity creatureIn) {
		return super.getRunDelay(creatureIn);
	}

	@Override
	public double getTargetDistanceSq() {
		return 1.5 * 1.5;
	}
}
