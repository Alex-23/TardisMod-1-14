package net.tardis.mod.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.MoverType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

/**
 *
 * A class for places the TARDIS should show up, but only for display purposes, think 3D particle
 * 
 * @author Spectre0987 8-12-2020
 *
 *
 */
public class TardisDisplayEntity extends Entity{

	public static final DataParameter<String> EXTERIOR = EntityDataManager.createKey(TardisDisplayEntity.class, DataSerializers.STRING);
    
	private ExteriorTile tile;
	private int ticksUntilDeath = 100;
	private float friction = 0.97F;
	
	public TardisDisplayEntity(EntityType<?> entityTypeIn, World worldIn) {
		super(entityTypeIn, worldIn);
		this.setInvulnerable(true);
		this.noClip = true;
	}
	
	public TardisDisplayEntity(World worldIn) {
		this(TEntities.DISPLAY_TARDIS.get(), worldIn);
	}
	
	public TardisDisplayEntity setTile(ExteriorTile tile) {
		this.tile = tile;
		this.getDataManager().set(EXTERIOR, tile.getType().getRegistryName().toString());
		return this;
	}
	
	public void setTicksUntilDeath(int death) {
		this.ticksUntilDeath = death;
	}
	
	public int getTicksUntilDeath() {
		return this.ticksUntilDeath;
	}
	
	public void setFriction(float friction) {
		this.friction = friction;
	}

	@Override
	protected void registerData() {
		this.getDataManager().register(EXTERIOR, "");
	}

	@Override
	protected void readAdditional(CompoundNBT compound) {
		this.getDataManager().set(EXTERIOR, compound.getString("exterior"));
	    this.friction = compound.getFloat("friction");
	}

	@Override
	protected void writeAdditional(CompoundNBT compound) {
        compound.putString("exterior", this.getDataManager().get(EXTERIOR));
        
        compound.putFloat("friction", this.friction);
	}

	@Override
	public IPacket<?> createSpawnPacket() {
		return NetworkHooks.getEntitySpawningPacket(this);
	}

	@Override
	public void tick() {
		super.tick();
		this.noClip = true;
		
		this.move(MoverType.SELF, this.getMotion());
		this.setMotion(this.getMotion().scale(this.friction));
		
		if(!world.isRemote) {
			--this.ticksUntilDeath;
			if(this.ticksUntilDeath <= 0)
				this.remove();
		}
	}

	public ExteriorTile getTile() {
		if(this.tile == null) {
			TileEntityType<?> type = ForgeRegistries.TILE_ENTITIES.getValue(new ResourceLocation(this.getDataManager().get(EXTERIOR)));
	        if(type != null) {
	            this.tile = (ExteriorTile) type.create();
	            tile.setWorldAndPos(world, tile.getPos());
	        }
		}
		return this.tile;
	}

}
