package net.tardis.mod.events;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.minecraft.block.BellBlock;
import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.INBT;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IFutureReloadListener;
import net.minecraft.resources.IResourceManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.gen.FlatChunkGenerator;
import net.minecraft.world.gen.GenerationStage.Decoration;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.settings.DimensionStructuresSettings;
import net.minecraft.world.gen.settings.StructureSeparationSettings;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AddReloadListenerEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.RegisterCommandsEvent;
import net.minecraftforge.event.RegistryEvent.MissingMappings;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.TickEvent.WorldTickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.EntityStruckByLightningEvent;
import net.minecraftforge.event.entity.living.*;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerChangedDimensionEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.event.world.ExplosionEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.server.FMLServerAboutToStartEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.api.events.ServerSoundEvent;
import net.tardis.api.space.OxygenHelper;
import net.tardis.api.space.entities.ISpaceImmuneEntity;
import net.tardis.api.space.items.IGravArmor;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.cap.*;
import net.tardis.mod.cap.ITardisWorldData.TardisWorldProvider;
import net.tardis.mod.cap.entity.IPlayerData;
import net.tardis.mod.cap.entity.PlayerDataCapability;
import net.tardis.mod.cap.items.*;
import net.tardis.mod.cap.items.sonic.SonicCapability;
import net.tardis.mod.cap.items.sonic.SonicProvider;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.commands.TardisCommand;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.damagesources.TDamageSources;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.entity.ai.FollowIntoTardisGoal;
import net.tardis.mod.entity.ai.FollowOutOfTardisGoal;
import net.tardis.mod.entity.humanoid.HumanoidEmotionalState;
import net.tardis.mod.entity.mission.HumanoidMissionEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.events.LivingEvents.SpaceAir;
import net.tardis.mod.events.LivingEvents.TardisEnterEvent;
import net.tardis.mod.events.LivingEvents.TardisLeaveEvent;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.BlockPosHelper;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.items.ISpaceHelmet;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.TardisDiagnosticItem;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.misc.TardisLikes;
import net.tardis.mod.misc.TardisNames;
import net.tardis.mod.missions.KillMission;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MissionUpdateMessage;
import net.tardis.mod.network.packets.VMTeleportMessage;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.registries.DisguiseRegistry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.schematics.SchematicDataListener;
import net.tardis.mod.sonic.AbstractSonicMode;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.TemporalGraceSubsystem;
import net.tardis.mod.tags.TardisEntityTypeTags;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.PlayerTelepathicConnection;
import net.tardis.mod.tileentities.exteriors.DisguiseExteriorTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.trades.ItemTrade;
import net.tardis.mod.trades.TVillagerProfession;
import net.tardis.mod.traits.AbstractEntityDeathTrait;
import net.tardis.mod.traits.TardisTrait;
import net.tardis.mod.world.WorldGen;
import net.tardis.mod.world.biomes.TBiomes;
import net.tardis.mod.world.dimensions.TDimensions;
import net.tardis.mod.world.feature.TFeatures;
import net.tardis.mod.world.structures.TStructures;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import static net.tardis.mod.Tardis.MODID;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class CommonEvents {

    public static final ResourceLocation LIGHT_CAP = new ResourceLocation(Tardis.MODID, "light");
    public static final ResourceLocation CHUNK_CAP = new ResourceLocation(Tardis.MODID, "loader");
    public static final ResourceLocation TARDIS_CAP = new ResourceLocation(Tardis.MODID, "tardis_data");
    public static final ResourceLocation WATCH_CAP = new ResourceLocation(Tardis.MODID, "watch");
    public static final ResourceLocation PLAYER_DATA_CAP = new ResourceLocation(Tardis.MODID, "player_data");
    public static final ResourceLocation REMOTE_CAP = new ResourceLocation(Tardis.MODID, "remote");
    public static final ResourceLocation VORTEX = new ResourceLocation(Tardis.MODID, "vortex");
    public static final ResourceLocation LOCATOR = new ResourceLocation(Tardis.MODID, "locator");
    public static final ResourceLocation RIFT = new ResourceLocation(Tardis.MODID, "rift");
    public static final ResourceLocation SPACE_DIM_CAP = new ResourceLocation(Tardis.MODID, "space_dim_property");
    public static final ResourceLocation SONIC_CAP = new ResourceLocation(MODID, "sonic_cap");
    public static final ResourceLocation MISSION_CAP = new ResourceLocation(Tardis.MODID, "mission");

    private static HashMap<ResourceLocation, ResourceLocation> remappedEntries = new HashMap<ResourceLocation, ResourceLocation>();

    @SubscribeEvent
    public static void onSoundPlayed(ServerSoundEvent serverSoundEvent) {
        BlockPos pos = serverSoundEvent.getBlockPos();
        World world = serverSoundEvent.getWorld();
        for (TileEntity tileEntity : world.loadedTileEntityList) {
            if (tileEntity instanceof ExteriorTile && tileEntity.getPos().distanceSq(pos) <serverSoundEvent.getVolume() * 16) {

                ExteriorTile exteriorTile = (ExteriorTile) tileEntity;

                float muffleCoeffecient = exteriorTile.getOpen() == EnumDoorState.BOTH ? 1.0F :
                    (exteriorTile.getOpen() == EnumDoorState.ONE ? 0.8F : 0.6F);
                TardisHelper.getConsole(world.getServer(), exteriorTile.getInteriorDimensionKey()).ifPresent(consoleTile -> {
                    consoleTile.getDoor().ifPresent(doorEntity -> {
                        doorEntity.playSound(serverSoundEvent.getSound(), serverSoundEvent.getVolume() * muffleCoeffecient, serverSoundEvent.getPitch());
                    });
                });
            }
        }
    }

    @SubscribeEvent
    public static void attachChunkCaps(AttachCapabilitiesEvent<Chunk> event) {
        if (WorldHelper.areDimensionTypesSame(event.getObject().getWorld(), TDimensions.DimensionTypes.TARDIS_TYPE)) {
            event.addCapability(LIGHT_CAP, new ILightCap.LightProvider(new LightCapability(event.getObject())));
        }
        else event.addCapability(RIFT, new IRift.Provider(new RiftCapability(event.getObject())));
    }

    @SubscribeEvent
    public static void attachItemStackCap(AttachCapabilitiesEvent<ItemStack> event) {
        if (event.getObject().getItem() == TItems.POCKET_WATCH.get())
            event.addCapability(WATCH_CAP, new IWatch.Provider(new WatchCapability()));
        if (event.getObject().getItem() == TItems.STATTENHEIM_REMOTE.get())
            event.addCapability(REMOTE_CAP, new IRemote.Provider(new RemoteCapability(event.getObject())));
        if (event.getObject().getItem() == TItems.VORTEX_MANIP.get())
            event.addCapability(VORTEX, new IVortexCap.Provider(new VortexCapability(event.getObject(), 3)));
        if (event.getObject().getItem() instanceof TardisDiagnosticItem)
            event.addCapability(LOCATOR, new IDiagnostic.Provider(new DiagnosticToolCapability(event.getObject())));
        if (event.getObject().getItem() instanceof SonicItem) {
            event.addCapability(SONIC_CAP, new SonicProvider(new SonicCapability(event.getObject())));
        }
    }

    @SubscribeEvent
    public static void attachPlayerCap(AttachCapabilitiesEvent<Entity> event) {
        if (event.getObject() instanceof PlayerEntity)
            event.addCapability(PLAYER_DATA_CAP, new IPlayerData.Provider(new PlayerDataCapability((PlayerEntity) event.getObject())));
    }

    @SubscribeEvent
    public static void registerTrades(VillagerTradesEvent event) {

        if (event.getType() == TVillagerProfession.STORY_TELLER.get()) {
            event.getTrades().get(2).add(new ItemTrade(new ItemStack(Items.EMERALD, 15), new ItemStack(TItems.ARTIFACT_MAP.get()), 1, 10));
            event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.PAPER, 24), new ItemStack(Items.EMERALD), 3, 3));
            event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.GLASS_PANE, 10), new ItemStack(Items.EMERALD), 3, 3));

            ItemStack manual = new ItemStack(TItems.MANUAL.get());
            manual.setDisplayName(new StringTextComponent("Strange Journal"));
            event.getTrades().get(1).add(new ItemTrade(new ItemStack(Items.EMERALD, 3), manual, 5, 3));
        }


    }

    @SubscribeEvent
    public static void attachWorldCaps(AttachCapabilitiesEvent<World> event) {

        //These two should be on the client, on the client these should be, this should not be server-only
        if (WorldHelper.areDimensionTypesSame(event.getObject(), TDimensions.DimensionTypes.TARDIS_TYPE))
            event.addCapability(TARDIS_CAP, new TardisWorldProvider(event.getObject()));

        if (event.getObject().getDimensionKey() == TDimensions.SPACE_DIM) {
            event.addCapability(MISSION_CAP, new IMissionCap.Provider(new MissionWorldCapability(event.getObject())));
            event.addCapability(SPACE_DIM_CAP, new ISpaceDimProperties.Provider(new SpaceDimensionCapability(event.getObject().getDimensionKey())));
        }
        if (event.getObject().getDimensionKey() == TDimensions.MOON_DIM) {
            event.addCapability(SPACE_DIM_CAP, new ISpaceDimProperties.Provider(new SpaceDimensionCapability(event.getObject().getDimensionKey())));
        }

        //If it shouldn't be on client
        //Stop moving this 50, I'll cut you
        if (event.getObject().isRemote())
            return;

        final LazyOptional<IChunkLoader> inst = LazyOptional.of(() -> new ChunkLoaderCapability((ServerWorld) event.getObject()));
        final ICapabilitySerializable<INBT> provider = new ICapabilitySerializable<INBT>() {
            @Override
            public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
                return Capabilities.CHUNK_LOADER.orEmpty(cap, inst);
            }

            @Override
            public INBT serializeNBT() {
                return Capabilities.CHUNK_LOADER.writeNBT(inst.orElse(null), null);
            }

            @Override
            public void deserializeNBT(INBT nbt) {
                Capabilities.CHUNK_LOADER.readNBT(inst.orElse(null), null, nbt);
            }
        };
        event.addCapability(CHUNK_CAP, provider);
        event.addListener(() -> inst.invalidate());
    }


    @SubscribeEvent
    public static void onBlockBreak(BlockEvent.BreakEvent event) {
        if (event.getState().getBlock() instanceof IDontBreak) {
            event.setCanceled(true);
        }

        if (event.getWorld() instanceof World) {
            for (TileEntity te : ((World) event.getWorld()).loadedTileEntityList) {
                if (te instanceof DisguiseExteriorTile) {
                    Disguise d = ((DisguiseExteriorTile) te).disguise;
                    if (d != null) {
                        for (BlockPos pos : d.getOtherBlocks().keySet()) {
                            if (event.getPos().equals(te.getPos().add(pos))) {
                                event.setCanceled(true);
                                break;
                            }
                        }
                    }
                }
            }
        }

    }

    @SubscribeEvent
    public static void onBlockClicked(PlayerInteractEvent.RightClickBlock event) {
        if (event.getWorld().getBlockState(event.getPos()).getBlock() instanceof BellBlock) {
            ChunkPos pos = event.getWorld().getChunk(event.getEntity().getPosition()).getPos();
            for (int x = -3; x <3; ++x) {
                for (int z = -3; z <3; ++z) {
                    for (TileEntity te : event.getWorld().getChunk(pos.x + x, pos.z + z).getTileEntityMap().values()) {
                        if (te instanceof ExteriorTile || te instanceof BrokenExteriorTile) {
                            event.getWorld().playSound(null, te.getTileEntity().getPos(), TSounds.SINGLE_CLOISTER.get(), SoundCategory.BLOCKS, 5F, 0.5F);
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void onWorldTick(WorldTickEvent event) {
        //Set Phase to Start to ensure our event is only called once
        if (event.phase == Phase.START) {
            event.world.getCapability(Capabilities.MISSION).ifPresent(cap -> cap.tick(event.world));
            if (!event.world.isRemote && event.world.getGameTime() % 200 == 0 && WorldHelper.areDimensionTypesSame(event.world, TDimensions.DimensionTypes.TARDIS_TYPE)) {
                for (PlayerEntity player : event.world.getPlayers()) {
                    ChunkPos pos = new ChunkPos(player.getPosition());
                    for (int x = -3; x <3; ++x) {
                        for (int z = -3; z <3; ++z) {
                            event.world.getChunk(pos.x + x, pos.z + z).getCapability(Capabilities.LIGHT).ifPresent(cap -> cap.onLoad());
                        }
                    }
                }
            }

        }
    }

    @SubscribeEvent
    public static void onServerTick(TickEvent.ServerTickEvent event){
        MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
        if(server != null && server.isServerRunning() && event.phase == Phase.START){
            for(World world : server.getWorlds()){
                world.getCapability(Capabilities.TARDIS_DATA).ifPresent(ITardisWorldData::tick);
            }
        }
    }


    @SubscribeEvent
    public static void onEntityJoin(EntityJoinWorldEvent event) {
    	if (!event.getWorld().isRemote()) {
    		event.getWorld().getServer().enqueue(new TickDelayedTask(1, () -> {
    			if (WorldHelper.areDimensionTypesSame(event.getWorld(), TDimensions.DimensionTypes.TARDIS_TYPE) && !event.getWorld().isRemote) {

    	            //Ring alarm for mobs and those not in the ignored alarm entity tag
    	            if (event.getEntity() instanceof IMob && !event.getEntity().getType().isContained(TardisEntityTypeTags.IGNORED_ALARM_ENTITIES)) {
    	                TileEntity te = event.getWorld().getTileEntity(TardisHelper.TARDIS_POS);
    	                if (te != null && te instanceof ConsoleTile)
    	                    ((ConsoleTile) te).getInteriorManager().setAlarmOn(true);
    	                else
    	                    Tardis.LOGGER.error("Could not find Console tile in: {}", event.getWorld().getDimensionKey().getLocation().toString());
    	            }

    	            //If a player enters the TARDIS
    	            if (event.getEntity() instanceof ServerPlayerEntity) {
    	                ServerPlayerEntity player = (ServerPlayerEntity) event.getEntity();
    	                TTriggers.OBTAINED.test(player); //Trigger Advancement
    	                //The music will be played by the advancement
    	            }
    	        }

    	        //Follow into and out of TARDIS
    	        if (event.getEntity() instanceof MonsterEntity) {
    	            MonsterEntity ent = (MonsterEntity) event.getEntity();
    	            ent.goalSelector.addGoal(0, new FollowIntoTardisGoal(ent, ent.getAttribute(Attributes.MOVEMENT_SPEED).getValue()));
    	            ent.goalSelector.addGoal(1, new FollowOutOfTardisGoal(ent, ent.getAttribute(Attributes.MOVEMENT_SPEED).getValue()));
    	        }

    	        //Mission
    	        if (event.getEntity() instanceof ServerPlayerEntity && !event.getWorld().isRemote) {
    	            event.getWorld().getCapability(Capabilities.MISSION).ifPresent(misCap -> {
    	                for (MiniMission mis : misCap.getAllMissions()) {
    	                    Network.sendTo(new MissionUpdateMessage(mis), (ServerPlayerEntity) event.getEntity());
    	                }
    	            });
    	        }
    		}));   	
    	}
    }
    
    @SubscribeEvent
    public static void onPlayerChangeDimension(PlayerChangedDimensionEvent event) {
        if (event.getPlayer() instanceof ServerPlayerEntity) {
            ServerPlayerEntity player = (ServerPlayerEntity)event.getEntityLiving();
            ServerWorld toWorld = player.getEntityWorld().getServer().getWorld(event.getTo());
            ServerWorld fromWorld = player.getEntityWorld().getServer().getWorld(event.getFrom());
            if (TardisHelper.getConsoleInWorld(toWorld).isPresent()) {
            	 
                TardisHelper.getConsoleInWorld(toWorld).ifPresent(console -> {
                	if (event.getTo().getLocation().equals(console.getWorld().getDimensionKey().getLocation())) {
                		//Relocate any players to the exterior if interior is changing
                		//Only relocate player if they tried to change dimension to the tardis dimension when it is deadlocked
                        //Allows players who logged off in the deadlocked Tardis to still remain in the interior when they log back on
                		console.relocatePlayerIfExteriorDeadlocked(player, toWorld);
                		
                		//Force load the exterior chunks to allow stuff like entities and blocks to tick
                		console.forceLoadExteriorChunk(true);
                	}
                });
            }
            else {
            	if (WorldHelper.areDimensionTypesSame(fromWorld, TDimensions.DimensionTypes.TARDIS_TYPE)) {
            		 TardisHelper.getConsoleInWorld(fromWorld).ifPresent(console -> {
            			if (event.getTo().getLocation().equals(console.getCurrentDimension().getLocation())) {
            				//unload the exterior chunks if there are no players inside the tardis
            				if (console.doesConsoleWorldHaveNoPlayers()) {
                 	            console.forceLoadExteriorChunk(false);
                 	        }  
            	    	}
                     });
            	}
            }
        }
    }

    @SubscribeEvent
    public static void onLivingTick(LivingEvent.LivingUpdateEvent event) {
        LivingEntity entity = event.getEntityLiving();
        if (entity instanceof PlayerEntity) {

            entity.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> cap.tick());

            //Rifts
            if (!entity.world.isRemote && entity.world.getGameTime() % 400 == 0) {
                ChunkPos playerChunk = new ChunkPos(entity.getPosition());
                for (int x = -3; x <= 3; ++x) {
                    for (int z = -3; z <= 3; ++z) {

                        ChunkPos pos = new ChunkPos(playerChunk.x + x, playerChunk.z + z);
                        BlockPos riftPos = pos.asBlockPos();
                        IRift rift = entity.world.getChunk(pos.x, pos.z).getCapability(Capabilities.RIFT).orElse(null);

                        if (rift == null)
                            continue;


                        if (entity.getRNG().nextDouble() <0.001) {
                            rift.setRift(true);
                            Tardis.LOGGER.log(Level.DEBUG, String.format("New rift created at: %s, %s", riftPos.getX() + 8, riftPos.getZ() + 8));
                        }

                        if (rift.isRift()) {
                            rift.addEnergy(10);
                            Tardis.LOGGER.log(Level.DEBUG, String.format("Rift refueled at: %s, %s now has %s", riftPos.getX() + 8, riftPos.getZ() + 8, rift.getRiftEnergy()));
                        }

                    }
                }
            }

        }

        //Dimension stuffs
        entity.world.getCapability(Capabilities.SPACE_DIM_PROPERTIES).ifPresent(cap -> {
            //Gravity mod
            if (!(entity instanceof PlayerEntity) || (!((PlayerEntity) entity).abilities.isFlying)) {

                ItemStack boots = entity.getItemStackFromSlot(EquipmentSlotType.FEET);
                boolean hasBoots = boots.getItem() instanceof IGravArmor && ((IGravArmor) boots.getItem()).useNormalGrav(entity, boots);

                if (!hasBoots)
                    entity.setMotion(cap.modMotion(entity));
            }

            //Oxygen
            if (!cap.hasAir()) {
                if (entity.ticksExisted % 20 == 0) {

                    ItemStack helm = event.getEntityLiving().getItemStackFromSlot(EquipmentSlotType.HEAD);
                    if (helm.getItem() instanceof ISpaceHelmet)
                        if (!((ISpaceHelmet) helm.getItem()).shouldSufficate(entity))
                            return;

                    if (!MinecraftForge.EVENT_BUS.post(new SpaceAir(entity)))
                        entity.attackEntityFrom(TDamageSources.SPACE, 2);
                }
            }
        });
    }
    
    @SubscribeEvent
    public static void onSleep(PlayerSleepInBedEvent event) {
        if (WorldHelper.areDimensionTypesSame(event.getEntity().getEntityWorld(), TDimensions.DimensionTypes.TARDIS_TYPE)) {
            if (event.getEntity() instanceof ServerPlayerEntity) {
                ServerPlayerEntity player = (ServerPlayerEntity)event.getEntity();
                TardisHelper.getConsoleInWorld(player.getEntityWorld()).ifPresent(console -> {
                    if(console.hasWorld() && !console.getWorld().isRemote){
                    	//Lookup a connection that matches the player uuid
                    	PlayerTelepathicConnection connection = console.getEmotionHandler().getConnectedPlayers().get(player.getUniqueID());
                        //If this TARDIS has a connection with a valid player (eg, is online) and if that is the persion who slept
                        if(connection != null){
                            connection.onSlept(player.getServer());
                        }
                    }
                });
            }
        }
    }


    //=== This is where I will handle special cases for blocks and entities that do something other than the intended sonic result
    @SubscribeEvent
    public static void sonicOnEntity(PlayerInteractEvent.EntityInteract event) {
        if (event.getItemStack().getItem() instanceof SonicItem) {
            ItemStack stack = event.getItemStack();
            AbstractSonicMode mode = SonicItem.getCurrentMode(stack);
            mode.processSpecialEntity(event);
        }
    }

    @SubscribeEvent
    public static void sonicOnSpecialBlock(PlayerInteractEvent.RightClickBlock event) {
        if (event.getItemStack().getItem() instanceof SonicItem) {
            ItemStack stack = event.getItemStack();
            AbstractSonicMode mode = SonicItem.getCurrentMode(stack);
            mode.processSpecialBlocks(event);
        }
    }

    @SubscribeEvent
    public static void onHurt(LivingHurtEvent event) {
        if (!event.getEntityLiving().world.isRemote) {
            if (event.getEntityLiving() instanceof HumanoidMissionEntity) {
                if (event.getSource().getTrueSource() instanceof PlayerEntity) {
                    HumanoidMissionEntity missionHumanoid = (HumanoidMissionEntity)event.getEntity();
                    missionHumanoid.world.getCapability(Capabilities.MISSION).ifPresent(cap -> {
                    	if (cap.getMissionForPos(missionHumanoid.getPosition()) != null) {
                    		MiniMission mission = cap.getMissionForPos(missionHumanoid.getPosition());
                    		if (missionHumanoid.getType() == mission.getMissionHostEntity())
                    			missionHumanoid.setEmotionalState(HumanoidEmotionalState.HOSTILE);
                    	}
                    });
                }
            }
            //Temporal Grace
            if (WorldHelper.areDimensionTypesSame(event.getEntityLiving().getEntityWorld(), TDimensions.DimensionTypes.TARDIS_TYPE) && !event.getSource().canHarmInCreative()) {
                TardisHelper.getConsoleInWorld(event.getEntityLiving().world).ifPresent(tile -> {
                    tile.getSubsystem(TemporalGraceSubsystem.class).ifPresent(grace -> {
                        if (grace.canBeUsed()) {
                            grace.damage((event.getEntityLiving() instanceof ServerPlayerEntity ? (ServerPlayerEntity)event.getEntityLiving() : null), 1);
                            event.setCanceled(true);
                        }
                    });
                });
            }
        }
    }
    
    /** When living entity has already had armor and absorption hearts accounted for*/
    @SubscribeEvent
    public static void onLivingDamaged(LivingDamageEvent event) {
    	if (!event.getEntityLiving().world.isRemote()) {
    		if (event.getEntityLiving() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getEntityLiving();
                //Tardis saving. We do the logic in this event because it accounts for armor and other factors, so when we check the health its figure should be more accurate
            	//Test if damaged enough to rescue. This is currently 6 health (3 hearts)
                if(event.getEntityLiving().getHealth() <= 6.0F){
                    for (ServerWorld interior : TardisHelper.getTardises(event.getEntityLiving().getServer())) {
                        if (WorldHelper.canTravelToDimension(player.world)) {
                            ConsoleTile console = TardisHelper.getConsoleInWorld(interior).orElse(null);
                            if (console != null) {
                                console.getControl(HandbrakeControl.class).ifPresent(handBrake -> {
                                    if (!console.isInFlight() && console.getEmotionHandler().getLoyalty(player.getUniqueID())> 100 && handBrake.isFree() && !console.getInteriorManager().isInteriorStillRegenerating()) {
                                        //Check if player position is above y level 0. If we don't do this check the exterior will try to place blocks but can't and cause an infinite server crash loop.
                                    	//Check if the player is still alive before trying to go there
                                    	if (player.getPosition().getY() > 0 && player.isAlive()) {
                                            //Check the position above the player is not a solid block, because the player position is actually the block below the player legs
                                        	if (player.world.getBlockState(player.getPosition().up()).isAir()) {
                                                console.getExteriorType().remove(console);
                                                console.setCurrentLocation(player.world.getDimensionKey(), player.getPosition());
                                                console.getExteriorType().place(console, player.world.getDimensionKey(), player.getPosition());
                                                ExteriorTile ext = console.getExteriorType().getExteriorTile(console);
                                                if (ext != null) {
                                                    ext.remat(console.getSoundScheme().getLandTime());
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
    		}
    	}
    }

    /**
     * Server-side only Reload Listener Event
     * <br> On client, use DistExecutor in mod constructor
     *
     * @param event
     */
    @SubscribeEvent
    public static void addReloadListeners(AddReloadListenerEvent event) {
         
        event.addListener(ConsoleRoom.DATA_LOADER);
        event.addListener(ARSPiece.DATA_LOADER);
        event.addListener(SchematicDataListener.INSTANCE);

        event.addListener(new IFutureReloadListener() {
            @Override
            public CompletableFuture<Void> reload(IStage stage, IResourceManager resourceManager, IProfiler preparationsProfiler, IProfiler reloadProfiler, Executor backgroundExecutor, Executor gameExecutor) {
                return stage.markCompleteAwaitingOthers(Unit.INSTANCE).thenRunAsync(() -> TardisNames.read(resourceManager));
            }
        });
        
        //Disguises
        event.addListener(new IFutureReloadListener() {
            @Override
            public CompletableFuture<Void> reload(IStage stage, IResourceManager resourceManager, IProfiler preparationsProfiler, IProfiler reloadProfiler, Executor backgroundExecutor, Executor gameExecutor) {
                return stage.markCompleteAwaitingOthers(Unit.INSTANCE).thenRunAsync(() -> {
                    DisguiseRegistry.DISGUISE_REGISTRY.get().getValues().forEach(d -> {
                        Tardis.LOGGER.log(Level.DEBUG, "Adding Blocks to Disguise " + d.getRegistryName().toString());
                        d.readData(resourceManager, d.getRegistryName());
                    });
                });
            }
        });
    }

    @SubscribeEvent
    public static void useVortexM(PlayerInteractEvent.RightClickEmpty e) {
        if (e.getPlayer().getHeldItemMainhand().isEmpty()) {
            ItemStack vm = new ItemStack(TItems.VORTEX_MANIP.get());
            if (TConfig.CLIENT.openVMEmptyHand.get()) {
                if (e.getPlayer().inventory.hasItemStack(vm)) {
                    int index = PlayerHelper.findItem(e.getPlayer(), vm).getAsInt();
                    ItemStack stack = e.getPlayer().inventory.getStackInSlot(index);
                    //Mimic right click behaviour of item, fixes issue where capability cannot be found for the stack if it's not in hand
                    if (e.getWorld().isRemote) {
                        if (!e.getPlayer().getCooldownTracker().hasCooldown(stack.getItem())) {
                            ClientHelper.openGUI(TardisConstants.Gui.VORTEX_MAIN, null);
                            if (!PlayerHelper.isInEitherHand(e.getPlayer(), vm.getItem())) //Prevents stack from being opened when another stack is in the offhand
                                stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> cap.setOpen(true));
                        }
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public static void onKilled(LivingDeathEvent event) {
        //Sad Boi Noises
        if (event.getEntityLiving() instanceof PlayerEntity && event.getEntityLiving().isServerWorld()) {
            for (ServerWorld world : TardisHelper.getTardises(event.getEntityLiving().getServer())) {
                TardisHelper.getConsoleInWorld(world).ifPresent(console -> {
                    if (console.getEmotionHandler().getLoyalty(event.getEntityLiving().getUniqueID())> 10) {
                        console.playSoundAtExterior(TSounds.SINGLE_CLOISTER.get(), SoundCategory.BLOCKS, 1F, 1F);
                        console.getWorld().playSound(null, console.getPos(), TSounds.SINGLE_CLOISTER.get(), SoundCategory.BLOCKS, 1F, 1F);
                    }
                });
            }
        }
        //Mision advancement
        event.getEntityLiving().world.getCapability(Capabilities.MISSION).ifPresent(missions -> {
            MiniMission current = missions.getMissionForPos(event.getEntityLiving().getPosition());
            if (current instanceof KillMission) {
                ((KillMission) current).onKill(event.getEntityLiving());
            }
        });

        //Tardis Traits
        if (event.getEntityLiving().isServerWorld()) {
            if (event.getSource().getTrueSource() instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) event.getSource().getTrueSource();
                //Loop though all TARDISes
                for (ServerWorld world : TardisHelper.getTardises(player.getServer())) {
                    TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {

                        //If the TARDIS is aware of this player
                        if (tile.getEmotionHandler().getLoyaltyTrackingCrew().contains(player.getUniqueID())) {
                            //Call onKilled for all traits that have it
                            for (TardisTrait trait : tile.getEmotionHandler().getTraits()) {
                                if (trait instanceof AbstractEntityDeathTrait)
                                    ((AbstractEntityDeathTrait) trait).onPlayerKilledEntity(player, tile, event.getEntityLiving());
                            }
                        }
                    });
                }
            }
        }

    }

    @SubscribeEvent
    public static void onBoomBoom(ExplosionEvent.Detonate event) {
        for (BlockPos pos : event.getExplosion().getAffectedBlockPositions()) {
            TileEntity te = event.getWorld().getTileEntity(pos);
            if (te instanceof ExteriorTile) {
                ExteriorTile tile = (ExteriorTile) te;
                tile.damage(10);
                TardisEntity tardis = tile.fall();
                if (tardis != null && !tardis.isAlive()) {
                    Vector3d mot = new Vector3d(tardis.getPosX(), tardis.getPosY(), tardis.getPosZ()).subtract(event.getExplosion().getPosition()).normalize();
                    tardis.setMotion(mot);
                }
            }
        }
    }

    @SubscribeEvent
    public static void onSuffocate(LivingEvents.SpaceAir event) {

        if (event.getEntityLiving() instanceof ISpaceImmuneEntity) {
            if (!((ISpaceImmuneEntity) event.getEntityLiving()).shouldTakeSpaceDamage()) {
                event.setCanceled(true);
                return;
            }
        }

        for (TileEntity te : event.getEntityLiving().world.loadedTileEntityList) {
            te.getCapability(Capabilities.OXYGEN_SEALER).ifPresent(oxy -> {
                if (oxy.getSealedPositions().contains(event.getEntityLiving().getPosition().add(0, event.getEntityLiving().getEyeHeight(), 0)))
                    event.setCanceled(true);
            });
        }
    }

    @SubscribeEvent
    public static void onEntityStruckByLightningEvent(EntityStruckByLightningEvent event) {
        if (event.getEntity() instanceof PlayerEntity) {
            PlayerEntity playerEntity = (PlayerEntity) event.getEntity();

            ItemStack vm = new ItemStack(TItems.VORTEX_MANIP.get());
            ItemStack stack = PlayerHelper.getHeldOrNearestStack(playerEntity, vm);
            if (stack != null && !stack.isEmpty()) {
                //figure out if the battery is full enough for a trip

                stack.getCapability(Capabilities.VORTEX_MANIP).ifPresent(cap -> {
                    float totalCurrentCharge = cap.getTotalCurrentCharge();
                    //don't bother trying a random teleport if there is obviously not enough charge.
                    if (totalCurrentCharge < 50) {
                        return;
                    }

                    //get a random position around the player
                    double d3 = playerEntity.getPosXRandom(500);
                    double d4 = MathHelper.clamp(playerEntity.getPosYRandom(), 0.0D, (double) (playerEntity.world.getHeight() - 1));
                    double d5 = playerEntity.getPosZRandom(500);

                    BlockPos tpPos = new BlockPos(d3, d4, d5);

                    //then find out if the VM can afford it.
                    double diff = Math.sqrt(playerEntity.getPosition().distanceSq(tpPos)); //Calculates difference between dest and current pos
                    float artronNeeded = (float) (TConfig.SERVER.vmBaseFuelUsage.get() + (TConfig.SERVER.vmFuelUsageMultiplier.get() * diff)); //Scales discharge amount

                    if (totalCurrentCharge < artronNeeded) {
                        //can't afford the full distance
                        //find a lerp position based on percentage
                        float affordablePercentage = (totalCurrentCharge - 10) / artronNeeded;

                        tpPos = BlockPosHelper.lerp(playerEntity.getPosition(), tpPos, affordablePercentage);
                    }

                    if (playerEntity.isPassenger()) {
                        playerEntity.stopRiding();
                    }

                    playerEntity.sendStatusMessage(new TranslationTextComponent("message.vm.lightning_malfunction"), false);

                    Network.sendToServer(new VMTeleportMessage(tpPos, playerEntity.getEntityId(), false));

                    event.setCanceled(true);
                });
            }
        }
    }

    @SubscribeEvent
    public static void onEntered(TardisEnterEvent event) {
        List<TameableEntity> list = event.getEntityLiving().world.getEntitiesWithinAABB(TameableEntity.class, new AxisAlignedBB(event.getEntityLiving().getPosition()).grow(16));

        list.removeIf(ent -> ent.isQueuedToSit() || ent.getOwnerId() == null || !ent.getOwnerId().equals(event.getEntityLiving().getUniqueID()));

        if (!list.isEmpty())
            event.getExterior().transferEntities(new ArrayList<Entity>(list));

    }

    @SubscribeEvent
    public static void onLeft(TardisLeaveEvent event) {
        List<TameableEntity> list = event.getEntityLiving().world.getEntitiesWithinAABB(TameableEntity.class, new AxisAlignedBB(event.getEntityLiving().getPosition()).grow(16));
        list.removeIf(ent -> ent.isQueuedToSit() || !event.getEntityLiving().getUniqueID().equals(ent.getOwnerId()));

        event.getDoor().teleportEntities(new ArrayList<Entity>(list));

    }
    
    @SubscribeEvent
    public static void onPlayerLogin(PlayerEvent.PlayerLoggedInEvent event) {
    	PlayerEntity player = event.getPlayer();
    	if (ModList.get().isLoaded(TardisConstants.OPTIFINE_MODID)) {
    	    if (TConfig.CLIENT.displayOptifineWarning.get()) {
        	    player.sendStatusMessage(TardisConstants.Translations.OPTIFINE_WARNING, false);
            }
    	}
    }
    
    @SubscribeEvent
    public static void onPlayerLogout(PlayerEvent.PlayerLoggedOutEvent event) {
        PlayerEntity ent = event.getPlayer();
        if (ent instanceof ServerPlayerEntity) {
        	ServerPlayerEntity player = (ServerPlayerEntity)ent;
        	ServerWorld world = player.getServerWorld();
        	if (WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)) {
        	    TardisHelper.getConsoleInWorld(world).ifPresent(console -> {
        	    	console.forceLoadExteriorChunk(false);
        	    	console.forceLoadInteriorChunk(false, false);
        	    });
        	}
        }
    }

    @SubscribeEvent
    public static void registerCommands(RegisterCommandsEvent event) {
        TardisCommand.register(event.getDispatcher());
    }


    @SubscribeEvent
    public static void onServerStarting(FMLServerStartingEvent event) {
        OxygenHelper.readOrCreate();        
        TardisLikes.register();
    }
    
    @SubscribeEvent
    public static void onServerAboutToStart(FMLServerAboutToStartEvent event) {
        WorldGen.addStructuresToJigsawPools(event);
    }


    /**
     * Adds the structure's spacing for modded code made dimensions so that the structure's spacing remains
     * correct in any dimension or worldtype instead of not spawning.
     * In {@link TStructures#setupStructure(Structure, StructureSeparationSettings, boolean)} we call {@link DimensionStructuresSettings#field_236191_b_}
     * but this sometimes does not work in code made dimensions.
     */
    @SubscribeEvent(priority = EventPriority.LOWEST)
    public static void addDimensionalSpacing(final WorldEvent.Load event) {
        if (event.getWorld() instanceof ServerWorld) {
            ServerWorld serverWorld = (ServerWorld) event.getWorld();

            /* Prevent spawning our structure in Vanilla's superflat world as
             * people seem to want their superflat worlds free of modded structures.
             * Also, vanilla superflat is really tricky and buggy to work with as mentioned in TStructures#registerConfiguredStructure
             * BiomeModificationEvent does not seem to fire for superflat biomes...you can't add structures to superflat without mixin it seems.
             * */
            if (serverWorld.getChunkProvider().getChunkGenerator() instanceof FlatChunkGenerator &&
                    serverWorld.getDimensionKey().equals(World.OVERWORLD)) {
                return;
            }
            //Add Dalek Ships and Crashed Ships to the Overworld structure list
            if (serverWorld.getDimensionKey().equals(World.OVERWORLD)) {
                Map<Structure<?>, StructureSeparationSettings> tempMap = new HashMap<>(serverWorld.getChunkProvider().generator.func_235957_b_().func_236195_a_());
                tempMap.put(TStructures.Structures.CRASHED_STRUCTURE.get(), DimensionStructuresSettings.field_236191_b_.get(TStructures.Structures.CRASHED_STRUCTURE.get()));
                tempMap.put(TStructures.Structures.DALEK_SHIP.get(), DimensionStructuresSettings.field_236191_b_.get(TStructures.Structures.DALEK_SHIP.get()));
                serverWorld.getChunkProvider().generator.func_235957_b_().field_236193_d_ = tempMap;
            }
            if (serverWorld.getDimensionKey().equals(TDimensions.SPACE_DIM)) {
                Map<Structure<?>, StructureSeparationSettings> tempMap = new HashMap<>(serverWorld.getChunkProvider().generator.func_235957_b_().func_236195_a_());
                tempMap.put(TStructures.Structures.SPACE_STATION_DRONE.get(), DimensionStructuresSettings.field_236191_b_.get(TStructures.Structures.SPACE_STATION_DRONE.get()));
                tempMap.put(TStructures.Structures.ABANDONED_SPACESHIP.get(), DimensionStructuresSettings.field_236191_b_.get(TStructures.Structures.ABANDONED_SPACESHIP.get()));
                serverWorld.getChunkProvider().generator.func_235957_b_().field_236193_d_ = tempMap;
            }
        }
    }

    /**
     * Add World Gen Objects to existing vanilla biomes or other mod biomes here
     * If we want to configure things, we must use the COMMON config as everything else fires too late.
     * <p> If a feature/structure is added in a json biome added by our mod, don't add it here
     * @param event
     */
    @SubscribeEvent(priority = EventPriority.HIGH)
    public static void onBiomeLoad(BiomeLoadingEvent event) {
        RegistryKey<Biome> biomeRegistryKey = RegistryKey.getOrCreateKey(Registry.BIOME_KEY, event.getName());
        Biome.Category biomeCategory = event.getCategory();
        if (biomeCategory != Biome.Category.NETHER && biomeCategory != Biome.Category.THEEND && biomeRegistryKey != TBiomes.TARDIS_BIOME_KEY && biomeRegistryKey != TBiomes.VORTEX_BIOME_KEY) {
            if (biomeRegistryKey != TBiomes.SPACE_BIOME_KEY && biomeRegistryKey != Biomes.THE_VOID && biomeRegistryKey != TBiomes.MOON_BIOME_KEY) {
                event.getGeneration().withFeature(Decoration.UNDERGROUND_ORES, TFeatures.ConfiguredFeatures.CONFIGURED_CINNABAR_ORE);
//                Tardis.LOGGER.log(Level.DEBUG, "Added Cinnabar Ore to: " + event.getName());

                event.getGeneration().withFeature(Decoration.UNDERGROUND_ORES, TFeatures.ConfiguredFeatures.CONFIGURED_XION_CRYSTAL);
//                Tardis.LOGGER.log(Level.DEBUG, "Added Xion Crystal to: " + event.getName());

                event.getGeneration().withFeature(Decoration.RAW_GENERATION, TFeatures.ConfiguredFeatures.CONFIGURED_BROKEN_EXTERIOR);
//                Tardis.LOGGER.log(Level.DEBUG, "Added Broken Tardis Exterior to: " + event.getName());
                if (biomeCategory != Biome.Category.OCEAN) {
                    event.getGeneration().getStructures().add(() -> TStructures.ConfiguredStructures.CONFIGURED_CRASHED_STRUCTURE);
                    event.getGeneration().getStructures().add(() -> TStructures.ConfiguredStructures.CONFIGURED_DALEK_SHIP);
//                    Tardis.LOGGER.log(Level.DEBUG, "Added Dalek Ship to: " + event.getName());
//                    Tardis.LOGGER.log(Level.DEBUG, "Added Crashed Ship to: " + event.getName());
                }
            }
            else {
                if (biomeRegistryKey == TBiomes.SPACE_BIOME_KEY) {
                    event.getGeneration().getStructures().add(() -> TStructures.ConfiguredStructures.CONFIGURED_SPACE_STATION_DRONE);
//                    Tardis.LOGGER.log(Level.DEBUG, "Added Spaceship Drone to: " + event.getName());
                    event.getGeneration().getStructures().add(() -> TStructures.ConfiguredStructures.CONFIGURED_ABANDONED_SPACESHIP);
//                    Tardis.LOGGER.log(Level.DEBUG, "Added Abandoned Spaceships to: " + event.getName());
                }
                if (biomeRegistryKey == TBiomes.MOON_BIOME_KEY) {
                    event.getGeneration().getStructures().add(() -> TStructures.ConfiguredStructures.CONFIGURED_MOON_LANDER);
//                    Tardis.LOGGER.log(Level.DEBUG, "Added Moon Lander to: " + event.getName());
                }
            }
        }
    }

    @SubscribeEvent
    public static void onSpawn(LivingSpawnEvent.CheckSpawn event){
        if(event.getWorld().func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY).getKey(event.getWorld().getDimensionType()).equals(TDimensions.DimensionTypes.TARDIS_TYPE.getLocation())){
            //Deny any entities blacklisted in our config to be spawned. We don't use an entity type tag because the entity may not be initalised yet, so it cannot lookup the tag,
            //This allows for mob farms to still be functional but also allows for compat with other mods
        	if (event.getEntityLiving() != null && event.getEntityLiving().getType() != null){
        	    if(event.getSpawnReason() == SpawnReason.NATURAL || !WorldHelper.canEntitySpawnInTardis(event.getEntityLiving().getType())) {
                    event.setResult(Event.Result.DENY);
                }
            }
        }
    }

    @SubscribeEvent
    public static void onMissingBlockMapping(MissingMappings<Block> event) {
        for (MissingMappings.Mapping<Block> entry : event.getAllMappings()) {
            remappedEntries.forEach((oldName, newName) -> {
                if (entry.key.equals(oldName)) {
                    entry.remap(ForgeRegistries.BLOCKS.getValue(newName));
                }
            });
        }
    }

    @SubscribeEvent
    public static void onMissingItemMapping(MissingMappings<Item> event) {
        for (MissingMappings.Mapping<Item> entry : event.getAllMappings()) {
            remappedEntries.forEach((oldName, newName) -> {
                if (entry.key.equals(oldName)) {
                    entry.remap(ForgeRegistries.ITEMS.getValue(newName));
                }
            });
        }
    }

    @SubscribeEvent
    public static void onMissingControlMapping(MissingMappings<ControlEntry> event) {
        for (MissingMappings.Mapping<ControlEntry> entry : event.getAllMappings()) {
            remappedEntries.forEach((oldName, newName) -> {
                if (entry.key.equals(oldName)) {
                    entry.remap(ControlRegistry.CONTROL_REGISTRY.get().getValue(newName));
                }
            });
        }
    }
    
    @SubscribeEvent
    public static void onMissingExteriorMapping(MissingMappings<AbstractExterior> event) {
        for (MissingMappings.Mapping<AbstractExterior> entry : event.getAllMappings()) {
            remappedEntries.forEach((oldName, newName) -> {
                if (entry.key.equals(oldName)) {
                    entry.remap(ExteriorRegistry.EXTERIOR_REGISTRY.get().getValue(newName));
                }
            });
        }
    }

    public static void getAllMappingEntries(){
        JsonObject obj = MissingMappingsLookup.getMissingMappings();
        for (Entry<String, JsonElement> entry : obj.entrySet()) {
            remappedEntries.put(new ResourceLocation(Tardis.MODID, entry.getKey()), new ResourceLocation(Tardis.MODID, entry.getValue().getAsString()));
        }
    }
}