package net.tardis.mod.registries;

import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.misc.BrokenExteriorType;

public class BrokenExteriors {
	
	public static final DeferredRegister<BrokenExteriorType> BROKEN_EXTERIOR_TYPES = DeferredRegister.create(BrokenExteriorType.class, Tardis.MODID);

    public static Supplier<IForgeRegistry<BrokenExteriorType>> BROKEN_EXTERIOR_TYPE_REGISTRY = BROKEN_EXTERIOR_TYPES.makeRegistry("broken_exterior_type", () -> new RegistryBuilder<BrokenExteriorType>().setMaxID(Integer.MAX_VALUE - 1));
    
    public static final RegistryObject<BrokenExteriorType> WATER = BROKEN_EXTERIOR_TYPES.register("water", () -> new BrokenExteriorType(() -> ExteriorRegistry.TRUNK.get(), () -> ConsoleRegistry.GALVANIC.get(), () -> ConsoleRoom.ABANDONED_PANAMAX, () -> new ConsoleRoom[]{ConsoleRoom.PANAMAX}));
    public static final RegistryObject<BrokenExteriorType> STEAM = BROKEN_EXTERIOR_TYPES.register("steam", () -> new BrokenExteriorType(() -> ExteriorRegistry.STEAMPUNK.get(), () -> ConsoleRegistry.STEAM.get(), () -> ConsoleRoom.ABANDONED_STEAM, () -> new ConsoleRoom[]{ConsoleRoom.STEAM}));
    public static final RegistryObject<BrokenExteriorType> ALABASTER = BROKEN_EXTERIOR_TYPES.register("alabaster", () -> new BrokenExteriorType(() -> ExteriorRegistry.TT_CAPSULE.get(), () -> ConsoleRegistry.HARTNELL.get(), () -> ConsoleRoom.ABANDONED_ALABASTER, () -> new ConsoleRoom[]{ConsoleRoom.ALABASTER}));
    public static final RegistryObject<BrokenExteriorType> JADE = BROKEN_EXTERIOR_TYPES.register("jade", () -> new BrokenExteriorType(() -> ExteriorRegistry.JAPAN.get(), () -> ConsoleRegistry.XION.get(), () -> ConsoleRoom.ABANDONED_JADE, () -> new ConsoleRoom[]{ConsoleRoom.JADE}));
    public static final RegistryObject<BrokenExteriorType> NAUTILUS = BROKEN_EXTERIOR_TYPES.register("nautilus", () -> new BrokenExteriorType(() -> ExteriorRegistry.FORTUNE.get(), () -> ConsoleRegistry.NEMO.get(), () -> ConsoleRoom.ABANDONED_NAUTILUS, () -> new ConsoleRoom[]{ConsoleRoom.NAUTILUS}));
    public static final RegistryObject<BrokenExteriorType> IMPERIAL = BROKEN_EXTERIOR_TYPES.register("imperial", () -> new BrokenExteriorType(() -> ExteriorRegistry.TT_2020.get(), () -> ConsoleRegistry.POLYMEDICAL.get(), () -> ConsoleRoom.ABANDONED_IMPERIAL, () -> new ConsoleRoom[]{ConsoleRoom.IMPERIAL}));
    public static final RegistryObject<BrokenExteriorType> ENVOY = BROKEN_EXTERIOR_TYPES.register("envoy", () -> new BrokenExteriorType(() -> ExteriorRegistry.SAFE.get(), () -> ConsoleRegistry.NEUTRON.get(), () -> ConsoleRoom.ENVOY, () -> new ConsoleRoom[]{}));
    
    public static BrokenExteriorType getRandomBrokenType(Random rand) {
    	List<BrokenExteriorType> types = Lists.newArrayList(BROKEN_EXTERIOR_TYPE_REGISTRY.get().getValues());
    	return types.get(rand.nextInt(types.size()));
    }

}
