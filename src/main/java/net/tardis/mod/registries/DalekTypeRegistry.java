package net.tardis.mod.registries;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.hostile.dalek.types.DalekType;
import net.tardis.mod.entity.hostile.dalek.types.RustyType;
import net.tardis.mod.entity.hostile.dalek.types.SpecialType;
import net.tardis.mod.items.TItems;

public class DalekTypeRegistry {
	
	public static final DeferredRegister<DalekType> DALEK_TYPES = DeferredRegister.create(DalekType.class, Tardis.MODID);

    public static Supplier<IForgeRegistry<DalekType>> DALEK_TYPE_REGISTRY = DALEK_TYPES.makeRegistry("dalek_type", () -> new RegistryBuilder<DalekType>().setMaxID(Integer.MAX_VALUE - 1));

    public final static RegistryObject<DalekType> DEFAULT = DALEK_TYPES.register("dalek_default", () -> new DalekType().setSpawnItem(TItems.SPAWN_EGG_DALEK_DEFAULT.get()).addTextureVariant(new ArrayList<String>(Arrays.asList("dalek_red", "dalek_blue", "dalek_grey", "dalek_black", "dalek_redsup", "dalek_skaro"))));
    public final static RegistryObject<DalekType> SPECIAL = DALEK_TYPES.register("dalek_spec", () -> new SpecialType().setSpawnItem(TItems.SPAWN_EGG_DALEK_SPECIAL_WEAPONS.get()).addTextureVariant(new ArrayList<String>(Arrays.asList("dalek_specweap"))));
    public final static RegistryObject<DalekType> RUSTY = DALEK_TYPES.register("dalek_rusty", () -> new RustyType().setSpawnItem(TItems.SPAWN_EGG_DALEK_RUSTY.get()).addTextureVariant(new ArrayList<String>(Arrays.asList("dalek_rusty", "dalek_rusty2", "dalek_rusty3"))));


    public static DalekType getRandom(Random random) {
        DalekType[] entries = DALEK_TYPE_REGISTRY.get().getValues().toArray(new DalekType[0]);
        return entries[random.nextInt(entries.length)];
    }


}
