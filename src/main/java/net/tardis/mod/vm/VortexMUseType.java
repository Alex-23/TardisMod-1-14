package net.tardis.mod.vm;
/** Enum to tell a function which logical side it should execute its code from 
 * <br> CLIENT - Visual rendering like particles are done here
 * <br> SERVER - Game logic like health change, player teleportation
 * <br> BOTH - The function will be executing code from both sides. sendActionOnClient for Client Side, and sendActionOnServer for ServerSide
 * */
public enum VortexMUseType {
	CLIENT, SERVER, BOTH
}
