package net.tardis.mod.damagesources;

import net.minecraft.entity.LivingEntity;
import net.minecraft.util.DamageSource;

public class TDamageSources {
	
	public static final DamageSource SPACE = new DamageSource("damagesrc.tardis.space").setDamageBypassesArmor().setDamageIsAbsolute();
	public static final String DALEK = "dalek";
	public static final String DALEK_SPECIAL = "dalek_spec";
	public static final String CYBERMAN = "cyberman";
	public static final String LASER = "laser";
    public static final DamageSource LASER_SONIC = new TSource("laser_sonic").setProjectile().setDamageBypassesArmor();
	public static final DamageSource CIRCUITS = new TSource("circuits", false);
	public static final DamageSource MERCURY = new TSource("mercury").setDamageBypassesArmor();
	public static final DamageSource TARDIS_SQUASH = new TSource("tardis_squash", false).setDamageBypassesArmor();
    
	public static DamageSource causeTardisMobDamage(String name, LivingEntity mob) {
	      return new TEntitySource(name, mob, false);
	}
}
