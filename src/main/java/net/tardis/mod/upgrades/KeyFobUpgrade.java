package net.tardis.mod.upgrades;

import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class KeyFobUpgrade extends Upgrade{

	public KeyFobUpgrade(UpgradeEntry entry, ConsoleTile tile, Class<? extends Subsystem> clazz) {
		super(entry, tile, clazz);
	}

	@Override
	public void onLand() {}

	@Override
	public void onTakeoff() {}

	@Override
	public void onFlightSecond() {}

}
