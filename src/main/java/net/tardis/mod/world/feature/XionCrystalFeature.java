package net.tardis.mod.world.feature;

import com.mojang.serialization.Codec;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.tardis.mod.blocks.TBlocks;

import java.util.Random;

public class XionCrystalFeature extends Feature<ProbabilityConfig> {

    //Sets the maximum crystals that can spawn per vein
    public static final int MAX_CRYSTALS = 5;

    public XionCrystalFeature(Codec<ProbabilityConfig> codec) {
        super(codec);
    }

    @Override
    public boolean generate(ISeedReader iSeedReader, ChunkGenerator chunkGenerator, Random rand, BlockPos pos, ProbabilityConfig config) {
        //Creates new BlockPos to be sure it is not Mutable
        BlockPos newPos = pos.toImmutable();

        boolean generated = false;

        for (int i = 0; i < MAX_CRYSTALS; ++i) {
            newPos = pos.add(rand.nextInt(2), 0, rand.nextInt(2));
            for (int y = newPos.getY(); y > 0 && y < 20; --y) {
                newPos = new BlockPos(newPos.getX(), y, newPos.getZ());
                //If this block cannot see the sky
                if(!iSeedReader.canBlockSeeSky(newPos)){
                    //If this is air or water and the block under is solid
                    if ((iSeedReader.getBlockState(newPos).isAir(iSeedReader, newPos) || iSeedReader.getFluidState(newPos).isTagged(FluidTags.WATER)) && iSeedReader.getBlockState(newPos.down()).isSolid()) {

                        //See if this random chance is met and set crystal if it is
                        boolean shouldGen = rand.nextFloat() <= config.probability;
                        if (shouldGen) {
                            iSeedReader.setBlockState(newPos, TBlocks.xion_crystal.get().getDefaultState()
                                    .with(BlockStateProperties.WATERLOGGED, iSeedReader.getFluidState(newPos).isTagged(FluidTags.WATER)), 2);
                            generated = true;
                            break;
                        }
                    }
                }
            }
        }
        return generated;
    }

}
