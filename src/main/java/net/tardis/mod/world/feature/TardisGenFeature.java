package net.tardis.mod.world.feature;

import java.util.Random;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.world.WorldGen;

public class TardisGenFeature extends Feature<ProbabilityConfig> {

    public TardisGenFeature(Codec<ProbabilityConfig> codec) {
        super(codec);
    }

    @Override
    public boolean generate(ISeedReader iSeedReader, ChunkGenerator chunkGenerator, Random rand, BlockPos pos, ProbabilityConfig config) {
        //Get the dynamic registries from the world and compare the biome RegistryKey. This is because the Biome object is no longer the same object as in the code, so we can only compare it by key.
        if (iSeedReader.func_241828_r().func_230521_a_(Registry.BIOME_KEY).get().getOptionalKey(iSeedReader.getBiome(pos)).get() == Biomes.THE_VOID)
            return false;
        if (WorldHelper.canBrokenTardisSpawn(iSeedReader.getWorld().getDimensionKey())) {
            for (int y = 0; y < 30; ++y) {
                BlockPos test = new BlockPos(pos.getX(), y, pos.getZ());
                boolean water = iSeedReader.getFluidState(test).getFluidState().isTagged(FluidTags.WATER);
                //3x3x2 box which is a possible spawn area
                AxisAlignedBB spawnBox = new AxisAlignedBB(test).grow(1, 0, 1);
                //1.17: Remove world and position contexts to isAir method. Forge patched this in 1.16 but will use vanilla's isAir method which as no parameters
                //If the spawn box is all composed of air blocks
                if (BlockPos.getAllInBox(spawnBox).allMatch(testPos -> {
                    BlockState state = iSeedReader.getBlockState(testPos);
                    return state.isAir(iSeedReader, testPos);
                })) {
                    if(!iSeedReader.canBlockSeeSky(test.down()) && iSeedReader.getBlockState(test.down()).isSolid()) {
                        if (rand.nextFloat() <= config.probability) {
                            //Place down our broken exterior block
                            BlockState state = TBlocks.broken_exterior.get().getDefaultState();
                            if (water) {
                                //if the position is waterlogged, make it waterlogged.
                                state = state.with(BlockStateProperties.WATERLOGGED, true);
                            }
                            BlockPos extPos = test.up();
                            iSeedReader.setBlockState(extPos, state, 2); //Offset exterior up by one block, because exterior is rendered below its block
                            WorldGen.BROKEN_EXTERIORS.add(extPos);
//                            System.out.println("Generated Broken Tardis at: " + WorldHelper.formatBlockPosDebug(extPos));
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
