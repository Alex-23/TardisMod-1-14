package net.tardis.mod.ars;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.block.Blocks;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.items.CapabilityItemHandler;
import net.tardis.mod.items.DataCrystalItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.tags.TardisBlockTags;
import net.tardis.mod.tileentities.WaypointBankTile;

public class ARSHelper {

	
	public static List<ItemStack> fillReclamationUnitAndRemove(World world, BlockPos start, BlockPos end) {
		
		List<ItemStack> allInvs = Lists.newArrayList();
		
		BlockPos.getAllInBox(start, end).forEach(pos -> {
			TileEntity tile = world.getTileEntity(pos);

			//If this is not a tile, we can't add anything
			if(tile == null) {
				world.setBlockState(pos, Blocks.AIR.getDefaultState(), 34);
				return;
			}

			//Do not save contents from tiles whose blocks are in our block tag. Fixes an item duplication exploit like leaving items in Modded Ender Chests which use the item handler capability
			if (tile.getBlockState().isIn(TardisBlockTags.RECLAMATION_BLACKLIST)) {
			    return;
			}
			if(tile instanceof IInventory) {
				IInventory inv = (IInventory)tile;
				for(int i = 0; i < inv.getSizeInventory(); ++ i) {
					ItemStack stack = inv.getStackInSlot(i);
					if(!stack.isEmpty())
						allInvs.add(stack.copy());
				}
			}
			else if(tile != null) {
				//Handle vanilla containers and those that use item handler capability
				tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
					for(int i = 0; i < cap.getSlots(); ++i) {
						ItemStack stack = cap.getStackInSlot(i);
						if(!stack.isEmpty())
							allInvs.add(stack.copy());
					}
				});
			}
			//Waypoint saving
			if(tile instanceof WaypointBankTile) {
				List<SpaceTimeCoord> stored = Lists.newArrayList(((WaypointBankTile)tile).getWaypoints());
				stored.removeIf(coord -> coord.equals(SpaceTimeCoord.UNIVERAL_CENTER));
				
				if(!stored.isEmpty()) {
					ItemStack stack = new ItemStack(TItems.DATA_CRYSTAL.get());
					DataCrystalItem.setStoredWaypoints(stack, stored);
					allInvs.add(stack);
				}
			}

			world.setBlockState(pos, Blocks.AIR.getDefaultState(), 34);
		});
		return allInvs;
	}
}
