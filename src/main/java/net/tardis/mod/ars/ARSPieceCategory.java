package net.tardis.mod.ars;

import java.util.List;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;

public class ARSPieceCategory {

    private String category;
    private List<ARSPieceCategory> children = Lists.newArrayList();
    private List<ARSPiece> pieces = Lists.newArrayList();
    private String translationKey;
    private String modid;

    public ARSPieceCategory(String categoryName, String modid){
        this.category = categoryName;
        this.modid = modid;
    }

    public ARSPieceCategory add(String string, String modid){
        if(get(string) == null){
            ARSPieceCategory newCat = new ARSPieceCategory(string, modid);
            this.children.add(newCat);
            return newCat;
        }
        return get(string);
    }

    public ARSPieceCategory get(String name){
        for(ARSPieceCategory cat : children){
            if(cat.categoryMatches(name))
                return cat;
        }
        return null;
    }

    public boolean categoryMatches(String categoryName){
        return category.contentEquals(categoryName);
    }
    
    public String getTranslationKey() {
    	if (this.translationKey == null) {
    		this.translationKey = Util.makeTranslationKey("ars.piece.category", new ResourceLocation(this.modid, this.category));
    	}
    	return this.translationKey;
    }

    public TranslationTextComponent getTranslation(){
        return new TranslationTextComponent(this.getTranslationKey());
    }

    public boolean addPieceToList(@Nullable ARSPiece piece) {
        if(piece == null)
            return false;
        return this.pieces.add(piece);
    }

    /** Gets the ARSPieces in this category*/
    public List<ARSPiece> getPieces(){
        return this.pieces;
    }
    /** Get child categories*/
    public List<ARSPieceCategory> getChildren() {
        return this.children;
    }
}
