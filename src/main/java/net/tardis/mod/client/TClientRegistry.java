
package net.tardis.mod.client;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

import net.tardis.mod.client.renderers.tiles.*;
import org.lwjgl.glfw.GLFW;

import com.google.common.collect.Maps;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScreenManager;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.client.world.DimensionRenderInfo;
import net.minecraft.item.ItemModelsProperties;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ParticleFactoryRegisterEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DeferredWorkQueue;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.IDiagnostic;
import net.tardis.mod.cap.items.IVortexCap;
import net.tardis.mod.client.guis.containers.AlembicScreen;
import net.tardis.mod.client.guis.containers.EngineContainerScreen;
import net.tardis.mod.client.guis.containers.QuantiscopeSonicScreen;
import net.tardis.mod.client.guis.containers.QuantiscopeWeldScreen;
import net.tardis.mod.client.guis.containers.ReclamationScreen;
import net.tardis.mod.client.guis.containers.ShipComputerScreen;
import net.tardis.mod.client.guis.containers.SpectrometerScreen;
import net.tardis.mod.client.guis.containers.VMContainerScreen;
import net.tardis.mod.client.guis.containers.WaypointContainerScreen;
import net.tardis.mod.client.guis.misc.QuantiscopePage;
import net.tardis.mod.client.models.exteriors.FortuneExteriorModel;
import net.tardis.mod.client.models.exteriors.JapanExteriorModel;
import net.tardis.mod.client.models.exteriors.SafeExteriorModel;
import net.tardis.mod.client.models.exteriors.SteamExteriorModel;
import net.tardis.mod.client.models.exteriors.TT2020ExteriorModel;
import net.tardis.mod.client.models.exteriors.TTCapsuleExteriorModel;
import net.tardis.mod.client.models.exteriors.TrunkExteriorModel;
import net.tardis.mod.client.models.interiordoors.ApertureInteriorModel;
import net.tardis.mod.client.models.interiordoors.FortuneInteriorModel;
import net.tardis.mod.client.models.interiordoors.GrandfatherClockInteriorModel;
import net.tardis.mod.client.models.interiordoors.JapanInteriorModel;
import net.tardis.mod.client.models.interiordoors.ModernPoliceBoxInteriorModel;
import net.tardis.mod.client.models.interiordoors.PoliceBoxInteriorModel;
import net.tardis.mod.client.models.interiordoors.SafeInteriorModel;
import net.tardis.mod.client.models.interiordoors.SteamInteriorModel;
import net.tardis.mod.client.models.interiordoors.TT2020InteriorModel;
import net.tardis.mod.client.models.interiordoors.TTCapsuleInteriorModel;
import net.tardis.mod.client.models.interiordoors.TelephoneInteriorModel;
import net.tardis.mod.client.models.interiordoors.TrunkInteriorModel;
import net.tardis.mod.client.renderers.InvisEntityRenderer;
import net.tardis.mod.client.renderers.consoles.CoralConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.GalvanicConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.HartnellConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.KeltConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.NemoConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.NeutronConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.SteamConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.ToyotaConsoleRenderer;
import net.tardis.mod.client.renderers.consoles.XionConsoleRenderer;
import net.tardis.mod.client.renderers.entity.DalekRenderer;
import net.tardis.mod.client.renderers.entity.DefaultHumanoidRenderer;
import net.tardis.mod.client.renderers.entity.DoorRenderer;
import net.tardis.mod.client.renderers.entity.HoloPilotEntityRenderer;
import net.tardis.mod.client.renderers.entity.SecDroidRenderer;
import net.tardis.mod.client.renderers.entity.TardisBackdoorEntityRenderer;
import net.tardis.mod.client.renderers.entity.TardisDisplayEntityRenderer;
import net.tardis.mod.client.renderers.entity.transport.RenderBessie;
import net.tardis.mod.client.renderers.entity.transport.TardisEntityRenderer;
import net.tardis.mod.client.renderers.exteriors.ApertureExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.BrokenExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.BrokenExteriorRenderer.BrokenTypeRenderer;
import net.tardis.mod.client.renderers.exteriors.ClockExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.DisguiseExteriorTileRenderer;
import net.tardis.mod.client.renderers.exteriors.FortuneExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.JapanExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.ModernPoliceBoxExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.PoliceBoxExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.SafeExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.SteamExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.TT2020CapsuleExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.TTCapsuleExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.TelephoneExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.TrunkExteriorRenderer;
import net.tardis.mod.client.renderers.layers.VortexMRenderLayer;
import net.tardis.mod.client.renderers.monitor.MonitorDynamicRenderer;
import net.tardis.mod.client.renderers.monitor.MonitorRenderer;
import net.tardis.mod.client.renderers.monitor.RotateMonitorTileRenderer;
import net.tardis.mod.client.renderers.projectiles.LaserRayRenderer;
import net.tardis.mod.client.renderers.sky.EmptyCloudRenderer;
import net.tardis.mod.client.renderers.sky.EmptyRenderer;
import net.tardis.mod.client.renderers.sky.EmptyWeatherRenderer;
import net.tardis.mod.client.renderers.sky.VortexSkyRenderer;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.containers.TContainers;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.items.PocketWatchItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.sonicparts.SonicBasePart;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.misc.quantiscope.QuantiscopeScreenType;
import net.tardis.mod.particles.TParticleTypes;
import net.tardis.mod.particles.client.ArtronParticleFactory;
import net.tardis.mod.particles.client.BubbleParticle;
import net.tardis.mod.registries.BrokenExteriors;
import net.tardis.mod.tileentities.TTiles;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;
import net.tardis.mod.world.dimensions.MoonSkyProperty;
import net.tardis.mod.world.dimensions.SpaceSkyProperty;
import net.tardis.mod.world.dimensions.TDimensions;
import net.tardis.mod.world.dimensions.TardisSkyProperty;
import net.tardis.mod.world.dimensions.VortexSkyProperty;

/**
 * Replacement class for ModelRegistry
 * @author 50ap5ud5
 *
 */
@SuppressWarnings("deprecation")
@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = Tardis.MODID, bus = Bus.MOD)
public class TClientRegistry {
    
    public static EmptyRenderer EMPTY_RENDER = new EmptyRenderer();
    public static EmptyWeatherRenderer EMPTY_WEATHER_RENDER = new EmptyWeatherRenderer();
    public static EmptyCloudRenderer EMPTY_CLOUD_RENDER = new EmptyCloudRenderer();
    public static VortexSkyRenderer VORTEX_RENDER = new VortexSkyRenderer();
    
    public static HashMap<ResourceLocation, String> DIMENSION_NAMES = new HashMap<>();
    public static final EnumMap<QuantiscopeScreenType, QuantiscopePage> QUANTISCOPE_SCREENS = Maps.newEnumMap(QuantiscopeScreenType.class);
    
    public static KeyBinding SNAP_KEY;
    public static KeyBinding RELOAD_GUN;

    @SubscribeEvent
    public static void register(FMLClientSetupEvent event) {
        
        //DimensionRenderInfo
        DimensionRenderInfo.field_239208_a_.put(TDimensions.TARDIS_SKY_PROPERTY_KEY, new TardisSkyProperty());
        DimensionRenderInfo.field_239208_a_.put(TDimensions.VORTEX_SKY_PROPERTY_KEY, new VortexSkyProperty());
        DimensionRenderInfo.field_239208_a_.put(TDimensions.MOON_SKY_PROPERTY_KEY, new MoonSkyProperty());
        DimensionRenderInfo.field_239208_a_.put(TDimensions.SPACE_SKY_PROPERTY_KEY, new SpaceSkyProperty());
        
        //Make this defferred because RenderTypeLookup is not thread safe
        event.enqueueWork(() -> {
            //Block Render Layers
            RenderTypeLookup.setRenderLayer(TBlocks.console_steam.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.console_nemo.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.console_galvanic.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.console_coral.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.console_hartnell.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.console_toyota.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.console_xion.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.console_neutron.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.console_polymedical.get(), RenderType.getTranslucent());
    
            //Exteriors - TRANSLUCENT
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_clock.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_steampunk.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_trunk.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_telephone.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_police_box.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_fortune.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_modern_police_box.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_safe.get(), RenderType.getTranslucent());
    
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_tt_capsule.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_tt2020.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_japan.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_aperture.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.exterior_disguise.get(), RenderType.getTranslucent());
    
            RenderTypeLookup.setRenderLayer(TBlocks.bottom_exterior.get(), RenderType.getTranslucent());
    
            // Generic Interior Blocks - Cutout
            RenderTypeLookup.setRenderLayer(TBlocks.alabaster_wall.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.alabaster.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.alabaster_stairs.get(), RenderType.getSolid());
            RenderTypeLookup.setRenderLayer(TBlocks.alabaster_slab.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.alabaster_bookshelf.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.ebony_bookshelf.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.steam_grate.get(), RenderType.getCutoutMipped());
    
            //Roundels - Cutout
            //Wood
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_alabaster.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_offset_alabaster.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_oak_planks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_oak_planks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_spruce_planks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_spruce_planks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_birch_planks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_birch_planks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_jungle_planks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_jungle_planks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_acacia_planks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_acacia_planks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_dark_oak_planks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_dark_oak_planks_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_oak_stripped_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_oak_stripped_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_spruce_stripped_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_spruce_stripped_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_birch_stripped_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_birch_stripped_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_jungle_stripped_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_jungle_stripped_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_acacia_stripped_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_acacia_stripped_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_dark_oak_stripped_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_dark_oak_stripped_offset.get(), RenderType.getCutout());
            
            //Concrete
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_white_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_white_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_orange_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_orange_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_magenta_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_magenta_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_light_blue_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_light_blue_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_yellow_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_yellow_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_lime_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_lime_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_pink_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_pink_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_gray_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_gray_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_light_gray_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_light_gray_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_cyan_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_cyan_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_purple_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_purple_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_blue_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_blue_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_brown_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_brown_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_green_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_green_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_red_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_red_concrete_offset.get(), RenderType.getCutout());
            
            //Terracotta
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_black_concrete_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_black_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_white_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_white_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_orange_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_orange_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_magenta_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_magenta_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_light_blue_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_light_blue_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_yellow_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_yellow_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_lime_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_lime_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_pink_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_pink_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_gray_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_gray_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_light_gray_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_light_gray_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_cyan_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_cyan_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_purple_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_purple_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_blue_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_blue_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_brown_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_brown_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_green_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_green_terracotta_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_red_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_red_concrete_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_black_terracotta_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_black_terracotta_offset.get(), RenderType.getCutout());
            
            //Stone
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_stone_bricks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_stone_bricks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_smooth_stone_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_smooth_stone_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_andesite_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_andesite_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_granite_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_granite_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_diorite_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_diorite_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_cut_red_sandstone_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_cut_red_sandstone_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_cut_sandstone_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_cut_sandstone_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_sandstone_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_sandstone_offset.get(), RenderType.getCutout());
            
            //End
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_end_stone_bricks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_end_stone_bricks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_end_stone_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_end_stone_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_purpur_bricks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_purpur_bricks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_purpur_pillar_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_purpur_pillar_offset.get(), RenderType.getCutout());
            
            //Nether
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_nether_bricks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_nether_bricks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_nether_wart_block_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_nether_wart_block_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_red_nether_bricks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_red_nether_bricks_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_gilded_blackstone_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_gilded_blackstone_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_basalt_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_basalt_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_blackstone_bricks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_blackstone_bricks_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_blackstone_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_polished_blackstone_offset.get(), RenderType.getCutout());
            
            //Prismarine
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_prismarine_bricks_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_prismarine_bricks_offset.get(), RenderType.getCutout());
            
            //Bone
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_bone_block_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_bone_block_offset.get(), RenderType.getCutout());
            
            
            //Quartz
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_quartz.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_quartz_pillar_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_quartz_pillar_offset.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_chiseled_quartz_full.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_chiseled_quartz_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_coralised.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_divider.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_coralised_large.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_coralised_small.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_tech_wall_lamp.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_tech_wall_lamp_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_blue_runner_lights.get(), RenderType.getCutout());
    
    
            // Tech - Stairs, Slabs - CUTOUT
            RenderTypeLookup.setRenderLayer(TBlocks.tech_strut.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_plate_slab.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_pipes_slab.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_pattern_slab.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_slab.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_plate_stairs.get(), RenderType.getSolid());
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_pipes_stairs.get(), RenderType.getSolid());
    
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_pattern_stairs.get(), RenderType.getSolid());
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_stairs.get(), RenderType.getSolid());
    
            RenderTypeLookup.setRenderLayer(TBlocks.holo_ladders.get(), RenderType.getCutout());
    
            //Grates - Blocks, Slabs, Stairs - CUTOUT
            RenderTypeLookup.setRenderLayer(TBlocks.metal_grate.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.metal_grate_slab.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.metal_grate_stairs.get(), RenderType.getSolid());
            RenderTypeLookup.setRenderLayer(TBlocks.metal_grate_trapdoor.get(), RenderType.getCutout());
    
            // Whitaker Interior - CUTOUT
            RenderTypeLookup.setRenderLayer(TBlocks.steel_grate_solid.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.steel_grate_solid_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.steel_grate_frame.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.steel_grate_frame_offset.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.blue_concrete_slab.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.gallifreyan_yellow_slab.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.yellow_crystal.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.yellow_crystal_slab.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.yellow_crystal_stairs.get(), RenderType.getTranslucent());
    
    
            // Toyota Interior - CUTOUT
            RenderTypeLookup.setRenderLayer(TBlocks.blue_pillar.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.foam_pipes.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.three_point_lamp.get(), RenderType.getCutout());
    
            //Machines - Maintenance ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.alembic.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.engine.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.ars_egg.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.quantiscope_brass.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.quantiscope_iron.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.atrium_block.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.anti_grav.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.reclamation_unit.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.roundel_tap.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.corridor_spawn.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.corridor_kill.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.waypoint_bank.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.landing_pad.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.transduction_barrier.get(), RenderType.getCutout());
            
            RenderTypeLookup.setRenderLayer(TBlocks.ARTRON_PYLON.get(), RenderType.getTranslucent());
            RenderTypeLookup.setRenderLayer(TBlocks.ARTRON_COLLECTOR.get(), RenderType.getTranslucent());
            
            //Future ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.ship_computer.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.psychic_cube.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.oxygen_sealer.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.air_lock.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.item_access_panel.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.wirebox.get(), RenderType.getCutoutMipped());
    
            //Monitors - Main ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.steampunk_monitor.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.eye_monitor.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.rca_monitor.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.rotate_monitor.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.dynamic_monitor.get(), RenderType.getCutout());
    
            //Resources - Future ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.cinnabar_ore.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.xion_crystal.get(), RenderType.getCutoutMipped());
    
            // Misc - Future ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_pattern.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_pipes.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_plate.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten_plate_small.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.tungsten.get(), RenderType.getCutout());
    
            //Misc - Main ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.broken_exterior.get(), RenderType.getCutout());
    
            //Decoration - Future ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.clutter_books.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.clutter_paper.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.wire_hang.get(), RenderType.getCutoutMipped());
            RenderTypeLookup.setRenderLayer(TBlocks.comfy_chair.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.comfy_chair_green.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.crash_chair.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.crash_chair_wood.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.crash_chair_red.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.telescope.get(), RenderType.getCutoutMipped());
            RenderTypeLookup.setRenderLayer(TBlocks.blinking_lights.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.metal_grate_stairs.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.spinny_block.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.plaque.get(), RenderType.getSolid());
    
    
            RenderTypeLookup.setRenderLayer(TBlocks.squareness_block.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.chemlight.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.chemlamp.get(), RenderType.getCutout());
    
    
            //Moon - Future ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.regolith_sand.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.regolith_packed.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.moon_base_glass.get(), RenderType.getCutout());
    
            RenderTypeLookup.setRenderLayer(TBlocks.multiblock.get(), RenderType.getCutout());
            RenderTypeLookup.setRenderLayer(TBlocks.multiblock_master.get(), RenderType.getCutout());
    
            //Debug use only - No ItemGroup
            RenderTypeLookup.setRenderLayer(TBlocks.structure_block.get(), RenderType.getCutout());
        });
        
        registerTileRenderers();
        registerEntityRenderers();
        registerKeyBindings();
        registerItemProperties();
        registerScreens();
        
        registerInteriorDoorRenderers();
        registerBrokenExteriorRenderers();
        
        
        MonitorView.NORTH_WEST.setAngle(() -> 180F);
        MonitorView.SOUTH_EAST.setAngle(() -> 22.5F);
        MonitorView.PANORAMIC.setAngle(() -> {
            return ClientHelper.getClientWorld().getGameTime() * 0.75F % 360.0F;
        });
        
        if(!Minecraft.getInstance().getFramebuffer().isStencilEnabled())
            DeferredWorkQueue.runLater(() -> Minecraft.getInstance().getFramebuffer().enableStencil());

    }
    
    private static void registerTileRenderers() {
    	//Consoles
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_CORAL.get(), CoralConsoleRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_GALVANIC.get(), GalvanicConsoleRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_HARTNEL.get(), HartnellConsoleRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_POLYMEDICAL.get(), KeltConsoleRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_NEMO.get(), NemoConsoleRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_NEUTRON.get(), NeutronConsoleRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_STEAM.get(), SteamConsoleRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_TOYOTA.get(), ToyotaConsoleRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.CONSOLE_XION.get(), XionConsoleRenderer::new);
        
        //Exteriors
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_CLOCK.get(), ClockExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_STEAMPUNK.get(), SteamExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_TRUNK.get(), TrunkExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_TELEPHONE.get(), TelephoneExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_POLICE_BOX.get(), PoliceBoxExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_FORTUNE.get(), FortuneExteriorRenderer::new );
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_MODERN_POLICE_BOX.get(), ModernPoliceBoxExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_SAFE.get(), SafeExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_TT_CAPSULE.get(), TTCapsuleExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_TT2020_CAPSULE.get(), TT2020CapsuleExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_JAPAN.get(), JapanExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_APERTURE.get(), ApertureExteriorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.EXTERIOR_DISGUISE.get(), DisguiseExteriorTileRenderer::new);
        
        ClientRegistry.bindTileEntityRenderer(TTiles.BROKEN_TARDIS.get(), BrokenExteriorRenderer::new);
        
        //Monitors
        ClientRegistry.bindTileEntityRenderer(TTiles.STEAMPUNK_MONITOR.get(), MonitorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.ROTATE_MONITOR.get(), RotateMonitorTileRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.DYNAMIC_MONITOR.get(), MonitorDynamicRenderer::new);
        
        //Other Tiles
        ClientRegistry.bindTileEntityRenderer(TTiles.ENGINE.get(), TardisEngineRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.TOYOTA_SPIN.get(), ToyotaSpinnyTileRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.SUPER_STRUCTURE.get(), SuperStructureTileRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.ARTRON_COLLECTOR.get(), ArtronCollectorRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.ANTI_GRAV.get(), AntiGravTileRenderer::new);
        ClientRegistry.bindTileEntityRenderer(TTiles.SPECTROMETER.get(), SpectrometerRenderer::new);
        
    }
    
    private static void registerEntityRenderers() {
    	//Entities
        RenderingRegistry.registerEntityRenderingHandler(TEntities.CONTROL.get(), InvisEntityRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.DOOR.get(), DoorRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.CHAIR.get(), InvisEntityRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.TARDIS.get(), TardisEntityRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.HOLO_PILOT.get(), HoloPilotEntityRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.DISPLAY_TARDIS.get(), TardisDisplayEntityRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.TARDIS_BACKDOOR.get(), TardisBackdoorEntityRenderer::new);

        //Mobs
        RenderingRegistry.registerEntityRenderingHandler(TEntities.LASER.get(), LaserRayRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.SECURITY_DROID.get(), SecDroidRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.DALEK.get(), DalekRenderer::new);

        //Humans
        RenderingRegistry.registerEntityRenderingHandler(TEntities.CREWMATE.get(), DefaultHumanoidRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.SHIP_CAPTAIN.get(), DefaultHumanoidRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(TEntities.COMPANION.get(), DefaultHumanoidRenderer::new);
        //Decorations
        ClientRegistry.bindTileEntityRenderer(TTiles.PLAQUE.get(), PlaqueRenderer::new);

        RenderingRegistry.registerEntityRenderingHandler(TEntities.BESSIE.get(), RenderBessie::new);
        
        //Layer renderers
        Map<String, PlayerRenderer> skinMap = Minecraft.getInstance().getRenderManager().getSkinMap();
        for (PlayerRenderer renderPlayer : skinMap.values()) {
            renderPlayer.addLayer(new VortexMRenderLayer(renderPlayer));
        }
    }
    
    private static void registerBrokenExteriorRenderers() {
    	BrokenExteriorRenderer.registerBrokenRenderer(BrokenExteriors.WATER.get(),
                new BrokenTypeRenderer(() -> TrunkExteriorRenderer.TEXTURE, new TrunkExteriorModel()));
        
        BrokenExteriorRenderer.registerBrokenRenderer(BrokenExteriors.STEAM.get(),
                new BrokenTypeRenderer(() -> SteamExteriorRenderer.TEXTURE, new SteamExteriorModel()));
        
        BrokenExteriorRenderer.registerBrokenRenderer(BrokenExteriors.ALABASTER.get(),
                new BrokenTypeRenderer(() -> TTCapsuleExteriorRenderer.TEXTURE, new TTCapsuleExteriorModel()));
        
        BrokenExteriorRenderer.registerBrokenRenderer(BrokenExteriors.ENVOY.get(),
                new BrokenTypeRenderer(() -> SafeExteriorRenderer.TEXTURE, new SafeExteriorModel()));
        
        BrokenExteriorRenderer.registerBrokenRenderer(BrokenExteriors.IMPERIAL.get(),
                new BrokenTypeRenderer(() -> TT2020CapsuleExteriorRenderer.TEXTURE, new TT2020ExteriorModel()));
        
        BrokenExteriorRenderer.registerBrokenRenderer(BrokenExteriors.JADE.get(),
                new BrokenTypeRenderer(() -> JapanExteriorRenderer.TEXTURE, new JapanExteriorModel()));
        
        BrokenExteriorRenderer.registerBrokenRenderer(BrokenExteriors.NAUTILUS.get(),
                new BrokenTypeRenderer(() -> FortuneExteriorRenderer.TEXTURE, new FortuneExteriorModel()));
    }
    
    private static void registerInteriorDoorRenderers() {
    	EnumDoorType.STEAM.setInteriorDoorModel(new SteamInteriorModel());
        EnumDoorType.TRUNK.setInteriorDoorModel(new TrunkInteriorModel());
        EnumDoorType.TELEPHONE.setInteriorDoorModel(new TelephoneInteriorModel());
        EnumDoorType.POLICE_BOX.setInteriorDoorModel(new PoliceBoxInteriorModel());
        EnumDoorType.FORTUNE.setInteriorDoorModel(new FortuneInteriorModel());
        EnumDoorType.MODERN_POLICE_BOX.setInteriorDoorModel(new ModernPoliceBoxInteriorModel());
        EnumDoorType.SAFE.setInteriorDoorModel(new SafeInteriorModel());
        EnumDoorType.TT_CAPSULE.setInteriorDoorModel(new TTCapsuleInteriorModel());
        EnumDoorType.CLOCK.setInteriorDoorModel(new GrandfatherClockInteriorModel());
        EnumDoorType.TT_2020_CAPSULE.setInteriorDoorModel(new TT2020InteriorModel());
        EnumDoorType.JAPAN.setInteriorDoorModel(new JapanInteriorModel());
        EnumDoorType.APERTURE.setInteriorDoorModel(new ApertureInteriorModel());
    }
    
    private static void registerKeyBindings() {
        ClientRegistry.registerKeyBinding(SNAP_KEY = new KeyBinding("key.tardis.snap", GLFW.GLFW_KEY_H, TardisConstants.Strings.KEYBINDING_CATEGORY));
        
        ClientRegistry.registerKeyBinding(RELOAD_GUN = new KeyBinding("key.tardis.reload_laser_gun", GLFW.GLFW_KEY_LEFT_ALT, TardisConstants.Strings.KEYBINDING_CATEGORY));

    }
    
    private static void registerItemProperties() {
        //ItemPropertyOverrides
        
        ItemModelsProperties.registerProperty(TItems.POCKET_WATCH.get(), new ResourceLocation(Tardis.MODID, "rot"), (stack,world,entity) -> {
            PocketWatchItem watch = (PocketWatchItem) stack.getItem();
            return watch.createPropertyOverride(stack, world, entity);
        });
        
        ItemModelsProperties.registerProperty(TItems.VORTEX_MANIP.get(), new ResourceLocation("open"), (stack, worldIn, entityIn) -> {
            IVortexCap cap = stack.getCapability(Capabilities.VORTEX_MANIP).orElse(null);
            if (cap == null)
                return 0F;
            return cap.getOpen() ? 1F : 0F;
         });
        
        ItemModelsProperties.registerProperty(TItems.DIAGNOSTIC_TOOL.get(), new ResourceLocation(Tardis.MODID, "on"), (stack, worldIn, entityIn) -> {

            IDiagnostic cap = stack.getCapability(Capabilities.DIAGNOSTIC).orElse(null);
            if (cap == null)
                return 0F;
            return cap.getOn() ? 1F : 0F;
        });
        
        ItemModelsProperties.registerProperty(TItems.SONIC_ACTIVATOR.get(), new ResourceLocation("sonic_type"), (stack, worldIn, entityIn) -> SonicBasePart.getType(stack));
        ItemModelsProperties.registerProperty(TItems.SONIC_EMITTER.get(), new ResourceLocation("sonic_type"), (stack, worldIn, entityIn) -> SonicBasePart.getType(stack));
        ItemModelsProperties.registerProperty(TItems.SONIC_END.get(), new ResourceLocation("sonic_type"), (stack, worldIn, entityIn) -> SonicBasePart.getType(stack));
        ItemModelsProperties.registerProperty(TItems.SONIC_HANDLE.get(), new ResourceLocation("sonic_type"), (stack, worldIn, entityIn) -> SonicBasePart.getType(stack));
        
        ItemModelsProperties.registerProperty(TItems.EARTHSHOCK_GUN.get(), new ResourceLocation(Tardis.MODID, "aimed"), (stack, worldIn, livingEntity) -> {
            if (livingEntity == null) {
                return 0F;
            }
            return livingEntity.getItemInUseCount() > 0 ? 1F : 0F;
        });
    }

    public static void registerScreens(){
        ScreenManager.registerFactory(TContainers.QUANTISCOPE.get(), QuantiscopeSonicScreen::new);
        ScreenManager.registerFactory(TContainers.QUANTISCOPE_WELD.get(), QuantiscopeWeldScreen::new);
        ScreenManager.registerFactory(TContainers.ALEMBIC.get(), AlembicScreen::new);
        ScreenManager.registerFactory(TContainers.RECLAMATION_UNIT.get(), ReclamationScreen::new);
        ScreenManager.registerFactory(TContainers.VORTEX_M_BATTERY.get(), VMContainerScreen::new);
        ScreenManager.registerFactory(TContainers.SHIP_COMPUTER.get(), ShipComputerScreen::new);
        ScreenManager.registerFactory(TContainers.ENGINE.get(), EngineContainerScreen::new);
        ScreenManager.registerFactory(TContainers.WAYPOINT.get(), WaypointContainerScreen::new);
        ScreenManager.registerFactory(TContainers.SPECTROMETER.get(), SpectrometerScreen::new);
    }
    
    @SubscribeEvent
    public static void registerParticles(ParticleFactoryRegisterEvent event) {
         Minecraft.getInstance().particles.registerFactory(TParticleTypes.ARTRON.get(), ArtronParticleFactory::new);
         Minecraft.getInstance().particles.registerFactory(TParticleTypes.BUBBLE.get(), BubbleParticle.Factory::new);
    }
}
