package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.culling.ClippingHelper;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TRenderHelper;
import net.tardis.mod.items.TItems;

public class InvisEntityRenderer extends EntityRenderer<Entity> {

	public InvisEntityRenderer(EntityRendererManager rendererManager) {
        super(rendererManager);
    }

    @Override
	protected boolean canRenderName(Entity entity) {
		return true;
	}

	@Override
	public void render(Entity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int packedLightIn) {
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		if(entity instanceof ControlEntity && ((ControlEntity)entity).getControl() != null) {
			if(((ControlEntity) entity).getControl().isGlowing() || Minecraft.getInstance().player.getHeldItemMainhand().getItem() == TItems.DEBUG.get()) {
				IVertexBuilder builder = bufferIn.getBuffer(RenderType.getLines());
				WorldRenderer.drawBoundingBox(matrixStackIn, builder, entity.getBoundingBox().offset(entity.getPositionVec().scale(-1)), 0.22F, 0.86F, 0.84F, 1F);
			}
		}
	}

	//Comment out the if check to view control hitboxes whilst in debug mode
    @Override
	public boolean shouldRender(Entity entity, ClippingHelper camera, double camX, double camY, double camZ) {
		return super.shouldRender(entity, camera, camX, camY, camZ);
	}

	@Override
	protected void renderName(Entity entity, ITextComponent displayNameIn, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int packedLightIn) {
		matrixStackIn.push();
		if(entity instanceof ControlEntity &&
        		Minecraft.getInstance().objectMouseOver instanceof EntityRayTraceResult && PlayerHelper.isInEitherHand(Minecraft.getInstance().player, TItems.MANUAL.get()))
        	if(((EntityRayTraceResult)Minecraft.getInstance().objectMouseOver).getEntity() == entity) {
        		matrixStackIn.translate(0, -0.2F, 0);
        		super.renderName(entity, entity.getDisplayName(), matrixStackIn, bufferIn, packedLightIn);
        	}
		matrixStackIn.pop();
	}

	@Override
	public ResourceLocation getEntityTexture(Entity entity) {
		return null;
	}

}
