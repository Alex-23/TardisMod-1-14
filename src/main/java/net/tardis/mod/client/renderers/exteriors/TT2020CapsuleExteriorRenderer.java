package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.models.exteriors.TT2020ExteriorModel;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.TT2020ExteriorTile;

public class TT2020CapsuleExteriorRenderer extends ExteriorRenderer<TT2020ExteriorTile> {

    public TT2020CapsuleExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/tt2020.png");
    public static final TT2020ExteriorModel MODEL = new TT2020ExteriorModel();
    public static final WorldText TEXT = new WorldText(1, 1, 1, TextFormatting.DARK_AQUA.getColor());
    
    @Override
    public void renderExterior(TT2020ExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
        matrixStackIn.push();
        matrixStackIn.translate(0, -1, 0);
        translateFloating(matrixStackIn);
        
        MODEL.render(tile, 1.0F, matrixStackIn, bufferIn.getBuffer(TRenderTypes.getTardis(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
        matrixStackIn.push();
        
        //nameplate
        long time = Minecraft.getInstance().world.getGameTime();
        matrixStackIn.rotate(Vector3f.YP.rotationDegrees((float) (time % 360.0)));
        matrixStackIn.translate(-0.5, -1.5, -1.2);
        matrixStackIn.scale(1F, (time % 120 < 20) ? (float) Math.cos(time % 20 * 0.1) : 1F, 1F);
        TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
        matrixStackIn.pop();
        
        matrixStackIn.pop();
    }
    
    @Override
    public boolean floatInAir() {
        return true;
    }

    public static void translateFloating(MatrixStack matrix){
        matrix.translate(0, Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.25, 0);
    }

}
