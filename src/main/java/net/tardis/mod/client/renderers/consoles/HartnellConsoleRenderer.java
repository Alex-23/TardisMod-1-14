package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.HartnellConsoleModel;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;

public class HartnellConsoleRenderer extends TileEntityRenderer<HartnellConsoleTile> {
    public static final HartnellConsoleModel MODEL = new HartnellConsoleModel();
    
    public HartnellConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
        super(rendererDispatcherIn);
    }
    
    
    @Override
    public void render(HartnellConsoleTile console, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrixStackIn.push();
        matrixStackIn.translate(0.5, 0.8, 0.5);
        matrixStackIn.rotate(Vector3f.ZP.rotationDegrees(180));
        ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/hartnell.png");
        MODEL.render(console, 0.5F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
        
        //Sonic
        matrixStackIn.push();
        matrixStackIn.translate(-0.7, -0.15, 0.16);
        matrixStackIn.rotate(Vector3f.XP.rotationDegrees(15));
        matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(15));
        matrixStackIn.scale(0.3F, 0.3F, 0.3F);
        Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();
        
        matrixStackIn.pop();
    }
    
    

}
