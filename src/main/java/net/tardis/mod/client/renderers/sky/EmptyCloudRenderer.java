package net.tardis.mod.client.renderers.sky;


import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.ICloudRenderHandler;

@OnlyIn(Dist.CLIENT)
public class EmptyCloudRenderer implements ICloudRenderHandler{

    @Override
    public void render(int ticks, float partialTicks, MatrixStack matrixStack, ClientWorld world, Minecraft mc,
            double viewEntityX, double viewEntityY, double viewEntityZ) {
        
    }


}
