package net.tardis.mod.client.renderers.boti;

import java.util.List;
import java.util.Map.Entry;

import net.tardis.mod.boti.BotiPlayer;
import net.tardis.mod.boti.stores.EntityStorage;
import net.tardis.mod.boti.stores.PlayerEntityStorage;
import org.apache.logging.log4j.Level;
import org.lwjgl.opengl.GL11;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.FogRenderer;
import net.minecraft.client.renderer.FogRenderer.FogType;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.IRenderTypeBuffer.Impl;
import net.minecraft.client.renderer.LightTexture;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.LightType;
import net.minecraftforge.client.model.data.EmptyModelData;
import net.minecraftforge.client.model.data.IModelData;
import net.tardis.mod.Tardis;
import net.tardis.mod.boti.BotiWorld;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.boti.stores.BlockStore;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.TranslatingVertexBuilder;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.BiFunctionVoid;

public class BOTIRenderer {

    public static final BotiManager BOTI = new BotiManager();
    public static final List<PortalInfo> PORTALS = Lists.newArrayList();
    public static final BotiVBO BOTI_VBO = new BotiVBO();
    public static final BotiTileBuffers BOTI_TILE_BUFFERS = new BotiTileBuffers();
    public static BotiWorldRenderer worldRenderer;
    private static Minecraft mc = Minecraft.getInstance();
    private static final BufferBuilder ENTITY_BUFFER = new BufferBuilder(DefaultVertexFormats.ENTITY.getIntegerSize());

    public static void addPortal(PortalInfo info) {
        PORTALS.add(info);
    }
    
    public static void renderBOTI(MatrixStack ms, PortalInfo info, float partialTicks) {

        if(info.getWorldShell() == null)
            return;

        if (TConfig.CLIENT.enableBoti.get()) {
            ms.push();
            
            mc.getFramebuffer().unbindFramebuffer();

            BOTI.setupFramebuffer();

            Vector3d skyColor = mc.world.getSkyColor(mc.player.getPosition(), mc.getRenderPartialTicks());
            BOTI.fbo.setFramebufferColor((float)skyColor.x, (float)skyColor.y, (float)skyColor.z, 0);

            //Render World
            ms.push();

            info.translatePortal(ms);

            //GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT | GL11.GL_COLOR_BUFFER_BIT);
            GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);

            ClientWorld oldWorld = ClientHelper.getClientWorldCasted();
            if(info.getWorldShell().getWorld() != null) {
                info.getWorldShell().getWorld().setShell(info.getWorldShell());
                Minecraft.getInstance().world = info.getWorldShell().getWorld();
            }
            
            FogRenderer.setupFog(mc.getRenderManager().info, FogType.FOG_TERRAIN, 80, false);
    
//            PlayerEntity player = mc.player;
    
            //verifyAndGetRenderer(info.getWorldShell().getWorld()).renderSky(ms, Minecraft.getInstance().getRenderPartialTicks());
//            mc.worldRenderer.renderClouds(ms, Minecraft.getInstance().getRenderPartialTicks(),0, 100, 0);

            renderWorld(ms, info.getWorldShell(), LightModelRenderer.MAX_LIGHT, OverlayTexture.NO_OVERLAY, partialTicks);
            Minecraft.getInstance().world = oldWorld;
            FogRenderer.resetFog();
            ms.pop();

            BufferBuilder underlyingBuffer = Tessellator.getInstance().getBuffer();
            Impl imBuffer = IRenderTypeBuffer.getImpl(underlyingBuffer);

            info.renderDoor(ms, imBuffer);
            imBuffer.finish();
            Minecraft.getInstance().getFramebuffer().bindFramebuffer(false);
    
            setupStencil(info::renderPortal, ms, imBuffer);
            BOTI.fbo.framebufferRenderExt(Minecraft.getInstance().getMainWindow().getFramebufferWidth(), Minecraft.getInstance().getMainWindow().getFramebufferHeight(), true);
            endStencil(info::renderPortal, ms, imBuffer);

            BOTI.endFBO();

            ms.pop();
        }

    }

    public static BotiWorldRenderer verifyAndGetRenderer(BotiWorld botiWorld){
        if(worldRenderer == null) {
            worldRenderer = new BotiWorldRenderer(Minecraft.getInstance(), Minecraft.getInstance().getRenderTypeBuffers());
            worldRenderer.setDimensionType(botiWorld.getDimensionType(), botiWorld);
            return worldRenderer;
        }

        if(worldRenderer.getDimensionType() != botiWorld.getDimensionType())
            worldRenderer.setDimensionType(botiWorld.getDimensionType(), botiWorld);
        return worldRenderer;
    }

    public static void setupStencil(BiFunctionVoid< MatrixStack, IRenderTypeBuffer.Impl > drawPortal, MatrixStack stack, IRenderTypeBuffer.Impl buffer) {
        GL11.glEnable(GL11.GL_STENCIL_TEST);

        // Always write to stencil buffer
        GL11.glStencilFunc(GL11.GL_ALWAYS, 1, 0xFF);
        GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_REPLACE);
        GL11.glStencilMask(0xFF);
        GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);

        RenderSystem.depthMask(false);
        drawPortal.run(stack, buffer);
        buffer.finish();
        RenderSystem.depthMask(true);

        // Only pass stencil test if equal to 1(So only if rendered before)
        GL11.glStencilMask(0x00);
        GL11.glStencilFunc(GL11.GL_EQUAL, 1, 0xFF);

    }

    public static void endStencil(BiFunctionVoid< MatrixStack, IRenderTypeBuffer.Impl > drawPortal, MatrixStack stack, IRenderTypeBuffer.Impl buffer) {
        GL11.glDisable(GL11.GL_STENCIL_TEST);
        GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);

        GL11.glColorMask(false, false, false, false);
        RenderSystem.depthMask(false);
        drawPortal.run(stack, buffer);
        buffer.finish();

        //Set things back
        RenderSystem.depthMask(true);
        GL11.glColorMask(true, true, true, true);
    }

    @SuppressWarnings("deprecation")
    public static void renderWorld(MatrixStack matrixStack, WorldShell shell, int combinedLight, int combinedOverlay, float partialTicks) {
        matrixStack.push();
        
        mc.textureManager.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);

        BlockPos offset = shell.getOffset();
        
        //Translate the inverse of the offset position because the portal faces in the opposite direction to the viewer
        //This translation will allow blocks to be rendered at the correct offsets
        matrixStack.translate(-offset.getX(), -offset.getY(), -offset.getZ());
        
        /** Blocks Start */
        if(shell.needsUpdate()){
            shell.setNeedsUpdate(false);

            for(RenderType type : RenderType.getBlockRenderTypes()){
                BOTI_VBO.begin(type);
                type.setupRenderState();
                BOTI_VBO.resetData(type);
                BufferBuilder builder = BOTI_VBO.getBufferBuilder(type);
                
                //Validate if the buffer is building. Required to prevent crashing
                if (!builder.isDrawing())
                    builder.begin(GL11.GL_QUADS, BOTI_VBO.format);
                
                boolean hasRenderedInLayer = false;

                MatrixStack testStack = new MatrixStack();

                for(Entry<BlockPos, BlockStore> entry : shell.getBlockMap().entrySet()){
                    testStack.push();
                    testStack.translate(entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ());
                    
                    //If we're rendering normal blockstates that are not fluids
                    if(entry.getValue().getFluidState().isEmpty()){
                        if (RenderTypeLookup.canRenderInLayer(entry.getValue().getState(), type)) {
                            if (Helper.canRenderInBOTI(entry.getValue().getState())) {
                                
                                /** Attempt to handle a possible compatibility issue reported by XFactHD based on their experience with their Framed Blocks mod
                                 * Some mods that use an alternative method of rendering their block model based on a Forge system.
                                 * <br> This involves using IModelData to pass dynamic data to their BakedModel for the block
                                 * <br> The IModelData from IBakedModel#getModelData() or TileEntity#getModelData()
                                 * <p> If another mod calls requestModelDataUpdate through ModelDataManager#requestModelDataUpdate() directly or indirectly through TileEntity#requestModelDataUpdate(),
                                 * <br> it will cause the logic in the following Forge class to throw an exception because Forge does not allow a BakedModel of this nature to render in another fake world
                                 * <br> https://github.com/MinecraftForge/MinecraftForge/blob/a8b2ade614efe3649e8d7305424d3a0476d25786/src/main/java/net/minecraftforge/client/model/ModelDataManager.java#L71
                                 * Many thanks again to XFactHD for getting to the bottom of the issue and notifying us of this issue*/
                                
                            	//Test if this block has a tile entity
                            	if (entry.getValue().getState().hasTileEntity()) {
                                    TileEntity te = shell.getWorld().getTileEntity(entry.getKey());
                                    if (te != null) {
                                        IModelData data = te.getModelData();
                                        //If this tile has model data, pass that to the block 
                                        if (data != null) {
                                            mc.getBlockRendererDispatcher().renderModel(entry.getValue().getState(), entry.getKey(), shell.getWorld(), testStack, builder, true, mc.world.rand, data);
                                        }
                                    }
                                }
                                //If there is no TE, render with empty model data as usual
                                else {
                                    mc.getBlockRendererDispatcher().renderModel(entry.getValue().getState(), entry.getKey(), shell.getWorld(), testStack, builder, true, mc.world.rand, EmptyModelData.INSTANCE);
                                }
                                
                            }
                            hasRenderedInLayer = true;
                        }
                    }
                    else {
                        IVertexBuilder fluidVertexBuilder;
                        if (RenderTypeLookup.canRenderInLayer(entry.getValue().getFluidState(), type)) {
                            //Set vertex builder to previous buffer builder state
                            fluidVertexBuilder = builder;
                            
                            //Validate if the buffer is building. Required to prevent crashing
                            if (!builder.isDrawing())
                                builder.begin(GL11.GL_QUADS, BOTI_VBO.format);
                            
                            TranslatingVertexBuilder translatingBuilder = new TranslatingVertexBuilder(fluidVertexBuilder);
                            translatingBuilder.offset = new Vector3d(
                                    ((int) Helper.getChunkOffset(entry.getKey().getX(), 16)) * 16,
                                    ((int) Helper.getChunkOffset(entry.getKey().getY(), 16)) * 16,
                                    ((int) Helper.getChunkOffset(entry.getKey().getZ(), 16)) * 16
                            );
                            //Set vertexbuilder to the translated version
                            fluidVertexBuilder = translatingBuilder;
                            if (Helper.canRenderInBOTI(entry.getValue().getState())) {
                                mc.getBlockRendererDispatcher().renderFluid(entry.getKey(), shell.getWorld(), fluidVertexBuilder, entry.getValue().getFluidState());
                            }
                            hasRenderedInLayer = true;
                        }
                    }
                    testStack.pop();
                }

                if(hasRenderedInLayer)
                    BOTI_VBO.upload(type);
                BOTI_VBO.unbind(type);

                type.clearRenderState();
            }
            
        }
        else{
            matrixStack.push();
            Matrix4f matrix = matrixStack.getLast().getMatrix();
            BOTI_VBO.draw(matrix);
            matrixStack.pop();
        }
        /** Blocks End */
        
        //Undo the translations done during block rendering so entities render correctly
        matrixStack.translate(offset.getX(), offset.getY(), offset.getZ());
        
        /** Entities Start */
        Impl entityRenderer = IRenderTypeBuffer.getImpl(ENTITY_BUFFER);
        for (EntityStorage e : shell.getEntities()) {

            Entity entity = e.getOrCreateEntity(shell.getWorld());

            if (entity != null && Helper.canRenderInBOTI(entity)) { //If entity is not blacklisted, render it.
                matrixStack.push();
                Vector3d pos = e.createPosVec().subtract(offset.getX(), offset.getY(), offset.getZ());
                matrixStack.translate(pos.x, pos.y, pos.z);
                matrixStack.rotate(Vector3f.YP.rotationDegrees(e.yaw));
                if (e instanceof PlayerEntityStorage) {
                    renderPlayer((AbstractClientPlayerEntity) entity, matrixStack, entityRenderer, shell);
                }
                else {
                    EntityRenderer<? super Entity> renderer = mc.getRenderManager().getRenderer(entity);
                    if(renderer != null) {
                        try {
                            renderer.render(entity, entity.getRotationYawHead(), 0, matrixStack, entityRenderer, LightTexture.packLight(shell.getLightValue(entity.getPosition()), 15));
                        }
                        catch (Exception error) {
                            error.printStackTrace();
                        }
                    } 
                }
                
                matrixStack.pop();
            }

        }
        entityRenderer.finish();
        /** Entities End */
        
        //Translate the same way as blocks to block entities to render in similar locations
        matrixStack.translate(-offset.getX(), -offset.getY(), -offset.getZ());
        
        /** BlockEntities Start */
        IRenderTypeBuffer.Impl tileRenderer = BOTI_TILE_BUFFERS.getBufferSource();
        //Tiles
        for (Entry<BlockPos, TileEntity> entry : shell.getTiles().entrySet()) {
              if (Helper.canRenderInBOTI(entry.getValue().getBlockState())) {

              //Skip it if the shell doesn't have the block for this
              if(entry.getValue().getType() == null || !entry.getValue().getType().isValidBlock(shell.getWorld().getBlockState(entry.getKey()).getBlock())) {
                  entry.getValue().setWorldAndPos(shell.getWorld(), entry.getKey());
                  entry.getValue().updateContainingBlockInfo();
                  continue;
              }

                try {
                    matrixStack.push();
                    matrixStack.translate(entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ());
                    int light = combinedLight;
                   
                    if(shell.getBlockMap().containsKey(entry.getKey())){
                          //Get light based on vanilla lighting values
                        light = LightTexture.packLight(
                                shell.getWorld().getLightFor(LightType.BLOCK, entry.getKey()),
                                shell.getWorld().getLightFor(LightType.SKY, entry.getKey())
                        );
                    }
                    renderTile(entry.getValue(), partialTicks, matrixStack, tileRenderer, light, combinedOverlay);
                    matrixStack.pop();
                }
                catch (Exception e) {
                    //Translate back by position if render was successful, but some other error was thrown. 
                    //E.g. The fake world's tile relies on accessing its blockstate properties to change the rendering logic. 
                    //In this case the fake world's tile may not have a valid blockstate, so will consider it as air and silently error, but cause the portal to be translated
                    //This is most prevelant with our monitor tile renderers, which have been fixed, but can be a good idea to keep this workaround for now, in case other mods don't do enough sanity checks
                    matrixStack.translate(entry.getKey().getX(), entry.getKey().getY(), entry.getKey().getZ());

                    //Enable logging. Can be spammy for client logs, but at least we can identify the issue better.
                    Tardis.LOGGER.catching(Level.DEBUG, e);
                }
            }
        }
        tileRenderer.finish();
        /** BlockEntities End */
        
        //Translate back
        matrixStack.translate(offset.getX(), offset.getY(), offset.getZ());
        
        matrixStack.pop();
    }

    public static <T extends AbstractClientPlayerEntity> void renderPlayer(T player, MatrixStack matrix, IRenderTypeBuffer buffer, WorldShell shell){
        try {
            EntityRenderer<T> renderer = (EntityRenderer<T>) mc.getRenderManager().getRenderer(player);
            if (renderer != null) {
                renderer.render(player, player.getRotationYawHead(), 0, matrix, buffer, LightTexture.packLight(shell.getLightValue(player.getPosition()), 15));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static <T extends TileEntity> void renderTile(T te, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer buffer, int combinedLight, int combinedOverlay) {
        TileEntityRenderer<T> render = TileEntityRendererDispatcher.instance.getRenderer(te);
        if (render != null)
            render.render(te, partialTicks, matrixStack, buffer, combinedLight, combinedOverlay);
    }

}
