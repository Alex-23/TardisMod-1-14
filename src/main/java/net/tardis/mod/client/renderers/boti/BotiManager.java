package net.tardis.mod.client.renderers.boti;

import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.shader.Framebuffer;
import net.tardis.mod.boti.BotiWorld;


public class BotiManager {

	public Framebuffer fbo;
	public WorldRenderer worldRenderer;
	public BotiWorld world;

	public void setupFramebuffer() {
		
		MainWindow res = Minecraft.getInstance().getMainWindow();
		
		if(fbo != null && (fbo.framebufferWidth != res.getFramebufferWidth() || fbo.framebufferHeight != res.getFramebufferHeight()))
			fbo = null;
		
		if(fbo == null) {
			fbo = new Framebuffer(Minecraft.getInstance().getMainWindow().getFramebufferWidth(), Minecraft.getInstance().getMainWindow().getFramebufferHeight(), true, Minecraft.IS_RUNNING_ON_MAC);
		}
		
		fbo.bindFramebuffer(false);
		fbo.checkFramebufferComplete();
		
		if(!fbo.isStencilEnabled())
			fbo.enableStencil();
		
	}
	
	public void endFBO() {
		fbo.unbindFramebuffer();
		fbo.framebufferClear(Minecraft.IS_RUNNING_ON_MAC);
	}
	
	

}
