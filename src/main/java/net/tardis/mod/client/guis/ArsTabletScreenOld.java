package net.tardis.mod.client.guis;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ARSPiece;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateARSTablet;
import net.tardis.mod.sounds.TSounds;

import java.util.ArrayList;
import java.util.List;

public class ArsTabletScreenOld extends Screen {

    private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/ars_tablet.png");
    private static final int MAX_PER_PAGE = 6;

    private int index = 0;

    public ArsTabletScreenOld() {
        super(new StringTextComponent(""));
    }

    public ArsTabletScreenOld(GuiContext cont) {
        this();
    }

    @Override
    protected void init() {
        super.init();
        Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.SCREEN_BEEP_SINGLE.get(), 1.0F));
        for (Widget w : this.buttons) {
            w.active = false;
        }
        this.buttons.clear();

        int x = width / 2 - 100, y = height / 2 + 50;

        this.addButtons(x, y - 20);

        int size = ARSPiece.getRegistry().values().size() / MAX_PER_PAGE;

        this.addButton(new TextButton(x, height / 2 + 45, TardisConstants.Translations.GUI_NEXT.getString(), but -> modIndex(1, size)));
        this.addButton(new TextButton(x, height / 2 + 55, TardisConstants.Translations.GUI_PREV.getString(), but -> modIndex(-1, size)));
    }

    public void addButtons(int x, int y) {

        ArrayList<ARSPiece> all = Lists.newArrayList(ARSPiece.getRegistry().values());
//        all.sort((one, two) -> {
//            return one.getDisplayName().getUnformattedComponentText()
//                    .compareToIgnoreCase(two.getDisplayName().getUnformattedComponentText());
//        });
        
        all.sort((one, two) -> {
            return one.getRegistryName().getPath()
                    .compareToIgnoreCase(two.getRegistryName().getPath());
        });

        int startIndex = index * MAX_PER_PAGE;

        if (startIndex < all.size()) {

            int size = startIndex + MAX_PER_PAGE;
            size = size >= all.size() ? all.size() : size;

            List<ARSPiece> page = all.subList(startIndex, size);

            int id = 0;

            for (ARSPiece p : page) {
            	if (p != null) {
            		this.addButton(new TextButton(x, y - (id * this.font.FONT_HEIGHT) + 5, p.isDataPack() ? p.getDisplayName().getKey() : p.getDisplayName().getString(), but -> {
                        Network.sendToServer(new UpdateARSTablet(p.getRegistryName()));
                        Minecraft.getInstance().displayGuiScreen(null);
                    }));
            	}
                ++id;
            }
        } else index = 0;

    }

    public void modIndex(int mod, int size) {
        if (this.index + mod > size)
            this.index = 0;
        else if (this.index + mod < 0)
            this.index = size;
        else this.index += mod;

        this.init();
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {

        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        int width = 256, height = 173;
        this.blit(matrixStack, this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);

        super.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.font, new TranslationTextComponent(TardisConstants.Strings.GUI + "ars_tablet.gui_title").getString(), this.width / 2, this.height / 2 - (height / 2 - 30), 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, new TranslationTextComponent(TardisConstants.Strings.GUI + "ars_tablet.gui_info").getString(), this.width / 2, this.height / 2 - (height / 2 - 50), 0xFFFFFF);
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }


}
