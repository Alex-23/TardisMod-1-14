package net.tardis.mod.client.guis.misc;

import net.minecraft.util.ResourceLocation;

public class QuantiscopePage {

    private ResourceLocation texture;
    private String nameSuffix;
    private int offsetX, offsetY, width, height;

    public QuantiscopePage(ResourceLocation texture, String nameSuffix, int offsetX, int offsetY, int width, int height){
        this.texture = texture;
        this.nameSuffix = nameSuffix;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
    }

    public QuantiscopePage(ResourceLocation texture, String nameSuffix, int width, int height){
        this(texture, nameSuffix, 0, 0, width, height);
    }

    public ResourceLocation getTexture() {
        return texture;
    }

    public String getNameSuffix() {
        return nameSuffix;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
