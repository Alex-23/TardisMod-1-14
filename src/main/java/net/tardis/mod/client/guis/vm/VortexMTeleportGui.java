package net.tardis.mod.client.guis.vm;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMTeleportMessage;

/**
 * @implSpec Extend this class
 * @implNote Override getMinY, getMinX, getMaxX, getMaxY
 * Override renderScreen, then set your own texture dimensions and blit it
 **/
public class VortexMTeleportGui extends VortexMFunctionScreen {

    private final TranslationTextComponent title = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.title");
    private final TranslationTextComponent tp_type = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.tp_type");
    private final TranslationTextComponent warning = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.precise_warning");
    private TextFieldWidget xCoord;
    private TextFieldWidget yCoord;
    private TextFieldWidget zCoord;
    private Button teleport;
    private Button back;
    private Button teleportSetting;
    private boolean preciseTeleport = false;
    private int settingId = 0;


    public VortexMTeleportGui(ITextComponent title) {
        super(title);
    }

    public VortexMTeleportGui() {
    }


    @Override
    public void init() {
        super.init();

        String teleportTypeTop = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.setting.top").getString();
        String teleportTypePrecise = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.setting.precise").getString();
        String teleportButton = new TranslationTextComponent(TardisConstants.Strings.GUI_VM + "teleport.tp").getString();
        String backButton = TardisConstants.Translations.GUI_BACK.getString();
        final int btnH = 20;

        xCoord = new TextFieldWidget(this.font, this.getMinX() + 45, this.getMaxY() + 75, 50, this.font.FONT_HEIGHT + 2, new TranslationTextComponent(""));
        yCoord = new TextFieldWidget(this.font, this.getMinX() + 45, this.getMaxY() + 90, 50, this.font.FONT_HEIGHT + 2, new TranslationTextComponent(""));
        zCoord = new TextFieldWidget(this.font, this.getMinX() + 45, this.getMaxY() + 105, 50, this.font.FONT_HEIGHT + 2, new TranslationTextComponent(""));
        teleport = new Button(this.getMinX() + 44, this.getMaxY() + 121, this.font.getStringWidth(teleportButton) + 10, btnH, new TranslationTextComponent(teleportButton), new Button.IPressable() {

            @Override
            public void onPress(Button button) {
                checkPosInput();
            }
        });
        back = new Button(this.getMinX(), this.getMaxY() + 35, this.font.getStringWidth(backButton) + 8, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(backButton), new Button.IPressable() {

            @Override
            public void onPress(Button button) {
                ClientHelper.openGUI(TardisConstants.Gui.VORTEX_MAIN, null);
            }
        });
        teleportSetting = new Button(this.getMinX() + 170, this.getMaxY() + 50, 55, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(teleportTypeTop), new Button.IPressable() {

            @Override
            public void onPress(Button button) {
                incrementId();
                switch (settingId) {
                    case 0:
                        teleportSetting.setMessage(new TranslationTextComponent(teleportTypeTop));
                        setTeleportType(!preciseTeleport);
                        break;
                    case 1:
                        teleportSetting.setMessage(new TranslationTextComponent(teleportTypePrecise));
                        setTeleportType(!preciseTeleport); //reset flag
                        break;
                }

            }
        });

        this.buttons.clear();
        this.addButton(teleport);
        this.addButton(xCoord); //addButton also adds other widget types, mapping name can be misleading
        this.addButton(yCoord);
        this.addButton(zCoord);
        this.addButton(back);
        this.addButton(teleportSetting);
        xCoord.setFocused2(true);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.font, title.getString(), this.getMinX() + 73, this.getMaxY() + 40, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, "X:", this.getMinX() + 35, this.getMaxY() + 76, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, "Y:", this.getMinX() + 35, this.getMaxY() + 91, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, "Z:", this.getMinX() + 35, this.getMaxY() + 106, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, tp_type.getString(), this.getMinX() + 198, this.getMaxY() + 35, 0xFFFFFF);
        if (this.preciseTeleport) {
        	this.font.func_238418_a_(this.warning, this.getMinX() + 162, this.getMaxY() + 75, 80, 0xffcc00);
        }
        displayTextBoxCoords(this.xCoord, COORD_TYPE.X);
        displayTextBoxCoords(this.yCoord, COORD_TYPE.Y);
        displayTextBoxCoords(this.zCoord, COORD_TYPE.Z);
    }

    @Override
    public boolean shouldCloseOnEsc() {
        return true;
    }

    @Override
    public void closeScreen() {
        super.closeScreen();
    }

    @Override
    public boolean isPauseScreen() {
        return true;
    }

    private void checkPosInput() {
        boolean isValid = isStringNumeric(xCoord.getText(), COORD_TYPE.X) && isStringNumeric(yCoord.getText(), COORD_TYPE.Y) && isStringNumeric(zCoord.getText(), COORD_TYPE.Z);
        boolean withinRange = isWithinConfigRange(xCoord.getText(), COORD_TYPE.X) && isWithinConfigRange(yCoord.getText(), COORD_TYPE.Y) && isWithinConfigRange(zCoord.getText(), COORD_TYPE.Z);
        if (isValid && withinRange) {
            BlockPos tpPos = new BlockPos(getInt(xCoord.getText(), COORD_TYPE.X), getInt(yCoord.getText(), COORD_TYPE.Y), getInt(zCoord.getText(), COORD_TYPE.Z));
            Network.sendToServer(new VMTeleportMessage(tpPos, this.minecraft.player.getEntityId(), preciseTeleport));
        }
    }

    private boolean isStringNumeric(String input, COORD_TYPE type) {
        if (input != null && !input.isEmpty()) {
            try {
                Integer.parseInt(input);
                return true;
            } catch (NumberFormatException nfe) {
            	this.minecraft.player.sendStatusMessage(TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.invalidInput", type.toString())), false);
                return false;
            }
        } else if (input.isEmpty()) {
            return true;
        }
        return false;
    }

    private int getInt(String num, COORD_TYPE type) {
        if (isStringNumeric(num, type)) {
            if (num.isEmpty()) { //Make it so that if a textbox is empty, it will use player current coords
                return getDefaultCoordInt(type);
            } else {
                return Integer.parseInt(num);
            }
        }
        return getDefaultCoordInt(type);
    }

    private void displayTextBoxCoords(TextFieldWidget widget, COORD_TYPE type) {
        if (widget.isFocused()) {
            widget.setSuggestion("");
        } else if (widget.getText().isEmpty()) {
            widget.setSuggestion(Integer.toString(this.getDefaultCoordInt(type)));
        }
    }

    private int getDefaultCoordInt(COORD_TYPE type) {
        switch (type) { //Teleports user to current position if they try to parse in non-integers
            case X:
                return (int) this.minecraft.player.getPosX();
            case Y:
                return (int) this.minecraft.player.getPosY();
            case Z:
                return (int) this.minecraft.player.getPosZ();
            default:
                return 0;
        }
    }

    public boolean getTeleportType() {
        return preciseTeleport;
    }

    public void setTeleportType(boolean type) {
        this.preciseTeleport = type;
    }

    public boolean isWithinConfigRange(String num, COORD_TYPE type) {
        if ((getInt(num, type) - getDefaultCoordInt(type)) <= TConfig.SERVER.vmTeleportRange.get()) {
            return true;
        } else {
            this.minecraft.player.sendStatusMessage(TextHelper.createVortexManipMessage(new TranslationTextComponent("message.vm.illegalPos", TConfig.SERVER.vmTeleportRange.get(), type.toString())), false);
            return false;
        }
    }

    public void incrementId() {
        if (settingId + 1 > 1) {
            settingId = 0;
        } else {
            ++settingId;
        }
    }

    @Override
    public int getMinY() {
        return super.getMinY();
    }

    @Override
    public int getMinX() {
        return super.getMinX();
    }

    @Override
    public int getMaxX() {
        return super.getMaxX();
    }

    @Override
    public int getMaxY() {
        return super.getMaxY();
    }

    @Override
    public int texWidth() {
        return super.texWidth();
    }

    @Override
    public int texHeight() {
        return super.texHeight();
    }

    public enum COORD_TYPE {
        X, Y, Z
    }
}
