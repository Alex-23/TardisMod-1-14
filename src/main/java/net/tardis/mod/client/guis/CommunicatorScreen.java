package net.tardis.mod.client.guis;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.DimensionType;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.CommunicatorMessage;
import net.tardis.mod.network.packets.CommunicatorMessage.Type;
import net.tardis.mod.tileentities.console.misc.DistressSignal;

public class CommunicatorScreen extends Screen {

    private static final StringTextComponent TITLE = new StringTextComponent("");
    private final TranslationTextComponent load = new TranslationTextComponent(TardisConstants.Strings.GUI + "communicator.load");
    private final TranslationTextComponent ignore = new TranslationTextComponent(TardisConstants.Strings.GUI + "communicator.ignore");
    private final TranslationTextComponent ignore_all = new TranslationTextComponent(TardisConstants.Strings.GUI + "communicator.ignore_all");
    private final TranslationTextComponent message_title = new TranslationTextComponent(TardisConstants.Strings.GUI + "communicator.message_title");
    private final TranslationTextComponent message_sub_title = new TranslationTextComponent(TardisConstants.Strings.GUI + "communicator.message_subtitle");

    private DistressSignal signal;

    public CommunicatorScreen() {
        super(TITLE);
    }

    @Override
    protected void init() {
        super.init();

        this.addButton(new TextButton(width / 2 - 130, height / 2 + 90, load.getString(), but -> {
            Network.sendToServer(new CommunicatorMessage(Type.ACCEPT));
            Minecraft.getInstance().displayGuiScreen(null);
        }));
        this.addButton(new TextButton(width / 2 + 60, height / 2 + 90, ignore.getString(), but -> {
            Network.sendToServer(new CommunicatorMessage(Type.IGNORE));
            Minecraft.getInstance().displayGuiScreen(null);
        }));

        this.addButton(new TextButton(width / 2 - 20, height / 2 + 90, ignore_all.getString(), but -> {
            Network.sendToServer(new CommunicatorMessage(Type.IGNORE_ALL));
            Minecraft.getInstance().displayGuiScreen(null);
        }));

        TardisHelper.getConsoleInWorld(this.minecraft.world).ifPresent(tile -> {
            if (!tile.getDistressSignals().isEmpty())
                this.signal = tile.getDistressSignals().get(0);
        });
    }

    @Override
    public void render(MatrixStack matrixStack, int p_render_1_, int p_render_2_, float p_render_3_) {
        this.renderBackground(matrixStack);
        drawCenteredString(matrixStack, this.font, message_title.getString(), width / 2, height / 2 - 90, 0xFBB828);
        drawCenteredString(matrixStack, this.font, message_sub_title.getString(), width / 2, height / 2 - 60, 0xFFFFFF);
        if (this.signal != null) {
            SpaceTimeCoord pos = this.signal.getSpaceTimeCoord();
            drawCenteredString(matrixStack, this.font, TardisConstants.Translations.LOCATION.getString() + WorldHelper.formatBlockPos(pos.getPos()), width / 2, height / 2, 0xFFFFFF);
            drawCenteredString(matrixStack, this.font, TardisConstants.Translations.DIMENSION.getString() + WorldHelper.formatDimName(pos.getDim()), width / 2, height / 2 + 15, 0xFFFFFF);
            drawCenteredString(matrixStack, font, this.signal.getMessage(), width / 2, height / 2 - 40, 0xFFFFF);
        }

        super.render(matrixStack, p_render_1_, p_render_2_, p_render_3_);
    }

}
