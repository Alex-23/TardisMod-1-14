package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.console.DataTypes;
import net.tardis.mod.network.packets.console.SoundSchemeData;
import net.tardis.mod.registries.SoundSchemeRegistry;
import net.tardis.mod.sounds.AbstractSoundScheme;

public class SoundSchemeMonitorScreen extends MonitorScreen {

    public SoundSchemeMonitorScreen(IMonitorGui monitor, String type) {
        super(monitor, type);
    }

    @Override
    protected void init() {
        super.init();

        for (AbstractSoundScheme sound : SoundSchemeRegistry.SOUND_SCHEME_REGISTRY.get().getValues()) {
            this.addSubmenu(sound.getDisplayName(),
                    but -> {
                        Network.sendToServer(new ConsoleUpdateMessage(DataTypes.SOUND_SCHEME, new SoundSchemeData(sound)));
                        ClientHelper.openGui(null);
                    });
        }

    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.minecraft.fontRenderer, new TranslationTextComponent(TardisConstants.Strings.GUI + "sound_scheme.title").getString(), this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2), this.parent.getMaxY() + 10, 0xFFFFFF);
        drawCenteredString(matrixStack, this.minecraft.fontRenderer, new TranslationTextComponent(TardisConstants.Strings.GUI + "sound_scheme.desc").getString(), this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2), this.parent.getMaxY() + 30, 0xFFFFFF);
    }

}
