package net.tardis.mod.client.guis.vm;

import com.mojang.blaze3d.matrix.MatrixStack;

/**
 * Created by 50ap5ud5
 * on 7 May 2020 @ 4:44:30 pm
 *
 * @implNote Override getMinY, getMinX, getMaxX, getMaxY
 * Override renderScreen, then set your own texture dimensions and blit it
 */
public interface IVortexMScreen {

    void renderScreen(MatrixStack stack);

    /**
     * Bottom most part of GUI Texture
     *
     * @return
     * @implSpec Subtract from this to move widget up
     */
    int getMinY();

    /**
     * Left most part of GUI texture
     *
     * @return
     * @implSpec Add from this to move widget to the right
     */
    int getMinX();

    /**
     * Right most part of GUI texture
     *
     * @return
     * @implSpec Subtract from this to move widget to the left
     */
    int getMaxX();

    /**
     * Top most part of GUI Texture
     *
     * @return
     * @implSpec Add from this to move widget down
     */
    int getMaxY();

    int texWidth();

    int texHeight();
}
