package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.gui.widget.button.Button.IPressable;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ChangeInteriorMessage;
import net.tardis.mod.network.packets.ProtocolMessage;
import net.tardis.mod.network.packets.WaypointOpenMessage;
import net.tardis.mod.protocols.Protocol;
import net.tardis.mod.registries.ProtocolRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * The template for new Monitor Main Menus with their own background textures
 * E.g. Steampunk Monitor GUI
 * E.g.2 Wall Eye Monitor GUI
 *
 * @implSpec Extend this class when you want to make a Main Monitor Menu with your own texture
 * @implNote If you want to make a sub menu, extend MonitorScreen
 **/
public abstract class BaseMonitorScreen extends Screen implements IMonitorGui {

    public static TranslationTextComponent TITLE = new TranslationTextComponent("gui.tardis.monitor." + Tardis.MODID + ".steam");
    protected ConsoleTile tile;
    private int id;
    private String menu = "main";

    public BaseMonitorScreen(ITextComponent titleIn) {
        super(titleIn);
    }

    public BaseMonitorScreen() {
        super(TITLE);
    }

    @Override
    protected void init() {
        super.init();

        TardisHelper.getConsoleInWorld(this.minecraft.world).ifPresent(tile -> this.tile = tile);

        id = 0;
        this.buttons.clear();
        //Don't add any buttons if we can't find the console tile
        if (this.tile != null) {
	        ProtocolRegistry.PROTOCOL_REGISTRY.get().getEntries().forEach(entry -> {
	            if (entry.getValue().getSubmenu().equals(this.menu))
	                this.addButton(this.addButton(this.getMinX(), this.getMinY(), entry.getKey().getLocation()));
	        });
	        this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "exterior_properties"), but -> Minecraft.getInstance().displayGuiScreen(new ExteriorPropMonitorScreen(this)));
	        this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "interior_properties"),
	                (button) -> Minecraft.getInstance().displayGuiScreen(new InteriorEditScreen(this, "interior")));
	        this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "security"), "security");
	        this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "exterior"), but -> Minecraft.getInstance().displayGuiScreen(new ExteriorScreen(this, "exterior")));
	        this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "console"),
	                (button) -> Minecraft.getInstance().displayGuiScreen(new ConsoleSelectionScreen(this, "console")));
	
	        
	    	boolean isCancellingInteriorChange = tile.getInteriorManager().isInteriorStillRegenerating(); //If the interior changing is underway, display the cancel option
	        if (isCancellingInteriorChange) {
	        	this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "interior"),
	                    (button) -> cancelInteriorChangeScreen());
	        }
	        else {
	        	this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "interior"),
	                    (button) -> Minecraft.getInstance().displayGuiScreen(new InteriorChangeScreen(this, "change_interior")));
	        }
	
	        this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "waypoints"), but -> {
	            Minecraft.getInstance().displayGuiScreen(new WaypointMonitorScreen(this, "waypoints"));
	            Network.sendToServer(new WaypointOpenMessage(null));
	        });
	        if (this.tile.hasNavCom()) {
	        	this.addSubmenu(new TranslationTextComponent(TardisConstants.Strings.GUI_PROTOCOL_TITLE + "info"),
		                (button) -> Minecraft.getInstance().displayGuiScreen(new InfoDisplayScreen(this, "info_display")));
	        }
        }
    }

    public void addSubmenu(TextComponent com, IPressable press) {

        TextButton button = new TextButton(this.getMinX(), this.getMinY() - (id * (int) (this.minecraft.fontRenderer.FONT_HEIGHT * 1.25)),
                "> " + com.getString(), press);

        this.addButton(button);
        ++id;
    }

    public void addSubmenu(TextComponent com, String menu) {
        IPressable press = (button) -> {
            Minecraft.getInstance().displayGuiScreen(new MonitorScreen(this, menu));
        };
        this.addSubmenu(com, press);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float p_render_3_) {

        this.renderMonitor(matrixStack);

        super.render(matrixStack, mouseX, mouseY, p_render_3_);
    }

    @Override
    public boolean shouldCloseOnEsc() {
        return true;
    }

    @Override
    public void onClose() {
        super.onClose();
    }

    public Button addButton(int x, int y, ResourceLocation key) {
        Protocol prot = ProtocolRegistry.PROTOCOL_REGISTRY.get().getValue(key);
        IPressable press = (button) -> {
            TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile) {
                Network.sendToServer(new ProtocolMessage(prot.getRegistryName()));
                prot.call(this.minecraft.world, this.minecraft.player, (ConsoleTile) te);
            }
        };
        TextButton button = new TextButton(x, y - (id * (int) (this.minecraft.fontRenderer.FONT_HEIGHT * 1.25)), "> " + (prot != null ? prot.getDisplayName(tile).getString() : "ERROR"), press);
        ++id;
        return button;
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }
    
    private void cancelInteriorChangeScreen() {
        this.getMinecraft().displayGuiScreen(new MonitorConfirmScreen(this, menu, (isCancelling) -> {
            if (isCancelling) {
                Network.sendToServer(new ChangeInteriorMessage(this.tile.getNextConsoleRoom().getRegistryName(), true));
                this.minecraft.displayGuiScreen(null);
            } else {
                this.minecraft.displayGuiScreen(this);
            }
        }, new TranslationTextComponent("gui.tardis.interior.change_cancel.warning_message"), new TranslationTextComponent("gui.tardis.interior.cancel_confirm.confirm_message"), TardisConstants.Translations.GUI_CONFIRM, TardisConstants.Translations.GUI_CANCEL));
    }


}
