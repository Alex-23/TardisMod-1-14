package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.SimpleSound;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.sounds.TSounds;

public class EyeMonitorScreen extends BaseMonitorScreen {

    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/monitors/eye.png");


    @Override
    public void renderMonitor(MatrixStack matrixStack) {
        Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
        int guiWidth = 255, guiHeight = 182;
        this.renderBackground(matrixStack);
        this.blit(matrixStack, this.width / 2 - guiWidth / 2, this.height / 2 - guiHeight / 2, 0, 0, guiWidth, guiHeight);
    }

    @Override
    public int getMinY() {
        return this.height / 2 + 45;
    }

    @Override
    public int getMinX() {
        return this.width / 2 - 100;
    }

    @Override
    public int getMaxX() {
        return this.getMinX() + 200;
    }

    @Override
    public int getMaxY() {
        return this.getMinY() - 120;
    }

    @Override
    protected void init() {
        super.init();
        Minecraft.getInstance().getSoundHandler().play(SimpleSound.master(TSounds.EYE_MONITOR_INTERACT.get(), 1.0F));
    }

    @Override
    public int getWidth() {
        return 200;
    }

    @Override
    public int getHeight() {
        return 135;
    }

    @Override
    public int getGuiID() {
        return TardisConstants.Gui.MONITOR_MAIN_EYE;
    }

}
