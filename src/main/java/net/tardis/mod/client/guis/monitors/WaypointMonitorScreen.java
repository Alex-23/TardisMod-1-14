package net.tardis.mod.client.guis.monitors;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.WaypointWrapper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.WaypointDeleteMessage;
import net.tardis.mod.network.packets.WaypointLoadMessage;

public class WaypointMonitorScreen extends MonitorScreen {

    private List<WaypointWrapper> list = new ArrayList<>();
    private WaypointWrapper coord;
    private int index = 0;
    private boolean hasBanks = false;
    private TranslationTextComponent waypoint_title = new TranslationTextComponent(TardisConstants.Strings.GUI + "waypoint.title");

    public WaypointMonitorScreen(IMonitorGui monitor, String menu) {
        super(monitor, menu);
    }

    @Override
    protected void init() {
        super.init();

        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), TardisConstants.Translations.GUI_PREV, press -> change(-1)));
        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), TardisConstants.Translations.GUI_SELECT, press -> {
            if (coord != null) {
            	Network.sendToServer(new WaypointLoadMessage(coord.bankPos, coord.index));	
            }
            Minecraft.getInstance().displayGuiScreen(null);
        }));
        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY(), MonitorScreen.nextTranslation, press -> change(1)));
        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY() - 5, new StringTextComponent("> " + new TranslationTextComponent(TardisConstants.Strings.GUI + "waypoint.new").getString()), pres -> {
            Minecraft.getInstance().displayGuiScreen(new WaypointNewMonitorScreen(this.parent));
        }));

        this.addButton(this.createButton(this.parent.getMinX(), this.parent.getMinY() - 6, new StringTextComponent("> " + new TranslationTextComponent(TardisConstants.Strings.GUI + "waypoint.delete").getString()), pres -> {
            this.confirmAction();
        }));

        change(0);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float p_render_3_) {
        super.render(matrixStack, mouseX, mouseY, p_render_3_);
        drawCenteredString(matrixStack, this.font, waypoint_title.getString(), this.parent.getMinX() + ((this.parent.getMaxX() - this.parent.getMinX()) / 2), this.parent.getMaxY(), 0xFFFFFF);
        if (coord != null) {
            this.minecraft.fontRenderer.drawString(matrixStack, coord.coord.getName().isEmpty() ? "Waypoint Name: NONE" : "Waypoint Name: " + coord.coord.getName(),
                    this.parent.getMinX(), this.parent.getMaxY() + 23, 0xFFFFFF);

            this.minecraft.fontRenderer.drawString(matrixStack, TardisConstants.Translations.LOCATION.getString() + WorldHelper.formatBlockPos(coord.coord.getPos()),
                    this.parent.getMinX(), this.parent.getMaxY() + (this.font.FONT_HEIGHT * 4 - 3), 0xFFFFFF);

            this.minecraft.fontRenderer.drawString(matrixStack, TardisConstants.Translations.DIMENSION.getString() + WorldHelper.formatDimName(coord.coord.getDim()),
                    this.parent.getMinX(), this.parent.getMaxY() + (this.font.FONT_HEIGHT * 5 - 3), 0xFFFFFF);

            this.font.drawString(matrixStack, "Waypoint Bank: " + WorldHelper.formatBlockPos(coord.bankPos), this.parent.getMinX(), this.parent.getMaxY() + this.font.FONT_HEIGHT * 6 - 3, 0xFFFFFF);
        } else {
            if (hasBanks)
                this.font.drawString(matrixStack, new TranslationTextComponent(TardisConstants.Strings.GUI + "waypoint.no_saved").getString(), this.parent.getMinX(), this.parent.getMaxY() + 23, 0xFFFFFF);
            else
                this.font.drawString(matrixStack, new TranslationTextComponent(TardisConstants.Strings.GUI + "waypoint.no_banks").getString(), this.parent.getMinX(), this.parent.getMaxY() + 23, 0xFFFFFF);
        }
    }

    public void change(int i) {

        if (this.list == null || this.list.isEmpty()) {
            this.index = 0;
            return;
        }


        if (index + i >= this.list.size())
            index = 0;
        else if (index + i < 0)
            index = this.list.size() - 1;
        else index += i;

        this.coord = this.list.get(index);

    }

    public void setWaypoints(Map<BlockPos, List<SpaceTimeCoord>> map) {
        list.clear();

        if (map == null) {
            hasBanks = false;
            return;
        }

        if (map.size() > 0)
            hasBanks = true;
        else hasBanks = false;

        for (Entry<BlockPos, List<SpaceTimeCoord>> entry : map.entrySet()) {
            int index = 0;
            for (SpaceTimeCoord coord : entry.getValue()) {
                this.list.add(new WaypointWrapper(entry.getKey(), coord, index));
                ++index;
            }
        }
        this.list.removeIf(wrap -> wrap.coord.equals(SpaceTimeCoord.UNIVERAL_CENTER));

        this.change(0);
    }

    private void confirmAction() {

        if (this.coord == null)
            return;

        this.getMinecraft().displayGuiScreen(new MonitorConfirmScreen(parent, menu, (shouldChange) -> {
            if (shouldChange) {
                Network.sendToServer(new WaypointDeleteMessage(this.coord.bankPos, this.index));
                this.minecraft.displayGuiScreen(null);
            } else {
                this.minecraft.displayGuiScreen(null);
            }
        }, new TranslationTextComponent("gui.tardis.waypoint.delete.confirm_title"), new TranslationTextComponent("gui.tardis.waypoint.delete.confirm_message"), TardisConstants.Translations.GUI_CONFIRM, TardisConstants.Translations.GUI_CANCEL));
    }
}
