package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
/** Wrapper class around IInteriorDoorRenderer and EntityModel to define logic common logic for when the Interior Door model may be facing into the BOTI window when opened*/
public abstract class AbstractInteriorDoorModel extends EntityModel<Entity> implements IInteriorDoorRenderer{
    
    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        
    }

    @Override
    public void setRotationAngles(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks,
            float netHeadYaw, float headPitch) {
    }

    @Override
    public void render(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        this.renderBones(door, matrixStack, buffer, packedLight, packedOverlay);
        this.renderBoti(door, matrixStack, buffer, packedLight, packedOverlay);
    }

    @Override
    public void renderDoorWhenClosed(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay, ModelRenderer... doorBones) {
        for (ModelRenderer bone : doorBones) {
            this.renderDoorWhenClosed(door, matrixStack, buffer, packedLight, packedOverlay, bone);
        }
    }

    @Override
    public void renderDoorWhenClosed(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay, ModelRenderer doorBone) {
        if (this.doesDoorOpenIntoBotiWindow()) {
            if (TConfig.CLIENT.enableBoti.get()) {
                if (door.getOpenState() == EnumDoorState.CLOSED) {
                    doorBone.render(matrixStack, buffer, packedLight, packedOverlay);
                }
            }
            else {
                doorBone.render(matrixStack, buffer, packedLight, packedOverlay);
            }
        }
        else {
            doorBone.render(matrixStack, buffer, packedLight, packedOverlay);
        }
    }

    @Override
    public boolean doesDoorOpenIntoBotiWindow() {
        return false;
    }

}
