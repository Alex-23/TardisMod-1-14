package net.tardis.mod.client.models.interiordoors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.entity.DoorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

public class PoliceBoxInteriorModel extends AbstractInteriorDoorModel{
    public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/police_box.png");
    
    private final ModelRenderer Newbery;
    private final ModelRenderer CornerPosts;
    private final ModelRenderer UpperSignage;
    private final ModelRenderer Trim;
    private final ModelRenderer RoofStacks;
    private final ModelRenderer Lamp;
    private final ModelRenderer LeftDoor;
    private final ModelRenderer bone4;
    private final ModelRenderer bone5;
    private final ModelRenderer RightDoor;
    private final ModelRenderer bone6;
    private final ModelRenderer boti;

    public PoliceBoxInteriorModel() {
        textureWidth = 512;
        textureHeight = 512;

        Newbery = new ModelRenderer(this);
        Newbery.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        CornerPosts = new ModelRenderer(this);
        CornerPosts.setRotationPoint(0.0F, 0.0F, 0.0F);
        Newbery.addChild(CornerPosts);
        CornerPosts.setTextureOffset(82, 131).addBox(15.5F, -69.0F, -20.5F, 5.0F, 70.0F, 5.0F, 0.0F, false);
        CornerPosts.setTextureOffset(82, 131).addBox(-20.5F, -69.0F, -20.5F, 5.0F, 70.0F, 5.0F, 0.0F, false);

        UpperSignage = new ModelRenderer(this);
        UpperSignage.setRotationPoint(0.0F, 0.0F, 0.0F);
        Newbery.addChild(UpperSignage);
        UpperSignage.setTextureOffset(160, 138).addBox(-18.5F, -66.0F, -22.5F, 37.0F, 4.0F, 4.0F, 0.0F, false);

        Trim = new ModelRenderer(this);
        Trim.setRotationPoint(0.0F, 0.0F, 0.0F);
        Newbery.addChild(Trim);
        Trim.setTextureOffset(210, 212).addBox(14.5F, -61.0F, -18.5F, 4.0F, 62.0F, 2.0F, 0.0F, false);
        Trim.setTextureOffset(210, 212).addBox(-18.5F, -61.0F, -18.5F, 4.0F, 62.0F, 2.0F, 0.0F, false);
        Trim.setTextureOffset(149, 127).addBox(-18.5F, -62.0F, -18.5F, 37.0F, 1.0F, 2.0F, 0.0F, false);

        RoofStacks = new ModelRenderer(this);
        RoofStacks.setRotationPoint(0.0F, 0.0F, 0.0F);
        Newbery.addChild(RoofStacks);
        RoofStacks.setTextureOffset(35, 228).addBox(-19.5F, -69.0F, -19.5F, 39.0F, 3.0F, 3.0F, 0.0F, false);

        Lamp = new ModelRenderer(this);
        Lamp.setRotationPoint(0.0F, 0.0F, 0.0F);
        Newbery.addChild(Lamp);
        

        LeftDoor = new ModelRenderer(this);
        LeftDoor.setRotationPoint(14.5F, -3.0F, -17.5F);
        Newbery.addChild(LeftDoor);
        LeftDoor.setTextureOffset(45, 135).addBox(-12.0F, 2.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        LeftDoor.setTextureOffset(0, 120).addBox(-12.0F, -13.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        LeftDoor.setTextureOffset(0, 120).addBox(-12.0F, -28.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        LeftDoor.setTextureOffset(0, 120).addBox(-12.0F, -43.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        LeftDoor.setTextureOffset(0, 120).addBox(-12.0F, -58.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        LeftDoor.setTextureOffset(10, 215).addBox(-15.0F, -58.0F, 0.0F, 3.0F, 62.0F, 2.0F, 0.0F, true);
        LeftDoor.setTextureOffset(234, 146).addBox(-15.0F, -58.0F, -1.0F, 1.0F, 62.0F, 1.0F, 0.0F, true);
        LeftDoor.setTextureOffset(230, 146).addBox(-15.0F, -58.0F, 2.0F, 1.0F, 62.0F, 1.0F, 0.0F, true);
        LeftDoor.setTextureOffset(232, 224).addBox(-2.0F, -58.0F, 0.0F, 2.0F, 62.0F, 2.0F, 0.0F, true);
        LeftDoor.setTextureOffset(20, 0).addBox(-12.0F, -26.0F, 1.1F, 10.0F, 28.0F, 0.0F, 0.0F, true);
        LeftDoor.setTextureOffset(0, 131).addBox(-12.0F, -26.0F, 0.9F, 10.0F, 28.0F, 0.0F, 0.0F, true);

        bone4 = new ModelRenderer(this);
        bone4.setRotationPoint(-14.5F, -47.0F, 1.0F);
        LeftDoor.addChild(bone4);
        bone4.setTextureOffset(0, 106).addBox(2.5F, -9.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, true);

        bone5 = new ModelRenderer(this);
        bone5.setRotationPoint(-2.0F, -32.0F, 0.0F);
        LeftDoor.addChild(bone5);
        bone5.setTextureOffset(0, 64).addBox(-10.0F, -9.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, true);

        RightDoor = new ModelRenderer(this);
        RightDoor.setRotationPoint(-14.5F, -3.0F, -17.5F);
        Newbery.addChild(RightDoor);
        RightDoor.setTextureOffset(45, 135).addBox(2.0F, 2.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        RightDoor.setTextureOffset(0, 120).addBox(2.0F, -13.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        RightDoor.setTextureOffset(0, 120).addBox(2.0F, -28.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        RightDoor.setTextureOffset(0, 120).addBox(2.0F, -43.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        RightDoor.setTextureOffset(0, 120).addBox(2.0F, -58.0F, 0.0F, 10.0F, 2.0F, 2.0F, 0.0F, true);
        RightDoor.setTextureOffset(232, 224).addBox(12.0F, -58.0F, 0.0F, 2.0F, 62.0F, 2.0F, 0.0F, true);
        RightDoor.setTextureOffset(232, 224).addBox(0.0F, -58.0F, 0.0F, 2.0F, 62.0F, 2.0F, 0.0F, true);
        RightDoor.setTextureOffset(0, 0).addBox(2.0F, -41.0F, 1.1F, 10.0F, 43.0F, 0.0F, 0.0F, true);
        RightDoor.setTextureOffset(0, 131).addBox(2.0F, -41.0F, 0.9F, 10.0F, 43.0F, 0.0F, 0.0F, true);

        bone6 = new ModelRenderer(this);
        bone6.setRotationPoint(14.5F, -43.0F, 1.0F);
        RightDoor.addChild(bone6);
        bone6.setTextureOffset(0, 106).addBox(-12.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, true);

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, 0.0F);
        boti.setTextureOffset(0, 297).addBox(-14.55F, -61.0F, -16.45F, 29.0F, 62.0F, 2.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        matrixStack.push();
        this.translateToDoor(matrixStack);
        EnumDoorState state = door.getOpenState();
        switch (state) {
        case ONE:
            this.RightDoor.rotateAngleY = (float) Math.toRadians(
                    EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default
            this.LeftDoor.rotateAngleY = (float) Math.toRadians(
                    EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
            break;
        case BOTH:
            this.RightDoor.rotateAngleY = (float) Math.toRadians(
                    EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.ONE));
            this.LeftDoor.rotateAngleY = (float) Math.toRadians(
                    EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open
            break;
        case CLOSED://close both doors
            this.RightDoor.rotateAngleY = (float) Math.toRadians(
                    EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
            this.LeftDoor.rotateAngleY = (float) Math.toRadians(
                    EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
            break;
        default:
            break;

        }
        Newbery.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
    }
    
    

    @Override
    public void renderBoti(DoorEntity door, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay) {
        if(Minecraft.getInstance().world != null && door.getOpenState() != EnumDoorState.CLOSED){
            Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data ->{

                PortalInfo info = new PortalInfo();
                info.setPosition(door.getPositionVec());
                info.setWorldShell(data.getBotiWorld());

                //Translators
                info.setTranslate(matrix -> {
                    DoorRenderer.applyTranslations(matrix, door);
                });
                info.setTranslatePortal(matrix -> {
                    matrix.translate(0, 1.5, 0);
                    matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                    matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(data.getBotiWorld().getPortalDirection())));
                    matrix.translate(-0.5, 0, -0.5);
                });

                //Renderers
                info.setRenderPortal((matrix, buf) -> {
                    matrix.push();
                    matrix.rotate(Vector3f.YP.rotationDegrees(180));
                    matrix.translate(0, 0.72, 1.05);
                    matrix.scale(0.5F, 0.5F, 0.5F);
                    this.boti.render(matrix, buf.getBuffer(RenderType.getEntityCutout(this.getTexture())), packedLight, packedOverlay);
                    matrix.pop();
                });

                BOTIRenderer.addPortal(info);
            });
        }
    }

    @Override
    public ResourceLocation getTexture() {
        return TEXTURE;
    }

    public void translateToDoor(MatrixStack matrix){
        matrix.rotate(Vector3f.YP.rotationDegrees(180));
        matrix.translate(0, 0.72, 1.05);
        matrix.scale(0.5F, 0.5F, 0.5F);
    }
}