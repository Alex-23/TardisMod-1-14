package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.BrokenExteriorRenderer.IBrokenExteriorModel;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.TT2020CapsuleExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class TT2020ExteriorModel extends ExteriorModel implements IBrokenExteriorModel{
    private final LightModelRenderer glow;
    private final ModelRenderer glow_plate_2;
    private final ModelRenderer glow_plate_3;
    private final ModelRenderer glow_plate_4;
    private final ModelRenderer glow_plate_5;
    private final ModelRenderer glow_plate_6;
    private final ModelRenderer door;
    private final ModelRenderer door_west_rotate_y;
    private final ModelRenderer door_dot_west_a;
    private final ModelRenderer door_dot_west_b;
    private final ModelRenderer door_dot_west_c;
    private final ModelRenderer door_dot_west_d;
    private final ModelRenderer door_dot_west_e;
    private final ModelRenderer door_east_rotate_y;
    private final ModelRenderer door_dot_east_a;
    private final ModelRenderer door_dot_east_b;
    private final ModelRenderer door_dot_east_c;
    private final ModelRenderer door_dot_east_d;
    private final ModelRenderer door_dot_east_e;
    private final ModelRenderer side_walls;
    private final ModelRenderer front;
    private final ModelRenderer door_frame;
    private final ModelRenderer frame;
    private final ModelRenderer panel2;
    private final ModelRenderer main_panel_2;
    private final ModelRenderer deviders2;
    private final ModelRenderer lower_roundel_2;
    private final ModelRenderer upper_roundel_2;
    private final ModelRenderer panel3;
    private final ModelRenderer main_panel_3;
    private final ModelRenderer deviders3;
    private final ModelRenderer upper_roundel_3;
    private final ModelRenderer mid_roundel_3;
    private final ModelRenderer lower_roundel_3;
    private final ModelRenderer panel4;
    private final ModelRenderer main_panel_4;
    private final ModelRenderer deviders4;
    private final ModelRenderer lower_roundel_4;
    private final ModelRenderer upper_roundel_4;
    private final ModelRenderer panel5;
    private final ModelRenderer main_panel_5;
    private final ModelRenderer deviders5;
    private final ModelRenderer upper_roundel_5;
    private final ModelRenderer mid_roundel_2;
    private final ModelRenderer lower_roundel_5;
    private final ModelRenderer panel6;
    private final ModelRenderer main_panel_6;
    private final ModelRenderer deviders6;
    private final ModelRenderer lower_roundel_6;
    private final ModelRenderer upper_roundel_6;
    private final ModelRenderer center;
    private final ModelRenderer door_jam;
    private final ModelRenderer frames;
    private final ModelRenderer front_frame;
    private final ModelRenderer cap_1;
    private final ModelRenderer head_nub_1;
    private final ModelRenderer head_1;
    private final ModelRenderer spine_1;
    private final ModelRenderer upper_peak_1;
    private final ModelRenderer base_1;
    private final ModelRenderer foot_nub_1;
    private final ModelRenderer head_2;
    private final ModelRenderer spine_2;
    private final ModelRenderer lower_peak_1;
    private final ModelRenderer lamp_trim;
    private final ModelRenderer frame2;
    private final ModelRenderer cap_2;
    private final ModelRenderer head_nub_2;
    private final ModelRenderer head_3;
    private final ModelRenderer spine_3;
    private final ModelRenderer upper_peak_2;
    private final ModelRenderer base_2;
    private final ModelRenderer foot_nub_2;
    private final ModelRenderer head_4;
    private final ModelRenderer spine_4;
    private final ModelRenderer lower_peak_2;
    private final ModelRenderer lamp_trim2;
    private final ModelRenderer frame3;
    private final ModelRenderer cap_3;
    private final ModelRenderer head_nub_3;
    private final ModelRenderer head_5;
    private final ModelRenderer spine_5;
    private final ModelRenderer upper_peak_3;
    private final ModelRenderer base_3;
    private final ModelRenderer foot_nub_3;
    private final ModelRenderer head_6;
    private final ModelRenderer spine_6;
    private final ModelRenderer lower_peak_3;
    private final ModelRenderer lamp_trim3;
    private final ModelRenderer frame4;
    private final ModelRenderer cap_4;
    private final ModelRenderer head_nub_4;
    private final ModelRenderer head_7;
    private final ModelRenderer spine_7;
    private final ModelRenderer upper_peak_4;
    private final ModelRenderer base_4;
    private final ModelRenderer foot_nub_4;
    private final ModelRenderer head_8;
    private final ModelRenderer spine_8;
    private final ModelRenderer lower_peak_4;
    private final ModelRenderer lamp_trim4;
    private final ModelRenderer frame5;
    private final ModelRenderer cap_5;
    private final ModelRenderer head_nub_5;
    private final ModelRenderer head_9;
    private final ModelRenderer spine_9;
    private final ModelRenderer upper_peak_5;
    private final ModelRenderer base_5;
    private final ModelRenderer foot_nub_5;
    private final ModelRenderer head_10;
    private final ModelRenderer spine_10;
    private final ModelRenderer lower_peak_5;
    private final ModelRenderer lamp_trim5;
    private final ModelRenderer frame6;
    private final ModelRenderer cap_6;
    private final ModelRenderer head_nub_6;
    private final ModelRenderer head_11;
    private final ModelRenderer spine_11;
    private final ModelRenderer upper_peak_6;
    private final ModelRenderer base_6;
    private final ModelRenderer foot_nub_6;
    private final ModelRenderer head_12;
    private final ModelRenderer spine_12;
    private final ModelRenderer lower_peak_6;
    private final ModelRenderer lamp_trim6;
    private final ModelRenderer boti;

    public TT2020ExteriorModel() {
        textureWidth = 512;
        textureHeight = 512;

        glow = new LightModelRenderer(this);
        glow.setRotationPoint(0.0F, 24.0F, 1.0F);
        

        glow_plate_2 = new ModelRenderer(this);
        glow_plate_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow.addChild(glow_plate_2);
        setRotationAngle(glow_plate_2, 0.0F, -1.0472F, 0.0F);
        glow_plate_2.setTextureOffset(248, 110).addBox(-24.0F, -74.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);
        glow_plate_2.setTextureOffset(248, 110).addBox(-24.0F, -122.0F, -47.0F, 48.0F, 48.0F, 1.0F, 0.0F, false);
        glow_plate_2.setTextureOffset(248, 110).addBox(-24.0F, -186.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);

        glow_plate_3 = new ModelRenderer(this);
        glow_plate_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow.addChild(glow_plate_3);
        setRotationAngle(glow_plate_3, 0.0F, -2.0944F, 0.0F);
        glow_plate_3.setTextureOffset(248, 110).addBox(-24.0F, -74.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);
        glow_plate_3.setTextureOffset(248, 110).addBox(-24.0F, -122.0F, -47.0F, 48.0F, 48.0F, 1.0F, 0.0F, false);
        glow_plate_3.setTextureOffset(248, 110).addBox(-24.0F, -186.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);

        glow_plate_4 = new ModelRenderer(this);
        glow_plate_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow.addChild(glow_plate_4);
        setRotationAngle(glow_plate_4, 0.0F, 3.1416F, 0.0F);
        glow_plate_4.setTextureOffset(248, 110).addBox(-23.0F, -74.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);
        glow_plate_4.setTextureOffset(248, 110).addBox(-23.0F, -122.0F, -47.0F, 48.0F, 48.0F, 1.0F, 0.0F, false);
        glow_plate_4.setTextureOffset(248, 110).addBox(-23.0F, -186.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);

        glow_plate_5 = new ModelRenderer(this);
        glow_plate_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow.addChild(glow_plate_5);
        setRotationAngle(glow_plate_5, 0.0F, 2.0944F, 0.0F);
        glow_plate_5.setTextureOffset(248, 110).addBox(-22.0F, -74.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);
        glow_plate_5.setTextureOffset(248, 110).addBox(-22.0F, -122.0F, -47.0F, 48.0F, 48.0F, 1.0F, 0.0F, false);
        glow_plate_5.setTextureOffset(248, 110).addBox(-22.0F, -186.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);

        glow_plate_6 = new ModelRenderer(this);
        glow_plate_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        glow.addChild(glow_plate_6);
        setRotationAngle(glow_plate_6, 0.0F, 1.0472F, 0.0F);
        glow_plate_6.setTextureOffset(248, 110).addBox(-22.0F, -74.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);
        glow_plate_6.setTextureOffset(248, 110).addBox(-22.0F, -122.0F, -47.0F, 48.0F, 48.0F, 1.0F, 0.0F, false);
        glow_plate_6.setTextureOffset(248, 110).addBox(-22.0F, -186.0F, -47.0F, 48.0F, 64.0F, 1.0F, 0.0F, false);

        door = new ModelRenderer(this);
        door.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        door_west_rotate_y = new ModelRenderer(this);
        door_west_rotate_y.setRotationPoint(-60.0F, -72.0F, -30.0F);
        door.addChild(door_west_rotate_y);
        door_west_rotate_y.setTextureOffset(397, 249).addBox(36.0F, 24.0F, -19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(397, 206).addBox(36.0F, -20.0F, -19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(397, 163).addBox(36.0F, -64.0F, -19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(56, 105).addBox(38.0F, -62.0F, -15.0F, 20.0F, 42.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(56, 105).addBox(38.0F, 24.0F, -15.0F, 20.0F, 42.0F, 4.0F, 0.0F, false);
        door_west_rotate_y.setTextureOffset(56, 105).addBox(38.0F, -20.0F, -15.0F, 20.0F, 44.0F, 4.0F, 0.0F, false);

        door_dot_west_a = new ModelRenderer(this);
        door_dot_west_a.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_a);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-18.0F, -130.0F, -48.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-17.0F, -131.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-19.0F, -129.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-6.0F, -129.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-5.0F, -127.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-20.0F, -127.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-17.0F, -118.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-15.0F, -117.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_a.setTextureOffset(288, 98).addBox(-15.0F, -132.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_west_b = new ModelRenderer(this);
        door_dot_west_b.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_b);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-18.0F, -103.0F, -48.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-17.0F, -104.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-19.0F, -102.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-6.0F, -102.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-5.0F, -100.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-20.0F, -100.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-17.0F, -91.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-15.0F, -90.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_b.setTextureOffset(288, 98).addBox(-15.0F, -105.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_west_c = new ModelRenderer(this);
        door_dot_west_c.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_c);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-18.0F, -76.0F, -48.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-17.0F, -77.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-19.0F, -75.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-6.0F, -75.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-5.0F, -73.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-20.0F, -73.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-17.0F, -64.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-15.0F, -63.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_c.setTextureOffset(288, 98).addBox(-15.0F, -78.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_west_d = new ModelRenderer(this);
        door_dot_west_d.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_d);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-18.0F, -49.0F, -48.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-17.0F, -50.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-19.0F, -48.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-6.0F, -48.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-5.0F, -46.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-20.0F, -46.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-17.0F, -37.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-15.0F, -36.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_d.setTextureOffset(288, 98).addBox(-15.0F, -51.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_west_e = new ModelRenderer(this);
        door_dot_west_e.setRotationPoint(60.0F, 72.0F, 30.0F);
        door_west_rotate_y.addChild(door_dot_west_e);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-18.0F, -22.0F, -48.0F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-17.0F, -23.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-19.0F, -21.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-6.0F, -21.0F, -48.0F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-5.0F, -19.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-20.0F, -19.0F, -48.0F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-17.0F, -10.0F, -48.0F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-15.0F, -9.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_west_e.setTextureOffset(288, 98).addBox(-15.0F, -24.0F, -48.0F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_east_rotate_y = new ModelRenderer(this);
        door_east_rotate_y.setRotationPoint(60.0F, -72.0F, -30.0F);
        door.addChild(door_east_rotate_y);
        door_east_rotate_y.setTextureOffset(422, 249).addBox(-60.0F, 24.0F, -19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(419, 207).addBox(-60.0F, -20.0F, -19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(422, 163).addBox(-60.0F, -64.0F, -19.0F, 24.0F, 44.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(56, 105).addBox(-58.0F, -62.0F, -15.0F, 20.0F, 42.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(56, 105).addBox(-58.0F, 24.0F, -15.0F, 20.0F, 42.0F, 4.0F, 0.0F, false);
        door_east_rotate_y.setTextureOffset(56, 105).addBox(-58.0F, -20.0F, -15.0F, 20.0F, 44.0F, 4.0F, 0.0F, false);

        door_dot_east_a = new ModelRenderer(this);
        door_dot_east_a.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_a);
        door_dot_east_a.setTextureOffset(288, 98).addBox(5.9734F, -130.0F, -48.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(6.9734F, -131.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(4.9734F, -129.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(17.9734F, -129.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(18.9734F, -127.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(3.9734F, -127.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(6.9734F, -118.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(8.9734F, -117.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_a.setTextureOffset(288, 98).addBox(8.9734F, -132.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_east_b = new ModelRenderer(this);
        door_dot_east_b.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_b);
        door_dot_east_b.setTextureOffset(288, 98).addBox(5.9734F, -103.0F, -48.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(6.9734F, -104.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(4.9734F, -102.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(17.9734F, -102.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(18.9734F, -100.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(3.9734F, -100.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(6.9734F, -91.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(8.9734F, -90.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_b.setTextureOffset(288, 98).addBox(8.9734F, -105.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_east_c = new ModelRenderer(this);
        door_dot_east_c.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_c);
        door_dot_east_c.setTextureOffset(288, 98).addBox(5.9734F, -76.0F, -48.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(6.9734F, -77.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(4.9734F, -75.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(17.9734F, -75.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(18.9734F, -73.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(3.9734F, -73.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(6.9734F, -64.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(8.9734F, -63.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_c.setTextureOffset(288, 98).addBox(8.9734F, -78.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_east_d = new ModelRenderer(this);
        door_dot_east_d.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_d);
        door_dot_east_d.setTextureOffset(288, 98).addBox(5.9734F, -49.0F, -48.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(6.9734F, -50.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(4.9734F, -48.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(17.9734F, -48.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(18.9734F, -46.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(3.9734F, -46.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(6.9734F, -37.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(8.9734F, -36.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_d.setTextureOffset(288, 98).addBox(8.9734F, -51.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        door_dot_east_e = new ModelRenderer(this);
        door_dot_east_e.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
        door_east_rotate_y.addChild(door_dot_east_e);
        door_dot_east_e.setTextureOffset(288, 98).addBox(5.9734F, -22.0F, -48.6101F, 12.0F, 12.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(6.9734F, -23.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(4.9734F, -21.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(17.9734F, -21.0F, -48.6101F, 1.0F, 10.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(18.9734F, -19.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(3.9734F, -19.0F, -48.6101F, 1.0F, 6.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(6.9734F, -10.0F, -48.6101F, 10.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(8.9734F, -9.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);
        door_dot_east_e.setTextureOffset(288, 98).addBox(8.9734F, -24.0F, -48.6101F, 6.0F, 1.0F, 8.0F, 0.0F, false);

        side_walls = new ModelRenderer(this);
        side_walls.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        front = new ModelRenderer(this);
        front.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_walls.addChild(front);
        

        door_frame = new ModelRenderer(this);
        door_frame.setRotationPoint(0.0F, 0.0F, 0.0F);
        front.addChild(door_frame);
        

        frame = new ModelRenderer(this);
        frame.setRotationPoint(0.0F, 0.0F, 0.0F);
        door_frame.addChild(frame);
        frame.setTextureOffset(421, 227).addBox(-28.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        frame.setTextureOffset(470, 228).addBox(24.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        frame.setTextureOffset(424, 111).addBox(24.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        frame.setTextureOffset(424, 107).addBox(-28.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        frame.setTextureOffset(424, 163).addBox(-28.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        frame.setTextureOffset(424, 176).addBox(24.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        frame.setTextureOffset(370, 302).addBox(-24.0F, -4.0F, -49.0F, 48.0F, 4.0F, 4.0F, 0.0F, false);
        frame.setTextureOffset(372, 107).addBox(-24.0F, -194.0F, -49.0F, 48.0F, 58.0F, 6.0F, 0.0F, false);

        panel2 = new ModelRenderer(this);
        panel2.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_walls.addChild(panel2);
        setRotationAngle(panel2, 0.0F, -1.0472F, 0.0F);
        

        main_panel_2 = new ModelRenderer(this);
        main_panel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        panel2.addChild(main_panel_2);
        

        deviders2 = new ModelRenderer(this);
        deviders2.setRotationPoint(0.0F, 0.0F, 0.0F);
        main_panel_2.addChild(deviders2);
        deviders2.setTextureOffset(455, 220).addBox(25.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(424, 104).addBox(25.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(370, 109).addBox(-27.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(370, 165).addBox(-27.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(424, 171).addBox(25.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(370, 285).addBox(-23.0F, -12.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(455, 220).addBox(-27.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(380, 99).addBox(-23.0F, -194.0F, -49.0F, 48.0F, 14.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(370, 177).addBox(5.0F, -132.0F, -49.0F, 20.0F, 72.0F, 4.0F, 0.0F, false);
        deviders2.setTextureOffset(370, 171).addBox(-23.0F, -132.0F, -49.0F, 20.0F, 72.0F, 4.0F, 0.0F, false);

        lower_roundel_2 = new ModelRenderer(this);
        lower_roundel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders2.addChild(lower_roundel_2);
        lower_roundel_2.setTextureOffset(370, 249).addBox(9.0F, -60.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(370, 249).addBox(-23.0F, -60.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(370, 288).addBox(-23.0F, -16.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(445, 272).addBox(9.0F, -16.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(424, 203).addBox(21.0F, -56.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(370, 249).addBox(-23.0F, -56.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(370, 276).addBox(-23.0F, -28.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(445, 261).addBox(21.0F, -28.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(445, 272).addBox(17.0F, -20.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(424, 203).addBox(17.0F, -56.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(370, 249).addBox(-19.0F, -56.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_2.setTextureOffset(370, 288).addBox(-19.0F, -20.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        upper_roundel_2 = new ModelRenderer(this);
        upper_roundel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders2.addChild(upper_roundel_2);
        upper_roundel_2.setTextureOffset(424, 108).addBox(9.0F, -180.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(424, 104).addBox(-23.0F, -180.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(370, 171).addBox(-23.0F, -136.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(370, 171).addBox(9.0F, -136.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(449, 106).addBox(21.0F, -176.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(406, 104).addBox(-23.0F, -176.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(370, 171).addBox(-23.0F, -148.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(370, 171).addBox(21.0F, -148.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(370, 171).addBox(17.0F, -140.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(424, 110).addBox(17.0F, -176.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(424, 104).addBox(-19.0F, -176.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_2.setTextureOffset(370, 171).addBox(-19.0F, -140.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        panel3 = new ModelRenderer(this);
        panel3.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_walls.addChild(panel3);
        setRotationAngle(panel3, 0.0F, -2.0944F, 0.0F);
        

        main_panel_3 = new ModelRenderer(this);
        main_panel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        panel3.addChild(main_panel_3);
        

        deviders3 = new ModelRenderer(this);
        deviders3.setRotationPoint(0.0F, 0.0F, 0.0F);
        main_panel_3.addChild(deviders3);
        deviders3.setTextureOffset(448, 130).addBox(25.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(455, 243).addBox(25.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(370, 302).addBox(-23.0F, -12.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(381, 128).addBox(-27.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(455, 236).addBox(-27.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(424, 172).addBox(-27.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(424, 184).addBox(25.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(380, 133).addBox(-23.0F, -194.0F, -49.0F, 48.0F, 14.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(367, 171).addBox(-23.0F, -156.0F, -49.0F, 20.0F, 36.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(370, 250).addBox(-23.0F, -72.0F, -49.0F, 20.0F, 36.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(428, 232).addBox(5.0F, -72.0F, -49.0F, 20.0F, 36.0F, 4.0F, 0.0F, false);
        deviders3.setTextureOffset(411, 171).addBox(5.0F, -156.0F, -49.0F, 20.0F, 36.0F, 4.0F, 0.0F, false);

        upper_roundel_3 = new ModelRenderer(this);
        upper_roundel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders3.addChild(upper_roundel_3);
        upper_roundel_3.setTextureOffset(406, 166).addBox(-23.0F, -160.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_3.setTextureOffset(411, 171).addBox(9.0F, -160.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_3.setTextureOffset(406, 166).addBox(-23.0F, -172.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_3.setTextureOffset(451, 158).addBox(21.0F, -172.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_3.setTextureOffset(411, 171).addBox(17.0F, -164.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_3.setTextureOffset(406, 166).addBox(-19.0F, -164.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        mid_roundel_3 = new ModelRenderer(this);
        mid_roundel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders3.addChild(mid_roundel_3);
        mid_roundel_3.setTextureOffset(424, 199).addBox(9.0F, -120.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 196).addBox(-23.0F, -120.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 214).addBox(-23.0F, -76.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 217).addBox(9.0F, -76.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 199).addBox(21.0F, -116.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 199).addBox(-23.0F, -116.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 217).addBox(-23.0F, -88.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 217).addBox(21.0F, -88.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 219).addBox(17.0F, -80.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 199).addBox(17.0F, -116.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 207).addBox(-19.0F, -116.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_3.setTextureOffset(424, 215).addBox(-19.0F, -80.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        lower_roundel_3 = new ModelRenderer(this);
        lower_roundel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders3.addChild(lower_roundel_3);
        lower_roundel_3.setTextureOffset(428, 266).addBox(9.0F, -36.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_3.setTextureOffset(370, 284).addBox(-23.0F, -36.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_3.setTextureOffset(428, 267).addBox(21.0F, -32.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_3.setTextureOffset(379, 285).addBox(-23.0F, -32.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_3.setTextureOffset(428, 267).addBox(17.0F, -32.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_3.setTextureOffset(381, 284).addBox(-19.0F, -32.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        panel4 = new ModelRenderer(this);
        panel4.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_walls.addChild(panel4);
        setRotationAngle(panel4, 0.0F, 3.1416F, 0.0F);
        

        main_panel_4 = new ModelRenderer(this);
        main_panel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        panel4.addChild(main_panel_4);
        

        deviders4 = new ModelRenderer(this);
        deviders4.setRotationPoint(0.0F, 0.0F, 0.0F);
        main_panel_4.addChild(deviders4);
        deviders4.setTextureOffset(455, 220).addBox(25.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(424, 104).addBox(25.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(370, 109).addBox(-27.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(370, 165).addBox(-27.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(424, 171).addBox(25.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(370, 285).addBox(-23.0F, -12.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(455, 220).addBox(-27.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(380, 99).addBox(-23.0F, -194.0F, -49.0F, 48.0F, 14.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(370, 177).addBox(5.0F, -132.0F, -49.0F, 20.0F, 72.0F, 4.0F, 0.0F, false);
        deviders4.setTextureOffset(370, 171).addBox(-23.0F, -132.0F, -49.0F, 20.0F, 72.0F, 4.0F, 0.0F, false);

        lower_roundel_4 = new ModelRenderer(this);
        lower_roundel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders4.addChild(lower_roundel_4);
        lower_roundel_4.setTextureOffset(370, 249).addBox(9.0F, -60.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(370, 249).addBox(-23.0F, -60.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(370, 288).addBox(-23.0F, -16.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(445, 272).addBox(9.0F, -16.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(424, 203).addBox(21.0F, -56.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(370, 249).addBox(-23.0F, -56.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(370, 276).addBox(-23.0F, -28.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(445, 261).addBox(21.0F, -28.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(445, 272).addBox(17.0F, -20.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(424, 203).addBox(17.0F, -56.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(370, 249).addBox(-19.0F, -56.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_4.setTextureOffset(370, 288).addBox(-19.0F, -20.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        upper_roundel_4 = new ModelRenderer(this);
        upper_roundel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders4.addChild(upper_roundel_4);
        upper_roundel_4.setTextureOffset(424, 108).addBox(9.0F, -180.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(424, 104).addBox(-23.0F, -180.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(370, 171).addBox(-23.0F, -136.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(370, 171).addBox(9.0F, -136.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(449, 106).addBox(21.0F, -176.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(406, 104).addBox(-23.0F, -176.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(370, 171).addBox(-23.0F, -148.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(370, 171).addBox(21.0F, -148.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(370, 171).addBox(17.0F, -140.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(424, 110).addBox(17.0F, -176.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(424, 104).addBox(-19.0F, -176.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_4.setTextureOffset(370, 171).addBox(-19.0F, -140.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        panel5 = new ModelRenderer(this);
        panel5.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_walls.addChild(panel5);
        setRotationAngle(panel5, 0.0F, 2.0944F, 0.0F);
        

        main_panel_5 = new ModelRenderer(this);
        main_panel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        panel5.addChild(main_panel_5);
        

        deviders5 = new ModelRenderer(this);
        deviders5.setRotationPoint(0.0F, 0.0F, 0.0F);
        main_panel_5.addChild(deviders5);
        deviders5.setTextureOffset(448, 130).addBox(25.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(455, 243).addBox(25.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(370, 302).addBox(-23.0F, -12.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(381, 128).addBox(-27.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(455, 236).addBox(-27.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(424, 172).addBox(-27.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(424, 184).addBox(25.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(380, 133).addBox(-23.0F, -194.0F, -49.0F, 48.0F, 14.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(367, 171).addBox(-23.0F, -156.0F, -49.0F, 20.0F, 36.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(370, 250).addBox(-23.0F, -72.0F, -49.0F, 20.0F, 36.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(428, 232).addBox(5.0F, -72.0F, -49.0F, 20.0F, 36.0F, 4.0F, 0.0F, false);
        deviders5.setTextureOffset(411, 171).addBox(5.0F, -156.0F, -49.0F, 20.0F, 36.0F, 4.0F, 0.0F, false);

        upper_roundel_5 = new ModelRenderer(this);
        upper_roundel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders5.addChild(upper_roundel_5);
        upper_roundel_5.setTextureOffset(406, 166).addBox(-23.0F, -160.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_5.setTextureOffset(411, 171).addBox(9.0F, -160.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_5.setTextureOffset(406, 166).addBox(-23.0F, -172.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_5.setTextureOffset(451, 158).addBox(21.0F, -172.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_5.setTextureOffset(411, 171).addBox(17.0F, -164.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_5.setTextureOffset(406, 166).addBox(-19.0F, -164.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        mid_roundel_2 = new ModelRenderer(this);
        mid_roundel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders5.addChild(mid_roundel_2);
        mid_roundel_2.setTextureOffset(424, 199).addBox(9.0F, -120.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 196).addBox(-23.0F, -120.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 214).addBox(-23.0F, -76.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 217).addBox(9.0F, -76.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 199).addBox(21.0F, -116.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 199).addBox(-23.0F, -116.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 217).addBox(-23.0F, -88.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 217).addBox(21.0F, -88.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 219).addBox(17.0F, -80.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 199).addBox(17.0F, -116.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 207).addBox(-19.0F, -116.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        mid_roundel_2.setTextureOffset(424, 215).addBox(-19.0F, -80.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        lower_roundel_5 = new ModelRenderer(this);
        lower_roundel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders5.addChild(lower_roundel_5);
        lower_roundel_5.setTextureOffset(428, 266).addBox(9.0F, -36.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_5.setTextureOffset(370, 284).addBox(-23.0F, -36.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_5.setTextureOffset(428, 267).addBox(21.0F, -32.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_5.setTextureOffset(379, 285).addBox(-23.0F, -32.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_5.setTextureOffset(428, 267).addBox(17.0F, -32.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_5.setTextureOffset(381, 284).addBox(-19.0F, -32.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        panel6 = new ModelRenderer(this);
        panel6.setRotationPoint(0.0F, 0.0F, 0.0F);
        side_walls.addChild(panel6);
        setRotationAngle(panel6, 0.0F, 1.0472F, 0.0F);
        

        main_panel_6 = new ModelRenderer(this);
        main_panel_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        panel6.addChild(main_panel_6);
        

        deviders6 = new ModelRenderer(this);
        deviders6.setRotationPoint(0.0F, 0.0F, 0.0F);
        main_panel_6.addChild(deviders6);
        deviders6.setTextureOffset(455, 220).addBox(25.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(424, 104).addBox(25.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(370, 109).addBox(-27.0F, -194.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(370, 165).addBox(-27.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(424, 171).addBox(25.0F, -130.0F, -49.0F, 4.0F, 66.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(370, 285).addBox(-23.0F, -12.0F, -49.0F, 48.0F, 12.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(455, 220).addBox(-27.0F, -64.0F, -49.0F, 4.0F, 64.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(380, 99).addBox(-23.0F, -194.0F, -49.0F, 48.0F, 14.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(370, 177).addBox(5.0F, -132.0F, -49.0F, 20.0F, 72.0F, 4.0F, 0.0F, false);
        deviders6.setTextureOffset(370, 171).addBox(-23.0F, -132.0F, -49.0F, 20.0F, 72.0F, 4.0F, 0.0F, false);

        lower_roundel_6 = new ModelRenderer(this);
        lower_roundel_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders6.addChild(lower_roundel_6);
        lower_roundel_6.setTextureOffset(370, 249).addBox(9.0F, -60.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(370, 249).addBox(-23.0F, -60.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(370, 288).addBox(-23.0F, -16.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(445, 272).addBox(9.0F, -16.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(424, 203).addBox(21.0F, -56.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(370, 249).addBox(-23.0F, -56.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(370, 276).addBox(-23.0F, -28.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(445, 261).addBox(21.0F, -28.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(445, 272).addBox(17.0F, -20.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(424, 203).addBox(17.0F, -56.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(370, 249).addBox(-19.0F, -56.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        lower_roundel_6.setTextureOffset(370, 288).addBox(-19.0F, -20.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        upper_roundel_6 = new ModelRenderer(this);
        upper_roundel_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        deviders6.addChild(upper_roundel_6);
        upper_roundel_6.setTextureOffset(424, 108).addBox(9.0F, -180.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(424, 104).addBox(-23.0F, -180.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(370, 171).addBox(-23.0F, -136.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(370, 171).addBox(9.0F, -136.0F, -49.0F, 16.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(449, 106).addBox(21.0F, -176.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(406, 104).addBox(-23.0F, -176.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(370, 171).addBox(-23.0F, -148.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(370, 171).addBox(21.0F, -148.0F, -49.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(370, 171).addBox(17.0F, -140.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(424, 110).addBox(17.0F, -176.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(424, 104).addBox(-19.0F, -176.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);
        upper_roundel_6.setTextureOffset(370, 171).addBox(-19.0F, -140.0F, -49.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

        center = new ModelRenderer(this);
        center.setRotationPoint(0.0F, 24.0F, 0.0F);
        center.setTextureOffset(49, 43).addBox(-4.0F, -218.0F, -4.5F, 8.0F, 8.0F, 8.0F, 0.0F, false);
        center.setTextureOffset(51, 64).addBox(-4.0F, 13.6F, -4.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);

        door_jam = new ModelRenderer(this);
        door_jam.setRotationPoint(0.0F, 24.0F, 0.0F);
        door_jam.setTextureOffset(64, 57).addBox(22.0F, -69.0F, -42.0F, 4.0F, 64.0F, 12.0F, 0.0F, false);
        door_jam.setTextureOffset(64, 57).addBox(22.0F, -133.0F, -42.0F, 4.0F, 64.0F, 12.0F, 0.0F, false);
        door_jam.setTextureOffset(64, 57).addBox(-26.0F, -69.0F, -42.0F, 4.0F, 64.0F, 12.0F, 0.0F, false);
        door_jam.setTextureOffset(64, 57).addBox(-26.0F, -133.0F, -42.0F, 4.0F, 64.0F, 12.0F, 0.0F, false);
        door_jam.setTextureOffset(34, 116).addBox(0.0F, -141.0F, -42.0F, 26.0F, 8.0F, 12.0F, 0.0F, false);
        door_jam.setTextureOffset(34, 116).addBox(-26.0F, -141.0F, -42.0F, 26.0F, 8.0F, 12.0F, 0.0F, false);
        door_jam.setTextureOffset(34, 76).addBox(0.0F, -5.0F, -46.0F, 26.0F, 4.0F, 16.0F, 0.0F, false);
        door_jam.setTextureOffset(39, 103).addBox(-26.0F, -5.0F, -46.0F, 26.0F, 4.0F, 16.0F, 0.0F, false);

        frames = new ModelRenderer(this);
        frames.setRotationPoint(0.0F, 24.0F, 0.0F);
        

        front_frame = new ModelRenderer(this);
        front_frame.setRotationPoint(0.0F, 0.0F, 0.0F);
        frames.addChild(front_frame);
        

        cap_1 = new ModelRenderer(this);
        cap_1.setRotationPoint(-0.2F, -193.0F, 0.0F);
        front_frame.addChild(cap_1);
        

        head_nub_1 = new ModelRenderer(this);
        head_nub_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_1.addChild(head_nub_1);
        setRotationAngle(head_nub_1, 0.0F, -0.5236F, 0.0F);
        head_nub_1.setTextureOffset(455, 155).addBox(-2.8F, -3.4F, -58.4F, 7.0F, 10.0F, 10.0F, 0.0F, false);
        head_nub_1.setTextureOffset(77, 124).addBox(-2.0F, -22.9F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        head_nub_1.setTextureOffset(80, 102).addBox(-1.0F, -22.9F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_1 = new ModelRenderer(this);
        head_1.setRotationPoint(1.2F, -0.4F, -58.4F);
        head_nub_1.addChild(head_1);
        setRotationAngle(head_1, -1.2217F, 0.0F, 0.0F);
        head_1.setTextureOffset(455, 105).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_1 = new ModelRenderer(this);
        spine_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_1.addChild(spine_1);
        setRotationAngle(spine_1, 0.0F, -0.5236F, 0.0F);
        spine_1.setTextureOffset(455, 108).addBox(-0.8F, 6.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);
        spine_1.setTextureOffset(455, 169).addBox(-0.8F, 70.6F, -57.4F, 3.0F, 48.0F, 9.0F, 0.0F, false);
        spine_1.setTextureOffset(455, 220).addBox(-0.8F, 118.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);

        upper_peak_1 = new ModelRenderer(this);
        upper_peak_1.setRotationPoint(0.0F, -1.0F, -46.0F);
        cap_1.addChild(upper_peak_1);
        setRotationAngle(upper_peak_1, 0.4363F, 0.0F, 0.0F);
        upper_peak_1.setTextureOffset(368, 134).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 11.0F, 0.0F, false);
        upper_peak_1.setTextureOffset(381, 117).addBox(-19.0F, -1.0F, 9.0F, 40.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_1.setTextureOffset(379, 111).addBox(-15.5F, -1.0F, 16.0F, 33.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_1.setTextureOffset(391, 103).addBox(-12.0F, -1.0F, 23.0F, 26.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_1.setTextureOffset(400, 72).addBox(-8.0F, -1.0F, 30.0F, 18.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_1.setTextureOffset(405, 91).addBox(-5.0F, -1.0F, 37.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_1.setTextureOffset(286, 137).addBox(-3.0F, -2.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        base_1 = new ModelRenderer(this);
        base_1.setRotationPoint(-0.2F, 1.0F, 0.0F);
        front_frame.addChild(base_1);
        

        foot_nub_1 = new ModelRenderer(this);
        foot_nub_1.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_1.addChild(foot_nub_1);
        setRotationAngle(foot_nub_1, 0.0F, -0.5236F, 0.0F);
        foot_nub_1.setTextureOffset(455, 277).addBox(-2.8F, -11.4F, -58.4F, 7.0F, 13.0F, 10.0F, 0.0F, false);
        foot_nub_1.setTextureOffset(77, 124).addBox(-2.0F, 13.1F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        foot_nub_1.setTextureOffset(80, 102).addBox(-1.0F, 14.1F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_2 = new ModelRenderer(this);
        head_2.setRotationPoint(1.2F, -0.4F, -58.4F);
        foot_nub_1.addChild(head_2);
        setRotationAngle(head_2, -1.9199F, 0.0F, 0.0F);
        head_2.setTextureOffset(455, 228).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_2 = new ModelRenderer(this);
        spine_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_1.addChild(spine_2);
        setRotationAngle(spine_2, 0.0F, -0.5236F, 0.0F);
        

        lower_peak_1 = new ModelRenderer(this);
        lower_peak_1.setRotationPoint(0.0F, -1.0F, -46.0F);
        base_1.addChild(lower_peak_1);
        setRotationAngle(lower_peak_1, -0.4363F, 0.0F, 0.0F);
        lower_peak_1.setTextureOffset(368, 270).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 10.0F, 0.0F, false);
        lower_peak_1.setTextureOffset(380, 264).addBox(-20.0F, -1.0F, 8.0F, 41.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_1.setTextureOffset(380, 258).addBox(-16.5F, -1.0F, 15.0F, 34.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_1.setTextureOffset(380, 254).addBox(-13.0F, -1.0F, 22.0F, 27.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_1.setTextureOffset(380, 252).addBox(-9.0F, -1.0F, 29.0F, 19.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_1.setTextureOffset(380, 252).addBox(-5.0F, -1.0F, 36.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_1.setTextureOffset(286, 137).addBox(-3.0F, -1.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        lamp_trim = new ModelRenderer(this);
        lamp_trim.setRotationPoint(0.0F, 0.0F, 0.0F);
        front_frame.addChild(lamp_trim);
        lamp_trim.setTextureOffset(295, 118).addBox(-3.0F, -215.25F, -7.4F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        lamp_trim.setTextureOffset(280, 97).addBox(-2.0F, 17.25F, -7.4F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        frame2 = new ModelRenderer(this);
        frame2.setRotationPoint(0.0F, 0.0F, 0.0F);
        frames.addChild(frame2);
        setRotationAngle(frame2, 0.0F, -1.0472F, 0.0F);
        

        cap_2 = new ModelRenderer(this);
        cap_2.setRotationPoint(-0.2F, -193.0F, 0.0F);
        frame2.addChild(cap_2);
        

        head_nub_2 = new ModelRenderer(this);
        head_nub_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_2.addChild(head_nub_2);
        setRotationAngle(head_nub_2, 0.0F, -0.5236F, 0.0F);
        head_nub_2.setTextureOffset(455, 155).addBox(-2.8F, -3.4F, -58.4F, 7.0F, 10.0F, 10.0F, 0.0F, false);
        head_nub_2.setTextureOffset(77, 124).addBox(-2.0F, -22.9F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        head_nub_2.setTextureOffset(80, 102).addBox(-1.0F, -22.9F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_3 = new ModelRenderer(this);
        head_3.setRotationPoint(1.2F, -0.4F, -58.4F);
        head_nub_2.addChild(head_3);
        setRotationAngle(head_3, -1.2217F, 0.0F, 0.0F);
        head_3.setTextureOffset(455, 105).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_3 = new ModelRenderer(this);
        spine_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_2.addChild(spine_3);
        setRotationAngle(spine_3, 0.0F, -0.5236F, 0.0F);
        spine_3.setTextureOffset(455, 108).addBox(-0.8F, 6.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);
        spine_3.setTextureOffset(455, 169).addBox(-0.8F, 70.6F, -57.4F, 3.0F, 48.0F, 9.0F, 0.0F, false);
        spine_3.setTextureOffset(455, 220).addBox(-0.8F, 118.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);

        upper_peak_2 = new ModelRenderer(this);
        upper_peak_2.setRotationPoint(0.0F, -1.0F, -46.0F);
        cap_2.addChild(upper_peak_2);
        setRotationAngle(upper_peak_2, 0.4363F, 0.0F, 0.0F);
        upper_peak_2.setTextureOffset(368, 134).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 11.0F, 0.0F, false);
        upper_peak_2.setTextureOffset(381, 117).addBox(-19.0F, -1.0F, 9.0F, 40.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_2.setTextureOffset(379, 111).addBox(-15.5F, -1.0F, 16.0F, 33.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_2.setTextureOffset(391, 103).addBox(-12.0F, -1.0F, 23.0F, 26.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_2.setTextureOffset(400, 72).addBox(-8.0F, -1.0F, 30.0F, 18.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_2.setTextureOffset(405, 91).addBox(-5.0F, -1.0F, 37.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_2.setTextureOffset(286, 137).addBox(-3.0F, -2.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        base_2 = new ModelRenderer(this);
        base_2.setRotationPoint(-0.2F, 1.0F, 0.0F);
        frame2.addChild(base_2);
        

        foot_nub_2 = new ModelRenderer(this);
        foot_nub_2.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_2.addChild(foot_nub_2);
        setRotationAngle(foot_nub_2, 0.0F, -0.5236F, 0.0F);
        foot_nub_2.setTextureOffset(455, 277).addBox(-2.8F, -11.4F, -58.4F, 7.0F, 13.0F, 10.0F, 0.0F, false);
        foot_nub_2.setTextureOffset(77, 124).addBox(-2.0F, 13.1F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        foot_nub_2.setTextureOffset(80, 102).addBox(-1.0F, 14.1F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_4 = new ModelRenderer(this);
        head_4.setRotationPoint(1.2F, -0.4F, -58.4F);
        foot_nub_2.addChild(head_4);
        setRotationAngle(head_4, -1.9199F, 0.0F, 0.0F);
        head_4.setTextureOffset(455, 228).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_4 = new ModelRenderer(this);
        spine_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_2.addChild(spine_4);
        setRotationAngle(spine_4, 0.0F, -0.5236F, 0.0F);
        

        lower_peak_2 = new ModelRenderer(this);
        lower_peak_2.setRotationPoint(0.0F, -1.0F, -46.0F);
        base_2.addChild(lower_peak_2);
        setRotationAngle(lower_peak_2, -0.4363F, 0.0F, 0.0F);
        lower_peak_2.setTextureOffset(368, 270).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 10.0F, 0.0F, false);
        lower_peak_2.setTextureOffset(380, 264).addBox(-20.0F, -1.0F, 8.0F, 41.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_2.setTextureOffset(380, 258).addBox(-16.5F, -1.0F, 15.0F, 34.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_2.setTextureOffset(380, 254).addBox(-13.0F, -1.0F, 22.0F, 27.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_2.setTextureOffset(380, 252).addBox(-9.0F, -1.0F, 29.0F, 19.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_2.setTextureOffset(380, 252).addBox(-5.0F, -1.0F, 36.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_2.setTextureOffset(286, 137).addBox(-3.0F, -1.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        lamp_trim2 = new ModelRenderer(this);
        lamp_trim2.setRotationPoint(0.0F, 0.0F, 0.0F);
        frame2.addChild(lamp_trim2);
        lamp_trim2.setTextureOffset(295, 118).addBox(-3.0F, -215.25F, -7.4F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        lamp_trim2.setTextureOffset(280, 97).addBox(-2.0F, 17.25F, -7.4F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        frame3 = new ModelRenderer(this);
        frame3.setRotationPoint(0.0F, 0.0F, 0.0F);
        frames.addChild(frame3);
        setRotationAngle(frame3, 0.0F, -2.0944F, 0.0F);
        

        cap_3 = new ModelRenderer(this);
        cap_3.setRotationPoint(-0.2F, -193.0F, 0.0F);
        frame3.addChild(cap_3);
        

        head_nub_3 = new ModelRenderer(this);
        head_nub_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_3.addChild(head_nub_3);
        setRotationAngle(head_nub_3, 0.0F, -0.5236F, 0.0F);
        head_nub_3.setTextureOffset(455, 155).addBox(-2.8F, -3.4F, -58.4F, 7.0F, 10.0F, 10.0F, 0.0F, false);
        head_nub_3.setTextureOffset(77, 124).addBox(-2.0F, -22.9F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        head_nub_3.setTextureOffset(80, 102).addBox(-1.0F, -22.9F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_5 = new ModelRenderer(this);
        head_5.setRotationPoint(1.2F, -0.4F, -58.4F);
        head_nub_3.addChild(head_5);
        setRotationAngle(head_5, -1.2217F, 0.0F, 0.0F);
        head_5.setTextureOffset(455, 105).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_5 = new ModelRenderer(this);
        spine_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_3.addChild(spine_5);
        setRotationAngle(spine_5, 0.0F, -0.5236F, 0.0F);
        spine_5.setTextureOffset(455, 108).addBox(-0.8F, 6.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);
        spine_5.setTextureOffset(455, 169).addBox(-0.8F, 70.6F, -57.4F, 3.0F, 48.0F, 9.0F, 0.0F, false);
        spine_5.setTextureOffset(455, 220).addBox(-0.8F, 118.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);

        upper_peak_3 = new ModelRenderer(this);
        upper_peak_3.setRotationPoint(0.0F, -1.0F, -46.0F);
        cap_3.addChild(upper_peak_3);
        setRotationAngle(upper_peak_3, 0.4363F, 0.0F, 0.0F);
        upper_peak_3.setTextureOffset(368, 134).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 11.0F, 0.0F, false);
        upper_peak_3.setTextureOffset(381, 117).addBox(-19.0F, -1.0F, 9.0F, 40.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_3.setTextureOffset(379, 111).addBox(-15.5F, -1.0F, 16.0F, 33.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_3.setTextureOffset(391, 103).addBox(-12.0F, -1.0F, 23.0F, 26.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_3.setTextureOffset(400, 72).addBox(-8.0F, -1.0F, 30.0F, 18.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_3.setTextureOffset(405, 91).addBox(-5.0F, -1.0F, 37.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_3.setTextureOffset(286, 137).addBox(-3.0F, -2.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        base_3 = new ModelRenderer(this);
        base_3.setRotationPoint(-0.2F, 1.0F, 0.0F);
        frame3.addChild(base_3);
        

        foot_nub_3 = new ModelRenderer(this);
        foot_nub_3.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_3.addChild(foot_nub_3);
        setRotationAngle(foot_nub_3, 0.0F, -0.5236F, 0.0F);
        foot_nub_3.setTextureOffset(455, 277).addBox(-2.8F, -11.4F, -58.4F, 7.0F, 13.0F, 10.0F, 0.0F, false);
        foot_nub_3.setTextureOffset(77, 124).addBox(-2.0F, 13.1F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        foot_nub_3.setTextureOffset(80, 102).addBox(-1.0F, 14.1F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_6 = new ModelRenderer(this);
        head_6.setRotationPoint(1.2F, -0.4F, -58.4F);
        foot_nub_3.addChild(head_6);
        setRotationAngle(head_6, -1.9199F, 0.0F, 0.0F);
        head_6.setTextureOffset(455, 228).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_6 = new ModelRenderer(this);
        spine_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_3.addChild(spine_6);
        setRotationAngle(spine_6, 0.0F, -0.5236F, 0.0F);
        

        lower_peak_3 = new ModelRenderer(this);
        lower_peak_3.setRotationPoint(0.0F, -1.0F, -46.0F);
        base_3.addChild(lower_peak_3);
        setRotationAngle(lower_peak_3, -0.4363F, 0.0F, 0.0F);
        lower_peak_3.setTextureOffset(368, 270).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 10.0F, 0.0F, false);
        lower_peak_3.setTextureOffset(380, 264).addBox(-20.0F, -1.0F, 8.0F, 41.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_3.setTextureOffset(380, 258).addBox(-16.5F, -1.0F, 15.0F, 34.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_3.setTextureOffset(380, 254).addBox(-13.0F, -1.0F, 22.0F, 27.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_3.setTextureOffset(380, 252).addBox(-9.0F, -1.0F, 29.0F, 19.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_3.setTextureOffset(380, 252).addBox(-5.0F, -1.0F, 36.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_3.setTextureOffset(286, 137).addBox(-3.0F, -1.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        lamp_trim3 = new ModelRenderer(this);
        lamp_trim3.setRotationPoint(0.0F, 0.0F, 0.0F);
        frame3.addChild(lamp_trim3);
        lamp_trim3.setTextureOffset(295, 118).addBox(-3.0F, -215.25F, -7.4F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        lamp_trim3.setTextureOffset(280, 97).addBox(-2.0F, 17.25F, -7.4F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        frame4 = new ModelRenderer(this);
        frame4.setRotationPoint(0.0F, 0.0F, 0.0F);
        frames.addChild(frame4);
        setRotationAngle(frame4, 0.0F, 3.1416F, 0.0F);
        

        cap_4 = new ModelRenderer(this);
        cap_4.setRotationPoint(-0.2F, -193.0F, 0.0F);
        frame4.addChild(cap_4);
        

        head_nub_4 = new ModelRenderer(this);
        head_nub_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_4.addChild(head_nub_4);
        setRotationAngle(head_nub_4, 0.0F, -0.5236F, 0.0F);
        head_nub_4.setTextureOffset(455, 155).addBox(-2.8F, -3.4F, -58.4F, 7.0F, 10.0F, 10.0F, 0.0F, false);
        head_nub_4.setTextureOffset(77, 124).addBox(-2.0F, -22.9F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        head_nub_4.setTextureOffset(80, 102).addBox(-1.0F, -22.9F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_7 = new ModelRenderer(this);
        head_7.setRotationPoint(1.2F, -0.4F, -58.4F);
        head_nub_4.addChild(head_7);
        setRotationAngle(head_7, -1.2217F, 0.0F, 0.0F);
        head_7.setTextureOffset(455, 105).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_7 = new ModelRenderer(this);
        spine_7.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_4.addChild(spine_7);
        setRotationAngle(spine_7, 0.0F, -0.5236F, 0.0F);
        spine_7.setTextureOffset(455, 108).addBox(-0.8F, 6.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);
        spine_7.setTextureOffset(455, 169).addBox(-0.8F, 70.6F, -57.4F, 3.0F, 48.0F, 9.0F, 0.0F, false);
        spine_7.setTextureOffset(455, 220).addBox(-0.8F, 118.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);

        upper_peak_4 = new ModelRenderer(this);
        upper_peak_4.setRotationPoint(0.0F, -1.0F, -46.0F);
        cap_4.addChild(upper_peak_4);
        setRotationAngle(upper_peak_4, 0.4363F, 0.0F, 0.0F);
        upper_peak_4.setTextureOffset(368, 134).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 11.0F, 0.0F, false);
        upper_peak_4.setTextureOffset(381, 117).addBox(-19.0F, -1.0F, 9.0F, 40.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_4.setTextureOffset(379, 111).addBox(-15.5F, -1.0F, 16.0F, 33.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_4.setTextureOffset(391, 103).addBox(-12.0F, -1.0F, 23.0F, 26.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_4.setTextureOffset(400, 72).addBox(-8.0F, -1.0F, 30.0F, 18.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_4.setTextureOffset(405, 91).addBox(-5.0F, -1.0F, 37.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_4.setTextureOffset(286, 137).addBox(-3.0F, -2.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        base_4 = new ModelRenderer(this);
        base_4.setRotationPoint(-0.2F, 1.0F, 0.0F);
        frame4.addChild(base_4);
        

        foot_nub_4 = new ModelRenderer(this);
        foot_nub_4.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_4.addChild(foot_nub_4);
        setRotationAngle(foot_nub_4, 0.0F, -0.5236F, 0.0F);
        foot_nub_4.setTextureOffset(455, 277).addBox(-2.8F, -11.4F, -58.4F, 7.0F, 13.0F, 10.0F, 0.0F, false);
        foot_nub_4.setTextureOffset(77, 124).addBox(-2.0F, 13.1F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        foot_nub_4.setTextureOffset(80, 102).addBox(-1.0F, 14.1F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_8 = new ModelRenderer(this);
        head_8.setRotationPoint(1.2F, -0.4F, -58.4F);
        foot_nub_4.addChild(head_8);
        setRotationAngle(head_8, -1.9199F, 0.0F, 0.0F);
        head_8.setTextureOffset(455, 228).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_8 = new ModelRenderer(this);
        spine_8.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_4.addChild(spine_8);
        setRotationAngle(spine_8, 0.0F, -0.5236F, 0.0F);
        

        lower_peak_4 = new ModelRenderer(this);
        lower_peak_4.setRotationPoint(0.0F, -1.0F, -46.0F);
        base_4.addChild(lower_peak_4);
        setRotationAngle(lower_peak_4, -0.4363F, 0.0F, 0.0F);
        lower_peak_4.setTextureOffset(368, 270).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 10.0F, 0.0F, false);
        lower_peak_4.setTextureOffset(380, 264).addBox(-20.0F, -1.0F, 8.0F, 41.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_4.setTextureOffset(380, 258).addBox(-16.5F, -1.0F, 15.0F, 34.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_4.setTextureOffset(380, 254).addBox(-13.0F, -1.0F, 22.0F, 27.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_4.setTextureOffset(380, 252).addBox(-9.0F, -1.0F, 29.0F, 19.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_4.setTextureOffset(380, 252).addBox(-5.0F, -1.0F, 36.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_4.setTextureOffset(286, 137).addBox(-3.0F, -1.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        lamp_trim4 = new ModelRenderer(this);
        lamp_trim4.setRotationPoint(0.0F, 0.0F, 0.0F);
        frame4.addChild(lamp_trim4);
        lamp_trim4.setTextureOffset(295, 118).addBox(-3.0F, -215.25F, -7.4F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        lamp_trim4.setTextureOffset(280, 97).addBox(-2.0F, 17.25F, -7.4F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        frame5 = new ModelRenderer(this);
        frame5.setRotationPoint(0.0F, 0.0F, 0.0F);
        frames.addChild(frame5);
        setRotationAngle(frame5, 0.0F, 2.0944F, 0.0F);
        

        cap_5 = new ModelRenderer(this);
        cap_5.setRotationPoint(-0.2F, -193.0F, 0.0F);
        frame5.addChild(cap_5);
        

        head_nub_5 = new ModelRenderer(this);
        head_nub_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_5.addChild(head_nub_5);
        setRotationAngle(head_nub_5, 0.0F, -0.5236F, 0.0F);
        head_nub_5.setTextureOffset(455, 155).addBox(-2.8F, -3.4F, -58.4F, 7.0F, 10.0F, 10.0F, 0.0F, false);
        head_nub_5.setTextureOffset(77, 124).addBox(-2.0F, -22.9F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        head_nub_5.setTextureOffset(80, 102).addBox(-1.0F, -22.9F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_9 = new ModelRenderer(this);
        head_9.setRotationPoint(1.2F, -0.4F, -58.4F);
        head_nub_5.addChild(head_9);
        setRotationAngle(head_9, -1.2217F, 0.0F, 0.0F);
        head_9.setTextureOffset(455, 105).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_9 = new ModelRenderer(this);
        spine_9.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_5.addChild(spine_9);
        setRotationAngle(spine_9, 0.0F, -0.5236F, 0.0F);
        spine_9.setTextureOffset(455, 108).addBox(-0.8F, 6.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);
        spine_9.setTextureOffset(455, 169).addBox(-0.8F, 70.6F, -57.4F, 3.0F, 48.0F, 9.0F, 0.0F, false);
        spine_9.setTextureOffset(455, 220).addBox(-0.8F, 118.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);

        upper_peak_5 = new ModelRenderer(this);
        upper_peak_5.setRotationPoint(0.0F, -1.0F, -46.0F);
        cap_5.addChild(upper_peak_5);
        setRotationAngle(upper_peak_5, 0.4363F, 0.0F, 0.0F);
        upper_peak_5.setTextureOffset(368, 134).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 11.0F, 0.0F, false);
        upper_peak_5.setTextureOffset(381, 117).addBox(-19.0F, -1.0F, 9.0F, 40.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_5.setTextureOffset(379, 111).addBox(-15.5F, -1.0F, 16.0F, 33.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_5.setTextureOffset(391, 103).addBox(-12.0F, -1.0F, 23.0F, 26.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_5.setTextureOffset(400, 72).addBox(-8.0F, -1.0F, 30.0F, 18.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_5.setTextureOffset(405, 91).addBox(-5.0F, -1.0F, 37.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_5.setTextureOffset(286, 137).addBox(-3.0F, -2.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        base_5 = new ModelRenderer(this);
        base_5.setRotationPoint(-0.2F, 1.0F, 0.0F);
        frame5.addChild(base_5);
        

        foot_nub_5 = new ModelRenderer(this);
        foot_nub_5.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_5.addChild(foot_nub_5);
        setRotationAngle(foot_nub_5, 0.0F, -0.5236F, 0.0F);
        foot_nub_5.setTextureOffset(455, 277).addBox(-2.8F, -11.4F, -58.4F, 7.0F, 13.0F, 10.0F, 0.0F, false);
        foot_nub_5.setTextureOffset(77, 124).addBox(-2.0F, 13.1F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        foot_nub_5.setTextureOffset(80, 102).addBox(-1.0F, 14.1F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_10 = new ModelRenderer(this);
        head_10.setRotationPoint(1.2F, -0.4F, -58.4F);
        foot_nub_5.addChild(head_10);
        setRotationAngle(head_10, -1.9199F, 0.0F, 0.0F);
        head_10.setTextureOffset(455, 228).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_10 = new ModelRenderer(this);
        spine_10.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_5.addChild(spine_10);
        setRotationAngle(spine_10, 0.0F, -0.5236F, 0.0F);
        

        lower_peak_5 = new ModelRenderer(this);
        lower_peak_5.setRotationPoint(0.0F, -1.0F, -46.0F);
        base_5.addChild(lower_peak_5);
        setRotationAngle(lower_peak_5, -0.4363F, 0.0F, 0.0F);
        lower_peak_5.setTextureOffset(368, 270).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 10.0F, 0.0F, false);
        lower_peak_5.setTextureOffset(380, 264).addBox(-20.0F, -1.0F, 8.0F, 41.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_5.setTextureOffset(380, 258).addBox(-16.5F, -1.0F, 15.0F, 34.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_5.setTextureOffset(380, 254).addBox(-13.0F, -1.0F, 22.0F, 27.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_5.setTextureOffset(380, 252).addBox(-9.0F, -1.0F, 29.0F, 19.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_5.setTextureOffset(380, 252).addBox(-5.0F, -1.0F, 36.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_5.setTextureOffset(286, 137).addBox(-3.0F, -1.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        lamp_trim5 = new ModelRenderer(this);
        lamp_trim5.setRotationPoint(0.0F, 0.0F, 0.0F);
        frame5.addChild(lamp_trim5);
        lamp_trim5.setTextureOffset(295, 118).addBox(-3.0F, -215.25F, -7.4F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        lamp_trim5.setTextureOffset(280, 97).addBox(-2.0F, 17.25F, -7.4F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        frame6 = new ModelRenderer(this);
        frame6.setRotationPoint(0.0F, 0.0F, 0.0F);
        frames.addChild(frame6);
        setRotationAngle(frame6, 0.0F, 1.0472F, 0.0F);
        

        cap_6 = new ModelRenderer(this);
        cap_6.setRotationPoint(-0.2F, -193.0F, 0.0F);
        frame6.addChild(cap_6);
        

        head_nub_6 = new ModelRenderer(this);
        head_nub_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_6.addChild(head_nub_6);
        setRotationAngle(head_nub_6, 0.0F, -0.5236F, 0.0F);
        head_nub_6.setTextureOffset(455, 155).addBox(-2.8F, -3.4F, -58.4F, 7.0F, 10.0F, 10.0F, 0.0F, false);
        head_nub_6.setTextureOffset(77, 124).addBox(-2.0F, -22.9F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        head_nub_6.setTextureOffset(80, 102).addBox(-1.0F, -22.9F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_11 = new ModelRenderer(this);
        head_11.setRotationPoint(1.2F, -0.4F, -58.4F);
        head_nub_6.addChild(head_11);
        setRotationAngle(head_11, -1.2217F, 0.0F, 0.0F);
        head_11.setTextureOffset(455, 105).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_11 = new ModelRenderer(this);
        spine_11.setRotationPoint(0.0F, 0.0F, 0.0F);
        cap_6.addChild(spine_11);
        setRotationAngle(spine_11, 0.0F, -0.5236F, 0.0F);
        spine_11.setTextureOffset(455, 108).addBox(-0.8F, 6.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);
        spine_11.setTextureOffset(455, 169).addBox(-0.8F, 70.6F, -57.4F, 3.0F, 48.0F, 9.0F, 0.0F, false);
        spine_11.setTextureOffset(455, 220).addBox(-0.8F, 118.6F, -57.4F, 3.0F, 64.0F, 9.0F, 0.0F, false);

        upper_peak_6 = new ModelRenderer(this);
        upper_peak_6.setRotationPoint(0.0F, -1.0F, -46.0F);
        cap_6.addChild(upper_peak_6);
        setRotationAngle(upper_peak_6, 0.4363F, 0.0F, 0.0F);
        upper_peak_6.setTextureOffset(368, 134).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 11.0F, 0.0F, false);
        upper_peak_6.setTextureOffset(381, 117).addBox(-19.0F, -1.0F, 9.0F, 40.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_6.setTextureOffset(379, 111).addBox(-15.5F, -1.0F, 16.0F, 33.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_6.setTextureOffset(391, 103).addBox(-12.0F, -1.0F, 23.0F, 26.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_6.setTextureOffset(400, 72).addBox(-8.0F, -1.0F, 30.0F, 18.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_6.setTextureOffset(405, 91).addBox(-5.0F, -1.0F, 37.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        upper_peak_6.setTextureOffset(286, 137).addBox(-3.0F, -2.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        base_6 = new ModelRenderer(this);
        base_6.setRotationPoint(-0.2F, 1.0F, 0.0F);
        frame6.addChild(base_6);
        

        foot_nub_6 = new ModelRenderer(this);
        foot_nub_6.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_6.addChild(foot_nub_6);
        setRotationAngle(foot_nub_6, 0.0F, -0.5236F, 0.0F);
        foot_nub_6.setTextureOffset(455, 277).addBox(-2.8F, -11.4F, -58.4F, 7.0F, 13.0F, 10.0F, 0.0F, false);
        foot_nub_6.setTextureOffset(77, 124).addBox(-2.0F, 13.1F, -11.4F, 5.0F, 7.0F, 5.0F, 0.0F, false);
        foot_nub_6.setTextureOffset(80, 102).addBox(-1.0F, 14.1F, -6.4F, 3.0F, 6.0F, 4.0F, 0.0F, false);

        head_12 = new ModelRenderer(this);
        head_12.setRotationPoint(1.2F, -0.4F, -58.4F);
        foot_nub_6.addChild(head_12);
        setRotationAngle(head_12, -1.9199F, 0.0F, 0.0F);
        head_12.setTextureOffset(455, 228).addBox(-2.7F, -52.0F, -2.75F, 4.0F, 51.0F, 4.0F, 0.0F, false);

        spine_12 = new ModelRenderer(this);
        spine_12.setRotationPoint(0.0F, 0.0F, 0.0F);
        base_6.addChild(spine_12);
        setRotationAngle(spine_12, 0.0F, -0.5236F, 0.0F);
        

        lower_peak_6 = new ModelRenderer(this);
        lower_peak_6.setRotationPoint(0.0F, -1.0F, -46.0F);
        base_6.addChild(lower_peak_6);
        setRotationAngle(lower_peak_6, -0.4363F, 0.0F, 0.0F);
        lower_peak_6.setTextureOffset(368, 270).addBox(-23.0F, -1.0F, -2.0F, 48.0F, 1.0F, 10.0F, 0.0F, false);
        lower_peak_6.setTextureOffset(380, 264).addBox(-20.0F, -1.0F, 8.0F, 41.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_6.setTextureOffset(380, 258).addBox(-16.5F, -1.0F, 15.0F, 34.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_6.setTextureOffset(380, 254).addBox(-13.0F, -1.0F, 22.0F, 27.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_6.setTextureOffset(380, 252).addBox(-9.0F, -1.0F, 29.0F, 19.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_6.setTextureOffset(380, 252).addBox(-5.0F, -1.0F, 36.0F, 11.0F, 1.0F, 7.0F, 0.0F, false);
        lower_peak_6.setTextureOffset(286, 137).addBox(-3.0F, -1.25F, 41.0F, 7.0F, 2.0F, 3.0F, 0.0F, false);

        lamp_trim6 = new ModelRenderer(this);
        lamp_trim6.setRotationPoint(0.0F, 0.0F, 0.0F);
        frame6.addChild(lamp_trim6);
        lamp_trim6.setTextureOffset(295, 118).addBox(-3.0F, -215.25F, -7.4F, 7.0F, 2.0F, 4.0F, 0.0F, false);
        lamp_trim6.setTextureOffset(280, 97).addBox(-2.0F, 17.25F, -7.4F, 6.0F, 2.0F, 4.0F, 0.0F, false);

        boti = new ModelRenderer(this);
        boti.setRotationPoint(0.0F, 24.0F, 0.0F);
        boti.setTextureOffset(22, 263).addBox(-26.0F, -65.0F, -30.0F, 52.0F, 64.0F, 1.0F, 0.0F, false);
        boti.setTextureOffset(22, 263).addBox(-26.0F, -141.0F, -30.0F, 52.0F, 64.0F, 1.0F, 0.0F, false);
        boti.setTextureOffset(22, 263).addBox(-26.0F, -77.0F, -30.0F, 52.0F, 12.0F, 1.0F, 0.0F, false);
    }

    public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
        matrixStack.push();
        matrixStack.scale(0.25F, 0.25F, 0.25F);
        this.door_east_rotate_y.rotateAngleY = (float) Math.toRadians(EnumDoorType.TT_2020_CAPSULE.getRotationForState(exterior.getOpen()));
        this.door_west_rotate_y.rotateAngleY = -(float) Math.toRadians(EnumDoorType.TT_2020_CAPSULE.getRotationForState(exterior.getOpen()));
        
        glow.setBright(exterior.getLightLevel());
        
        door.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        side_walls.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        center.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        door_jam.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        frames.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        //boti.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        glow.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        matrixStack.pop();
        
    }

    @Override
    public void renderBoti(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer,
            int packedLight, int packedOverlay, float alpha) {
        if(exterior.getBotiWorld() != null && exterior.getMatterState() == EnumMatterState.SOLID && exterior.getOpen() != EnumDoorState.CLOSED) {
            PortalInfo info = new PortalInfo();
            info.setPosition(exterior.getPos());
            info.setWorldShell(exterior.getBotiWorld());
            info.setTranslate(matrix -> {
                matrix.translate(-0.5, 0, -0.5);
                ExteriorRenderer.applyTransforms(matrix, exterior);
                matrix.translate(0, -0.7, 0);
                matrix.translate(0, Math.cos(ClientHelper.getClientWorld().getGameTime() * 0.05) * 0.25, 0);
            });
            info.setTranslatePortal(matrix -> {
                matrix.rotate(Vector3f.ZN.rotationDegrees(180));
                matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(exterior.getBotiWorld().getPortalDirection())));
                matrix.translate(-0.5, 0, -0.5);
            });
            
            info.setRenderPortal((stack, buf) -> {
                stack.push();
                stack.translate(0, -0.2, 0);
                stack.scale(0.25F, 0.25F, 0.25F);
                this.boti.render(stack, buf.getBuffer(RenderType.getEntityCutoutNoCull(TT2020CapsuleExteriorRenderer.TEXTURE)), packedLight, packedOverlay);
                stack.pop();
            });
            
            BOTIRenderer.addPortal(info);
        }
    }
    
    @Override
    public void renderBrokenExterior(MatrixStack matrix, IVertexBuilder buffer, int combinedLight,
            int combinedOverlay) {
        matrix.push();
        float scale = 0.25F;
        matrix.scale(scale, scale, scale);;
        ClientWorld world = ClientHelper.getClientWorldCasted();
        float flickeringLight = 0;
        if(world.rand.nextBoolean()){
            flickeringLight = world.rand.nextFloat();
        }
        glow.setBright((float)Math.cos(flickeringLight / 0.3 * world.rand.nextFloat()));
        
        door.render(matrix, buffer, combinedLight, combinedOverlay);
        side_walls.render(matrix, buffer, combinedLight, combinedOverlay);
        center.render(matrix, buffer, combinedLight, combinedOverlay);
        door_jam.render(matrix, buffer, combinedLight, combinedOverlay);
        frames.render(matrix, buffer, combinedLight, combinedOverlay);
        glow.render(matrix, buffer, combinedLight, combinedOverlay);
        matrix.pop();
    }
}