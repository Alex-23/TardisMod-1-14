package net.tardis.mod.client.models.consoles;

import java.util.function.Function;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.util.ResourceLocation;

public abstract class AbstractConsoleRenderTypedModel<T> extends Model{

    public AbstractConsoleRenderTypedModel(Function<ResourceLocation, RenderType> renderTypeIn) {
        super(renderTypeIn);
    }
    
    public abstract void render(T tileEntity, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight,
            int packedOverlay, float red, float green, float blue, float alpha);

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn,
            float red, float green, float blue, float alpha) {
    }

}
