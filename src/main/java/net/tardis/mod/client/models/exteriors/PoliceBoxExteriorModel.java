package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.TRenderTypes;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.PoliceBoxExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;


public class PoliceBoxExteriorModel extends ExteriorModel {

	private final ModelRenderer boti;
	private final ModelRenderer glow;
	private final ModelRenderer Newbery;
	private final ModelRenderer Base;
	private final ModelRenderer CornerPosts;
	private final ModelRenderer UpperSignage;
	private final ModelRenderer bone7;
	private final ModelRenderer bone8;
	private final ModelRenderer bone9;
	private final ModelRenderer Trim;
	private final ModelRenderer RoofStacks;
	private final ModelRenderer SideWall1;
	private final ModelRenderer bone;
	private final ModelRenderer SideWall2;
	private final ModelRenderer bone2;
	private final ModelRenderer SideWall3;
	private final ModelRenderer bone3;
	private final ModelRenderer LeftDoor;
	private final ModelRenderer bone4;
	private final ModelRenderer bone5;
	private final ModelRenderer RightDoor;
	private final ModelRenderer bone6;

	public PoliceBoxExteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		boti = new ModelRenderer(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.setTextureOffset(0, 297).addBox(-13.0F, -65.0F, -14.5F, 26.0F, 61.0F, 0.0F, 0.0F, false);

		glow = new ModelRenderer(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		glow.setTextureOffset(34, 36).addBox(-1.5F, -82.5F, -1.5F, 3.0F, 4.0F, 3.0F, 0.0F, false);
		glow.setTextureOffset(22, 50).addBox(-2.0F, -83.0F, -2.0F, 4.0F, 5.0F, 4.0F, 0.0F, false);

		Newbery = new ModelRenderer(this);
		Newbery.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		Base = new ModelRenderer(this);
		Base.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(Base);
		Base.setTextureOffset(0, 0).addBox(-23.5F, -3.0F, -23.5F, 47.0F, 3.0F, 47.0F, 0.0F, false);

		CornerPosts = new ModelRenderer(this);
		CornerPosts.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(CornerPosts);
		CornerPosts.setTextureOffset(82, 131).addBox(15.5F, -73.0F, -20.5F, 5.0F, 70.0F, 5.0F, 0.0F, false);
		CornerPosts.setTextureOffset(22, 74).addBox(16.0F, -74.0F, -20.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		CornerPosts.setTextureOffset(22, 74).addBox(-20.0F, -74.0F, -20.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		CornerPosts.setTextureOffset(22, 74).addBox(-20.0F, -74.0F, 16.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		CornerPosts.setTextureOffset(22, 74).addBox(16.0F, -74.0F, 16.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		CornerPosts.setTextureOffset(82, 131).addBox(-20.5F, -73.0F, -20.5F, 5.0F, 70.0F, 5.0F, 0.0F, false);
		CornerPosts.setTextureOffset(82, 131).addBox(-20.5F, -73.0F, 15.5F, 5.0F, 70.0F, 5.0F, 0.0F, false);
		CornerPosts.setTextureOffset(82, 131).addBox(15.5F, -73.0F, 15.5F, 5.0F, 70.0F, 5.0F, 0.0F, false);

		UpperSignage = new ModelRenderer(this);
		UpperSignage.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(UpperSignage);
		UpperSignage.setTextureOffset(160, 138).addBox(-18.5F, -70.0F, -22.5F, 37.0F, 4.0F, 4.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
		UpperSignage.addChild(bone7);
		setRotationAngle(bone7, 0.0F, -1.5708F, 0.0F);
		bone7.setTextureOffset(160, 138).addBox(-18.5F, -70.0F, -22.5F, 37.0F, 4.0F, 4.0F, 0.0F, false);

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		UpperSignage.addChild(bone8);
		setRotationAngle(bone8, 0.0F, 3.1416F, 0.0F);
		bone8.setTextureOffset(160, 138).addBox(-18.5F, -70.0F, -22.5F, 37.0F, 4.0F, 4.0F, 0.0F, false);

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		UpperSignage.addChild(bone9);
		setRotationAngle(bone9, 0.0F, 1.5708F, 0.0F);
		bone9.setTextureOffset(160, 138).addBox(-18.5F, -70.0F, -22.5F, 37.0F, 4.0F, 4.0F, 0.0F, false);

		Trim = new ModelRenderer(this);
		Trim.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(Trim);
		Trim.setTextureOffset(208, 210).addBox(14.5F, -65.0F, -18.5F, 4.0F, 62.0F, 4.0F, 0.0F, false);
		Trim.setTextureOffset(208, 210).addBox(-18.5F, -65.0F, -18.5F, 4.0F, 62.0F, 4.0F, 0.0F, false);
		Trim.setTextureOffset(208, 210).addBox(-18.5F, -65.0F, 14.5F, 4.0F, 62.0F, 4.0F, 0.0F, false);
		Trim.setTextureOffset(208, 210).addBox(14.5F, -65.0F, 14.5F, 4.0F, 62.0F, 4.0F, 0.0F, false);
		Trim.setTextureOffset(114, 92).addBox(-18.5F, -66.0F, -18.5F, 37.0F, 1.0F, 37.0F, 0.0F, false);

		RoofStacks = new ModelRenderer(this);
		RoofStacks.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(RoofStacks);
		RoofStacks.setTextureOffset(0, 50).addBox(-19.5F, -73.0F, -19.5F, 39.0F, 3.0F, 39.0F, 0.0F, false);
		RoofStacks.setTextureOffset(0, 92).addBox(-19.0F, -74.0F, -19.0F, 38.0F, 1.0F, 38.0F, 0.0F, false);
		RoofStacks.setTextureOffset(117, 50).addBox(-17.5F, -75.5F, -17.5F, 35.0F, 2.0F, 35.0F, 0.0F, false);
		RoofStacks.setTextureOffset(13, 36).addBox(-3.5F, -78.5F, -3.5F, 7.0F, 3.0F, 7.0F, 0.0F, false);

		SideWall1 = new ModelRenderer(this);
		SideWall1.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(SideWall1);
		setRotationAngle(SideWall1, 0.0F, 1.5708F, 0.0F);
		SideWall1.setTextureOffset(220, 210).addBox(-12.5F, -5.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall1.setTextureOffset(224, 214).addBox(-12.5F, -20.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall1.setTextureOffset(224, 214).addBox(-12.5F, -35.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall1.setTextureOffset(224, 214).addBox(-12.5F, -50.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall1.setTextureOffset(224, 214).addBox(-12.5F, -65.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall1.setTextureOffset(224, 224).addBox(12.5F, -65.0F, -17.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall1.setTextureOffset(212, 146).addBox(-2.5F, -65.0F, -17.5F, 5.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall1.setTextureOffset(230, 146).addBox(-0.5F, -65.0F, -18.5F, 1.0F, 62.0F, 1.0F, 0.0F, false);
		SideWall1.setTextureOffset(224, 224).addBox(-14.5F, -65.0F, -17.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall1.setTextureOffset(141, 0).addBox(-12.5F, -48.0F, -16.5F, 25.0F, 43.0F, 0.0F, 0.0F, false);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, -50.0F, -16.5F);
		SideWall1.addChild(bone);
		bone.setTextureOffset(0, 92).addBox(2.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);
		bone.setTextureOffset(0, 92).addBox(-12.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);

		SideWall2 = new ModelRenderer(this);
		SideWall2.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(SideWall2);
		setRotationAngle(SideWall2, 0.0F, 3.1416F, 0.0F);
		SideWall2.setTextureOffset(220, 210).addBox(-12.5F, -5.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall2.setTextureOffset(224, 214).addBox(-12.5F, -20.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall2.setTextureOffset(224, 214).addBox(-12.5F, -35.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall2.setTextureOffset(224, 214).addBox(-12.5F, -50.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall2.setTextureOffset(224, 214).addBox(-12.5F, -65.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall2.setTextureOffset(224, 224).addBox(12.5F, -65.0F, -17.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall2.setTextureOffset(212, 146).addBox(-2.5F, -65.0F, -17.5F, 5.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall2.setTextureOffset(230, 146).addBox(-0.5F, -65.0F, -18.5F, 1.0F, 62.0F, 1.0F, 0.0F, false);
		SideWall2.setTextureOffset(224, 224).addBox(-14.5F, -65.0F, -17.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall2.setTextureOffset(141, 0).addBox(-12.5F, -48.0F, -16.5F, 25.0F, 43.0F, 0.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, -50.0F, -16.5F);
		SideWall2.addChild(bone2);
		setRotationAngle(bone2, 0.0873F, 0.0F, 0.0F);
		bone2.setTextureOffset(0, 92).addBox(2.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);
		bone2.setTextureOffset(0, 92).addBox(-12.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);

		SideWall3 = new ModelRenderer(this);
		SideWall3.setRotationPoint(0.0F, 0.0F, 0.0F);
		Newbery.addChild(SideWall3);
		setRotationAngle(SideWall3, 0.0F, -1.5708F, 0.0F);
		SideWall3.setTextureOffset(220, 210).addBox(-12.5F, -5.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall3.setTextureOffset(224, 214).addBox(-12.5F, -20.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall3.setTextureOffset(224, 214).addBox(-12.5F, -35.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall3.setTextureOffset(224, 214).addBox(-12.5F, -50.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall3.setTextureOffset(224, 214).addBox(-12.5F, -65.0F, -17.5F, 25.0F, 2.0F, 2.0F, 0.0F, false);
		SideWall3.setTextureOffset(224, 224).addBox(12.5F, -65.0F, -17.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall3.setTextureOffset(212, 146).addBox(-2.5F, -65.0F, -17.5F, 5.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall3.setTextureOffset(230, 146).addBox(-0.5F, -65.0F, -18.5F, 1.0F, 62.0F, 1.0F, 0.0F, false);
		SideWall3.setTextureOffset(224, 224).addBox(-14.5F, -65.0F, -17.5F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		SideWall3.setTextureOffset(141, 0).addBox(-12.5F, -48.0F, -16.5F, 25.0F, 43.0F, 0.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, -50.0F, -16.5F);
		SideWall3.addChild(bone3);
		bone3.setTextureOffset(0, 92).addBox(2.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);
		bone3.setTextureOffset(0, 92).addBox(-12.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);

		LeftDoor = new ModelRenderer(this);
		LeftDoor.setRotationPoint(-14.5F, -3.0F, -15.5F);
		Newbery.addChild(LeftDoor);
		LeftDoor.setTextureOffset(45, 131).addBox(2.0F, -2.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		LeftDoor.setTextureOffset(0, 124).addBox(2.0F, -17.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		LeftDoor.setTextureOffset(0, 124).addBox(2.0F, -32.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		LeftDoor.setTextureOffset(0, 124).addBox(2.0F, -47.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		LeftDoor.setTextureOffset(0, 124).addBox(2.0F, -62.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		LeftDoor.setTextureOffset(0, 215).addBox(12.0F, -62.0F, -2.0F, 3.0F, 62.0F, 2.0F, 0.0F, false);
		LeftDoor.setTextureOffset(230, 146).addBox(14.0F, -62.0F, -3.0F, 1.0F, 62.0F, 1.0F, 0.0F, false);
		LeftDoor.setTextureOffset(224, 224).addBox(0.0F, -62.0F, -2.0F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		LeftDoor.setTextureOffset(20, 0).addBox(2.0F, -30.0F, -1.0F, 10.0F, 28.0F, 0.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(14.5F, -47.0F, -1.0F);
		LeftDoor.addChild(bone4);
		bone4.setTextureOffset(0, 92).addBox(-12.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(2.0F, -32.0F, -2.0F);
		LeftDoor.addChild(bone5);
		bone5.setTextureOffset(0, 64).addBox(0.0F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);

		RightDoor = new ModelRenderer(this);
		RightDoor.setRotationPoint(14.5F, -3.0F, -15.5F);
		Newbery.addChild(RightDoor);
		RightDoor.setTextureOffset(45, 131).addBox(-12.0F, -2.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		RightDoor.setTextureOffset(0, 124).addBox(-12.0F, -17.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		RightDoor.setTextureOffset(0, 124).addBox(-12.0F, -32.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		RightDoor.setTextureOffset(0, 124).addBox(-12.0F, -47.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		RightDoor.setTextureOffset(0, 124).addBox(-12.0F, -62.0F, -2.0F, 10.0F, 2.0F, 2.0F, 0.0F, false);
		RightDoor.setTextureOffset(224, 224).addBox(-14.0F, -62.0F, -2.0F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		RightDoor.setTextureOffset(224, 224).addBox(-2.0F, -62.0F, -2.0F, 2.0F, 62.0F, 2.0F, 0.0F, false);
		RightDoor.setTextureOffset(0, 0).addBox(-12.0F, -45.0F, -1.0F, 10.0F, 43.0F, 0.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(-14.5F, -47.0F, -1.0F);
		RightDoor.addChild(bone6);
		bone6.setTextureOffset(0, 92).addBox(2.5F, -13.0F, 0.0F, 10.0F, 13.0F, 1.0F, 0.0F, false);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void renderBones(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
		EnumDoorState state = exterior.getOpen();
        switch (state) {
            case ONE:
                this.RightDoor.rotateAngleY = (float) Math.toRadians(
                        EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default
                this.LeftDoor.rotateAngleY = (float) Math.toRadians(
                        EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
                break;
            case BOTH:
                this.RightDoor.rotateAngleY = (float) Math.toRadians(
                        EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.ONE));
                this.LeftDoor.rotateAngleY = (float) Math.toRadians(
                        EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open
                break;
            case CLOSED://close both doors
                this.RightDoor.rotateAngleY = (float) Math.toRadians(
                        EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
                this.LeftDoor.rotateAngleY = (float) Math.toRadians(
                        EnumDoorType.POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
                break;
            default:
                break;
        }

        Newbery.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        //boti.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
        this.glow.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);

	}

	public void translateToTile(MatrixStack matrix){
		matrix.translate(0, -0.25, 0); //This renders the box upwards on the y axis by 0.25
		matrix.scale(0.5f, 0.5f, 0.5f);
	}

	@Override
	public void renderBoti(ExteriorTile exterior, float scale, MatrixStack matrixStack, IVertexBuilder buffer,
	        int packedLight, int packedOverlay, float alpha) {
		if(exterior.getBotiWorld() != null && exterior.getMatterState() == EnumMatterState.SOLID && exterior.getOpen() != EnumDoorState.CLOSED){
			PortalInfo info = new PortalInfo();
			info.setPosition(exterior.getPos());
			info.setWorldShell(exterior.getBotiWorld());

			//Translation
			info.setTranslate(matrix -> {
				matrix.translate(-0.5, 0, -0.5);
				ExteriorRenderer.applyTransforms(matrix, exterior);
			});
			info.setTranslatePortal(matrix -> {
				matrix.rotate(Vector3f.ZN.rotationDegrees(180));
				matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(exterior.getBotiWorld().getPortalDirection())));
				matrix.translate(-0.5, -0.40625, -0.5);

			});

			//Renderers
			ResourceLocation texture = Helper.getVariantTextureOr(exterior.getVariant(), PoliceBoxExteriorRenderer.TEXTURE);
			info.setRenderPortal((matrix, buf) -> {
				matrix.push();
				this.translateToTile(matrix);
				this.boti.render(matrix, buf.getBuffer(TRenderTypes.getTardis(texture)), packedLight, packedOverlay);
				matrix.pop();
			});

			info.setRenderDoor((matrix, buf) -> {
				matrix.push();
				this.translateToTile(matrix);
				matrix.translate(0, 1.5, 0);
				this.RightDoor.render(matrix, buf.getBuffer(TRenderTypes.getTardis(texture)), packedLight, packedOverlay);
				this.LeftDoor.render(matrix, buf.getBuffer(TRenderTypes.getTardis(texture)), packedLight, packedOverlay);
				matrix.pop();
			});

			BOTIRenderer.addPortal(info);
		}
	}
}