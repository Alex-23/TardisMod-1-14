package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientPacketHandler;
import net.tardis.mod.missions.MiniMission;

public class MissionUpdateMessage {

	private BlockPos pos; public BlockPos getPos() {return this.pos;}
	private ResourceLocation key; public ResourceLocation getKey() {return this.key;}
	private int range; public int getRange() {return this.range;}
	private CompoundNBT nbt; public CompoundNBT getNBT() {return this.nbt;}
	
	public MissionUpdateMessage(BlockPos pos, ResourceLocation key, int range, CompoundNBT nbt) {
		this.pos = pos;
		this.key = key;
		this.range = range;
		this.nbt = nbt;
	}
	
	public MissionUpdateMessage(MiniMission mis) {
		this(mis.getPos(), mis.getType().getRegistryName(), mis.getRange(), mis.serializeNBT());
	}
	
	public static void encode(MissionUpdateMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeResourceLocation(mes.key);
		buf.writeInt(mes.range);
		buf.writeCompoundTag(mes.nbt);
	}
	
	public static MissionUpdateMessage decode(PacketBuffer buf) {
		return new MissionUpdateMessage(buf.readBlockPos(), buf.readResourceLocation(), buf.readInt(), buf.readCompoundTag());
	}
	
	public static void handle(MissionUpdateMessage mes, Supplier<NetworkEvent.Context> cont) {
		
		cont.get().enqueueWork(() -> ClientPacketHandler.handleMissionUpdateClient(mes));
		
		cont.get().setPacketHandled(true);
	}
	
}
