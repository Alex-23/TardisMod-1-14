package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.sounds.InteriorHum;

import java.util.function.Supplier;

public class InteriorEffectsMessage {

    private InteriorHum hum;

    public InteriorEffectsMessage(InteriorHum hum){
        this.hum = hum;
    }

    public static void encode(InteriorEffectsMessage mes, PacketBuffer buf){
        buf.writeRegistryId(mes.hum);
    }

    public static InteriorEffectsMessage decode(PacketBuffer buf){
        return new InteriorEffectsMessage(buf.readRegistryId());
    }

    public static void handle(InteriorEffectsMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientHelper.getClientWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
                data.getInteriorEffectsHandler().setHum(mes.hum);
            });
        });
        context.get().setPacketHandled(true);

    }

}
