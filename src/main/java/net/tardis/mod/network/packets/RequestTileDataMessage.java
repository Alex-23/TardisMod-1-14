package net.tardis.mod.network.packets;

import java.util.HashMap;
import java.util.function.Supplier;

import com.google.common.collect.Maps;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.network.packets.data.Requestors.Requestor;

public class RequestTileDataMessage {

    private static final HashMap<Integer, Requestor<?>> REQUESTORS = Maps.newHashMap();
    private static int ID = 0;

    public Requestor<?> request;
    public BlockPos pos;

    public RequestTileDataMessage(Requestor<?> request, BlockPos pos){
        this.request = request;
        this.pos = pos;
    }

    public static void encode(RequestTileDataMessage mes, PacketBuffer buf){
        Requestor<?> request = mes.request;
        buf.writeInt(request.getId());
        buf.writeBlockPos(mes.pos);
    }

    public static RequestTileDataMessage decode(PacketBuffer buf){
        Requestor<?> request = REQUESTORS.get(buf.readInt());
        return new RequestTileDataMessage(request, buf.readBlockPos());
    }

    public static void handle(RequestTileDataMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            TileEntity te = context.get().getSender().getServerWorld().getTileEntity(mes.pos);
            if(te != null){
                mes.request.isValid(te).ifPresent(tile -> {
                    mes.request.tryToActOnTile(tile, context.get().getSender());
                });
            }

        });
        context.get().setPacketHandled(true);
    }


    public static <T extends Requestor<?>> T registerRequestor(T request){
        int id = id();
        request.setId(id);
        REQUESTORS.put(id, request);
        return request;
    }

    public static int id(){
        return ++ID;
    }

}
