package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.BotiChunk;
import net.tardis.mod.client.ClientPacketHandler;

import java.util.function.Supplier;

public class BOTIChunkMessage {

    private BotiChunk chunk;
    /**
     * Type is 0 if destination is a world capability, 1 if a tile
     */
    private int type = 0;
    private BlockPos pos = BlockPos.ZERO;

    public BOTIChunkMessage(BlockPos pos, BotiChunk chunk) {
        this(chunk);
        this.pos = pos;
        this.type = 1;
    }

    public BOTIChunkMessage(BotiChunk chunk){
        this.chunk = chunk;
        this.type = 0;
    }

    public static void encode(BOTIChunkMessage mes, PacketBuffer buf){
        buf.writeInt(mes.type);

        //For tiles
        if(mes.type == 1)
            buf.writeBlockPos(mes.pos);

        mes.chunk.encode(buf);
    }

    public static BOTIChunkMessage decode(PacketBuffer buf){
        int type = buf.readInt();

        if(type == 1)
            return new BOTIChunkMessage(buf.readBlockPos(), new BotiChunk(buf));

        return new BOTIChunkMessage(new BotiChunk(buf));
    }

    public static void handle(BOTIChunkMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> {
            ClientPacketHandler.handleBotiChunkMessage(mes.chunk, mes.pos);
        });
        context.get().setPacketHandled(true);
    }

}
