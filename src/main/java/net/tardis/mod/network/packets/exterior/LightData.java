package net.tardis.mod.network.packets.exterior;

import net.minecraft.network.PacketBuffer;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class LightData extends ExteriorData{

    float light;

    public LightData(Type type) {
        super(type);
    }

    public LightData(float light) {
        this(Type.LIGHT);
    }

    @Override
    public void encode(PacketBuffer buf) {
        buf.writeFloat(this.light);
    }

    @Override
    public void decode(PacketBuffer buf) {
        light = buf.readFloat();
    }

    @Override
    public void apply(ExteriorTile tile) {
        tile.setLightLevel(this.light);
    }

    public static LightData create(ExteriorTile tile){
        return new LightData(tile.getLightLevel());
    }
}
