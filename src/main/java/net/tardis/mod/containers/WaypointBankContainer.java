package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointBankContainer extends BlockEntityContextContainer<WaypointBankTile>{
	
	protected WaypointBankContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	/** Server Only constructor */
	public WaypointBankContainer(int id, PlayerInventory player, WaypointBankTile tile) {
		this(TContainers.WAYPOINT.get(), id);
		init(player, tile);
	}
	
	/** Client Only constructor */
	public WaypointBankContainer(int id, PlayerInventory player, PacketBuffer buf) {
		this(TContainers.WAYPOINT.get(), id);
		TileEntity te = player.player.world.getTileEntity(buf.readBlockPos());
		if(te instanceof WaypointBankTile)
			init(player, (WaypointBankTile)te);
	}
	
	//Common 
	public void init(PlayerInventory inv, WaypointBankTile waypoint) {
		this.blockEntity = waypoint;
		
		this.addSlot(new SlotItemHandler(waypoint.getItemHandler(), 0, 79, 89));
		
		TInventoryHelper.addPlayerInvContainer(this, inv, 0, 34);
	}

}
