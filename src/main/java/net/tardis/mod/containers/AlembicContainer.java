package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.containers.slot.SlotItemHandlerFiltered;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.tags.TardisItemTags;
import net.tardis.mod.tileentities.machines.AlembicTile;

public class AlembicContainer extends BlockEntityContextContainer<AlembicTile>{
	
	public AlembicContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	/** Client Only constructor */
	public AlembicContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(TContainers.ALEMBIC.get(), id);
		this.init(inv, this.blockEntity = (AlembicTile)inv.player.world.getTileEntity(buf.readBlockPos()));
	}
	/** Server Only constructor */
	public AlembicContainer(int id, PlayerInventory inv, AlembicTile tile) {
		super(TContainers.ALEMBIC.get(), id);
		this.init(inv, this.blockEntity = tile);
	}
	
	public void init(PlayerInventory inv, AlembicTile tile) {
		

		this.addSlot(new SlotItemHandlerFiltered(tile.getItemStackHandler(), 0, 25, 11, stack -> stack.getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).isPresent()));//Insert water
		this.addSlot(new SlotItemHandlerFiltered(tile.getItemStackHandler(), 1, 25, 47, item -> false));//Empty Buckets. Can take, no insert
		this.addSlot(new SlotItemHandlerFiltered(tile.getItemStackHandler(), 2, 62, 11, item -> true)); //Cinnabar
		this.addSlot(new SlotItemHandler(tile.getItemStackHandler(), 3, 62, 47));//Furnace Fuel slot
		this.addSlot(new SlotItemHandler(tile.getItemStackHandler(), 4, 140, 11));//Fluid Collection items
		this.addSlot(new SlotItemHandler(tile.getItemStackHandler(), 5, 140, 47));//Result slot. Can take, no insert
		
		//Player Inv
		TInventoryHelper.addPlayerInvContainer(this, inv, 0, -2);
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		Slot slot = this.getSlot(index);
		if(slot.inventory instanceof PlayerInventory) {
			
			//Fuel
			if(slot.getStack().getBurnTime() > 0) {
				ItemStack stack = this.blockEntity.getItemStackHandler().insertItem(3, slot.getStack().copy(), false);
				slot.putStack(stack);
				return stack;
			}
			//Water
			if(slot.getStack().getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).orElse(null) != null) {
				if(blockEntity.getItemStackHandler().getStackInSlot(0).isEmpty()){
					ItemStack stack = blockEntity.getItemStackHandler().insertItem(0, slot.getStack().copy(), false);
					slot.putStack(ItemStack.EMPTY);
					return stack;
				}
			}
			//Cinnabar
			if(slot.getStack().getItem().isIn(TardisItemTags.CINNABAR)) {
				ItemStack stack = blockEntity.getItemStackHandler().insertItem(2, slot.getStack().copy(), false);
				slot.putStack(stack);
				return stack;
			}
			else {
				return super.transferStackInSlot(playerIn, index);
			}
		}
		else {
			ItemStack stack = slot.getStack().copy();
			if(playerIn.addItemStackToInventory(stack)) {
				slot.putStack(ItemStack.EMPTY);
				return stack;
			}
		}
		return ItemStack.EMPTY;
	}


}
