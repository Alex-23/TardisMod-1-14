package net.tardis.mod.containers;

import javax.annotation.Nullable;

import net.minecraft.inventory.container.ContainerType;
import net.tardis.mod.tileentities.machines.QuantiscopeTile;

public abstract class QuantiscopeContainer extends BlockEntityContextContainer<QuantiscopeTile> {

    protected QuantiscopeContainer(@Nullable ContainerType<?> type, int id) {
        super(type, id);
    }
}
