package net.tardis.mod.trades;
import java.util.function.Supplier;

import com.google.common.collect.ImmutableSet;

import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.village.PointOfInterestType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TVillagerProfession {
    
    public static DeferredRegister<VillagerProfession> VILLAGE_PROFFESIONS = DeferredRegister.create(ForgeRegistries.PROFESSIONS, Tardis.MODID);
    
    public static final RegistryObject<VillagerProfession> STORY_TELLER = VILLAGE_PROFFESIONS.register("story_teller", () -> registerProfession("story_teller", () -> TPointOfInterest.TELESCOPE.get(), SoundEvents.ENTITY_VILLAGER_WORK_LIBRARIAN));
    
    public static VillagerProfession registerProfession(String name, Supplier<PointOfInterestType> poi, SoundEvent workSound) {
        return new VillagerProfession("tardis:" + name, poi.get(), ImmutableSet.of(), ImmutableSet.of(), workSound);
    }

}
