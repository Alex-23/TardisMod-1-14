package net.tardis.mod.schematics;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.Console;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.schematics.types.SchematicType;

public class Schematics {

    public static Map<ResourceLocation, Schematic> SCHEMATIC_REGISTRY = new HashMap<ResourceLocation, Schematic>();
    /** Premade instances for use in datagen or logic. Doesn't actually register them to the registry */
    public static class Consoles {
        public static final ConsoleUnlockSchematic NEMO = createConsoleSchematicWithTranslation("consoles/nemo", ConsoleRegistry.NEMO.get());
        public static final ConsoleUnlockSchematic STEAM = createConsoleSchematicWithTranslation("consoles/steam", ConsoleRegistry.STEAM.get());
        public static final ConsoleUnlockSchematic GALVANIC = createConsoleSchematicWithTranslation("consoles/galvanic", ConsoleRegistry.GALVANIC.get());
        public static final ConsoleUnlockSchematic CORAL = createConsoleSchematicWithTranslation("consoles/coral", ConsoleRegistry.CORAL.get());
        public static final ConsoleUnlockSchematic HARTNELL = createConsoleSchematicWithTranslation("consoles/hartnell", ConsoleRegistry.HARTNELL.get());
        public static final ConsoleUnlockSchematic TOYOTA = createConsoleSchematicWithTranslation("consoles/toyota", ConsoleRegistry.TOYOTA.get());
        public static final ConsoleUnlockSchematic XION = createConsoleSchematicWithTranslation("consoles/xion", ConsoleRegistry.XION.get());
        public static final ConsoleUnlockSchematic NEUTRON = createConsoleSchematicWithTranslation("consoles/neutron", ConsoleRegistry.NEUTRON.get());
        public static final ConsoleUnlockSchematic POLYMEDICAL = createConsoleSchematicWithTranslation("consoles/polymedical", ConsoleRegistry.POLYMEDICAL.get());
   } 
   /** Premade instances for use in datagen or logic. Doesn't actually register them to the registry */
   public static class Exteriors {
       public static final ExteriorUnlockSchematic STEAMPUNK = createExteriorSchematicWithTranslation("exteriors/steampunk", ExteriorRegistry.STEAMPUNK.get());
       public static final ExteriorUnlockSchematic TRUNK = createExteriorSchematicWithTranslation("exteriors/trunk", ExteriorRegistry.TRUNK.get());
       public static final ExteriorUnlockSchematic TELEPHONE = createExteriorSchematicWithTranslation("exteriors/telephone", ExteriorRegistry.TELEPHONE.get());
       public static final ExteriorUnlockSchematic FORTUNE = createExteriorSchematicWithTranslation("exteriors/fortune", ExteriorRegistry.FORTUNE.get());
       public static final ExteriorUnlockSchematic SAFE = createExteriorSchematicWithTranslation("exteriors/safe", ExteriorRegistry.SAFE.get());
       public static final ExteriorUnlockSchematic TT_CAPSULE = createExteriorSchematicWithTranslation("exteriors/tt_capsule", ExteriorRegistry.TT_CAPSULE.get());
       public static final ExteriorUnlockSchematic CLOCK = createExteriorSchematicWithTranslation("exteriors/clock", ExteriorRegistry.CLOCK.get());
       public static final ExteriorUnlockSchematic TT_2020 = createExteriorSchematicWithTranslation("exteriors/tt_2020", ExteriorRegistry.TT_2020.get());
       public static final ExteriorUnlockSchematic JAPAN = createExteriorSchematicWithTranslation("exteriors/japan", ExteriorRegistry.JAPAN.get());
       public static final ExteriorUnlockSchematic APERTURE = createExteriorSchematicWithTranslation("exteriors/aperture", ExteriorRegistry.APERTURE.get());
       public static final ExteriorUnlockSchematic POLICE_BOX = createExteriorSchematicWithTranslation("exteriors/police_box", ExteriorRegistry.POLICE_BOX.get());
       public static final ExteriorUnlockSchematic POLICE_BOX_MODERN = createExteriorSchematicWithTranslation("exteriors/modern_police_box", ExteriorRegistry.MODERN_POLICE_BOX.get());
       public static final ExteriorUnlockSchematic DISGUISE = createExteriorSchematicWithTranslation("exteriors/disguise", ExteriorRegistry.DISGUISE.get());
   }
   
   /** Premade instances for use in datagen or logic. Doesn't actually register them to the registry */
   public static class Interiors{
       public static final InteriorUnlockSchematic STEAM = createInteriorSchematicWithTranslation("interiors/steam", () -> ConsoleRoom.STEAM);
       public static final InteriorUnlockSchematic JADE = createInteriorSchematicWithTranslation("interiors/bokkusu", () -> ConsoleRoom.JADE);
       public static final InteriorUnlockSchematic NAUTILUS = createInteriorSchematicWithTranslation("interiors/nautilus", () -> ConsoleRoom.NAUTILUS);
       public static final InteriorUnlockSchematic OMEGA = createInteriorSchematicWithTranslation("interiors/omega", () -> ConsoleRoom.OMEGA);
       public static final InteriorUnlockSchematic ALABASTER = createInteriorSchematicWithTranslation("interiors/alabaster", () -> ConsoleRoom.ALABASTER);
       public static final InteriorUnlockSchematic ARCHITECT = createInteriorSchematicWithTranslation("interiors/architect", () -> ConsoleRoom.ARCHITECT);
       public static final InteriorUnlockSchematic CORAL = createInteriorSchematicWithTranslation("interiors/coral", () -> ConsoleRoom.CORAL);
       public static final InteriorUnlockSchematic PANAMAX = createInteriorSchematicWithTranslation("interiors/panamax", () -> ConsoleRoom.PANAMAX);
       public static final InteriorUnlockSchematic TOYOTA = createInteriorSchematicWithTranslation("interiors/toyota", () -> ConsoleRoom.TOYOTA);
       public static final InteriorUnlockSchematic TRAVELER = createInteriorSchematicWithTranslation("interiors/traveler", () -> ConsoleRoom.TRAVELER);
       public static final InteriorUnlockSchematic ENVOY = createInteriorSchematicWithTranslation("interiors/envoy", () -> ConsoleRoom.ENVOY);
       public static final InteriorUnlockSchematic AMETHYST = createInteriorSchematicWithTranslation("interiors/amethyst", () -> ConsoleRoom.AMETHYST);
       public static final InteriorUnlockSchematic IMPERIAL = createInteriorSchematicWithTranslation("interiors/imperial", () -> ConsoleRoom.IMPERIAL);
   }
   
   /** === Registration Methods Start === */
   
   /**
    * Creates and registers a ConsoleUnlockSchematic
    * <br> For internal use only
    * @param key
    * @param consoleKey - the registry name for the {@linkplain Console} this schematic should have
    * @param type - the SchematicType which defines extra data for the Schematic
    * @param displayName - the text we want users to see
    * @return
    */
   private static ConsoleUnlockSchematic registerConsoleSchematic(String key, String consoleKey, String displayName) {
       ConsoleUnlockSchematic schematic = createConsoleSchematic(key, consoleKey, displayName);
       Schematics.SCHEMATIC_REGISTRY.put(schematic.getId(), schematic);
       return schematic;
   }

   /**
    * Creates and registers an ExteriorUnlockSchematic
    * <br> For internal use only
    * @param key
    * @param exteriorKey - the registry name for the {@linkplain Exterior} this schematic should have.
    * @param displayName - the text we want users to see
    * @return
    */
   private static ExteriorUnlockSchematic registerExteriorSchematic(String key, String exteriorKey, String displayName) {
       ExteriorUnlockSchematic schematic = createExteriorSchematic(key, exteriorKey, displayName);
       Schematics.SCHEMATIC_REGISTRY.put(schematic.getId(), schematic);
       return schematic;
   }
   
   /**
    * Creates and registers an InteriorUnlockSchematic
    * <br> For internal use only
    * @param key
    * @param consoleRoomKey - the registry name for the {@linkplain ConsoleRoom} this schematic should have.
    * @param displayName - the text we want users to see
    * @return
    */
   private static InteriorUnlockSchematic registerInteriorSchematic(String key, String consoleRoomKey, String displayName) {
       InteriorUnlockSchematic schematic = createInteriorSchematic(key, consoleRoomKey, displayName);
       Schematics.SCHEMATIC_REGISTRY.put(schematic.getId(), schematic);
       return schematic;
   }
   
   /** === Registration Methods End === */


   /** === Schematic Helper Methods Start === */
   
   /** Creates a ConsoleUnlockSchematic instance. This won't register it to the data loader.
    * <br> To register to the data loader, use {@linkplain Schematics#registerConsoleSchematic(String, String, String) registerConsoleSchematic}*/
   private static ConsoleUnlockSchematic createConsoleSchematic(String key, String consoleKey, String displayName) {
        ConsoleUnlockSchematic schematic = new ConsoleUnlockSchematic();
        ResourceLocation registryName = new ResourceLocation(Tardis.MODID, key);
        ResourceLocation consoleID = new ResourceLocation(Tardis.MODID, consoleKey);
        schematic.setId(registryName).setDisplayName(displayName);
        schematic.setConsole(consoleID);
        return schematic;
   }
   
   private static ConsoleUnlockSchematic createConsoleSchematicWithTranslation(String key, Console console) {
	   ConsoleUnlockSchematic consoleSchematic = createConsoleSchematicWithTranslation(Helper.createRL(key), console);
	   return consoleSchematic;
   }
   
   public static ConsoleUnlockSchematic createConsoleSchematicWithTranslation(ResourceLocation key, Console console) {
	   ConsoleUnlockSchematic consoleSchematic = new ConsoleUnlockSchematic();
       consoleSchematic.setConsole(console.getRegistryName());
       consoleSchematic.setId(key);
       consoleSchematic.setTranslation(console.getDisplayName().getString());
       return consoleSchematic;
   }
   
   /** Creates a ConsoleUnlockSchematic instance.
    * <br> This doesn't register the Schematic.
    * <br> You can use this as a helper tool for data generation */
   public static ConsoleUnlockSchematic createConsoleSchematic(ResourceLocation registryName, ResourceLocation consoleID, String displayName, RegistryObject<SchematicType> type) {
        ConsoleUnlockSchematic schematic = new ConsoleUnlockSchematic(type.get());
        schematic.setId(registryName).setDisplayName(displayName);
        schematic.setConsole(consoleID);
        return schematic;
   }
   
   /** Creates an ExteriorUnlockSchematic instance. This won't register it to the data loader.
    * <br> To register to the data loader, use {@linkplain Schematics#registerExteriorSchematic(String, String, String, RegistryObject) registerExteriorSchematic}*/
   private static ExteriorUnlockSchematic createExteriorSchematic(String key, String exteriorKey, String displayName) {
       ExteriorUnlockSchematic schematic = new ExteriorUnlockSchematic();
       ResourceLocation registryName = new ResourceLocation(Tardis.MODID, key);
       ResourceLocation exteriorID = new ResourceLocation(Tardis.MODID, exteriorKey);
       schematic.setId(registryName).setDisplayName(displayName);
       schematic.setExterior(exteriorID);
       return schematic;
   }
   
   /** Creates an ExteriorUnlockSchematic instance.
    * <br> This doesn't register the Schematic.
    * <br> You can use this as a helper tool for data generation */
   public static ExteriorUnlockSchematic createExteriorSchematic(ResourceLocation registryName, ResourceLocation exteriorID, String displayName) {
       ExteriorUnlockSchematic schematic = new ExteriorUnlockSchematic();
       schematic.setId(registryName).setDisplayName(displayName);
       schematic.setExterior(exteriorID);
       return schematic;
   }
   
   public static ExteriorUnlockSchematic createExteriorSchematicWithTranslation(ResourceLocation key, AbstractExterior exterior) {
	   ExteriorUnlockSchematic schematic = new ExteriorUnlockSchematic();
       schematic.setExterior(exterior.getRegistryName());
       schematic.setId(key);
       schematic.setTranslation(exterior.getTranslationKey());
       return schematic;
   }
   
   private static ExteriorUnlockSchematic createExteriorSchematicWithTranslation(String key, AbstractExterior exterior) {
	   ExteriorUnlockSchematic schematic = createExteriorSchematicWithTranslation(Helper.createRL(key), exterior);
	   return schematic;
   }
   
   /** Creates an InteriorUnlockSchematic instance. This won't register it to the data loader.
    * <br> To register to the data loader, use {@linkplain Schematics#registerInteriorSchematic(String, String, String) registerInteriorSchematic}*/
   private static InteriorUnlockSchematic createInteriorSchematic(String key, String consoleRoomKey, String displayName) {
       InteriorUnlockSchematic schematic = new InteriorUnlockSchematic();
       ResourceLocation registryName = new ResourceLocation(Tardis.MODID, key);
       ResourceLocation consoleRoomID = new ResourceLocation(Tardis.MODID, consoleRoomKey);
       schematic.setId(registryName).setDisplayName(displayName);
       schematic.setConsoleRoom(consoleRoomID);
       return schematic;
   }
   
   /** Creates an InteriorUnlockSchematic instance.
    * <br> This doesn't register the Schematic.
    * <br> You can use this as a helper tool for data generation */
   public static InteriorUnlockSchematic createInteriorSchematic(ResourceLocation registryName, ResourceLocation consoleRoomID, String displayName) {
       InteriorUnlockSchematic schematic = new InteriorUnlockSchematic();
       schematic.setId(registryName).setDisplayName(displayName);
       schematic.setConsoleRoom(consoleRoomID);
       return schematic;
   }
   
   public static InteriorUnlockSchematic createInteriorSchematicWithTranslation(ResourceLocation registryName, Supplier<ConsoleRoom> room) {
       InteriorUnlockSchematic schematic = new InteriorUnlockSchematic();
       ConsoleRoom interior = room.get();
       schematic.setConsoleRoom(interior.getRegistryName());
       schematic.setTranslation(interior.getTranslationKey());
       schematic.setId(registryName);
       return schematic;
   }
   
   private static InteriorUnlockSchematic createInteriorSchematicWithTranslation(String key, Supplier<ConsoleRoom> room) {
       InteriorUnlockSchematic schematic = createInteriorSchematicWithTranslation(Helper.createRL(key), room);
       return schematic;
   }

}
