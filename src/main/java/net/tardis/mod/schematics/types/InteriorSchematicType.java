package net.tardis.mod.schematics.types;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.schematics.InteriorUnlockSchematic;
import net.tardis.mod.schematics.Schematic;

public class InteriorSchematicType extends SchematicType {


    @Override
    public Schematic deserialize(JsonObject root) {
        InteriorUnlockSchematic schematic = new InteriorUnlockSchematic();
        schematic.setConsoleRoom(new ResourceLocation(root.get("result").getAsString()));
        setTranslationOrDisplayName(root, schematic);
        return schematic;
    }

    @Override
    public JsonObject serialize(Schematic schematic) {
        InteriorUnlockSchematic intSchematic = this.getSchematicAs(InteriorUnlockSchematic.class, schematic);
        JsonObject root = this.createBaseJson(schematic.getDisplayName(), schematic.isUsingTranslatedName());
        root.add("result", new JsonPrimitive(intSchematic.getConsoleRoom().toString()));
        return root;
    }

	@Override
	public Schematic deserialize(PacketBuffer buffer) {
		InteriorUnlockSchematic schematic = new InteriorUnlockSchematic(this);
        schematic.setConsoleRoom(buffer.readResourceLocation());
        return schematic;
	}

	@Override
	public Schematic serialize(Schematic schematic, PacketBuffer buffer) {
		InteriorUnlockSchematic schem = this.getSchematicAs(InteriorUnlockSchematic.class, schematic);
        buffer.writeResourceLocation(schem.getConsoleRoom());
        return schem;

	}
}
