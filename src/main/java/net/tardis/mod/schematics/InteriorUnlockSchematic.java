package net.tardis.mod.schematics;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.registries.SchematicTypes;
import net.tardis.mod.schematics.types.SchematicType;
import net.tardis.mod.tileentities.ConsoleTile;
import org.apache.logging.log4j.Level;

/** Unlocks a ConsoleRoom for a Tardis*/
public class InteriorUnlockSchematic extends Schematic{

    private ResourceLocation consoleRoom;
    
    public InteriorUnlockSchematic() {
        super(SchematicTypes.INTERIOR.get());
    }
    
    public InteriorUnlockSchematic(SchematicType type) {
        super(type);
    }

    public void setConsoleRoom(ResourceLocation loc){
        this.consoleRoom = loc;
    }

    public ResourceLocation getConsoleRoom() {
        return consoleRoom;
    }

    @Override
    public boolean onConsumedByTARDIS(ConsoleTile tile, PlayerEntity entity) {
        ConsoleRoom interior = this.getRoomObject();
        if(interior != null) {
            if (!tile.getUnlockManager().getUnlockedConsoleRooms().contains(interior)) {
                tile.getUnlockManager().addConsoleRoom(interior);
                entity.sendStatusMessage(new TranslationTextComponent(TardisConstants.Translations.UNLOCKED_INTERIOR, interior.getDisplayName().getString()), true);
                return true;
            }
            else {
                entity.sendStatusMessage(new TranslationTextComponent(TardisConstants.Translations.ALREADY_UNLOCKED, this.getDisplayName()), true);
                return false;
            }
        }
        else {
        	Tardis.LOGGER.log(Level.ERROR, String.format("Error in interior schematic %s! %s is not a valid interior!"),
    		        this.getId().toString(), consoleRoom.toString());
    		return false;
        }
    }

    public ConsoleRoom getRoomObject(){
        return ConsoleRoom.getRegistry().get(this.consoleRoom);
    }

}
