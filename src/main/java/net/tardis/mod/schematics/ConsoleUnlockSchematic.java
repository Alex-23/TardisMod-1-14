package net.tardis.mod.schematics;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.misc.Console;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.registries.SchematicTypes;
import net.tardis.mod.schematics.types.SchematicType;
import net.tardis.mod.tileentities.ConsoleTile;

public class ConsoleUnlockSchematic extends Schematic{

    private ResourceLocation console;

    public ConsoleUnlockSchematic() {
        super(SchematicTypes.CONSOLE.get());
    }
    
    public ConsoleUnlockSchematic(SchematicType type) {
        super(type);
    }

    public void setConsole(ResourceLocation console){
        this.console = console;
    }

    public ResourceLocation getConsole(){
        return this.console;
    }

    @Override
    public boolean onConsumedByTARDIS(ConsoleTile tile, PlayerEntity player) {
        Console console = ConsoleRegistry.CONSOLE_REGISTRY.get().getValue(this.console);

        if(console != null && !tile.getUnlockManager().getUnlockedConsoles().contains(console)){
            tile.getUnlockManager().addConsole(console);
            player.sendStatusMessage(new TranslationTextComponent(TardisConstants.Translations.UNLOCKED_CONSOLE, this.getDisplayName()), true);
            return true;
        }
        else {
            player.sendStatusMessage(new TranslationTextComponent(TardisConstants.Translations.ALREADY_UNLOCKED, this.getDisplayName()), true);
            return false;
        }

    }
}
