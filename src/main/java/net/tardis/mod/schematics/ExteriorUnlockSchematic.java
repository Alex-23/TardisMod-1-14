package net.tardis.mod.schematics;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.registries.SchematicTypes;
import net.tardis.mod.schematics.types.SchematicType;
import net.tardis.mod.tileentities.ConsoleTile;
/** Unlocks an exterior for a Tardis*/
public class ExteriorUnlockSchematic extends Schematic{

    private ResourceLocation exterior;
    
    public ExteriorUnlockSchematic() {
        super(SchematicTypes.EXTERIOR.get());
    }
    
    public ExteriorUnlockSchematic(SchematicType type) {
        super(type);
    }

    public void setExterior(ResourceLocation loc){
        this.exterior = loc;
    }

    public ResourceLocation getExterior(){
        return this.exterior;
    }
    
    @Override
    public boolean onConsumedByTARDIS(ConsoleTile tile, PlayerEntity entity) {
        AbstractExterior ext = ExteriorRegistry.getExterior(this.exterior);

        if(ext != null && !tile.getUnlockManager().getUnlockedExteriors().contains(ext)) {
            tile.getUnlockManager().addExterior(ext);
            entity.sendStatusMessage(new TranslationTextComponent(TardisConstants.Translations.UNLOCKED_EXTERIOR, ext.getDisplayName().getString()), true);
            return true;
        }
        else {
            entity.sendStatusMessage(new TranslationTextComponent(TardisConstants.Translations.ALREADY_UNLOCKED, this.getDisplayName()), true);
            return false;
        }
    }

}
