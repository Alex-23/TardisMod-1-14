package net.tardis.mod.tileentities.exteriors;

import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.tileentities.TTiles;

public class TT2020ExteriorTile extends ExteriorTile{
	
	public static final AxisAlignedBB RENDER = new AxisAlignedBB(-1, -2, -1, 2, 4, 2);

	public TT2020ExteriorTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public TT2020ExteriorTile() {
		super(TTiles.EXTERIOR_TT2020_CAPSULE.get());
	}

	@Override
	public AxisAlignedBB getDoorAABB() {
		if(world != null && world.getBlockState(getPos()).hasProperty(BlockStateProperties.HORIZONTAL_FACING)) {
			switch(world.getBlockState(this.getPos()).get(BlockStateProperties.HORIZONTAL_FACING)) {
				case EAST: return new AxisAlignedBB(0.5, -1, 0, 1.1, 1, 1);
				case SOUTH: return new AxisAlignedBB(0, -1, 0.5, 1, 1, 1.1);
				case WEST: return new AxisAlignedBB(-0.1, -1, 0, 0.5, 1, 1);
				default: return new AxisAlignedBB(0, 0, -0.1, 1, 2, 0.5);
			}
		}
		return new AxisAlignedBB(0, 0, 0, 0, 0, 0);
		
	}

    @Override
    public AxisAlignedBB getRenderBoundingBox() {
        return RENDER.offset(getPos());
    }

}
