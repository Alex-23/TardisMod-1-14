package net.tardis.mod.tileentities;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;

/**
 * Created by Swirtzly
 * on 15/04/2020 @ 11:27
 */

public class SquarenessChamelonTile extends TileEntity implements ITickableTileEntity {

    private long timeplaced = -1;
    private BlockState state = Blocks.AIR.getDefaultState();

    public SquarenessChamelonTile() {
        super(TTiles.SQUARENESS.get());
    }

    public BlockState getState() {
        return state;
    }

    public void setState(BlockState state) {
        this.state = state;
    }

    @Override
    public void tick() {
        if (timeplaced == -1) {
            timeplaced = System.currentTimeMillis();
        }

        if (System.currentTimeMillis() - timeplaced >= 5000) {
            if (world != null) {
                if (world.isAirBlock(pos)) {
                    world.setBlockState(pos, state);
                } else {
                    InventoryHelper.spawnItemStack(world, getPos().getX(), getPos().getY(), getPos().getZ(), new ItemStack(state.getBlock()));
                }
            }
        }
    }


    @Override
    public CompoundNBT write(CompoundNBT compound) {
        compound.put("stored_block", NBTUtil.writeBlockState(getState()));
        return super.write(compound);
    }

    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        if (compound.contains("stored_block")) {
            setState(NBTUtil.readBlockState((CompoundNBT) compound.get("stored_block")));
        }
    }
}
