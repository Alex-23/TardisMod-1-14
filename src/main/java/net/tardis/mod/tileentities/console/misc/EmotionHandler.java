package net.tardis.mod.tileentities.console.misc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.registries.TraitRegistry;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.traits.TardisTrait;
import net.tardis.mod.traits.TardisTraitType;

public class EmotionHandler implements ITickable, INBTSerializable<CompoundNBT>{
    
    private static final ResourceLocation SAVE_KEY = new ResourceLocation(Tardis.MODID, "emotion");
    private TardisTrait[] traits = new TardisTrait[5];
    private ConsoleTile tile;
    /**
     * Map of player UUIDs and the loyalty value for that player.
     * <p> This loyalty value is between -100 and 100
     * <br> At 100 a Tardis will give the player all the perks (Transmat when low health, snap to open door, etc)
     */
    private HashMap<UUID, Integer> loyalties = Maps.newHashMap();
    private double mood = EnumHappyState.CONTENT.threshold;
    /** The last time a player occupied this TARDIS interior */
    private long lastTimePlayerInTardis = 0;
    /** If the Tardis already has existing traits*/
    private boolean hasGeneratedTraits = false;
    private Map<UUID, PlayerTelepathicConnection> connectedPlayers = Maps.newHashMap();
    
    public EmotionHandler(ConsoleTile console) {
        console.registerDataHandler(SAVE_KEY, this);
        console.registerTicker(this);
        this.tile = console;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        ListNBT loyaltyList = tag.getList("loyalties", NBT.TAG_COMPOUND);
        for(INBT l : loyaltyList) {
            CompoundNBT loyaltyTag = (CompoundNBT)l;
            this.loyalties.put(UUID.fromString(loyaltyTag.getString("id")), loyaltyTag.getInt("loyalty"));
        }
        
        this.mood = tag.getInt("mood");
        this.hasGeneratedTraits = tag.getBoolean("has_generated_traits");
        
        traits = new TardisTrait[5];
        
        if(tag.contains("trait_list")) {
            ListNBT list = tag.getList("trait_list", NBT.TAG_COMPOUND);
            int i = 0;
            for(INBT base : list) {
                CompoundNBT nbt = (CompoundNBT)base;
                TardisTrait trait = TraitRegistry.TRAIT_REGISTRY.get().getValue(new ResourceLocation(nbt.getString("registry_name"))).create();
                trait.setWeight(nbt.getDouble("weight"));
                this.traits[i] = trait;
                ++i;
            }
        }
        
        ListNBT connectedList = tag.getList("connectedPlayers", NBT.TAG_COMPOUND);
        this.connectedPlayers.clear();
        for(INBT con : connectedList) {
        	CompoundNBT conTag = (CompoundNBT)con;
        	UUID id = conTag.getUniqueId("id");
        	CompoundNBT connectionInfo = (CompoundNBT) conTag.get("connection_info");
            this.connectedPlayers.put(id, new PlayerTelepathicConnection(this.tile, connectionInfo));
        }
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        
        ListNBT loyaltyList = new ListNBT();
        for(Entry<UUID, Integer> loyalty : this.loyalties.entrySet()) {
            CompoundNBT loyTag = new CompoundNBT();
            
            loyTag.putString("id", loyalty.getKey().toString());
            loyTag.putInt("loyalty", loyalty.getValue());
            
            loyaltyList.add(loyTag);
        }
        tag.put("loyalties", loyaltyList);
        
        tag.putDouble("mood", this.mood);
        tag.putBoolean("has_generated_traits", this.hasGeneratedTraits);
        
        ListNBT traits = new ListNBT();
        for(TardisTrait trait : this.traits) {
            if(trait != null) {
                CompoundNBT traitTag = new CompoundNBT();
                traitTag.putString("registry_name", trait.getType().getRegistryName().toString());
                traitTag.putDouble("weight", trait.getWeight());
                traits.add(traitTag);
            }
        }
        tag.put("trait_list", traits);
        
        ListNBT connectedList = new ListNBT();
        for(Entry<UUID, PlayerTelepathicConnection> con : this.connectedPlayers.entrySet()) {
        	CompoundNBT connectionTag = new CompoundNBT();
        	connectionTag.putUniqueId("id", con.getKey());
        	connectionTag.put("connection_info", con.getValue().serializeNBT());
            connectedList.add(connectionTag);
        }
        tag.put("connectedPlayers", connectedList);
        return tag;
    }

    @Override
    public void tick(ConsoleTile console) {
        
        if(!console.getWorld().isRemote())
            fixOldData();
        
        if(console.getWorld().getGameTime() % 80 == 0) {
            double prevMood = mood;
            
            //Tardis in flight mood stuff
            if(console.isInFlight()) {
                //Unstabilized Flight doubles the mood
                if(mood < EnumHappyState.ECSTATIC.getTreshold() * 1.5) {
                    console.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
                        mood += (sys.isControlActivated() ? 1 : 2);
                    });
                }    
            }
            
            //If server, and interior empty, and player is on
            if(!console.getWorld().isRemote()) {
                if(((ServerWorld)console.getWorld()).getPlayers().isEmpty() && this.mood > EnumHappyState.SAD.getTreshold() * 1.75) {
                    
                    //If the current pilot is online
                    if(console.getPilot() != null) {
                        long abandonedTime = console.getWorld().getGameTime() - this.lastTimePlayerInTardis;
                        //If left for more than a day
                        int ticksToAbandoment = TConfig.SERVER.tardisAbandonmentTimer.get() * 24000;
                        if(abandonedTime > ticksToAbandoment) {
                            //If abandoned for more than set amount of days, tank mood
                            this.mood -= 0.5;
                        }
                    }
                }
                else {
                    //If not empty, set that the lastPlayerTime
                    this.lastTimePlayerInTardis = console.getWorld().getGameTime();
                }
                
                //Leave if all of the players whom the Tardis tracks the loyalty of, has a poor reputation and the Tardis is feeling discontent
                //If there is no player piloting the Tardis currently and it isn't in flight
                for (Entry<UUID, Integer> loyaltyEntry : this.loyalties.entrySet()) {
                    if(loyaltyEntry.getValue() <= - 50 && this.mood < EnumHappyState.DISCONTENT.threshold) {
                        if (console.getPilot() == null && !console.isInFlight()) {
                            if(ConsoleTile.rand.nextDouble() < 0.01) {
                                getPissyAndLeave(console);
                            }
                        }
                    }
                }
                
                if(tile.hasWorld() && !tile.getWorld().isRemote){
                    Map<UUID, PlayerTelepathicConnection> connectionsToRemove = Maps.newHashMap();

                    //Go through all connections and see if they should disconnect
                    for(Entry<UUID, PlayerTelepathicConnection> con : this.getConnectedPlayers().entrySet()) {
                        if(con.getValue().decrementAndCheckRemove(tile.getWorld().getServer())) {
                            connectionsToRemove.put(con.getKey(), con.getValue());
                        }
                    }

                    //Remove connections (New map and loop to avoid CMEs)
                    for(Entry<UUID, PlayerTelepathicConnection> con : connectionsToRemove.entrySet()) {
                        this.connectedPlayers.remove(con.getKey());
                    }
                }
                
            }
            
            //Particle Spawning
            if(prevMood > this.mood) {
                console.getWorld().addParticle(ParticleTypes.ANGRY_VILLAGER,
                        console.getPos().getX() + 0.5, console.getPos().getY() + 2, console.getPos().getZ() + 0.5, 0, 0.5, 0);
            }
            else if(prevMood < this.mood) {
                console.getWorld().addParticle(ParticleTypes.HEART,
                        console.getPos().getX() + 0.5, console.getPos().getY() + 2, console.getPos().getZ() + 0.5, 0, 0.5, 0);
            }
            
        }
        
        
        for(TardisTrait trait : this.traits) {
        	if (!console.getWorld().isRemote()) {//Ensure we are ticking traits on the logical server
        		if(trait != null)
                    trait.tick(console);
        	}
        }
        
    }
    /** Make the Tardis take off and travel to a random coordinate within a 150 block radius of its current position*/
    private void getPissyAndLeave(ConsoleTile console) {
        if(!console.isInFlight()) {
            console.getControl(HandbrakeControl.class).ifPresent(handBrake -> {
                if(handBrake.isFree()) {
                    console.getControl(ThrottleControl.class).ifPresent(throt -> throt.setAmount(1.0F));
                    console.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> sys.setControlActivated(true));
                    console.setDestination(console.getCurrentDimension(), console.randomizeCoords(console.getCurrentLocation(), 150));
                    console.takeoff();
                }
            });
        }
    }
    
    public void fixOldData() {
        if(!this.hasGeneratedTraits)
            this.onInitialSpawn();
    }
    
    public void setLoyalty(UUID id, int loyalty) {
        this.loyalties.put(id, loyalty);
    }
    
    /**
     * Gets the loyalty value for a player UUID.
     * <br> Returns 0 if not found, or is 0
     */
    public int getLoyalty(UUID playerUUID) {
        return this.loyalties.getOrDefault(playerUUID, 0);
    }

    /** Gets a set of player's UUIDs whose player is being tracked by the Tardis' loyalty*/
    public Set<UUID> getLoyaltyTrackingCrew() {
        return this.loyalties.keySet();
    }
    /** Gets the current raw mood value of the Tardis*/
    public double getMood() {
        return this.mood;
    }
    /** Gets the current mood in terms of a Mood state*/
    public EnumHappyState getMoodState() {
        for(EnumHappyState state : EnumHappyState.values()) {
            if(mood >= state.getTreshold())
                return state;
        }
        return EnumHappyState.SAD;
    }
    
    public void setMood(double mood) {
        this.mood = mood;
    }
    
    public void addMood(double mood) {
        this.mood += mood;
    }
    /** Adds loyalty to a player's uuid*/
    public void addLoyalty(UUID playerUUID, int loyalty) {
        int current = this.getLoyalty(playerUUID);
        this.loyalties.put(playerUUID, current + loyalty);
    }
    
    public TardisTrait[] getTraits() {
        return this.traits;
    }

    public void regenerateTraits()
    {
        traits = null;
        generateTardisTraits();
    }
    
    public void onInitialSpawn() {
        generateTardisTraits();
    }

    private void generateTardisTraits() {
        traits = new TardisTrait[5];

        int i = 0;
        //Make a copy of registered traits
        List<TardisTraitType> possibleTraits = Lists.newArrayList(TraitRegistry.TRAIT_REGISTRY.get().getValues());

        for(int x = 0; x < 25; ++x) {
            //If there are no traits to choose from or the TARDIS has three, stop
            if(possibleTraits.isEmpty() || x >= 3)
                break;

            //Generated Type
            TardisTraitType type = possibleTraits.get(ConsoleTile.rand.nextInt(possibleTraits.size()));

            //If this TARDIS already has a trait that conflicts with this one
            if(hasIncompatibleTrait(type))
                continue;

            this.traits[i] = type.create().setWeight(ConsoleTile.rand.nextDouble());
            possibleTraits.remove(type);
            ++i;

        }

        this.hasGeneratedTraits = true;
    }

    private boolean hasIncompatibleTrait(TardisTraitType type) {
        for(int y = 0; y < this.traits.length; ++y) {
            TardisTrait old = this.traits[y];
            //Stop if a pre-existing trait exists already
            if(old != null && !old.getType().isCompatible(type))
                return true;
        }
        return false;
    }
    
    public void addLoyalty(@Nullable PlayerEntity player, int loyalty) {
        if(player != null)
            this.addLoyalty(player.getUniqueID(), loyalty);
    }
    
    public Map<UUID, PlayerTelepathicConnection> getConnectedPlayers() {
        return this.connectedPlayers;
    }
    
    public void addConnectedPlayer(PlayerTelepathicConnection connection) {
        this.connectedPlayers.put(connection.getPlayerId(), connection);
    }
    
    public void removeConnectedPlayer(PlayerTelepathicConnection connection) {
        this.connectedPlayers.remove(connection.getPlayerId());
    }

    public static enum EnumHappyState{
        ECSTATIC(100, "ecstatic"),
        HAPPY(90, "happy"),
        CONTENT(50, "content"),
        APATHETIC(0, "apathetic"),
        DISCONTENT(-20, "discontent"),
        SAD(-100, "sad");
        
        int threshold = 0;
        TranslationTextComponent trans;
        
        EnumHappyState(int threshold, String key){
            this.threshold = threshold;
            this.trans = new TranslationTextComponent("mood.tardis." + key);
        }
        
        public int getTreshold() {
            return this.threshold;
        }
        
        public TranslationTextComponent getTranslationComponent() {
            return this.trans;
        }
    }
}
