package net.tardis.mod.recipe;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tags.TardisItemTags;
/** An old attempt at solving an issue where we couldn't have both regular item based recipes and ones that add fluids like Mercury*/
@Deprecated
public class AlembicRecipeMercury extends AlembicRecipe{

	public AlembicRecipeMercury() {
		super(500, 1, Ingredient.fromTag(TardisItemTags.CINNABAR), new ItemStack(TItems.MERCURY_BOTTLE.get()));
	}
	
	public int getMercuryAmount() {
		return 500;
	}

}
