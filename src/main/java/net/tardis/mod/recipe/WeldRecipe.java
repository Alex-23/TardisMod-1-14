package net.tardis.mod.recipe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.google.gson.JsonObject;
import com.mojang.serialization.Codec;
import com.mojang.serialization.Dynamic;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;
import net.tardis.mod.misc.CodecJsonDataListener;
import net.tardis.mod.misc.IngredientCodec;
import net.tardis.mod.tileentities.machines.QuantiscopeTile;
/** Custom Recipe format for the {@linkplain QuantiscopeTile}
 * <br> Allows for new items to be created from a list of ingredients, or "repair" a damaged input item to form a full durability version of the input*/
public class WeldRecipe implements IRecipe<WeldRecipeWrapper>{
    
    private Optional<RecipeResult> output;
    private Optional<Integer> processingTicks;
    private Ingredient ingredients;
    private Optional<Item> repairItem;
    private boolean repair = false;
    private ResourceLocation id;
    
    private static final Codec<WeldRecipe> CODEC = RecordCodecBuilder.create(instance -> 
    instance.group(
            Codec.BOOL.fieldOf("repair").forGetter(WeldRecipe::isRepair),
            Registry.ITEM.optionalFieldOf("repair_item").forGetter(WeldRecipe::getRepairItem),
            Codec.INT.optionalFieldOf("processing_ticks").forGetter(WeldRecipe::getProcessingTicks),
            IngredientCodec.INGREDIENT_CODEC.fieldOf("ingredients").forGetter(WeldRecipe::getRequiredIngredients),
            RecipeResult.CODEC.optionalFieldOf("result").forGetter(WeldRecipe::getResult)
            ).apply(instance, WeldRecipe::new)
    );
    
    
    /** Deprecated in favour of using vanilla IRecipe. This is left here as legacy code in case we encouter issues with the vanilla method 
     * <p>Registry for all recipes. 
     * <br> To access the values, call {@link CodecJsonDataListener#getData}*/
    @Deprecated
    public static final CodecJsonDataListener<WeldRecipe> DATA_LOADER = new CodecJsonDataListener<WeldRecipe>(
            "quantiscope",
            CODEC,
            Tardis.LOGGER);
    
    public static Collection<WeldRecipe> getAllRecipes(World world){
        return world.getRecipeManager().getRecipesForType(TardisRecipeSerialisers.WELD_RECIPE_TYPE);
    }
    
    /**
     * Custom Recipe format for the {@linkplain QuantiscopeTile}
     * <br> Allows for new items to be created from a list of ingredients, or "repair" a damaged input item to form a full durability version of the input
     * @param stack - If isRepair is true, this is the item to repair, else, this is the item to create
     * @param isRepair - If this recipe is a repair recipe or a creation one
     * @param ingredients - Items required for this recipe
     * @param result - the optional result. This is optional because in repair recipes, there is technially no new "result". The output is just the input item but at full durability
     * @impleNote Each recipe must be unique
     */
    public WeldRecipe(boolean isRepair, Optional<Item> repairItem, Ingredient ingredients, Optional<RecipeResult> result) {
        this.ingredients = ingredients;
        this.repair = isRepair;
        this.repairItem = repairItem;
        this.output = result;
        this.processingTicks = Optional.of(200);
    }
    
    public WeldRecipe(boolean isRepair, Optional<Item> repairItem, Optional<Integer> processingTicks, Ingredient ingredients, Optional<RecipeResult> result) {
        this(isRepair, repairItem, ingredients, result);
        this.processingTicks = processingTicks;
    }
    
    /** Our codec, a sort of wrapper object that allows MC to have a "template" so it can efficiently serialise/deserialise other file types into this WeldRecipe object type*/
    public static Codec<WeldRecipe> getCodec(){
        return CODEC;
    }

    /** Wrapper Object for the "result":{"item":"modid:item"} structure in the recipe json.
     * <br> This is required because since we use Codecs to de/serialise recipes, we can't have nested codecs, hence this is needed*/
    public static class RecipeResult{
        
        public static final Codec<RecipeResult> CODEC = RecordCodecBuilder.create(instance -> 
            instance.group(
                Registry.ITEM.fieldOf("item").forGetter(RecipeResult::getOutput)
            ).apply(instance, RecipeResult::new)
        );
        
        private Item output;
        
        public RecipeResult(Item output) {
            this.output = output;
        }
        
        public Item getOutput() {
            return this.output;
        }
        
        public void setOutput(Item input) {
            this.output = input;
        }
        
    }
    
    /** Gets the output of the recipe. 
     * <br> This is optional because when the input item is being "repaired", there is technically no "result" item because the result is actually the input item but with full durability
     * <br> Hence we don't really need a "result".
     * <br> For purposes of satisfying serialisation/deserialisation this is made an optional field*/
    public Optional<RecipeResult> getResult() {
        if (output.isPresent()) {
            return this.output;
        }
        else {
            return Optional.of(new RecipeResult(this.repairItem.get()));
        }
    }

    /** Gets the item that's required to repair the input item 
     * <br> This is optional because we can have both non repair or repair recipes*/
    public Optional<Item> getRepairItem(){
        return this.repairItem;
    }
    
    public Optional<Integer> getProcessingTicks(){
    	return this.processingTicks;
    }
    
    /** Get the raw required input ingredients for the recipe
     * <br> For display purposes/JEI use {@link #getIngredients()}*/
    public Ingredient getRequiredIngredients() {
        return this.ingredients;
    }
    
    /** If this recipe is a repair recipe that "repairs" a damaged input item to full durability*/
    public boolean isRepair() {
        return this.repair;
    }

    /** Tests if the recipe matches that of the one in the Quantiscope*/
    @Override
    public boolean matches(WeldRecipeWrapper inv, World worldIn) {
        /** Item in the repair slot, or {@link ItemStack#EMPTY} if not a repair recipe*/
        ItemStack stackInRepairSlot = inv.getStackInSlot(5);
        /** Items to check this recipe against */
        List<Item> inputItems = new ArrayList<>();
        for (int i = 0; i < 5; i ++) {
        	ItemStack stack = inv.getStackInSlot(i);
        	if (!stack.isEmpty())
        		inputItems.add(stack.getItem());
        }

        List<ItemStack> ingredientStacks = Arrays.asList(this.ingredients.getMatchingStacks());
        List<Item> ingredientItems = new ArrayList<>();
        for (ItemStack stack : ingredientStacks) {
        	if (!stack.isEmpty())
        		ingredientItems.add(stack.getItem());
        }
        //If the output item is present, check if the repair item is not the same item instance as the fully repaired version
        if (this.output.isPresent()) {
            if(stackInRepairSlot.getItem() != this.output.get().getOutput() && this.repair)
                return false;
        }
        //If the output is not there (the repair recipe doesn't contain the "result" field), and we are trying to use a repair recipe, don't start repairing
        else if (!this.output.isPresent()) {
            if (this.repair && this.repairItem.isPresent()) {
                //If the repair item in the quantiscope does not match the recipe's repair item
                //Fixes an edge case where one recipe contains an item that is also present in another recipe is allowed to be repaired
                //E.g. Chameleon Circuit requires Exotronic circuit. Navcom requires Exotronic and Xion Crystal
                //If we only put Exotronic and Navcom togethor, the old logic would have thought you were using the Chameleon Circuit recipe, which needs less materials
                if (stackInRepairSlot.getItem() != this.repairItem.get()) { 
                    return false;
                }
            }
        }

        //If this is not repairing but has a repair item in the slot
        if(!this.repair && !stackInRepairSlot.isEmpty())
            return false;
        
    	boolean firstItemNotInIngredient = false;
    	boolean matchInputAndIngredientSize = false;
    	
    	//If there isn't anything in the input slots, don't do anything
    	if (inputItems.isEmpty()) {
    		return false;
    	}
    	//Check if the ingredient contains an instance of the input items
    	for (Item item : inputItems) {
    		if (!ingredientItems.contains(item)) {
    			firstItemNotInIngredient = true;
    		}
    		else {
    			firstItemNotInIngredient = false;
    		}
    	}
    	//Check if all input items contain the ingredients. 
    	//There can be cases where the ingredient contains one instance of the input, like 1 gold ingot in input, but the recipe actually needs 2 gold ingots
    	if (firstItemNotInIngredient) {
    		for (Item item : ingredientItems) {
        		if (!inputItems.contains(item)) {
        			return false;
        		}
        		else {
        			matchInputAndIngredientSize = true;
        		}
        	}
    	}
    	else {
    		matchInputAndIngredientSize = true;
    	}
    	//If both checks pass, check if the input item size is not equal to the ingredient size (for cases if the item is empty for some reason)
    	if (matchInputAndIngredientSize) {
    		if(inputItems.size() != ingredientItems.size()) {
    			return false;
    		}
    		//If every ingredient is in the input list
    		else if (!inputItems.containsAll(ingredientItems)){
    			return false;
    		}
    		//If every input is in the ingredient list
    		//Fixes edge cases like sonic acticator which contains the ingredients sonic end does, which produces a false positive
    		else if (!ingredientItems.containsAll(inputItems)){
    			return false;
    		}
    	}
    	
        return true;
    }

    /** The result of the recipe if this was being used in the Crafting Table
     * <br> Since this can only be used in a Quantiscope, this method is redundant and only here because it's part of the required methods of IRecipe*/
    @Override
    public ItemStack getCraftingResult(WeldRecipeWrapper inv) {
        if (output.isPresent()) {
            return new ItemStack(this.output.get().getOutput());
        }
        else {
            return new ItemStack(new RecipeResult(this.repairItem.get()).getOutput());
        }
    }

    @Override
    public boolean canFit(int width, int height) {
        return false;
    }

    /** Vanilla's way of getting the recipe's result with Itemstack sensitive context
     * <br> We do not need to use this*/
    @Override
    public ItemStack getRecipeOutput() {
        if (output.isPresent()) {
            return new ItemStack(this.output.get().getOutput());
        }
        else {
            return new ItemStack(new RecipeResult(this.repairItem.get()).getOutput());
        }
    }

    @Override
    public ResourceLocation getId() {
        return this.id;
    }

    /** Sets the id for this recipe. ALWAYS call this when making a new instance of this, as the registry name is null by default*/
    public WeldRecipe setRegistryId(ResourceLocation id) {
        this.id = id;
        return this;
    }

    @Override
    public IRecipeSerializer<?> getSerializer() {
        return TardisRecipeSerialisers.QUANTISCOPE_SERIALISER.get();
    }

    @Override
    public IRecipeType<?> getType() {
        return TardisRecipeSerialisers.WELD_RECIPE_TYPE;
    }
    
    @Override
    public boolean isDynamic() {
        return false; //If false, allows recipe unlocking to be available (though this doesn't actually work)
    }

    @Override
    public String getGroup() {
        return " "; //Must have a value, otherwise vanilla will complain about a null recipe category type
    }
    
    /** Gets the ingredients in the WeldRecipe. <br> Use this for JEI display purposes*/
    @Override
	public NonNullList<Ingredient> getIngredients() {
    	NonNullList<Ingredient> nonnulllist = NonNullList.create();
    	nonnulllist.add(this.ingredients);
		return nonnulllist;
	}


	/** The recipe serialiser implementation. 
     * <br> Making this allows vanilla to automatically add our recipe types onto its recipe packet entry and reload listener 
     * <p> A potential issue with this is if we have have many mods togethor that add loads of recipes, the packet could be too large
     * <br> Forge does have a workaround for this in the latest builds by splitting large vanilla packets
     * */
    public static class WeldRecipeSerializer extends ForgeRegistryEntry<IRecipeSerializer<?>> 
    implements IRecipeSerializer<WeldRecipe>{

        @Override
        public WeldRecipe read(ResourceLocation recipeId, JsonObject json) {
            WeldRecipe recipe = CODEC.parse(JsonOps.INSTANCE, json).resultOrPartial(Tardis.LOGGER::error).get();
            recipe.setRegistryId(recipeId); //We have to explicitly set the registry name to avoid a NPE
            return recipe;
        }

        @Override
        public WeldRecipe read(ResourceLocation recipeId, PacketBuffer buffer) {
            WeldRecipe recipe = CODEC.parse(NBTDynamicOps.INSTANCE, buffer.readCompoundTag()).resultOrPartial(Tardis.LOGGER::error).get();
            recipe.setRegistryId(recipeId); //We have to explicitly set the registry name to avoid a NPE
            return recipe;
        }

        @Override
        public void write(PacketBuffer buffer, WeldRecipe recipe) {
            buffer.writeCompoundTag((CompoundNBT) CODEC.encodeStart(NBTDynamicOps.INSTANCE, recipe).resultOrPartial(Tardis.LOGGER::error).orElse(new CompoundNBT()));
        }
    }
    
    /**
     * DEPRECATED - Use {@link WeldRecipe#matches(WeldRecipeWrapper, World)}
     * @param repair - Item in the repair slot, or {@link ItemStack#EMPTY} if not a repair recipe
     * @param recipe - Items to check this recipe against
     * @return - If it matches this recipe
     */
    @Deprecated
    public boolean matches(ItemStack repair, ItemStack... recipe) {
//        List<Item> rec = new ArrayList<>();
//        
//        if(repair.getItem() != this.output.get().getOutput() && this.repair)
//            return false;
//        
//        //If this is not repairing but has a repair item return false
//        if(!this.repair && !repair.isEmpty())
//            return false;
//        
//        for(ItemStack s : recipe) {
//            if(!s.isEmpty())
//                rec.add(s.getItem());
//        }
//        if(rec.size() != ingredients.size())
//            return false;
//        
//        for(Item s : ingredients) {
//            if(!rec.contains(s))
//                return false;
//        }
//        
//        for(Item s : rec) {
//            if(!ingredients.contains(s))
//                return false;
//        }
        
        return true;
    }

    /** Unused, if used, can be done in a sync packet or in nbt.*/
    @Deprecated
    public CompoundNBT serialize() {
        CompoundNBT tag = new CompoundNBT();
        CODEC.encodeStart(NBTDynamicOps.INSTANCE, this).resultOrPartial(Tardis.LOGGER::error).ifPresent(data -> {
           tag.put("weld_recipe", data);    
        });
        return tag;
    }

    /** Unused, if used, can be done in a sync packet or in nbt.*/
    @Deprecated
    public static WeldRecipe deserialize(CompoundNBT tag) {
        WeldRecipe recipe = CODEC.parse(new Dynamic<>(NBTDynamicOps.INSTANCE)).resultOrPartial(Tardis.LOGGER::error).get();
        return recipe;
    }
}
