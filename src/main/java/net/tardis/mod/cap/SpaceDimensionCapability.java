package net.tardis.mod.cap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.tardis.mod.world.dimensions.TDimensions;
/**
 * Implementation of ISpaceDimProperties
 * @author 50ap5ud5
 *
 */
public class SpaceDimensionCapability implements ISpaceDimProperties{
    
    private RegistryKey<World> world;
    
    public SpaceDimensionCapability(RegistryKey<World> world) {
        this.world = world;
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();
        tag.putString("world", this.world.getLocation().toString());
        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt) {
        this.world = RegistryKey.getOrCreateKey(Registry.WORLD_KEY, new ResourceLocation(nbt.getString("world")));
    }

    @Override
    public Vector3d modMotion(Entity ent) {
      if (this.world == TDimensions.SPACE_DIM) {
          ent.fallDistance = 0;
          if (ent instanceof ServerPlayerEntity)
              ((ServerPlayerEntity) ent).connection.floatingTickCount = 0;
      }
      if (this.world == TDimensions.MOON_DIM) {
        if(ent instanceof PlayerEntity) {
            if(((PlayerEntity)ent).abilities.isFlying)
                return ent.getMotion();
        }
        if(!ent.hasNoGravity()) {
            ent.fallDistance -= 0.25;
            return ent.getMotion().add(0, 0.05, 0);
        }
        return ent.getMotion();
      }
      return ent.getMotion().add(0, 0.08, 0);
    }

    @Override
    public boolean isZeroG() {
        return world == TDimensions.SPACE_DIM;
    }

    @Override
    public boolean hasAir() {
        if (this.world == TDimensions.SPACE_DIM || this.world == TDimensions.MOON_DIM)
            return false;
        return true;
    }

}
