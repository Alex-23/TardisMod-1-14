package net.tardis.mod.cap;

import it.unimi.dsi.fastutil.longs.Long2IntMap;
import it.unimi.dsi.fastutil.longs.Long2IntOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.LongArrayNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.Capability.IStorage;
import net.minecraftforge.common.world.ForgeChunkManager;
import net.tardis.mod.Tardis;

import javax.annotation.Nullable;

/**
 * Created by 50ap5ud5
 * on 7 Mar 2020 @ 9:55:32 am
 * based off https://github.com/LexManos/ChunkNoGoByeBye/blob/master/src/main/java/net/minecraftforge/lex/cngbb/ChunkLoaderList.java
 */
public class ChunkLoaderCapability implements IChunkLoader {
    @Nullable
    private final ServerWorld world;
    private Long2IntMap refCount = new Long2IntOpenHashMap();
    private LongSet loadedSet = new LongOpenHashSet();
    private boolean isChunkLoading = false;

    public ChunkLoaderCapability(@Nullable ServerWorld world) {
        refCount.defaultReturnValue(Integer.MIN_VALUE);
        this.world = world;
    }

    @Override
    public void add(BlockPos pos) {
        long block = pos.toLong();
        if (!loadedSet.contains(block)) {
            long chunk = toChunk(pos);
            int ref = refCount.get(chunk);
            if (ref == Integer.MIN_VALUE) {
                if (!isChunkLoading)
                    force(pos);
                ref = 1;
            } else {
                ref += 1;
            }
            refCount.put(chunk, ref);
            loadedSet.add(block);
        }
    }


    @Override
    public void remove(BlockPos pos) {
        if (loadedSet.remove(pos.toLong())) {
            long chunk = toChunk(pos);
            int ref = refCount.get(chunk);

            if (ref == Integer.MIN_VALUE || --ref <= 0) {
                if (!isChunkLoading)
                    unforce(pos);
                refCount.remove(chunk);
            } else {
                refCount.put(chunk, ref);
            }
        }
    }

    @Override
    public boolean contains(BlockPos pos) {
        return loadedSet.contains(pos.toLong());
    }

    private final long toChunk(BlockPos pos) {
        return ChunkPos.asLong(pos.getX() >> 4, pos.getZ() >> 4);
    }

    private void force(BlockPos pos) {
        forceload(pos, false, true);
    }

    private void unforce(BlockPos pos) {
        forceload(pos, false, true);
    }


    private void forceload(BlockPos pos, boolean force, boolean ticking) {
        if (this.world == null || this.world.getServer() == null) return;
        ChunkPos cPos = new ChunkPos(pos);
        ForgeChunkManager.forceChunk(world, Tardis.MODID, pos, cPos.x, cPos.z, force, ticking);
        Tardis.LOGGER.debug("Chunk Loader Capability: Attempting to {} chunk ticket. Pos: {} World: {}", force? "add" : "remove", pos, world.getDimensionKey().getLocation());
    }
    @Deprecated
    public static class LoaderStorage implements IStorage<IChunkLoader> {
        @Override
        public INBT writeNBT(Capability<IChunkLoader> capability, IChunkLoader instance, Direction side) {
            if (!(instance instanceof ChunkLoaderCapability))
                return null;

            ChunkLoaderCapability list = (ChunkLoaderCapability) instance;
            long[] data = new long[list.loadedSet.size()];
            int idx = 0;
            for (long l : list.loadedSet)
                data[idx++] = l;
            return new LongArrayNBT(data);
        }

        @Override
        public void readNBT(Capability<IChunkLoader> capability, IChunkLoader instance, Direction side, INBT nbt) {
            if (!(instance instanceof ChunkLoaderCapability) || !(nbt instanceof LongArrayNBT)) return;
            ChunkLoaderCapability list = (ChunkLoaderCapability) instance;
            list.isChunkLoading = true;
            list.refCount.clear();
            list.loadedSet.clear();
            try {
                for (long l : ((LongArrayNBT) nbt).getAsLongArray()) {
                    list.add(BlockPos.fromLong(l));
                }
                if (list.world != null) {
                    // Run the force commands next tick to make sure they wern't removed.
                    list.world.getServer().enqueue(new TickDelayedTask(1, () -> {
                        for (long l : list.refCount.keySet()) {
                            ChunkPos chunk = new ChunkPos(l);
                            list.force(new BlockPos(chunk.x << 4, 0, chunk.z << 4)); //force loads chunks with x and z positions offest by 4
                        }
                    }));
                }
            } finally {
                list.isChunkLoading = false;
            }
        }
    }
}
