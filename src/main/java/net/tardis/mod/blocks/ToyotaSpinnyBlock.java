package net.tardis.mod.blocks;

import net.tardis.mod.ars.IARS;
import net.tardis.mod.properties.Prop;

public class ToyotaSpinnyBlock extends MultiblockBlock implements IARS {
    public ToyotaSpinnyBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }
}
