package net.tardis.mod.blocks;


import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fml.network.NetworkHooks;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.containers.EngineContainer;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TardisEngineTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class TardisEngineBlock extends NotSolidTileBlock implements IARS {

    public TardisEngineBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {


        TileEntity te = worldIn.getTileEntity(pos);
        if (te instanceof TardisEngineTile && !worldIn.isRemote) {

			LazyOptional<ITardisWorldData> data = worldIn.getCapability(Capabilities.TARDIS_DATA);
			data.ifPresent((cap) -> {
				Direction dir = (hit.getFace() != Direction.UP && hit.getFace() != Direction.DOWN) ? hit.getFace() : player.getHorizontalFacing().getOpposite();
				PanelInventory inv = cap.getEngineInventoryForSide(dir);
				NetworkHooks.openGui((ServerPlayerEntity) player, new INamedContainerProvider() {

					@Override
					public Container createMenu(int id, PlayerInventory playerInv, PlayerEntity ent) {
						return new EngineContainer(id, playerInv, inv, dir);
					}

					@Override
					public ITextComponent getDisplayName() {
						return inv.getName();
					}
				}, buf -> buf.writeInt(dir.getIndex()));
			});

			if(!data.isPresent() && !worldIn.isRemote)
				player.sendStatusMessage(TardisConstants.Translations.NO_USE_OUTSIDE_TARDIS, true);
        }
        return ActionResultType.SUCCESS;
    }

}
