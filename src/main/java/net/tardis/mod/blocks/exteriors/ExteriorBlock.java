package net.tardis.mod.blocks.exteriors;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.IPlantable;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ExteriorBlock extends NotSolidTileBlock implements IDontBreak {

    public static TranslationTextComponent LOCKED = new TranslationTextComponent("message.tardis.door.locked");
    public static TranslationTextComponent UNLOCKED = new TranslationTextComponent("message.tardis.door.unlocked");
    public static TranslationTextComponent DEADLOCKED = new TranslationTextComponent("message.tardis.door.deadlocked");
    public static String INTERIOR_CHANGE = "message.tardis.door.interior_change";
    public int rand;

    public ExteriorBlock() {
        super(Properties.create(Material.BARRIER).sound(SoundType.WOOD).hardnessAndResistance(2.0F, 3.0F).variableOpacity().notSolid());
        this.setDefaultState(this.getDefaultState()
                .with(BlockStateProperties.HORIZONTAL_FACING, Direction.NORTH)
                .with(BlockStateProperties.WATERLOGGED, false));
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {

        if (!worldIn.isRemote) {
            TileEntity te = worldIn.getTileEntity(pos);
            if (te instanceof ExteriorTile) {
                ExteriorTile exterior = (ExteriorTile) te;
                if (exterior.getMatterState() == EnumMatterState.SOLID) {

                    //Exterior naming
                    ItemStack held = player.getHeldItemMainhand() != ItemStack.EMPTY ? player.getHeldItemMainhand() : player.getHeldItemOffhand();
                    if (held.getItem() == Items.NAME_TAG) {
                        exterior.setCustomName(held.hasDisplayName() ? held.getDisplayName().getString() : "");
                        if (!player.isCreative())
                            player.getHeldItem(handIn).shrink(1);
                        return ActionResultType.SUCCESS;
                    }

                    if (exterior.isKeyValid(held)) {
                        if (!exterior.isExteriorDeadLocked() && !exterior.isInteriorRegenerating()) {
                            exterior.toggleLocked();
                            worldIn.playSound(null, pos, exterior.getLocked() ? TSounds.DOOR_LOCK.get() : TSounds.DOOR_UNLOCK.get(), SoundCategory.BLOCKS, 1F, 1F);
                            player.sendStatusMessage(exterior.getLocked() ? LOCKED : UNLOCKED, true);
                        }
                        else {
                            if (exterior.isInteriorRegenerating())
                                player.sendStatusMessage(new TranslationTextComponent(ExteriorBlock.INTERIOR_CHANGE, exterior.getRemainingInteriorChangeTime()), true);
                            else if (exterior.isExteriorDeadLocked())
                                player.sendStatusMessage(DEADLOCKED, true);
                        }
                        
                    } else exterior.open(player);
                }
            }
            return ActionResultType.SUCCESS;
        }
        return ActionResultType.SUCCESS;//Return true so players don't teleport their held items into the interior
    }

    @Override
    public void onProjectileCollision(World worldIn, BlockState state, BlockRayTraceResult hit, ProjectileEntity projectile) {
        ((ExteriorTile) worldIn.getTileEntity(hit.getPos())).damage(4);
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.get(BlockStateProperties.WATERLOGGED) ? Fluids.WATER.getDefaultState() : Fluids.EMPTY.getDefaultState();
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.WATERLOGGED);
        builder.add(BlockStateProperties.HORIZONTAL_FACING);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.ENTITYBLOCK_ANIMATED;
    }

    @Override
    public void onBlockClicked(BlockState state, World worldIn, BlockPos pos, PlayerEntity player) {
        if (!worldIn.isRemote) {
            ExteriorTile exterior = (ExteriorTile) worldIn.getTileEntity(pos);
            if (exterior != null && exterior.getLocked()) {
                TardisHelper.getConsole(worldIn.getServer(), exterior.getInteriorDimensionKey()).ifPresent(console -> console.getWorld().playSound(null, console.getPos(), TSounds.DOOR_KNOCK.get(), SoundCategory.BLOCKS, 1F, 1F));
                worldIn.playSound(null, pos, TSounds.DOOR_KNOCK.get(), SoundCategory.BLOCKS, 1F, 1F);
            }
        }
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.hasTileEntity() && state.getBlock() != newState.getBlock()) {
            ((ExteriorTile) worldIn.getTileEntity(pos)).fall();
            worldIn.removeTileEntity(pos);

            if(worldIn instanceof ServerWorld)
                WorldHelper.unForceChunkIfLoaded((ServerWorld) worldIn, new ChunkPos(pos), pos);

        }
    }

    @Override
    public void onBlockAdded(BlockState state, World worldIn, BlockPos pos, BlockState oldState, boolean isMoving) {
        super.onBlockAdded(state, worldIn, pos, oldState, isMoving);
        if(worldIn instanceof ServerWorld)
            WorldHelper.forceChunkIfNotLoaded((ServerWorld) worldIn, new ChunkPos(pos), pos);
    }

    /**
      * TODO:1.16: Use Block tags.
      */
//    @Override
//    public boolean canBeConnectedTo(BlockState state, IBlockReader world, BlockPos pos, Direction facing) {
//        return true;
//    }

    @Override
    public boolean canSustainPlant(BlockState state, IBlockReader world, BlockPos pos, Direction facing, IPlantable plantable) {
        return true;
    }
    
    @Override
       public int getLightValue(BlockState state, IBlockReader world, BlockPos pos) {
           TileEntity te = world.getTileEntity(pos);
           if(te instanceof ExteriorTile)
               return ((ExteriorTile)te).getLightEmittingLevel();
           return super.getLightValue(state, world, pos);
       }

}
