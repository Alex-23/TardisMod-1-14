package net.tardis.mod.blocks.exteriors;

import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.misc.TardisFortunes;

public class FortuneExteriorBlock extends ExteriorBlock {

    public FortuneExteriorBlock() {
        super();
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (hit.getFace() == state.get(BlockStateProperties.HORIZONTAL_FACING).getOpposite()) {
            if (worldIn.isRemote) {
            	if (TardisFortunes.FORTUNES.isEmpty())
                    TardisFortunes.readFortunes();
                if (TardisFortunes.FORTUNES.size() > 0)
                    player.sendMessage(new StringTextComponent(TardisFortunes.FORTUNES.get(worldIn.rand.nextInt(TardisFortunes.FORTUNES.size() - 1))), Util.DUMMY_UUID);
            }
            return ActionResultType.SUCCESS;
        }
        return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
    }   

}
