package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.helper.VoxelShapeUtils;
import net.tardis.mod.properties.Prop;

import java.util.List;
import java.util.stream.Stream;

public class RoundelTapBlock extends NotSolidTileBlock {
    public static final BooleanProperty PUSH = BooleanProperty.create("push");
    public static final TranslationTextComponent PUSH_TO_TARDIS = new TranslationTextComponent("message.tardis.roundel_tap.push_to_tardis");
    public static final TranslationTextComponent PULL_TO_BLOCKS_FROM_TARDIS = new TranslationTextComponent("message.tardis.roundel_tap.push_to_blocks");
    public static final VoxelShape NORTH = createVoxelShape();
    public static final VoxelShape EAST = VoxelShapeUtils.rotateHorizontal(createVoxelShape(), Direction.EAST);
    public static final VoxelShape SOUTH = VoxelShapeUtils.rotateHorizontal(createVoxelShape(), Direction.SOUTH);
    public static final VoxelShape WEST = VoxelShapeUtils.rotateHorizontal(createVoxelShape(), Direction.WEST);
    public static final VoxelShape UP = VoxelShapeUtils.rotate(createVoxelShape(), Direction.fromAngle(90));
    public static final VoxelShape DOWN = VoxelShapeUtils.rotate(createVoxelShape(), Direction.fromAngle(-90));

    private final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.roundel_tap.purpose"));
	private final IFormattableTextComponent descriptionTooltipTwo = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip.roundel_tap.use"));
    
    
    public RoundelTapBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    public static VoxelShape createVoxelShape() {
        return Stream.of(
                Block.makeCuboidShape(1, 1, 9, 15, 15, 16),
                Block.makeCuboidShape(6, 13.5, 5, 10, 15.5, 9),
                Block.makeCuboidShape(6.625, 10, 5.625, 9.375, 16, 8.375),
                Block.makeCuboidShape(6, 10, 5, 10, 12, 9),
                Block.makeCuboidShape(4.125, 6, 5, 6.125, 10, 9),
                Block.makeCuboidShape(0.375, 6, 5, 2.375, 10, 9),
                Block.makeCuboidShape(0, 6.625, 5.625, 6, 9.375, 8.375),
                Block.makeCuboidShape(6, 6, 3, 10, 10, 5),
                Block.makeCuboidShape(6.5, 6.625, 0, 9.375, 9.375, 5),
                Block.makeCuboidShape(6, 6, 0.5, 10, 10, 2.5),
                Block.makeCuboidShape(6.625, 0, 5.625, 9.375, 6, 8.375),
                Block.makeCuboidShape(6, 0.25, 5, 10, 2.25, 9),
                Block.makeCuboidShape(6, 4, 5, 10, 6, 9),
                Block.makeCuboidShape(9.875, 6, 5, 11.875, 10, 9),
                Block.makeCuboidShape(10, 6.625, 5.625, 16, 9.375, 8.375),
                Block.makeCuboidShape(13.625, 6, 5, 15.625, 10, 9)
        ).reduce((v1, v2) -> {
            return VoxelShapes.combineAndSimplify(v1, v2, IBooleanFunction.OR);
        }).get();
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(BlockStateProperties.FACING)) {
            case NORTH:
                return NORTH;
            case EAST:
                return EAST;
            case SOUTH:
                return SOUTH;
            case UP:
                return UP;
            case DOWN:
                return DOWN;
            default:
                return WEST;
        }
    }

    @Override
    public void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.FACING).add(PUSH);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return super.getStateForPlacement(context).with(BlockStateProperties.FACING, context.getNearestLookingDirection().getOpposite());
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(TardisConstants.Translations.TOOLTIP_CONTROL);
        if (Screen.hasControlDown()) {
            tooltip.clear();
            tooltip.add(0, stack.getDisplayName());
            tooltip.add(TardisConstants.Translations.NO_USE_OUTSIDE_TARDIS);
            tooltip.add(descriptionTooltip);
            tooltip.add(descriptionTooltipTwo);
        }
    }

    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack) {
        if (placer instanceof PlayerEntity) { //Warn the player that it only works in the tardis
            if (WorldHelper.isDimensionBlocked(worldIn))
                PlayerHelper.sendMessageToPlayer((PlayerEntity) placer, TardisConstants.Translations.NO_USE_OUTSIDE_TARDIS, true);
        }
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player,
            Hand hand, BlockRayTraceResult hit) {
        if (!world.isRemote()) {
            BlockState newState = WorldHelper.cycleBlockStateProperty(state, PUSH);
            world.setBlockState(pos, newState, 3);
            boolean isPushingToTardis = newState.get(PUSH);
            player.sendStatusMessage(!isPushingToTardis ? PUSH_TO_TARDIS : PULL_TO_BLOCKS_FROM_TARDIS, true);
        }
        return ActionResultType.SUCCESS;
    }
    
}
