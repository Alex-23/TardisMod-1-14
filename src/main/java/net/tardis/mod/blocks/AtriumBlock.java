package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.properties.Prop;

public class AtriumBlock extends Block {

    public AtriumBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    @Override
    public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip,
            ITooltipFlag flagIn) {
        tooltip.add(TardisConstants.Translations.TOOLTIP_DISABLED_ITEM);
    }
}
