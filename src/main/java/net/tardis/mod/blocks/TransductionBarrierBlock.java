package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.IFormattableTextComponent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.TextHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.sounds.TSounds;

public class TransductionBarrierBlock extends TileBlock {
	
	public static final BooleanProperty ACTIVATED = BooleanProperty.create("activated");
    private final IFormattableTextComponent descriptionTooltip = TextHelper.createDescriptionItemTooltip(new TranslationTextComponent("tooltip.tardis.transduction_barrier.line1"));
    private final IFormattableTextComponent descriptionTooltipTwo = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip.tardis.transduction_barrier.line2"));
    private final IFormattableTextComponent descriptionTooltipThree = TextHelper.createExtraLineItemTooltip(new TranslationTextComponent("tooltip.tardis.transduction_barrier.line3"));

    public TransductionBarrierBlock(Properties prop) {
        super(prop);
        this.setShowTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        ItemStack sonic = new ItemStack(TItems.SONIC.get());
        if (player.getHeldItem(handIn).isItemEqual(sonic)) {
            return ActionResultType.SUCCESS;
        } else {
            PlayerHelper.sendMessageToPlayer(player, TardisConstants.Translations.REQUIRES_SONIC, true);
            return ActionResultType.SUCCESS;
        }
    }
    
    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(ACTIVATED);
        super.fillStateContainer(builder);
    }
    
    @Override
    public void neighborChanged(BlockState state, World world, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        if (!world.isRemote) {
           boolean powered = world.isBlockPowered(pos);
           if (powered != state.get(ACTIVATED)) {
        	   BlockState newState = state.with(ACTIVATED, Boolean.valueOf(powered));
        	   world.playSound(null, pos, powered ? TSounds.SUBSYSTEMS_ON.get() : TSounds.SUBSYSTEMS_OFF.get(), SoundCategory.BLOCKS, 0.5F, 1F);
        	   world.setBlockState(pos, newState, 2);
           }
        }
    }
    
    @Override
	public BlockState getStateForPlacement(BlockItemUseContext context) {
		return this.getDefaultState().with(ACTIVATED, false);
	}

	@Override
	public void createDescriptionTooltips(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
		tooltip.add(descriptionTooltip);
		tooltip.add(descriptionTooltipTwo);
		tooltip.add(descriptionTooltipThree);
	}
}
