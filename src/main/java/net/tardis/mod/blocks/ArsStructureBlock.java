package net.tardis.mod.blocks;

import java.util.List;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.template.TooltipProviderBlock;
import net.tardis.mod.properties.Prop;

/**
 * A normal cube block but allows for tooltips to be added
 */
public class ArsStructureBlock extends TooltipProviderBlock implements IARS{

    public ArsStructureBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
        this.setShowTooltips(true);
        this.setHasDescriptionTooltips(true);
    }

	@Override
	public void createDescriptionTooltips(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip,
	        ITooltipFlag flagIn) {
		tooltip.add(new TranslationTextComponent("tooltip.ars_structure.placement_offset", 2).mergeStyle(TextFormatting.GRAY));
	}

}
