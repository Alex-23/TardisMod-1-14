package net.tardis.mod.blocks;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootContext.Builder;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDontBreak;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.world.dimensions.TDimensions;

public class ConsoleBlock extends NotSolidTileBlock implements IDontBreak {

    public ConsoleBlock() {
        super(Properties.create(Material.BARRIER).variableOpacity().hardnessAndResistance(99999F, 99999F));
    }

    @Override
    public void onBlockAdded(BlockState state, World world, BlockPos pos, BlockState oldState, boolean isMoving) {
    	if (world.isRemote) return;
        //Make consoles go away if not in tardis dim
        if (!WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)) {
            world.setBlockState(pos, Blocks.AIR.getDefaultState(), 2);
            return;
        }
        if (!world.isRemote) {
        	ServerWorld sWorld = world.getServer().getWorld(world.getDimensionKey());
        	ChunkPos chunkPos = new ChunkPos(pos);
        	WorldHelper.forceChunkIfNotLoaded(sWorld, chunkPos, pos);       
        }
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return type.create();
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public List<ItemStack> getDrops(BlockState state, Builder builder) {
        return new ArrayList<ItemStack>();
    }

    @Override
    public int getOpacity(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return 0;
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
    	if (worldIn.isRemote) return;
    	if (state.hasTileEntity() && (state.getBlock() != newState.getBlock() || !newState.hasTileEntity())) {
            worldIn.removeTileEntity(pos);
            if(worldIn instanceof ServerWorld)
                WorldHelper.unForceChunkIfLoaded((ServerWorld) worldIn, new ChunkPos(pos), pos);
        }

        //Make consoles go away if not in tardis dim
        if (!WorldHelper.areDimensionTypesSame(worldIn, TDimensions.DimensionTypes.TARDIS_TYPE)) {
            worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 2);
            return;
        }

        if (!worldIn.isRemote) {
            if (newState.getBlock() != state.getBlock()) {
                TileEntity te = worldIn.getTileEntity(pos);
                if (te instanceof ConsoleTile)
                    ((ConsoleTile) te).removeControls();
            }
            if (newState.getBlock() instanceof ConsoleBlock) {
                TileEntity te = worldIn.getTileEntity(pos);
                if (te instanceof ConsoleTile)
                    ((ConsoleTile) te).getOrCreateControls();
            }
        }
        
    }

}
