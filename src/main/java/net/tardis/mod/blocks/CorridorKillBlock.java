package net.tardis.mod.blocks;

import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.template.NotSolidTileBlock;

public class CorridorKillBlock extends NotSolidTileBlock implements IARS{

    public CorridorKillBlock(Properties properties) {
        super(properties);
    }

}
