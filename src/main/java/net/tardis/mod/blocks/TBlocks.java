package net.tardis.mod.blocks;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.GlassBlock;
import net.minecraft.block.SandBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.WallBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.exteriors.ClockExteriorBlock;
import net.tardis.mod.blocks.exteriors.ExteriorBlock;
import net.tardis.mod.blocks.exteriors.DisguiseExteriorBlock;
import net.tardis.mod.blocks.exteriors.FortuneExteriorBlock;
import net.tardis.mod.blocks.exteriors.TardisExteriorBottomBlock;
import net.tardis.mod.blocks.monitor.MonitorDynamicBlock;
import net.tardis.mod.blocks.monitor.MonitorEyeBlock;
import net.tardis.mod.blocks.monitor.MonitorRCABlock;
import net.tardis.mod.blocks.monitor.MonitorRotateBlock;
import net.tardis.mod.blocks.monitor.MonitorSteampunkBlock;
import net.tardis.mod.blocks.template.CubeBlock;
import net.tardis.mod.blocks.template.LightBlock;
import net.tardis.mod.blocks.template.LightSlabBlock;
import net.tardis.mod.blocks.template.ModSlabBlock;
import net.tardis.mod.blocks.template.ModStairsBlock;
import net.tardis.mod.blocks.template.ModStairsClearBlock;
import net.tardis.mod.blocks.template.ModTrapdoorBlock;
import net.tardis.mod.blocks.template.NonCubeBlock;
import net.tardis.mod.blocks.template.TranslucentBlock;
import net.tardis.mod.blocks.template.TranslucentSlabBlock;
import net.tardis.mod.blocks.template.TranslucentStairsBlock;
import net.tardis.mod.blocks.template.TransparentGrateBlock;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.TItems;
import net.tardis.mod.properties.Prop;

public class TBlocks {
	public static List<Item> BLOCK_ITEMS_LIST = new ArrayList<Item>();
	public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Tardis.MODID);
    
    //Consoles - No ItemGroup
    public static final RegistryObject<Block> console_steam = register("console_steam", () -> setUpBlock(new ConsoleBlock()), false);
    public static final RegistryObject<Block> console_nemo = register("console_nemo", () -> setUpBlock(new ConsoleBlock()), false);
    public static final RegistryObject<Block> console_galvanic = register("console_galvanic", () -> setUpBlock(new ConsoleBlock()), false);
    public static final RegistryObject<Block> console_coral = register("console_coral", () -> setUpBlock(new ConsoleBlock()), false);
    public static final RegistryObject<Block> console_hartnell = register("console_hartnell", () -> setUpBlock(new ConsoleBlock()), false);
    public static final RegistryObject<Block> console_toyota = register("console_toyota", () -> setUpBlock(new ConsoleBlock()), false);
    public static final RegistryObject<Block> console_xion = register("console_xion", () -> setUpBlock(new ConsoleBlock()), false);
    public static final RegistryObject<Block> console_neutron = register("console_neutron", () -> setUpBlock(new ConsoleBlock()), false);
    public static final RegistryObject<Block> console_polymedical = register("console_polymedical", () -> setUpBlock(new ConsoleBlock()), false);

    //Exteriors - No ItemGroup
    public static final RegistryObject<Block> exterior_clock = register("exterior_clock", () -> setUpBlock(new ClockExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_steampunk = register("exterior_steampunk", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_trunk = register("exterior_trunk", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_telephone = register("exterior_telephone", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_police_box = register("exterior_police_box", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_fortune = register("exterior_fortune", () -> setUpBlock(new FortuneExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_modern_police_box = register("exterior_modern_police_box", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_safe = register("exterior_safe", () -> setUpBlock(new ExteriorBlock()), false);

    public static final RegistryObject<Block> exterior_tt_capsule = register("exterior_tt_capsule", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_tt2020 = register("exterior_tt2020", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_japan = register("exterior_bokkusu", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_aperture = register("exterior_aperture", () -> setUpBlock(new ExteriorBlock()), false);
    public static final RegistryObject<Block> exterior_disguise = register("exterior_disguise", () -> setUpBlock(new DisguiseExteriorBlock()), false);
    
    public static final RegistryObject<Block> bottom_exterior = register("bottom_exterior", () -> setUpBlock(new TardisExteriorBottomBlock(Prop.Blocks.UNBREAKABLE.get())), false);

    // Generic Interior Blocks - Future ItemGroup
    public static final RegistryObject<Block> alabaster_wall = register("alabaster_wall", () -> setUpBlock(new WallBlock(Block.Properties.create(Material.ROCK).hardnessAndResistance(1, 2).sound(SoundType.STONE))));
    public static final RegistryObject<Block> alabaster = register("alabaster", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1F, 2F)));
    public static final RegistryObject<Block> alabaster_stairs = register("alabaster_stairs", () -> setUpBlock(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> Blocks.STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> alabaster_slab = register("alabaster_slab", () -> setUpBlock(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> alabaster_bookshelf = register("alabaster_bookshelf", () -> setUpBlock(new CubeBlock(Prop.Blocks.BASIC_WOOD.get())));

    public static final RegistryObject<Block> ebony_bookshelf = register("ebony_bookshelf", () -> setUpBlock(new CubeBlock(Prop.Blocks.BASIC_WOOD.get())));
    public static final RegistryObject<Block> steam_grate = register("steam_grate", () -> setUpBlock(new SteamGrateBlock(Prop.Blocks.BASIC_TECH.get())));

    //Roundels - Roundel ItemGroup
    public static final RegistryObject<Block> roundel_alabaster = register("roundel/alabaster", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_offset_alabaster = register("roundel/alabaster_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    //Planks
    public static final RegistryObject<Block> roundel_oak_planks_full = register("roundel/oak_planks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_oak_planks_offset = register("roundel/oak_planks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_spruce_planks_full = register("roundel/spruce_planks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_spruce_planks_offset = register("roundel/spruce_planks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_birch_planks_full = register("roundel/birch_planks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_birch_planks_offset = register("roundel/birch_planks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_jungle_planks_full = register("roundel/jungle_planks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_jungle_planks_offset = register("roundel/jungle_planks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_acacia_planks_full = register("roundel/acacia_planks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_acacia_planks_offset = register("roundel/acacia_planks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_dark_oak_planks_full = register("roundel/dark_oak_planks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_dark_oak_planks_offset = register("roundel/dark_oak_planks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    //Stripped Wood
    public static final RegistryObject<Block> roundel_acacia_stripped_full = register("roundel/acacia_stripped_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_acacia_stripped_offset = register("roundel/acacia_stripped_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_birch_stripped_full = register("roundel/birch_stripped_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_birch_stripped_offset = register("roundel/birch_stripped_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_dark_oak_stripped_full = register("roundel/dark_oak_stripped_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_dark_oak_stripped_offset = register("roundel/dark_oak_stripped_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_jungle_stripped_full = register("roundel/jungle_stripped_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_jungle_stripped_offset = register("roundel/jungle_stripped_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_oak_stripped_full = register("roundel/oak_stripped_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_oak_stripped_offset = register("roundel/oak_stripped_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_spruce_stripped_full = register("roundel/spruce_stripped_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_spruce_stripped_offset = register("roundel/spruce_stripped_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_warped_stripped_full = register("roundel/warped_stripped_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_warped_stripped_offset = register("roundel/warped_stripped_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_crimson_stripped_full = register("roundel/crimson_stripped_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_crimson_stripped_offset = register("roundel/crimson_stripped_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.WOOD), SoundType.WOOD, 2.0F, 3.0F)), TItemGroups.ROUNDELS);
    
    
    //Concrete
    public static final RegistryObject<Block> roundel_white_concrete_full = register("roundel/white_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_white_concrete_offset = register("roundel/white_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_orange_concrete_full = register("roundel/orange_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_orange_concrete_offset = register("roundel/orange_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_magenta_concrete_full = register("roundel/magenta_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_magenta_concrete_offset = register("roundel/magenta_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_light_blue_concrete_full = register("roundel/light_blue_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_light_blue_concrete_offset = register("roundel/light_blue_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_yellow_concrete_full = register("roundel/yellow_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_yellow_concrete_offset = register("roundel/yellow_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_lime_concrete_full = register("roundel/lime_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_lime_concrete_offset = register("roundel/lime_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_pink_concrete_full = register("roundel/pink_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_pink_concrete_offset = register("roundel/pink_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_gray_concrete_full = register("roundel/gray_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_gray_concrete_offset = register("roundel/gray_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_light_gray_concrete_full = register("roundel/light_gray_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_light_gray_concrete_offset = register("roundel/light_gray_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_cyan_concrete_full = register("roundel/cyan_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_cyan_concrete_offset = register("roundel/cyan_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_purple_concrete_full = register("roundel/purple_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_purple_concrete_offset = register("roundel/purple_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_blue_concrete_full = register("roundel/blue_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_blue_concrete_offset = register("roundel/blue_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_brown_concrete_full = register("roundel/brown_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_brown_concrete_offset = register("roundel/brown_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_green_concrete_full = register("roundel/green_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_green_concrete_offset = register("roundel/green_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_red_concrete_full = register("roundel/red_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_red_concrete_offset = register("roundel/red_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_black_concrete_full = register("roundel/black_concrete_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_black_concrete_offset = register("roundel/black_concrete_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    //Terracotta
    public static final RegistryObject<Block> roundel_white_terracotta_full = register("roundel/white_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_white_terracotta_offset = register("roundel/white_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_orange_terracotta_full = register("roundel/orange_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_orange_terracotta_offset = register("roundel/orange_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_magenta_terracotta_full = register("roundel/magenta_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_magenta_terracotta_offset = register("roundel/magenta_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_light_blue_terracotta_full = register("roundel/light_blue_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_light_blue_terracotta_offset = register("roundel/light_blue_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_yellow_terracotta_full = register("roundel/yellow_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_yellow_terracotta_offset = register("roundel/yellow_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_lime_terracotta_full = register("roundel/lime_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_lime_terracotta_offset = register("roundel/lime_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_pink_terracotta_full = register("roundel/pink_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_pink_terracotta_offset = register("roundel/pink_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_gray_terracotta_full = register("roundel/gray_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_gray_terracotta_offset = register("roundel/gray_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_light_gray_terracotta_full = register("roundel/light_gray_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_light_gray_terracotta_offset = register("roundel/light_gray_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_cyan_terracotta_full = register("roundel/cyan_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_cyan_terracotta_offset = register("roundel/cyan_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_purple_terracotta_full = register("roundel/purple_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_purple_terracotta_offset = register("roundel/purple_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_blue_terracotta_full = register("roundel/blue_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_blue_terracotta_offset = register("roundel/blue_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_brown_terracotta_full = register("roundel/brown_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_brown_terracotta_offset = register("roundel/brown_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_green_terracotta_full = register("roundel/green_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_green_terracotta_offset = register("roundel/green_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);

    public static final RegistryObject<Block> roundel_red_terracotta_full = register("roundel/red_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_red_terracotta_offset = register("roundel/red_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_black_terracotta_full = register("roundel/black_terracotta_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_black_terracotta_offset = register("roundel/black_terracotta_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    //Quartz
    public static final RegistryObject<Block> roundel_quartz = register("roundel/quartz", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    //Migrate roundel_quartz_pillar to roundel/quartz_pillar_full
    public static final RegistryObject<Block> roundel_quartz_pillar_full = register("roundel/quartz_pillar_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    //New to 1.16
    public static final RegistryObject<Block> roundel_quartz_pillar_offset = register("roundel/quartz_pillar_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_chiseled_quartz_full = register("roundel/chiseled_quartz_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_chiseled_quartz_offset = register("roundel/chiseled_quartz_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    //Coral
    public static final RegistryObject<Block> roundel_coralised = register("roundel/coralised", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_divider = register("roundel/coral_divider", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_coralised_large = register("roundel/coralised_large", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_coralised_small = register("roundel/coralised_small", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    //Stone
    public static final RegistryObject<Block> roundel_stone_bricks_full = register("roundel/stone_bricks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_stone_bricks_offset = register("roundel/stone_bricks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_smooth_stone_full = register("roundel/smooth_stone_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_smooth_stone_offset = register("roundel/smooth_stone_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_polished_andesite_full = register("roundel/polished_andesite_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_polished_andesite_offset = register("roundel/polished_andesite_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_polished_granite_full = register("roundel/polished_granite_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_polished_granite_offset = register("roundel/polished_granite_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_polished_diorite_full = register("roundel/polished_diorite_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_polished_diorite_offset = register("roundel/polished_diorite_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_cut_red_sandstone_full = register("roundel/cut_red_sandstone_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_cut_red_sandstone_offset = register("roundel/cut_red_sandstone_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_cut_sandstone_full = register("roundel/cut_sandstone_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_cut_sandstone_offset = register("roundel/cut_sandstone_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_sandstone_full = register("roundel/sandstone_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_sandstone_offset = register("roundel/sandstone_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    //End
    public static final RegistryObject<Block> roundel_end_stone_bricks_full = register("roundel/end_stone_bricks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_end_stone_bricks_offset = register("roundel/end_stone_bricks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_end_stone_full = register("roundel/end_stone_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_end_stone_offset = register("roundel/end_stone_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_purpur_bricks_full = register("roundel/purpur_bricks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_purpur_bricks_offset = register("roundel/purpur_bricks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_purpur_pillar_full = register("roundel/purpur_pillar_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_purpur_pillar_offset = register("roundel/purpur_pillar_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    
    //Nether
    public static final RegistryObject<Block> roundel_nether_bricks_full = register("roundel/nether_bricks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_nether_bricks_offset = register("roundel/nether_bricks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_nether_wart_block_full = register("roundel/nether_wart_block_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_nether_wart_block_offset = register("roundel/nether_wart_block_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_red_nether_bricks_full = register("roundel/red_nether_bricks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_red_nether_bricks_offset = register("roundel/red_nether_bricks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_gilded_blackstone_full = register("roundel/gilded_blackstone_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_gilded_blackstone_offset = register("roundel/gilded_blackstone_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_polished_basalt_full = register("roundel/polished_basalt_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_polished_basalt_offset = register("roundel/polished_basalt_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_polished_blackstone_bricks_full = register("roundel/polished_blackstone_bricks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_polished_blackstone_bricks_offset = register("roundel/polished_blackstone_bricks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    public static final RegistryObject<Block> roundel_polished_blackstone_full = register("roundel/polished_blackstone_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_polished_blackstone_offset = register("roundel/polished_blackstone_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    //Prismarine
    public static final RegistryObject<Block> roundel_prismarine_bricks_full = register("roundel/prismarine_bricks_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_prismarine_bricks_offset = register("roundel/prismarine_bricks_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    //Bone
    public static final RegistryObject<Block> roundel_bone_block_full = register("roundel/bone_block_full", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK, MaterialColor.SAND), SoundType.BONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_bone_block_offset = register("roundel/bone_block_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK, MaterialColor.SAND), SoundType.BONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    
    //Non roundel blocks that need roundel properties
    public static final RegistryObject<Block> roundel_tech_wall_lamp = register("roundel/tech_wall_lamp", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    public static final RegistryObject<Block> roundel_tech_wall_lamp_offset = register("roundel/tech_wall_lamp_offset", () -> setUpBlock(new RoundelBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.ROUNDELS);
    
    
    // Tech - Stairs, Slabs - Future Tab
    public static final RegistryObject<Block> tech_strut = register("tech_strut", () -> setUpBlock(new TechStrutBlock()));
    public static final RegistryObject<Block> tungsten_plate_slab = register("tungsten_plate_slab", () -> setUpBlock(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> tungsten_pipes_slab = register("tungsten_pipes_slab", () -> setUpBlock(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)));

    //This is a special snowflake tech block - it's a RoundelBlock type so it can be lit up by the Tardis Interior Light Properties
    public static final RegistryObject<Block> tungsten_blue_runner_lights = register("tungsten_blue_runner_lights", () -> setUpBlock(new RoundelBlock(Prop.Blocks.BASIC_TECH.get(), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.FUTURE);
    
    public static final RegistryObject<Block> tungsten_pattern_slab = register("tungsten_pattern_slab", () -> setUpBlock(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> tungsten_slab = register("tungsten_slab", () -> setUpBlock(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)));

    public static final RegistryObject<Block> tungsten_plate_stairs = register("tungsten_plate_stairs", () -> setUpBlock(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> Blocks.STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> tungsten_pipes_stairs = register("tungsten_pipes_stairs", () -> setUpBlock(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> Blocks.STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F)));

    public static final RegistryObject<Block> tungsten_pattern_stairs = register("tungsten_pattern_stairs", () -> setUpBlock(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> Blocks.STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> tungsten_stairs = register("tungsten_stairs", () -> setUpBlock(new ModStairsBlock(Block.Properties.create(Material.ROCK), () -> Blocks.STONE.getDefaultState(), SoundType.STONE, 1.25F, 4.2F)));

    public static final RegistryObject<Block> holo_ladders = register("holo_ladders", () -> setUpBlock(new HoloLadderBlock(Prop.Blocks.BASIC_TECH.get().notSolid())));

    //Grates - Blocks, Slabs, Stairs - Future ItemGroup
    public static final RegistryObject<Block> metal_grate = register("metal_grate", () -> setUpBlock(new GrateBlock(Block.Properties.create(Material.IRON).notSolid(), SoundType.LANTERN, 1.25F, 4.2F)));
    public static final RegistryObject<Block> metal_grate_slab = register("metal_grate_slab", () -> setUpBlock(new ModSlabBlock(Block.Properties.create(Material.IRON), SoundType.LANTERN, 1.25F, 4.2F)));
    public static final RegistryObject<Block> metal_grate_stairs = register("metal_grate_stairs", () -> setUpBlock(new ModStairsClearBlock(Block.Properties.create(Material.IRON).notSolid(), () -> Blocks.IRON_BLOCK.getDefaultState(), SoundType.LANTERN, 1.25F, 4.2F)));
    public static final RegistryObject<Block> metal_grate_trapdoor = register("metal_grate_trapdoor", () -> setUpBlock(new ModTrapdoorBlock(Block.Properties.create(Material.WOOD).notSolid(), SoundType.LANTERN, 1.25F, 4.2F)));

    // Whitaker Interior - Future ItemGroup
    public static final RegistryObject<Block> steel_grate_solid = register("steel_grate_solid", () -> setUpBlock(new GrateBlock(Block.Properties.create(Material.IRON).notSolid(), SoundType.LANTERN, 1.25F, 4.2F)));
    public static final RegistryObject<Block> steel_grate_solid_offset = register("steel_grate_solid_offset", () -> setUpBlock(new GrateBlock(Block.Properties.create(Material.IRON).notSolid(), SoundType.LANTERN, 1.25F, 4.2F)));

    public static final RegistryObject<Block> steel_grate_frame = register("steel_grate_frame", () -> setUpBlock(new TransparentGrateBlock(Block.Properties.create(Material.IRON).notSolid(), SoundType.LANTERN, 1.25F, 4.2F)));
    public static final RegistryObject<Block> steel_grate_frame_offset = register("steel_grate_frame_offset", () -> setUpBlock(new TransparentGrateBlock(Block.Properties.create(Material.IRON).notSolid(), SoundType.LANTERN, 1.25F, 4.2F)));

    public static final RegistryObject<Block> blue_concrete_slab = register("blue_concrete_slab", () -> setUpBlock(new ModSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> gallifreyan_yellow_slab = register("gallifreyan_yellow_slab", () -> setUpBlock(new LightSlabBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> yellow_crystal = register("yellow_crystal", () -> setUpBlock(new TranslucentBlock(Block.Properties.create(Material.GLASS), SoundType.GLASS, 1.25F, 4.2F)));
    public static final RegistryObject<Block> yellow_crystal_slab = register("yellow_crystal_slab", () -> setUpBlock(new TranslucentSlabBlock(Block.Properties.create(Material.GLASS), SoundType.GLASS, 1.25F, 4.2F)));
    public static final RegistryObject<Block> yellow_crystal_stairs = register("yellow_crystal_stairs", () -> setUpBlock(new TranslucentStairsBlock(Block.Properties.create(Material.GLASS), () -> Blocks.PRISMARINE.getDefaultState(), SoundType.GLASS, 1.25F, 4.2F)));

    // Toyota Interior - Future ItemGroup
    public static final RegistryObject<Block> blue_pillar = register("blue_pillar", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)));
    public static final RegistryObject<Block> foam_pipes = register("foam_pipes", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.WOOL), SoundType.CLOTH, 1F, 2F)));
    public static final RegistryObject<Block> three_point_lamp = register("three_point_lamp", () -> setUpBlock(new LightBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F, 10)));

    //Machines - Maintenance ItemGroup
    public static final RegistryObject<Block> alembic = register("alembic", () -> setUpBlock(new AlembicBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> engine = register("tardis_engine", () -> setUpBlock(new TardisEngineBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> ars_egg = register("ars_egg", () -> setUpBlock(new ArsEggBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> quantiscope_brass = register("quantiscope_brass", () -> setUpBlock(new QuantiscopeBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> quantiscope_iron = register("quantiscope_iron", () -> setUpBlock(new QuantiscopeBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> atrium_block = register("atrium_block", () -> setUpBlock(new AtriumBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> anti_grav = register("anti_grav", () -> setUpBlock(new AntiGravBlock()), TItemGroups.MAINTENANCE);

    public static final RegistryObject<Block> reclamation_unit = register("reclamation_unit", () -> setUpBlock(new ReclamationBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> roundel_tap = register("roundel_tap", () -> setUpBlock(new RoundelTapBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> corridor_spawn = register("corridor_spawn", () -> setUpBlock(new ArsStructureBlock()), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> corridor_kill = register("corridor_kill", () -> setUpBlock(new CorridorKillBlock(Block.Properties.create(Material.IRON)
            .hardnessAndResistance(50.0F, 1200.0F) //Prevent explosions or users from destroying tile before it finishes deleting room
            .harvestLevel(0)
            .harvestTool(ToolType.PICKAXE))), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> waypoint_bank = registerBlockOnly("waypoint_bank", () -> setUpBlock(new WaypointBankBlock(Prop.Blocks.BASIC_TECH.get())));
    public static final RegistryObject<Block> landing_pad = register("landing_pad", () -> setUpBlock(new LandingPadBlock(Prop.Blocks.BASIC_TECH.get())), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> transduction_barrier = register("transduction_barrier", () -> setUpBlock(new TransductionBarrierBlock(Prop.Blocks.BASIC_TECH.get())), TItemGroups.MAINTENANCE);
    
    public static final RegistryObject<Block> ARTRON_PYLON = register("artron_pylon", () -> new ArtronPylonBlock(), TItemGroups.MAINTENANCE);
	public static final RegistryObject<Block> ARTRON_COLLECTOR = register("artron_collector", () -> new ArtronCollectorBlock(), TItemGroups.MAINTENANCE);
	public static final RegistryObject<Block> VORTEX_DETECTOR = register("vortex_detector", () -> new VortexDetectorBlock(), TItemGroups.FUTURE);
	
    //Future ItemGroup
    public static final RegistryObject<Block> ship_computer = register("ship_computer", () -> setUpBlock(new ShipComputerBlock()), TItemGroups.FUTURE);

    public static final RegistryObject<Block> psychic_cube = register("psychic_cube", () -> setUpBlock(new BeaconBlock()), TItemGroups.MAIN);

    public static final RegistryObject<Block> oxygen_sealer = register("oxygen_sealer", () -> setUpBlock(new TileBlock(Prop.Blocks.BASIC_TECH.get())), TItemGroups.FUTURE);

    public static final RegistryObject<Block> air_lock = registerBlockOnly("air_lock", () -> setUpBlock(new AirLockBlock(Prop.Blocks.BASIC_TECH.get())));

    public static final RegistryObject<Block> item_access_panel = register("item_access_panel", () -> setUpBlock(new ItemAccessPanelBlock(Prop.Blocks.BASIC_TECH.get())), TItemGroups.MAINTENANCE);
    public static final RegistryObject<Block> wirebox = register("wirebox", () -> setUpBlock(new WireboxBlock(Prop.Blocks.BASIC_TECH.get().notSolid())), TItemGroups.FUTURE);

    //Monitors - Main ItemGroup
    public static final RegistryObject<Block> steampunk_monitor = register("steampunk_monitor", () -> setUpBlock(new MonitorSteampunkBlock(Prop.Blocks.BASIC_TECH.get(), TardisConstants.Gui.MONITOR_MAIN_STEAM, -6 / 16.0, 3 / 16.0, -5.1 / 16.0, 0.77, 0.95)), TItemGroups.MAIN);
    public static final RegistryObject<Block> eye_monitor = register("eye_monitor", () -> setUpBlock(new MonitorEyeBlock(Prop.Blocks.BASIC_TECH.get(), TardisConstants.Gui.MONITOR_MAIN_EYE, -0.45, 0.25, 7 / 16.0, 0.9, 1.05)), TItemGroups.MAIN);
    public static final RegistryObject<Block> rca_monitor = register("rca_monitor", () -> setUpBlock(new MonitorRCABlock()), TItemGroups.MAIN);
    public static final RegistryObject<Block> rotate_monitor = register("rotate_monitor", () -> setUpBlock(new MonitorRotateBlock(Prop.Blocks.BASIC_TECH.get())), TItemGroups.MAIN);
    public static final RegistryObject<Block> dynamic_monitor = register("dynamic_monitor", () -> setUpBlock(new MonitorDynamicBlock()), TItemGroups.MAIN);

    //Resources - Future ItemGroup
    public static final RegistryObject<Block> cinnabar_ore = register("cinnabar_ore", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> xion_crystal = register("xion_crystal", () -> setUpBlock(new XionCrystalBlock()), TItemGroups.FUTURE);

    // Misc - Future ItemGroup
    public static final RegistryObject<Block> tungsten_pattern = register("tungsten_pattern", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> tungsten_pipes = register("tungsten_pipes", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> tungsten_plate = register("tungsten_plate", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> tungsten_plate_small = register("tungsten_plate_small", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.FUTURE);

    public static final RegistryObject<Block> tungsten = register("tungsten", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> tungsten_screen = register("tungsten_screen", () -> setUpBlock(new CubeBlock(Block.Properties.create(Material.ROCK), SoundType.STONE, 1.25F, 4.2F)), TItemGroups.FUTURE);
    //Misc - Main ItemGroup
    public static final RegistryObject<Block> broken_exterior = register("broken_exterior", () -> setUpBlock(new BrokenExteriorBlock()), false);

    //Decoration - Future ItemGroup
    public static final RegistryObject<Block> clutter_books = register("clutter_books", () -> setUpBlock(new BookStackBlock(Block.Properties.create(Material.WOOL).notSolid(), SoundType.CLOTH, 1F, 2F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> clutter_paper = register("clutter_paper", () -> setUpBlock(new FloorClutterBlock(Block.Properties.create(Material.WOOL).notSolid(), SoundType.CLOTH, 1F, 2F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> wire_hang = register("wire_hang", () -> setUpBlock(new HangingWireBlock(Prop.Blocks.BASIC_TECH.get().notSolid())), TItemGroups.FUTURE);
    public static final RegistryObject<Block> comfy_chair = register("comfy_chair_red", () -> setUpBlock(new ChairBlock(0.25F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> comfy_chair_green = register("comfy_chair_green", () -> setUpBlock(new ChairBlock(0.25F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> crash_chair = register("crash_chair", () -> setUpBlock(new CrashChairBlock(0.3F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> crash_chair_wood = register("crash_chair_wood", () -> setUpBlock(new CrashChairBlock(0.3F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> crash_chair_red = register("crash_chair_red", () -> setUpBlock(new CrashChairBlock(0.3F)), TItemGroups.FUTURE);
    public static final RegistryObject<Block> telescope = register("telescope", () -> setUpBlock(new NonCubeBlock(Prop.Blocks.BASIC_TECH.get().notSolid())), TItemGroups.FUTURE);
    public static final RegistryObject<Block> blinking_lights = register("blinking_lights", () -> setUpBlock(new CubeBlock(Prop.Blocks.BASIC_TECH.get().setLightLevel(value -> {
        return 15;
    }))), TItemGroups.FUTURE);
    public static final RegistryObject<Block> spinny_block = registerBlockOnly("spinny_thing", () -> setUpBlock(new ToyotaSpinnyBlock()));
    public static final RegistryObject<Block> plaque = register("plaque", () -> setUpBlock(new PlaqueBlock(Prop.Blocks.BASIC_TECH.get().notSolid())), TItemGroups.FUTURE);


    public static final RegistryObject<Block> squareness_block = register("squareness_block", () -> setUpBlock(new SquarenessBlock(Block.Properties.create(Material.AIR).doesNotBlockMovement())), false);

    public static final RegistryObject<Block> chemlight = register("chemlight", () -> setUpBlock(new ChemLightBlock(Block.Properties.create(Material.IRON).hardnessAndResistance(0.25F, 0.75F).setLightLevel(value -> {
        return 15;
    }).notSolid())));
    public static final RegistryObject<Block> chemlamp = register("chemlamp", () -> setUpBlock(new ChemLampBlock(Block.Properties.create(Material.IRON).hardnessAndResistance(0.25F, 0.75F).setLightLevel(value -> {
        return 15;
    }).notSolid())));
    public static final RegistryObject<Block> NEUTRONIC_SPECTROMETER = register("neutronic_spectrometer", NeutronicSpectrometerBlock::new, TItemGroups.MAINTENANCE);

    //Moon - Future ItemGroup
    public static final RegistryObject<Block> regolith_sand = register("regolith_sand", () -> setUpBlock(new SandBlock(0, Prop.Blocks.BASIC_SAND.get())));
    public static final RegistryObject<Block> regolith_packed = register("regolith_packed", () -> setUpBlock(new Block(Prop.Blocks.BASIC_STONE.get())));
    public static final RegistryObject<Block> moon_base_glass = register("moon_base_glass", () -> setUpBlock(new GlassBlock(Prop.Blocks.BASIC_GLASS.get().notSolid())));

    public static final RegistryObject<Block> multiblock = register("multiblock", () -> setUpBlock(new MultiblockBlock(Prop.Blocks.BASIC_TECH.get().notSolid())), false);
    public static final RegistryObject<Block> multiblock_master = register("multiblock_master", () -> setUpBlock(new MultiblockBlock(Prop.Blocks.BASIC_TECH.get().notSolid())), false);

    //Debug use only - No ItemGroup
    public static final RegistryObject<Block> structure_block = register("structure_block", () -> setUpBlock(new SuperStructureBlock()), null);

    private static <T extends Block> T setUpBlock(T block) {
        return block;
    }
    
    /**
     * Registers a Block and BlockItem to the ItemGroup of your choice
     * @param <T>
     * @param id
     * @param blockSupplier
     * @param itemGroup
     * @return
     */
    private static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, ItemGroup itemGroup){
    	RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
    	TItems.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().group(itemGroup)));
    	return registryObject;
    }

    /**
     * Registers a Block without a BlockItem
     * <br> Use when you need a special BlockItem. The BlockItem should be registered in TItems with the same registry name as the block
     * @param <T>
     * @param id
     * @param blockSupplier
     * @return
     */
    private static <T extends Block> RegistryObject<T> registerBlockOnly(String id, Supplier<T> blockSupplier){
    	RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
    	return registryObject;
    }
    
    /**
     * Registers a Block and BlockItem into the FutureItemGroup
     * @param <T>
     * @param id
     * @param blockSupplier
     * @return
     */
    private static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier){
    	RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
    	TItems.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().group(TItemGroups.FUTURE)));
    	return registryObject;
    }
    
    /**
     * Registers a Block and registers a BlockItem depending if hasItem is true
     * <br> If hasItem is true, register a blockItem into the Future ItemGroup
     * @param <T>
     * @param id
     * @param blockSupplier
     * @param hasItem
     * @return
     */
    private static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier, boolean hasItem){
    	RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
    	if (hasItem)
    		TItems.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().group(TItemGroups.FUTURE)));
    	return registryObject;
    }

    private static ToIntFunction<BlockState> getLightValueLit(int lightValue) {
        return (state) -> {
            return state.get(BlockStateProperties.LIT) ? lightValue : 0;
        };
    }
}
