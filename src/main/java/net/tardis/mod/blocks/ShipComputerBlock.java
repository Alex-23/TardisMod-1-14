package net.tardis.mod.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateContainer.Builder;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.Mirror;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.items.CapabilityItemHandler;
import net.tardis.mod.blocks.template.NotSolidTileBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.containers.ShipComputerContainer;
import net.tardis.mod.helper.TInventoryHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.ShipComputerTile;

public class ShipComputerBlock extends NotSolidTileBlock {

    public ShipComputerBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote) {
            TileEntity te = worldIn.getTileEntity(pos);
            if (te instanceof ShipComputerTile) {
                ShipComputerTile comp = (ShipComputerTile) te;
                ItemStack held = player.getHeldItem(handIn);
                if(comp.getLootTable() != null)
                    comp.fillWithLoot(player);
                
                if(comp.getSchematic() != null && held.getItem() == TItems.SONIC.get()) {
                    held.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(cap -> {
                        cap.getSchematics().add(comp.getSchematic());
                        comp.setSchematic(null);
                        //Remove schematic from computer
                        player.sendStatusMessage(new TranslationTextComponent("message.tardis.computer.downloaded"), true);
                    });
                }
                else NetworkHooks.openGui((ServerPlayerEntity)player, new INamedContainerProvider() {

                    @Override
                    public Container createMenu(int id, PlayerInventory inv, PlayerEntity p_createMenu_3_) {
                        return new ShipComputerContainer(id, inv, (ShipComputerTile) worldIn.getTileEntity(pos));
                    }

                    @Override
                    public ITextComponent getDisplayName() {
                        return new TranslationTextComponent("container.tardis.ship_computer");
                    }
                }, buf -> buf.writeBlockPos(pos));
            }
        }
        return ActionResultType.SUCCESS;
    }


    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(BlockStateProperties.HORIZONTAL_FACING, context.getPlayer().getHorizontalFacing().getOpposite());
    }

    @Override
    protected void fillStateContainer(Builder<Block, BlockState> builder) {
        builder.add(BlockStateProperties.HORIZONTAL_FACING);
    }
    
    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if(!worldIn.isRemote()) {
            if (state.getBlock() != newState.getBlock()) {
                TileEntity te = worldIn.getTileEntity(pos);
                if(te != null) {
                    te.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY).ifPresent(cap -> {
                        TInventoryHelper.dropInventoryItems(worldIn, pos, cap);
                    });
                }
                super.onReplaced(state, worldIn, pos, newState, isMoving);
            }
        }
        
    }
    
    @Override
    public BlockState mirror(BlockState state, Mirror mirrorIn) {
        return state.rotate(mirrorIn.toRotation(state.get(BlockStateProperties.HORIZONTAL_FACING)));
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(BlockStateProperties.HORIZONTAL_FACING, rot.rotate(state.get(BlockStateProperties.HORIZONTAL_FACING)));
    }
}
