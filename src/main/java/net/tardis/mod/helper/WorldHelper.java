package net.tardis.mod.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.AxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ShieldItem;
import net.minecraft.potion.Effect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.state.Property;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.math.vector.Vector3i;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.DimensionType;
import net.minecraft.world.ForcedChunksSaveData;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.feature.StructureFeature;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.world.ForgeChunkManager;
import net.minecraftforge.common.world.ForgeChunkManager.TicketOwner;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.world.dimensions.TDimensions;
/** Helper functions for everything world related*/
public class WorldHelper {
    
    public static String formatBlockPos(BlockPos pos) {
        return pos.getX() + ", " + pos.getY() + ", " + pos.getZ();
    }
    /** Formats a BlockPos for use in quick Copy Pasting*/
    public static String formatBlockPosDebug(BlockPos pos) {
        return pos.getX() + " " + pos.getY() + " " + pos.getZ();
    }
    /** <br> 1.16: Use RegistryKey<World> because there can be multiple dimensions for one dimension type*/
    public static String formatDimName(RegistryKey<World> dim) {
        if(dim == null)
            return "UNKNOWN";

        String original = dim.getLocation().getPath().trim().replace("    ", "").replace("_", " ");
        String output = Arrays.stream(original.split("\\s+"))
                .map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
                .collect(Collectors.joining(" "));

        return output;
    }
    /**
     * Formats a Biome's RegistryKey to show a user friendly name
     * <br> Temporary solution as Biome#getDisplayName is gone in 1.16
     * @param biomeKey
     * @return
     */
    public static String formatBiomeKey(RegistryKey<Biome> biomeKey) {
        if(biomeKey == null)
            return "UNKNOWN";

        String original = biomeKey.getLocation().getPath().trim().replace("    ", "").replace("_", " ");
        String output = Arrays.stream(original.split("\\s+"))
                .map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
                .collect(Collectors.joining(" "));
        return output;
    }
    
    /**
     * Formats a Structure's RegistryKey to show a user friendly name
     * <br> DO NOT use Structure#getStructureName as vanilla uses that as a pseudo key sometimes
     * @param biomeKey
     * @return
     */
    public static String formatStructureKey(RegistryKey<StructureFeature<?, ?>> structureKey) {
        if(structureKey == null)
            return "UNKNOWN";

        String original = structureKey.getLocation().getPath().trim().replace("    ", "").replace("_", " ");
        String output = Arrays.stream(original.split("\\s+"))
                .map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
                .collect(Collectors.joining(" "));
        return output;
    }
    
    public static String formatStructureKey(String structureKey) {
        if(structureKey == null)
            return "UNKNOWN";
        ResourceLocation parsedRL = new ResourceLocation(structureKey);
        String original = parsedRL.getPath().trim().replace("    ", "").replace("_", " ");
        String output = Arrays.stream(original.split("\\s+"))
                .map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
                .collect(Collectors.joining(" "));
        return output;
    }
    
    
    
    public static Effect getEffectFromRL(ResourceLocation loc) {
        return ForgeRegistries.POTIONS.getValue(loc);
    }
    
    public static ResourceLocation getKeyFromEffect(Effect effect) {
        return ForgeRegistries.POTIONS.getKey(effect);
    }
    
    public static void setFluidStateIfNot(FluidState state, World world, BlockPos pos) {
        BlockState currentState = world.getBlockState(pos);
        boolean isEmpty = state.getFluid() == Fluids.EMPTY;
        if(world.getFluidState(pos).getFluid() != state.getFluid()) {
            if(currentState.isAir(world, pos) || (isEmpty && currentState.getMaterial().isLiquid())) {
                world.setBlockState(pos, state.getBlockState());
            }
            else if(world.getBlockState(pos).getBlock() instanceof IWaterLoggable) {
                IWaterLoggable wat = (IWaterLoggable)currentState.getBlock();
                if(wat.canContainFluid(world, pos, currentState, state.getFluid()))
                    wat.receiveFluid(world, pos, currentState, state.getFluidState());
                else if(state.getFluid() == Fluids.EMPTY)
                    wat.pickupFluid(world, pos, currentState);
            }
        }
    }
    
    public static float getAngleBetweenPoints(Vector3d start, Vector3d end) {
        float rot = 0F;
        
        Vector3d p = start.subtract(end).normalize();
        
        float hype = (float)Math.sqrt(p.x * p.x + p.z * p.z);
        
        if(p.z < 0)
            rot = (float)Math.toDegrees(Math.asin(p.x / hype));
        else rot = -(float)Math.toDegrees(Math.asin(p.x / hype)) - 180;
        
        return rot;
    }
    
    /**
     * Converts from Direction to Rotation, assuming the default facing is north
     * @param dir
     * @return
     */
    public static Rotation getRotationFromDirection(Direction dir) {
        switch(dir) {
        case NORTH: return Rotation.NONE;
        case EAST: return Rotation.CLOCKWISE_90;
        case SOUTH: return Rotation.CLOCKWISE_180;
        case WEST: return Rotation.COUNTERCLOCKWISE_90;
        default: return Rotation.NONE;
        }
    }
    
    /**
     * Only supports Horrizontal directions, rotates around 0, 0, 0 
     * @param pos - The position to rotate
     * @param dir - The direction to rotate it, assumes facing north by default
     * @return - rotated block pos
     */
    public static BlockPos rotateBlockPos(BlockPos pos, Direction dir) {
        switch(dir) {
            case NORTH: return pos;
            case EAST: return new BlockPos(-pos.getZ(), pos.getY(), pos.getX());
            case SOUTH: return new BlockPos(-pos.getX(), pos.getY(), -pos.getZ());
            case WEST: return new BlockPos(pos.getZ(), pos.getY(), -pos.getX());
            default: return pos;
        }
    }

    public static Vector3d vecFromPos(Vector3i location) {
        return new Vector3d(location.getX() + 0.5, location.getY() + 0.5, location.getZ() + 0.5);
    }
    
    public static float getFixedRotation(float rot) {
        float newR = rot % 360.0F;
        if(newR < 0)
            return 360.0F + newR;
        
        return newR;
    }

    public static BlockPos scaleBlockPos(BlockPos pos, double scale) {
        return new BlockPos(pos.getX() * scale, pos.getY() * scale, pos.getZ() * scale);
    }
    
    public static float getAngleFromFacing(Direction dir) {
        switch(dir){
            case EAST: return 90.0F;
            case SOUTH: return 180.0F;
            case WEST: return 270.0F;
            default: return 0.0F;
        }
    }
    
    public static float getRotationDifference(float one, float two) {
        if(one > two)
            return one - two;
        return two - one;
    }
    
    /**
     * Cycles a Blockstate's Property. Derived from DebugStickItem#cycleProperty
     * <br> Use this to replace Blockstate#cycle as this was removed in 1.16
     * @param <T>
     * @param state
     * @param propertyIn
     * @param backwards
     * @return
     */
    public static <T extends Comparable<T>> BlockState cycleBlockStateProperty(BlockState state, Property<T> propertyIn) {
          return state.with(propertyIn, getAdjacentValue(propertyIn.getAllowedValues(), state.get(propertyIn), false));
    }
    
    /**
     * Cycles a Blockstate's Property. Derived from DebugStickItem#cycleProperty
     * <br> Use this to replace Blockstate#cycle as this was removed in 1.16
     * @param <T>
     * @param state
     * @param propertyIn
     * @param backwards
     * @return
     */
    public static <T extends Comparable<T>> BlockState cycleBlockStateProperty(BlockState state, Property<T> propertyIn, boolean backwards) {
          return state.with(propertyIn, getAdjacentValue(propertyIn.getAllowedValues(), state.get(propertyIn), backwards));
    }
    
    private static <T> T getAdjacentValue(Iterable<T> allowedValues, @Nullable T currentValue, boolean backwards) {
          return (T)(backwards ? Util.getElementBefore(allowedValues, currentValue) : Util.getElementAfter(allowedValues, currentValue));
    }
    
    public static RegistryKey<World> getWorldKeyFromRL(ResourceLocation loc){
        return RegistryKey.getOrCreateKey(Registry.WORLD_KEY, loc);
    }
    
    public static ServerWorld getWorldFromRL(MinecraftServer server, ResourceLocation loc){
        return server.getWorld(RegistryKey.getOrCreateKey(Registry.WORLD_KEY, loc));
    }
    
    /** <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type */
    public static ResourceLocation getKeyFromWorld(World world) {
        ResourceLocation rl = world.getDimensionKey().getLocation();
        return rl != null ? rl : new ResourceLocation("overworld");
    }
    
    public static List<TileEntity> getTEsInChunks(ServerWorld world, ChunkPos chunkPos, int i) {
        List<TileEntity> list = Lists.newArrayList();
        for(int x = -i; x <= i; ++x) {
            for(int z = -i; z <= i; ++z) {
                list.addAll(world.getChunk(chunkPos.x + x, chunkPos.z + z).getTileEntityMap().values());
            }
        }
        return list;
    }
    
    /**
     * Get a DimensionType at runtime
     * <br> Used for dynamically created Dimensions
     * @param server
     * @param dimTypeKey
     * @return
     */
    public static DimensionType getDimensionType(MinecraftServer server, RegistryKey<DimensionType> dimTypeKey){
        return server.getDynamicRegistries() // get dynamic registries
            .getRegistry(Registry.DIMENSION_TYPE_KEY)
            .getOrThrow(dimTypeKey);
    }
    
    public static DimensionType getDimensionType(ServerWorld world, RegistryKey<DimensionType> dimTypeKey){
        return world.getServer().getDynamicRegistries() // get dynamic registries
            .getRegistry(Registry.DIMENSION_TYPE_KEY)
            .getOrThrow(dimTypeKey);
    }
    
    /**
     * Get a Dimension Type world from a world
     * <br> For Client Worlds, use the method {@link ClientWorldHelper#getDimensionType(net.minecraft.client.world.ClientWorld, RegistryKey)}
     * @param world
     * @param dimTypeKey
     * @return
     */
    public static DimensionType getDimensionType(World world, RegistryKey<DimensionType> dimTypeKey){
        return world.func_241828_r() // get dynamic registries
            .getRegistry(Registry.DIMENSION_TYPE_KEY)
            .getOrThrow(dimTypeKey);
    }
    
    /**
     * Compares the current world's dimension type against a registry key of a json based dimension type
     * @param world
     * @param dimTypeKey
     * @return True if the registry keys match, false if they do not
     */
    public static boolean areDimensionTypesSame(World world, RegistryKey<DimensionType> dimTypeKey) {

        if(world.func_241828_r() == null || world.func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY) == null)
            return false;

        DimensionType type = world.func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY).getValueForKey(dimTypeKey);
        if(type != null && type.isSame(world.getDimensionType()))
            return true;
        return false;
    }
    
    /**
     * Used to determine if an object is able to travel to the target dimension
     * <br> Can be called on both server or client sides, but it's best if you call on the server side.
     * @implNote Usage: Config defined values
     * @implSpec Will blacklist dimensions
     * @param world
     * @return return true if allowed to travel in this world, false if not allowed to travel
     */
    public static boolean canTravelToDimension(World world) {
        ResourceLocation key = world.getDimensionKey().getLocation();
        if(key == null)
            return false;
        if (WorldHelper.canTravelInDimensionType(world, world.getDimensionType())) { //If this dimension type is not blacklisted
        	List<? extends String> blacklist = TConfig.SERVER.blacklistedDims.get();
            for(String s : blacklist) { //If any dimensions are blacklisted, return false
                if(key.toString().contentEquals(s))
                    return false;
            }
            //If this dimension is not blacklisted, make sure it is not a Tardis or Vortex dimension type
            return !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)
                    && !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.VORTEX_TYPE);
        }
        return false; //If this dimension type is blacklisted, always deny
    }
    
    /**
     * Used to determine if the VM is able to travel to the target dimension
     * <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type
     * @implNote Usage: Config defined values
     * @implSpec Will require a config option to blacklist dimensions
     * @param dimension
     * @return Boolean value
     */
    public static boolean canVMTravelToDimension(World world) {
        ResourceLocation key = world.getDimensionKey().getLocation();
        if(key == null)
            return false;
        if (TConfig.SERVER.toggleVMWhitelistDims.get()) { //If using whitelist
            return TConfig.SERVER.whitelistedVMDims.get().stream().allMatch(dimName ->
                 key.toString().contentEquals(dimName) && canVMBeUsedInDim(world));
        }
        else if (!TConfig.SERVER.toggleVMWhitelistDims.get()){ //If using blacklist
            return TConfig.SERVER.blacklistedVMDims.get().stream().allMatch(dimName ->
                 !key.toString().contentEquals(dimName) && canVMBeUsedInDim(world));
        }
        return canVMBeUsedInDim(world);
    }
    /**
     * Determines if VM can be opened/used in a particular dimension. More lightweight method than the above
     * <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type
     * @param dimension
     * @return true if can be used, false if cannot be used
     */
    public static boolean canVMBeUsedInDim(World world) {
        return !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE)
        && !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.VORTEX_TYPE);
    }
    
    /**
     * A more performance efficient version of the above to check if an object can be used in the current dimension
     * <br> 1.16: Compare RegistryKey<World> because there can be multiple dimensions for one dimension type
     * @implNote Used for items/blocks that can only be used in the Tardis dimension
     * @param world
     * @return true if blocked, false if not blocked
     */
    public static boolean isDimensionBlocked(World world) {
        return !WorldHelper.areDimensionTypesSame(world, TDimensions.DimensionTypes.TARDIS_TYPE);
    }
    
    public static List<World> getAllValidDimensions() {
        
        List<World> dims = new ArrayList<World>();
        
        for(ServerWorld dimension : ServerLifecycleHooks.getCurrentServer().getWorlds()) {
            if(WorldHelper.canTravelToDimension(dimension))
                dims.add(dimension);
        }
        
        return dims;
    }
    
    /** Filters out blacklisted Dimension Types, to account for mods that dynamically generate dimensions like we do.
     * <br> E.g. RFTools dynamically generated dimensions, Mystcraft, Hyperbox, Nomadic Tents
     * <br> Can be called on both sides, recommended you call on server side.
     * @return true if allowed to travel in this DimensionType, false if not allowed*/
    public static boolean canTravelInDimensionType(World world, DimensionType dimType) {
    	RegistryKey<DimensionType> dimTypeKey = world.func_241828_r().getRegistry(Registry.DIMENSION_TYPE_KEY).getOptionalKey(dimType).orElse(null);
    	if (dimTypeKey != null) {
    		ResourceLocation dimTypeLoc = dimTypeKey.getLocation();
        	for (String blocked : TConfig.SERVER.blacklistedDimTypes.get()) {
            	if (dimTypeLoc.toString().equals(blocked)) {
                    return false;
                }
                if (blocked.endsWith("*")) {
                    String modid = blocked.substring(0, blocked.indexOf(':'));
                    if (dimTypeLoc.getNamespace().equals(modid))
                        return false;
                }
            }
        	return true;
    	}
        return false;
    }
    
    public static boolean canBrokenTardisSpawn(RegistryKey<World> worldKey) {
        ResourceLocation worldLoc = worldKey.getLocation();
        for (String blocked : TConfig.COMMON.tardisSpawnDimBlacklist.get()) {
        	if (worldLoc.toString().equals(blocked)) {
                return false;
            }
            if (blocked.endsWith("*")) {
                String modid = blocked.substring(0, blocked.indexOf(':'));
                if (worldLoc.getNamespace().equals(modid))
                    return false;
            }
        }
        return true;
    }
    
    public static boolean canEntitySpawnInTardis(EntityType<?> type) {
        ResourceLocation entityTypeLoc = type.getRegistryName();
        for (String blocked : TConfig.SERVER.tardisDimEntitySpawnBlacklist.get()) {
        	if (entityTypeLoc.toString().equals(blocked)) {
                return false;
            }
            if (blocked.endsWith("*")) {
                String modid = blocked.substring(0, blocked.indexOf(':'));
                if (entityTypeLoc.getNamespace().equals(modid))
                    return false;
            }
        }
        return true;
    }
    
    /**
     * Force loads and starts ticking a chunk if it is not loaded. 
     * <p> Also marks the chunk dirty to be saved after the chunk is forced
     * @param world
     * @param chunkPos
     * @param blockPos
     * @return
     */
    public static boolean forceChunkIfNotLoaded(ServerWorld world, ChunkPos chunkPos, BlockPos blockPos) {
        if (ForgeChunkManager.forceChunk(world, Tardis.MODID, blockPos, chunkPos.x, chunkPos.z, true, true)) { //enable the chunk to receive full ticks
            WorldHelper.forceChunkVanillaIfNotLoaded(world, chunkPos);
            WorldHelper.markChunkDirty(world, blockPos);
            return true;
        }
        return false;
    }
    /**
     * Removes the force loading of a ticking chunk if it is loaded
     * <p> Also marks the chunk dirty to be saved after the chunk is unforced
     * <br> Call to disable chunks loaded by {@link WorldHelper#forceChunkIfNotLoaded(ServerWorld, ChunkPos, BlockPos)}
     * @param world
     * @param chunkPos
     * @param blockPos
     * @return
     */
    public static boolean unForceChunkIfLoaded(ServerWorld world, ChunkPos chunkPos, BlockPos blockPos) {
        if (ForgeChunkManager.forceChunk(world, Tardis.MODID, blockPos, chunkPos.x, chunkPos.z, false, true)) { //The ticking parameter needs to be true so that we are unloading the same type of chunk (ticking chunks) as the ones we loaded with WorldHelper#forceChunkIfNotLoaded
            WorldHelper.unforceChunkVanillaIfLoaded(world, chunkPos);
            WorldHelper.markChunkDirty(world, blockPos);
            return true;
        }
        return false;
    }
    
    /**
     * Checks if a position is in bounds of the world, and is loaded
     *
     * @param world world
     * @param pos   position
     *
     * @return True if the position is loaded or the given world is of a superclass of IWorldReader that does not have a concept of being loaded.
     */
     public static boolean isBlockLoaded(@Nullable IBlockReader world, @Nonnull BlockPos pos) {
         if (world == null || !World.isValid(pos)) {
             return false;
         } else if (world instanceof IWorldReader) {
             //Note: We don't bother checking if it is a world and then isBlockPresent because
             // all that does is also validate the y value is in bounds, and we already check to make
             // sure the position is valid both in the y and xz directions
             return ((IWorldReader) world).isBlockLoaded(pos);
         }
         return true;
     }
     /** Marks a chunk as dirty if it is currently loaded. Does validation if the position is within a world, whilst vanilla does not
      * <p> Call this after force loading a chunk to ensure the world saves that action*/
     public static void markChunkDirty(World world, BlockPos pos) {
         if (isBlockLoaded(world, pos)) {
             world.getChunkAt(pos).markDirty();
         }
     }
     
     /** Gets the map of force loaded chunks created at a block position, whose chunks are current ticking. 
      * <p> Used to check if a ticking chunk should be force loaded or not
      * <p> We do not use {@link ServerWorld#getForcedChunks()} because that only gets the non-ticking forced chunks*/
     public static Map<TicketOwner<BlockPos>, LongSet> getTickingBlockForcedChunks(ServerWorld serverWorld){
         ForcedChunksSaveData data = serverWorld.getSavedData().getOrCreate(ForcedChunksSaveData::new, "chunks");
         if (data != null)
             return data.getBlockForcedChunks().getTickingChunks();
         return null;
     }
     /**
      * Force Loads or Force Unloads a set amount of chunks in a Tardis dimension
      * <br> Used to allow Console to find the door, or unload the chunks that were forced by the Console
      * @param targetWorld
      * @param force, if true, force load chunks. If false, unloads chunks
      * @return
      */
     public static boolean preLoadTardisInteriorChunks(ServerWorld targetWorld, boolean force) {
         if (WorldHelper.areDimensionTypesSame(targetWorld, TDimensions.DimensionTypes.TARDIS_TYPE)) {
            ServerWorld serverWorld = targetWorld.getServer().getWorld(targetWorld.getDimensionKey());
            ChunkPos cPos = new ChunkPos(TardisHelper.TARDIS_POS);
            for (int xPos = -3; xPos < 3; xPos ++) {
                for (int zPos = -3; zPos < 3; zPos ++) {
                    ChunkPos toLoadPos = new ChunkPos(cPos.x + xPos, cPos.z + zPos);
                    if (force)
                        forceChunkIfNotLoaded(serverWorld, toLoadPos, TardisHelper.TARDIS_POS);
                    else
                        unForceChunkIfLoaded(serverWorld, toLoadPos, TardisHelper.TARDIS_POS);
                }
            }
            return true;
        }
         return false;
     }
     
     public static Entity teleportEntities(Entity e, ServerWorld world, BlockPos pos, float yaw, float pitch) {
         return teleportEntities(e, world, pos.getX(), pos.getY(), pos.getZ(), yaw, pitch);
     }
     
     public static Entity teleportEntities(Entity e, ServerWorld world, double x, double y, double z, float yaw, float pitch) {
    	 return TeleportUtil.teleportEntity(e, world, x, y, z, yaw, pitch);
     }

     /** Uses vanilla force chunk system.*/
     public static boolean forceChunkVanillaIfNotLoaded(ServerWorld world, ChunkPos pos) {
        if(!world.getForcedChunks().contains(pos.asLong())) {
            world.forceChunk(pos.x, pos.z, true);
            return true;
        }
        return false;
    }
     
     public static boolean unforceChunkVanillaIfLoaded(ServerWorld world, ChunkPos pos) {
        if(world.getForcedChunks().contains(pos.asLong())) {
            world.forceChunk(pos.x, pos.z, false);
            return true;
        }
        return false;
    }
     
    public static void maybeDisableShield(PlayerEntity player, LivingEntity attackingEntity, ItemStack heldItem, ItemStack playerHeldItem) {
        if (!heldItem.isEmpty() && !playerHeldItem.isEmpty() && heldItem.getItem() instanceof AxeItem && playerHeldItem.getItem() instanceof ShieldItem) {
            float f = 0.25F + (float)EnchantmentHelper.getEfficiencyModifier(attackingEntity) * 0.05F;
            if (attackingEntity.world.rand.nextFloat() < f) {
                ShieldItem shield = (ShieldItem)playerHeldItem.getItem();
                player.getCooldownTracker().setCooldown(shield, 100);
                attackingEntity.world.setEntityState(player, (byte)30);
            }
        }
    }
    
    public static void maybeDisableShieldNoEnchant(PlayerEntity player, LivingEntity attackingEntity, ItemStack heldItem, ItemStack playerHeldItem) {
        if (!heldItem.isEmpty() && !playerHeldItem.isEmpty() && playerHeldItem.getItem() instanceof ShieldItem) {
        	float f = 0.25F;
        	if (attackingEntity.world.rand.nextFloat() < f) {
                ShieldItem shield = (ShieldItem)playerHeldItem.getItem();
                player.getCooldownTracker().setCooldown(shield, 100);
                attackingEntity.world.setEntityState(player, (byte)30);
            }
        }
    }

    public static Quaternion getStandardRotationFor(BlockState state) {
         return Vector3f.YP.rotationDegrees(getAngleFromFacing(getFacingForState(state)));
    }

    public static Direction getFacingForState(BlockState state){

        if(state.hasProperty(BlockStateProperties.FACING))
            return state.get(BlockStateProperties.FACING);
        else if(state.hasProperty(BlockStateProperties.HORIZONTAL_FACING))
            return state.get(BlockStateProperties.HORIZONTAL_FACING);
        else if(state.hasProperty(BlockStateProperties.FACING_EXCEPT_UP))
            return state.get(BlockStateProperties.FACING_EXCEPT_UP);
        return Direction.NORTH;
    }
}
