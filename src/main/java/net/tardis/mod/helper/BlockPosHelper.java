package net.tardis.mod.helper;

import java.util.List;
import java.util.stream.Stream;

import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.MutableBoundingBox;
import net.minecraft.util.math.SectionPos;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.gen.feature.structure.StructureManager;
import net.minecraft.world.gen.feature.structure.StructurePiece;
import net.minecraft.world.gen.feature.structure.StructureStart;
import net.minecraft.world.server.ServerWorld;

public class BlockPosHelper {
    public static BlockPos lerp(BlockPos a,BlockPos b, float t){
        t = MathHelper.clamp(t, 0f, 1f);

        return a.add(
                (b.getX() - a.getX()) * t,
                (b.getY() - a.getY()) * t,
                (b.getZ() - a.getZ()) * t);
    }
    
    public static List<BlockPos> getFilteredBlockPositionsInStructure(BlockPos test, ServerWorld world, StructureManager manager, Structure<?> structure, Block filteredBlock){
        List<BlockPos> list = Lists.newArrayList();
        StructureStart<?> start = manager.getStructureStart(SectionPos.from(test), structure, world.getChunk(test));
        for (StructurePiece piece : start.getComponents()){
            MutableBoundingBox boundingBox = piece.getBoundingBox();
            Stream<BlockPos> positions = BlockPos.getAllInBox(boundingBox).filter(pos -> world.getBlockState(pos).matchesBlock(filteredBlock));
            positions.forEach(pos -> list.add(pos.toImmutable()));
        }
        return list;
    }
}
