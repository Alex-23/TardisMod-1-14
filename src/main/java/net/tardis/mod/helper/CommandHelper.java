package net.tardis.mod.helper;

import java.util.Collection;
import java.util.Map;

import com.mojang.brigadier.suggestion.SuggestionProvider;
import com.mojang.brigadier.suggestion.SuggestionsBuilder;

import net.minecraft.command.CommandSource;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;

public class CommandHelper {
	/** Adds available Tardis Names and their corresponding world keys via a tooltip*/
	public static SuggestionsBuilder addTardisNamesAndKeySuggestions(SuggestionsBuilder builder, MinecraftServer server) {
    	Map<RegistryKey<World>, String> map = TardisHelper.getTardisWorldKeyAndNames(server);
    	map.entrySet().forEach(entry -> {
    		builder.suggest(entry.getValue(), new StringTextComponent(entry.getKey().getLocation().toString()).modifyStyle((style) -> style.setFormatting(TextFormatting.GREEN)));
    	});
    	builder.buildFuture();
    	return builder;
    }
	
	/** Adds available Tardis Names and their corresponding world keys via a tooltip*/
	public static SuggestionsBuilder addTardisNamesAndKeySuggestionsForPlayer(SuggestionsBuilder builder, MinecraftServer server, ServerPlayerEntity player) {
    	Map<RegistryKey<World>, String> map = TardisHelper.getTardisNamesAndWorldsByPlayer(server, player);
    	if (!map.isEmpty()) {
    		map.entrySet().forEach(entry -> {
        		builder.suggest(entry.getValue(), new StringTextComponent(entry.getKey().getLocation().toString()).modifyStyle((style) -> style.setFormatting(TextFormatting.GREEN)));
        	});
    	}
    	else {
    		builder.suggest(new TranslationTextComponent("command.suggestion.tardis.no_connections").getString(), new TranslationTextComponent("command.suggestion.tardis.no_connections_tooltip"));
    	}
    	builder.buildFuture();
    	return builder;
    }

	/** Gets all Tardis UUID based dimension ids for the player and shows the Tardis Name for easy reference*/
	public static SuggestionsBuilder addTardisKeysWithNameTooltip(SuggestionsBuilder builder, MinecraftServer server) {
    	Map<RegistryKey<World>, String> map = TardisHelper.getTardisWorldKeyAndNames(server);
    	map.entrySet().forEach(entry -> {
    		builder.suggest(entry.getKey().getLocation().toString(), new StringTextComponent(entry.getValue()).modifyStyle((style) -> style.setFormatting(TextFormatting.GREEN)));
    	});
    	builder.buildFuture();
    	return builder;
    }

	/** Gets all Tardis UUID based dimension ids for the player who has telepathically connected to that Tardis*/
	public static SuggestionsBuilder addTardisKeysWithNameTooltipForPlayer(SuggestionsBuilder builder, MinecraftServer server, ServerPlayerEntity player) {
		Map<RegistryKey<World>, String> map = TardisHelper.getTardisNamesAndWorldsByPlayer(server, player);
    	if (!map.isEmpty()) {
    		map.entrySet().forEach(entry -> {
    			builder.suggest(entry.getKey().getLocation().toString(), new StringTextComponent(entry.getValue()).modifyStyle((style) -> style.setFormatting(TextFormatting.GREEN)));
        	});
    	}
    	else {
    		builder.suggest(new TranslationTextComponent("command.suggestion.tardis.no_connections").getString(), new TranslationTextComponent("command.suggestion.tardis.no_connections_tooltip"));
    	}
    	builder.buildFuture();
    	return builder;
    }
	
	public static final SuggestionProvider<CommandSource> SUGGEST_TARDISES = (context, suggestionBuilder) -> {
	      Collection<RegistryKey<World>> collection = TardisHelper.getTardisWorldKeys(context.getSource().getServer());
	      return ISuggestionProvider.func_212476_a(collection.stream().map(RegistryKey::getLocation), suggestionBuilder);
    };


}
