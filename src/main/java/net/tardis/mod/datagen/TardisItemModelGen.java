package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.items.TItems;

public class TardisItemModelGen implements IDataProvider {

	private final DataGenerator generator;
	
	public TardisItemModelGen(DataGenerator generator) {
		this.generator = generator;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
        final Path path = this.generator.getOutputFolder();
		//generateBlockItemModel(cache, TBlocks.NEUTRONIC_SPECTROMETER);
        
//		IDataProvider.save(DataGen.GSON, cache, createItemModel(TItems.TARDIS_BACKDOOR.get()), getPath(path, TItems.TARDIS_BACKDOOR.get()));
		
	}

	public void generateBlockItemModel(DirectoryCache cache, RegistryObject<Block> block) throws IOException{

		ResourceLocation key = block.getId();

		JsonObject root = new JsonObject();
		root.add("parent", new JsonPrimitive(key.getNamespace() + ":block/" + key.getPath()));

		IDataProvider.save(DataGen.GSON, cache, root, getPath(this.generator.getOutputFolder(), key));
		System.out.println("Success");
	}
	
	public JsonObject createItemModel(Item item) {
		
		ResourceLocation key = item.getRegistryName();
		
		JsonObject object = new JsonObject();
		
		object.add("parent", new JsonPrimitive("item/generated"));
		
		JsonObject texture = new JsonObject();
		texture.add("layer0", new JsonPrimitive(key.getNamespace() + ":item/" + key.getPath()));
		object.add("textures", texture);
		
		return object;
	}
	
	public static Path getPath(Path path, ResourceLocation key) {
		return path.resolve("assets/" + key.getNamespace() + "/models/item/" + key.getPath() + ".json");
	}

	public static Path getPath(Path path, Item item) {
		return getPath(path, item.getRegistryName());
	}

	@Override
	public String getName() {
		return "TARDIS Item Generator";
	}
}
