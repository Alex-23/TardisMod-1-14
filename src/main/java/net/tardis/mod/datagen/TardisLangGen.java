package net.tardis.mod.datagen;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.Collectors;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.entity.EntityType;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.Util;
import net.minecraftforge.common.data.LanguageProvider;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.RoundelBlock;
import net.tardis.mod.constants.TardisConstants;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.items.ArtronItemStackBatteryItem;
import net.tardis.mod.items.ArtronCapacitorItem;
import net.tardis.mod.items.DalekSpawnItem;
import net.tardis.mod.items.KeyItem;
import net.tardis.mod.items.SonicItem;
import net.tardis.mod.items.SpawnerItem;
import net.tardis.mod.items.TardisPartItem;
import net.tardis.mod.missions.stages.DroneKillMissionStage;
import net.tardis.mod.missions.stages.MissionStages;
import net.tardis.mod.potions.TardisPotions;
import net.tardis.mod.registries.ConsoleRegistry;
import net.tardis.mod.registries.ControlRegistry;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.registries.ExteriorAnimationRegistry;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.registries.InteriorHumRegistry;
import net.tardis.mod.registries.ProtocolRegistry;
import net.tardis.mod.registries.SoundSchemeRegistry;
import net.tardis.mod.registries.VortexMFunctions;
import net.tardis.mod.sounds.AbstractSoundScheme;
import net.tardis.mod.sounds.TSounds;

public class TardisLangGen extends LanguageProvider {
    private final DataGenerator generator;
    public TardisLangGen(DataGenerator gen) {
        super(gen, Tardis.MODID, "en_us");
        this.generator = gen;
    }

    @Override
    protected void addTranslations() {
        
        //Items and Blocks
        for (Item item : ForgeRegistries.ITEMS.getValues()) {
            if(item.getRegistryName().getNamespace().contentEquals(Tardis.MODID)) {
                if (item instanceof BlockItem) {
                    BlockItem blockItem = (BlockItem)item;
                    if (blockItem.getBlock() instanceof RoundelBlock) {
                        RoundelBlock block = (RoundelBlock)blockItem.getBlock();
                        add(block.getTranslationKey(), fixCapitalisations(block.getRegistryName().getPath().trim().replace("roundel/", "") + " Roundel"));
                        continue;
                    }
                    if (item.getRegistryName().getPath().contains("ars")) {
                    	add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath().trim().replace("ars", "ARS")));
                        continue;
                    }
                    if (item.getRegistryName().getPath().contains("rca")) {
                    	add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath().trim().replace("rca", "RCA")));
                        continue;
                    }
                    else {
                        Block block = blockItem.getBlock();
                        add(block.getTranslationKey(), fixCapitalisations(block.getRegistryName().getPath()));
                    }
                }
                else {
                    if (item instanceof TardisPartItem) {
                    	if (item.getRegistryName().getPath().equals("blank_upgrade")) {
                            add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath()));
                        }
                        if (item.getRegistryName().getPath().contains("upgrades")) {
                            add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath().trim().replace("upgrades/", "") + " Upgrade"));
                        }
                        if (item.getRegistryName().getPath().contains("subsystem")) {
                            add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath().trim().replace("subsystem/", "")));
                        }
                        continue;
                    }
                    if (item instanceof KeyItem) {
                         add(item.getTranslationKey(), "Tardis Key");
                         continue;
                    }
                    if (item instanceof ArtronItemStackBatteryItem) {
                        ArtronItemStackBatteryItem battery = (ArtronItemStackBatteryItem)item;
                        if (item.getRegistryName().getPath().equals("artron_battery")) {
                            add(item.getTranslationKey(), "Artron Battery (\u00A79Basic\u00A7r)");
                            continue;
                        }
                        if (item.getRegistryName().getPath().contains("medium")) {
                            add(item.getTranslationKey(), "Artron Battery (\u00A72Medium\u00A7r)");
                            continue;
                        }
                        if (item.getRegistryName().getPath().contains("high")) {
                            add(item.getTranslationKey(), "Artron Battery (\u00A7eHigh\u00A7r)");
                            continue;
                        }
                        if (battery.isCreative()) {
                            add(item.getTranslationKey(), "Artron Battery (\u00A7dCreative\u00A7r)");
                            continue;
                        }
                    }
                    if (item instanceof ArtronCapacitorItem) {
                        if (item.getRegistryName().getPath().contains("mid")) {
                            add(item.getTranslationKey(), "Artron Capacitor (\u00A7aMedium\u00A7r)");
                            continue;
                        }
                        if (item.getRegistryName().getPath().contains("high")) {
                            add(item.getTranslationKey(), "Artron Capacitor (\u00A7eHigh\u00A7r)");
                            continue;
                        }
                    }
                    if (item instanceof SpawnerItem) {
                        if (item.getRegistryName().getPath().contains("int_door")) {
                            add(item.getTranslationKey(), "Interior Door");
                            continue;
                        }
                    }
                    if (item instanceof DalekSpawnItem) {
                    	add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath().trim().replace("/", " - ")));
                    	continue;
                    }
                    if (item.getRegistryName().getPath().contains("vm")) {
                        add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath().trim().replace("vm", "Vortex Manipulator")));
                        continue;
                    }
                    if (item.getRegistryName().getPath().equals("circuits")) {
                    	add(item.getTranslationKey(), "Exotronic Circuit");
                        continue;
                    }
                    if (item.getRegistryName().getPath().contains("ars")) {
                    	add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath().trim()).replace("Ars", "ARS"));
                        continue;
                    }
                    else {
                        if (item instanceof SonicItem) {
                            add(item.getTranslationKey(), "Sonic Screwdriver");
                        }
                        else {
                            add(item.getTranslationKey(), fixCapitalisations(item.getRegistryName().getPath()));
                        }
                    }
                }
            }
        }

        //Entities
        for (EntityType<?> type : ForgeRegistries.ENTITIES) {
            if (type.getRegistryName().getNamespace().contentEquals(Tardis.MODID)) {
                add(type.getTranslationKey(), fixCapitalisations(type.getRegistryName().getPath()));
            }
        }
        add("entity.minecraft.villager.tardis.story_teller", "Story Teller");
        
        //Tile display names
        add("display_name.tardis.anti_grav", "Range: %s");
        
        //Effects
        add(TardisPotions.MERCURY.get().getName(), "Mercury Poisoning");        
        //ItemGroups/Tabs
        add("itemGroup." + TItemGroups.FUTURE.getPath(), "Future Blocks");
        add("itemGroup." + TItemGroups.ROUNDELS.getPath(), "Roundels");
        add("itemGroup." + TItemGroups.MAIN.getPath(), "TARDIS Mod");
        add("itemGroup." + TItemGroups.MAINTENANCE.getPath(), "Ship Maintenance");
        
        //Controls
        add(getControlTranslationKeyFromEntry(ControlRegistry.COMMUNICATOR.get()), "Communicator");
        add(getControlTranslationKeyFromEntry(ControlRegistry.DIMENSION.get()), "Dimensional Shifters");
        add(getControlTranslationKeyFromEntry(ControlRegistry.DOOR.get()), "Door Switch");
        add(getControlTranslationKeyFromEntry(ControlRegistry.FACING.get()), "Exterior Facing");
        add(getControlTranslationKeyFromEntry(ControlRegistry.FAST_RETURN.get()), "Fast Return");
        add(getControlTranslationKeyFromEntry(ControlRegistry.HANDBRAKE.get()), "Handbrake");
        add(getControlTranslationKeyFromEntry(ControlRegistry.INC_MOD.get()), "XYZ Increment");
        add(getControlTranslationKeyFromEntry(ControlRegistry.LAND_TYPE.get()), "Vertical Land Type");
        add(getControlTranslationKeyFromEntry(ControlRegistry.MONITOR.get()), "Tardis Monitor");
        add(getControlTranslationKeyFromEntry(ControlRegistry.RANDOM.get()), "Randomiser");
        add(getControlTranslationKeyFromEntry(ControlRegistry.REFUELER.get()), "Refueler");
        add(getControlTranslationKeyFromEntry(ControlRegistry.SONIC_PORT.get()), "Sonic Port");
        add(getControlTranslationKeyFromEntry(ControlRegistry.STABILIZERS.get()), "Stabilisers");
        add(getControlTranslationKeyFromEntry(ControlRegistry.TELEPATHIC.get()), "Telepathic Circuits");
        add(getControlTranslationKeyFromEntry(ControlRegistry.THROTTLE.get()), "Throttle");
        add(getControlTranslationKeyFromEntry(ControlRegistry.X.get()), "X");
        add(getControlTranslationKeyFromEntry(ControlRegistry.Y.get()), "Y");
        add(getControlTranslationKeyFromEntry(ControlRegistry.Z.get()), "Z");
        
        //Damage Sources
        add("damagesrc.tardis.cyberman", "%s was deleted");
        add("damagesrc.tardis.dalek", "%s was exterminated by %s");
        add("damagesrc.tardis.dalek_spec", "%s was blown up by a Special Weapons Dalek");
        add("damagesrc.tardis.laser", "%s was disintegrated by %s's hand!");
        add("damagesrc.tardis.laser_sonic", "%s was disintegrated by &s via Laser Screwdriver");
//        add("damagesrc.tardis.space","%s ran out of oxygen");
        add("death.attack.damagesrc.tardis.space", "If there's life on Mars, %s ain't it");
        add("damagesrc.tardis.circuits", "%s was electrocuted by short circuiting live wires");
        add("damagesrc.tardis.tardis_squash", "%s was squished to pulp by the true mass of a Timeship");
        
        //Containers
        add("container.tardis.engine.north", "Components");
        add("container.tardis.engine.west", "Artron Banks");
        add("container.tardis.engine.east", "Charging/Attunement Panel");
        add("container.tardis.engine.south", "Upgrades");
        add("container.tardis.reclamation_unit", "Tardis Item Reclamation Unit");
        add("container.tardis.ship_computer", "Ship Computer");
        add("container.tardis.vm_battery", "Vortex Manipulator Battery");
        add("container.tardis.waypoint", "Waypoint Bank");
        add("container.tardis.alembic", "Alembic");
        add("container.tardis.spectrometer", "Spectrometer");
        
        add(VortexMFunctions.SCANNER.get().getTranslationKey(), "Scanner");
        add(VortexMFunctions.DISTRESS_SIGNAL.get().getTranslationKey(), "Distress Signal");
        
        
        //Exteriors
        add(ExteriorRegistry.APERTURE.get().getTranslationKey(), "Aperture Exterior");
        add(ExteriorRegistry.CLOCK.get().getTranslationKey(), "Clock Exterior");
        add(ExteriorRegistry.DISGUISE.get().getTranslationKey(), "Phased Optic Shell (Chameleon)");
        add(ExteriorRegistry.FORTUNE.get().getTranslationKey(), "Fortune Teller Exterior");
        add(ExteriorRegistry.JAPAN.get().getTranslationKey(), "Bokkusu Exterior");
        add(ExteriorRegistry.MODERN_POLICE_BOX.get().getTranslationKey(), "Modern Police Box (Mk 7)");
        add(ExteriorRegistry.POLICE_BOX.get().getTranslationKey(), "Police Box (Mk 3)");
        add(ExteriorRegistry.SAFE.get().getTranslationKey(), "Safe Exterior");
        add(ExteriorRegistry.STEAMPUNK.get().getTranslationKey(), "Steampunk Exterior");
        add(ExteriorRegistry.TELEPHONE.get().getTranslationKey(), "Red Telephone Box");
        add(ExteriorRegistry.TRUNK.get().getTranslationKey(), "Trunk Exterior");
        add(ExteriorRegistry.TT_2020.get().getTranslationKey(), "TT 2020 Exterior");
        add(ExteriorRegistry.TT_CAPSULE.get().getTranslationKey(), "TT Capsule Exterior");
        
        //Console Units
        add(ConsoleRegistry.CORAL.get().getTranslationKey(), "Coral Console");
        add(ConsoleRegistry.GALVANIC.get().getTranslationKey(), "Galvanic Console");
        add(ConsoleRegistry.HARTNELL.get().getTranslationKey(), "Hartnell Console");
        add(ConsoleRegistry.NEMO.get().getTranslationKey(), "Nemo Console");
        add(ConsoleRegistry.NEUTRON.get().getTranslationKey(), "Neutron Console");
        add(ConsoleRegistry.POLYMEDICAL.get().getTranslationKey(), "Polymedical Console");
        add(ConsoleRegistry.STEAM.get().getTranslationKey(), "Steam Console");
        add(ConsoleRegistry.TOYOTA.get().getTranslationKey(), "Toyota Console");
        add(ConsoleRegistry.XION.get().getTranslationKey(), "Xion Console");
        
        //Console Rooms
        add("interiors.tardis.interior_abandoned_alabaster", "Abandoned Alabaster Interior");
        add("interiors.tardis.interior_abandoned_imperial", "Abandoned Imperial Interior");
        add("interiors.tardis.interior_abandoned_jade", "Abandoned Jade Interior");
        add("interiors.tardis.interior_abandoned_nautilus", "Abandoned Nautilus Interior");
        add("interiors.tardis.interior_abandoned_panamax", "Abandoned Panamax Interior");
        add("interiors.tardis.interior_abandoned_steam", "Abandoned Steam Interior");
        add("interiors.tardis.interior_alabaster", "Alabaster Interior");
        add("interiors.tardis.interior_amethyst", "Amethyst Interior");
        add("interiors.tardis.interior_architect", "Architect Interior");
        add("interiors.tardis.interior_coral", "Coralised Interior");
        add("interiors.tardis.interior_debug_clean", "[DEV ONLY] Debug Interior");
        add("interiors.tardis.interior_envoy", "Envoy Interior");
        add("interiors.tardis.interior_imperial", "Imperial Interior");
        add("interiors.tardis.interior_jade", "Jade Interior");
        add("interiors.tardis.interior_metallic", "Metallic Interior");
        add("interiors.tardis.interior_nautilus", "Nautilus Interior");
        add("interiors.tardis.interior_omega", "Omega Interior");
        add("interiors.tardis.interior_panamax", "Panamax Interior");
        add("interiors.tardis.interior_steam", "Steam Interior");
        add("interiors.tardis.interior_toyota", "Toyota Interior");
        add("interiors.tardis.interior_traveler", "Traveler Interior");
        
        //ARS Rooms
        add("ars.piece.tardis.corridor.alabaster.fourway", "Alabaster Fourway");
        add("ars.piece.tardis.corridor.alabaster.t_junction", "Alabaster T-Connection");
        add("ars.piece.tardis.corridor.alabaster.short", "Alabaster Short Straight Piece");
        add("ars.piece.tardis.corridor.alabaster.long", "Alabaster Long Straight Piece");
        add("ars.piece.tardis.corridor.alabaster.turn_left", "Alabaster Left Corner");
        add("ars.piece.tardis.corridor.alabaster.turn_right", "Alabaster Right Corner");
        
        add("ars.piece.tardis.corridor.dark.fourway", "Dark Fourway");
        add("ars.piece.tardis.corridor.dark.t_junction", "Dark T-Connection");
        add("ars.piece.tardis.corridor.dark.short", "Dark Short Straight Piece");
        add("ars.piece.tardis.corridor.dark.long", "Dark Long Straight Piece");
        add("ars.piece.tardis.corridor.dark.turn_left", "Dark Left Corner");
        add("ars.piece.tardis.corridor.dark.turn_right", "Dark Right Corner");
        
        add("ars.piece.tardis.room.large_room", "Empty Large");
        add("ars.piece.tardis.room.library", "Library");
//        add("ars.piece.tardis.room.small_lab", "Small Laboratory");
        add("ars.piece.tardis.room.lab", "Laboratory");
        add("ars.piece.tardis.room.workroom_1", "Workroom Variant 1");
        add("ars.piece.tardis.room.workroom_2", "Workroom Variant 2");
        add("ars.piece.tardis.room.workroom_3", "Workroom Variant 3");
        add("ars.piece.tardis.room.storeroom", "Storeroom");
        add("ars.piece.tardis.room.hydroponics", "Hydroponics Farm");
        add("ars.piece.tardis.room.hydroponics_large", "Large Hydroponics Farm");
        add("ars.piece.tardis.room.ars_tree", "ARS Tree");
        add("ars.piece.tardis.room.gravity_shaft_down", "Gravity Shaft Down");
        add("ars.piece.tardis.room.gravity_shaft_up", "Gravity Shaft Up");
        add("ars.piece.tardis.room.archives", "Archive Library");
        
        add("ars.piece.tardis.room.cloister", "Cloister Room");
        
        //ARS Piece Categories
        add("ars.piece.category.tardis.room", "Room");
        add("ars.piece.category.tardis.corridor", "Corridor");
        add("ars.piece.category.tardis.alabaster", "Alabaster");
        add("ars.piece.category.tardis.dark", "Dark");
        
        //Flight Events
        add("flight_events.tardis.scrap", "WARNING: Vortex Scrap Incoming!");
        add("flight_events.tardis.time_wind", "WARNING: Time Winds Approaching!");
        add("flight_events.tardis.bulkhead", "WARNING: Exterior Bulkhead Failing!");
        add("flight_events.tardis.x", "WARNING: Spatial Drift X!");
        add("flight_events.tardis.y", "WARNING: Spatial Drift Y!");
        add("flight_events.tardis.z", "WARNING: Spatial Drift Z!");
        add("flight_events.tardis.refueler", "WARNING: Low Artron Flow!");
        add("flight_events.tardis.door", "WARNING: Exterior Bulkhead Failing!");
        add("flight_events.tardis.dimension", "WARNING: Dimensional Drift!");
        add("flight_events.tardis.vertical", "WARNING: Vertical Displacement Error!");
        add("flight_events.tardis.collide_instigate", "CRITICAL: Time Ram Imminent!");
        add("flight_events.tardis.collide_recieve", "CRITICAL: Time Ram Incoming!");
        add("flight_events.tardis.residual_artron", "INFO: Artron Pocket Detected!");
        
        //Sound Schemes
        for (AbstractSoundScheme scheme: SoundSchemeRegistry.SOUND_SCHEME_REGISTRY.get().getValues()) {
            add(scheme.getTranslationKey(), fixCapitalisations(scheme.getRegistryName().getPath()));
        }
        
        //Protocols
        add("protocol.tardis.antigrav_on", "Deactivate Anti-gravs");
        add("protocol.tardis.antigrav_off", "Activate Anti-gravs");
        add("protocol.tardis.forcefield_on", "Turn Forcefield On");
        add("protocol.tardis.forcefield_off", "Turn Forcefield Off");
        add("protocol.tardis.forcefield_turned_on", "Turned Forcefields On");
        add("protocol.tardis.forcefield_turned_off", "Turned Forcefields Off");
        add("protocol.tardis.forcefield_broken", "ERROR: Unable to activate Forcefield! Repair the Shield Generator!");
        add(ProtocolRegistry.LIFE_SCAN.get().getTranslationKey(), "Scan for Life Signs");
        add(ProtocolRegistry.TOGGLE_ALARM.get().getTranslationKey(), "Toggle Alarm");
        
        //Schematics
        add("schematic.tardis.exterior", "Exterior Schematic: %s");
        add("schematic.tardis.interior", "Interior Schematic: %s");
        
        //Sonic Modes
        add("sonic.mode.type", "Type:\u00A77 %s");
        add("sonic.mode.type.desc", "Description:\u00A77 %s");
        add("sonic.mode.block_interaction", "Block Interaction");
        add("sonic.mode.block_interaction.desc", "Interact with blocks in the world");
        add("sonic.mode.entity_interaction", "Entity Interactions");
        add("sonic.mode.entity_interaction.desc", "Interact with entities in the world");
        add("sonic.mode.probe", "Passive Probe");
        add("sonic.mode.probe.desc", "Scans the nearby area every second for space-time anomolies");
        add("sonic.mode.set_destination", "Tardis Co-ordinates");
        add("sonic.mode.set_destination.desc", "Sets Tardis destination to block position, but doesn't summon it.");
        add("sonic.mode.laser_interaction", "Lasers");
        add("sonic.mode.laser_interaction.desc", "Fire Lasers");
        add("sonic.modes.info.interactable_blocks", "Interactable blocks:");
        add("sonic.mode.charge", "Charge: \u00A7d%s");
        add("sonic.modes.info.interactable_entities", "Interactable entities:");
        add("sonic.modes.info.set_coords", "Right clicking in the world will set the attuned Tardis's destination coordinates to the right clicked position");
        add(TardisConstants.Translations.STORED_SCHEMATICS, "Stored Schematics:");
        add("sonic.component_name.mk_1", "Prototype");
        add("sonic.component_name.mk_2", "Radiometric");
        add("sonic.component_name.mk_3", "Biopolymer");
        add("sonic.component_name.mk_4", "Electrostructural");
        add("sonic.component_name.mk_5", "Quadrophonic");
        add("sonic.component_name.mk_6", "Transtemporal");
        add("sonic.component_name.mk_7", "Phased-Photon");
        
        //VM Functions
        add(VortexMFunctions.TELEPORT.get().getTranslationKey(), "Teleport");
        add(VortexMFunctions.BATTERY.get().getTranslationKey(), "Battery");
        
        //Interior Hums
        add(InteriorHumRegistry.COPPER.get().getTranslationKey(), "Copper's Hum");
        add(InteriorHumRegistry.CORAL.get().getTranslationKey(), "Coral's Hum");
        add(InteriorHumRegistry.DISABLED.get().getTranslationKey(), "Disable Hums");
        add(InteriorHumRegistry.EIGHTY.get().getTranslationKey(), "80's Hum");
        add(InteriorHumRegistry.SEVENTY.get().getTranslationKey(), "70's Hum");
        add(InteriorHumRegistry.SIXTY_THREE.get().getTranslationKey(), "63's Hum");
        add(InteriorHumRegistry.TOYOTA.get().getTranslationKey(), "Toyota's Hum");
        
        add(ExteriorAnimationRegistry.CLASSIC.get().getTranslationKey(), "Exterior Animation: Classic");
        add(ExteriorAnimationRegistry.NEW_WHO.get().getTranslationKey(), "Exterior Animation: New Who");
        
        //Texture Variants
        add("texvar.tardis.tardis.common.normal", "Normal");
        add("texvar.tardis.exterior.trunk.dark", "Dark");
        add("texvar.tardis.console.steam.normal", "Normal");
        add("texvar.tardis.console.steam.ironclad", "Iron Clad");
        add("texvar.tardis.console.steam.rosewood", "Rosewood");
        add("texvar.tardis.console.nemo.normal", "Normal");
        add("texvar.tardis.console.nemo.ivory", "Ivory");
        add("texvar.tardis.console.nemo.wood", "Wooden");
        add("texvar.tardis.console.toyota.normal", "Normal");
        add("texvar.tardis.console.toyota.violet", "Violet");
        add("texvar.tardis.console.toyota.blue", "Blue");
        add("texvar.tardis.console.galvanic.imperial", "Imperial");
        add("texvar.tardis.console.galvanic.wood", "Wooden");
        add("texvar.tardis.console.galvanic.sith", "Sith");
        add("texvar.tardis.console.xion.glass", "Glass");
        add("texvar.tardis.console.xion.sapphire", "Sapphire");
        add("texvar.tardis.console.xion.ruby", "Ruby");
        add("texvar.tardis.exterior.steam.normal", "Normal");
        add("texvar.tardis.exterior.steam.blue", "Blue");
        add("texvar.tardis.exterior.steam.rust", "Rusted");
        add("texvar.tardis.exterior.fortune.normal", "Normal");
        add("texvar.tardis.exterior.fortune.blue", "Blue");
        add("texvar.tardis.exterior.fortune.red", "Red");
        add("texvar.tardis.console.coral.blue", "Blue");
        add("texvar.tardis.console.coral.white", "White");
        add("texvar.tardis.console.neutron.thaumic", "Thumamic");
        add("texvar.tardis.console.neutron.brass", "Brass");
        add("texvar.tardis.exterior.telephone.cyberpunk", "Cyberpunk");
        
        //Artron Uses
        add("artronuse.tardis.forcefield", "Forcefield");
        add("artronuse.tardis.converter", "Artron to FE Converter");
        add("artronuse.tardis.flight", "Flight");
        add("artronuse.tardis.antigravs", "Anti-Grav System");
        add("artronuse.tardis.interior_change", "Interior Reconfiguration");
        
        //Mood Types
        add("mood.tardis.ecstatic", "The TARDIS is feeling Ecstatic!");
        add("mood.tardis.happy", "The TARDIS is feeling Happy!");
        add("mood.tardis.content", "The TARDIS is feeling Content");
        add("mood.tardis.apathetic", "The TARDIS is feeling Apathetic!");
        add("mood.tardis.discontent", "The TARDIS is feeling Discontent");
        add("mood.tardis.sad", "The TARDIS is feeling Sad!");
        
        //Laser Power Tier
        add("laser_power_tier.tardis.low", "Low");
        add("laser_power_tier.tardis.medium", "Medium");
        add("laser_power_tier.tardis.high", "High");
        
        //Configs
        
        //Client Configs
        add("config.tardis.enableBoti","Enable Boti Rendering");
        add("config.tardis.botiBlacklistedBlocks", "BOTI Blacklisted Block IDs");
        add("config.tardis.botiBlacklistedEntities", "BOTI Blacklisted Entity IDs");
        
        add("config.tardis.show_more_machine_tooltips", "Show more Machine tooltips");
        add("config.tardis.play_tool_notification_sound", "Play Tool Notification Sounds");
        add("config.tardis.display_optifine_warning", "Display Optifine Warning");
        
        add("config.tardis.openVMEmptyHand", "Open VM GUI with Empty Hand");
        add("config.tardis.interiorShake", "Tardis Interior Camera Shake");
        
        //Common Configs
        add("config.tardis.tardis_spawn_chance", "Broken Tardis Generation Chance");
        add("config.tardis.tardis_spawn_dim_blacklist", "Broken Tardis Dimension Blacklist");
        add("config.tardis.cinnabar_ore_spawn_chance", "Cinnabar Ore Generation Chance");
        add("config.tardis.xion_crystal_spawn_chance", "Xion Crystal Generation Chance");
        add("config.tardis.dalek_join_village_raid", "Should Daleks join Village Raids?");
        add("config.tardis.dalek_village_raid_spawn_count","Dalek Village Raid Wave Spawns");
        
        add(TardisConstants.Translations.STRUCTURE_GEN_CHANCE, "Structure Generation Chance");
        add(TardisConstants.Translations.STRUCTURE_SEPERATION, "Structure Seperation Distance");
        add(TardisConstants.Translations.STRUCTURE_SPACING, "Structure Spacing Distance");
        
        add(TardisConstants.Translations.VILLAGE_STRUCTURE_WEIGHT, "Village Structure Spawn Weight");
        
        
        //Server Configs
        add("config.tardis.blacklistedDims", "Blacklisted Tardis Destination Dimensions");
        add("config.tardis.dims_handle_tardis_void_teleport", "Dimensions to Allow Tardis Void Teleport Mechanics");
        add("config.tardis.tardis_abandonment_timer", "Tardis Abandonment Timer");
        
        add("config.tardis.machines.transduction_drain_amount", "Transduction Barrier Energy Per Use");
        add("config.tardis.machines.transduction_energy_max_capacity", "Transduction Barrier Max Energy");
        add("config.tardis.machines.transduction_effective_range", "Transduction Barrier Effective Range");
        add("config.tardis.machines.artron_collector_battery_charge_rift", "Artron Collector Charge Rate");
        add("config.tardis.machines.artron_pylon_rift_drain_rate", "Artron Pylon Rift Drain Rate");
        add("config.tardis.machines.roundel_tap_energy_transfer_rate", "Roundel Tap Forge Energy Transfer Rate");
        
        add("config.tardis.vm.toggleVMDimWhitelist", "Toggle Vortex Manipulator Dimension Whitelist");
        add("config.tardis.vm.whitelistedDims", "Whitelisted VM Destination Dimensions");
        add("config.tardis.vm.blacklistedDims", "Blacklisted VM Destination Dimensions");
        add("config.tardis.vm.teleportRange", "Teleport Range");
        add("config.tardis.vm.cooldownTime", "Use Cooldown");
        add("config.tardis.vm.sideEffects", "Side Effect List");
        add("config.tardis.vm.sideEffectTime", "Side Effect Duration");
        add("config.tardis.vm.baseFuelUsage", "Base Fuel Usage");
        add("config.tardis.vm.fuelUsageMultiplier", "Fuel Usage Multiplier");
        add("config.tardis.vm.fuelUsageTime", "Fuel Usage Rate");

        add("config.tardis.open_doors", "Open Doors");
        add("config.tardis.detonate_tnt", "Should Sonics detonate TNT?");
        add("config.tardis.redstone_lamps", "Should Sonics toggle Redstone Lamps?");
        add("config.tardis.open_trapdoors", "Should Sonics open non-wooden trapdoors?");
        add("config.tardis.toggle_redstone", "Should Sonics toggle redstone?");
        add("config.tardis.detonate_creeper", "Should Sonics detonate Creepers?");
        add("config.tardis.shear_sheep", "Can Sonics shear sheep?");
        add("config.tardis.dismantle_skeleton", "Can Sonics dismantle Skeleton-type entities?");
        add("config.tardis.ink_squid", "Can Sonics make Squids squirt ink?");
        add("config.tardis.coordinate_tardis", "Can Sonics set the Tardis destination?");
        add("config.tardis.laser_fire", "Can Sonics shoot lasers?");
        
        add("config.tardis.dalek.dalek_block_grief", "Dalek Block Griefing");
        add("config.tardis.dalek_special_weapons_always_explode","Special Weapons Lasers Always Explode");
        add("config.tardis.dalek.special_weapon_explosion_radius", "Special Weapon Explosion Radius");
        add("config.tardis.dalek.dalek_target_all","Target All Living Entities");
        add("config.tardis.dalek.dalek_target_hostile_only","Target Hostiles Only");
        add("config.tardis.dalek.dalek_target_animals","Target Animals");
        add("config.tardis.dalek.dalek_target_players","Target Players");
        add("config.tardis.dalek.dalek_target_villagers", "Target Villagers");

        //Missions
        add("missions.tardis.drone_station", "Space Station Mission");
        add(TardisConstants.Translations.MISSION_OBJECTIVE.getKey(), "Objective");
        
        //Mission Stages
        add(MissionStages.NOT_STARTED.get().getTranslationKey(), "Mission Not Started");
        add(MissionStages.RETURN_TO_MISSION_HOST.get().getTranslationKey(), "Return to: ");
        add(DroneKillMissionStage.KILL_DRONES_DESCRIPTION.getKey(), "Kill the rogue Security Drones");
        add(MissionStages.MISSION_COMPLETED.get().getTranslationKey(), "Mission Complete!");
        
        //Mission Icons - scrapped concept
//        add("icon.tardis.mission.not_started_urgent","\u00A77[\u00A76!\u00A77]");
//        add("icon.tardis.mission.not_started_question", "\u00A77[\u00A7c?\u00A77]");
//        add("icon.tardis.mission.in_progress", "\u00A77[\u00A7e" + "-" + "\u00A77]"); //u2713 is a Checkmark in unicode
//        add("icon.tardis.mission.completed", "\u00A77[\u00A7a" + "\u2713" + "\u00A77]");
        
        //Mission Dialogue

        add(TardisConstants.Translations.MISSION_DISTRESS_SIGNAL, "SOS Detected!");
        add("mission.tardis.drone_station.dialog.character.capt.confused_greet", "Hello...what are you doing here?");
        add("mission.tardis.drone_station.dialog_option.player.respond_sos_with_intro", "The name is %s, I'm responding to your S.O.S");
        add("mission.tardis.drone_station.dialog_option.player.agree_to_help", "I'll help!");
        add("mission.tardis.drone_station.dialog.character.capt.hostile_greet", "Stop! Why are you. on. my. ship?!");
        add("mission.tardis.drone_station.dialog.character.capt.relieved_request", "Thank the stars! The ship's security drones seem to have malfunctioned and started killing the crew! We're all that's left. We have managed to lock ourselves here, in the bridge, but we need someone to take them out!");
        add("mission.tardis.drone_station.dialog_option.player.understand_request", "Alright.");
        add("mission.tardis.drone_station.dialog.character.capt.frustrated_request", "Why didn't you say so? The ship's security programs went rogue and started attacking us! I managed to lock us in here, but we need someone to take them out quick!");
        add("mission.tardis.drone_station.dialog_option.player.ignore_frustrated_request", "Alright. You have the best of luck with that.");
        add("mission.tardis.drone_station.dialog_option.player.respond_sos_pacifying", "Alright, take it easy! I'm responding to your SOS.");
        add("mission.tardis.drone_station.dialog_option.player.smartass", "Well, you know, I was in the neighborhood and I thought...");
        add("mission.tardis.drone_station.dialog.character.capt.followup_player", "Hurry, please!");
        add("mission.tardis.drone_station.dialog_option.player.followup_polite", "I'm working on it!");
        add("mission.tardis.drone_station.dialog_option.player.followup_rude", "Shut up and let me work!");
        add("mission.tardis.drone_station.dialog_option.player.followup_frustrated", "Do you want to do it instead?");
        add("mission.tardis.drone_station.dialog.character.capt.followup_player.more_drones", "Thank you! Our scans indicate there are still rogue drones around! Please hurry!");
        add("mission.tardis.drone_station.dialog_option.player.acknowledge_polite", "Understood, thanks.");
        add("mission.tardis.drone_station.dialog_option.player.acknowledge_rude", "I know, I know!");
        add("mission.tardis.drone_station.dialog_option.player.acknowledge_frustrated", "Come on, why couldn't this be easier?");
        add("mission.tardis.drone_station.dialog.character.capt.thankful_reward", "Here, take this, it's part of our navigation systems. It may not look much, but It should be very valuable in the right hands.");
        add("mission.tardis.drone_station.dialog_option.player.polite_accept_reward", "Thank you, I'm sure I can find use for it on my ship!");
        add("mission.tardis.drone_station.dialog_option.player.ungrateful_accept_reward", "Really? A piece of scrap?");
        
        //Companion dialogue
        add("companion.tardis.dialog.hello", "What's up?");
        add("companion.tardis.dialog_option.wait", "I need you to wait here");
        add("companion.tardis.dialog_option.follow", "Follow me");
        add("companion.tardis.dialog_option.fly_tardis", "I want you to watch me, learn how to fly the TARDIS");
        
        //Messages
        add("message.tardis.optifine_warning", "Optifine may cause issues with TARDIS Mod's portal effect! Please disable it or remove Optifine.");
        
        add("message.vm.header", "===VM===");
        add("message.vm.scanner.result", "There are \u00A7d%s \u00A7rOther Players and \u00A7d%s \u00A7rMonsters \u00A7rin a %s block radius!");
        add("message.vm.forbidden", "Safety Warning!\nNot permitted to use\nwithin this dimension!");
        add("message.vm.invalidPos", "Invalid Destination Position!");
        add("message.vm.invalidInput", "Invalid Coordinate Input!\nEnter a whole number for: \u00A7d%s coordinate!");
        add("message.vm.illegalPos", "Destination Position exceeds\nTeleport Range of %s blocks!\nEnter a valid position in the %s coordinate!");
        add("message.vm.fuel_empty", "You do not have enough fuel to make this journey! \nYou need at least: \u00A7d%s \u00A7rArtron Units in total!");
        add("message.vm.distress_sent", "Distress Signal sent to: %s");
        add("message.vm.lightning_malfunction", "Device Malfunction! Initiating Jump!");
        add("message.vm.distress_signal_message", "VM Distress Signal");

        add("message.tardis.distress_signal.player", "%s sent a distress signal");
        
        add("message.tardis.energy_buffer", "Forge Energy: %s/%s FE");
        add("message.tardis.transduction_barrier.code", "Transduction Code: %s");
        add("message.tardis.multiblock.invalid_place", "There isn't enough space to place this!");
        
        add("message.tardis.wood_fail" , "It doesn't work on wood!");
        add("message.tardis.detonate_tnt", "Detonating TNT!");
        add("message.tardis.use.requires_sonic", "You can only use this with a Sonic Screwdriver!");
        add("message.sonic.invalid_result", "ERROR: Target is not a valid block/entity!");
        add("message.tardis.found_rift", "Space-Time Rift Detected! Energy: %s AU");
        
        add("message.tardis.control.dimchange", "Target Dimension set to: ");
        add("message.tardis.control.facing", "Directional Placement set to: ");
        add("message.tardis.control.inc_mod", "Coordinate Increment Modifier set to: ");
        add("message.tardis.control.land_type", "Vertical Scanning Mode: ");
        add("message.tardis.control.refuel.true", "Refueling Protocols \u00A7dEngaged");
        add("message.tardis.control.refuel.false", "Refueling Protocols \u00A7dDisengaged");
        add("message.tardis.control.stabilizer.true", "Stabilizers \u00A7dActive");
        add("message.tardis.control.stabilizer.false", "Stabilizers \u00A7dDeactivated");
        add("message.tardis.door.locked", "TARDIS locked!");
        add("message.tardis.door.unlocked", "TARDIS unlocked!");
        
        add("message.tardis.scanner", "%s humanoid(s) and %s other life signs detected!");
        add("message.tardis.set_coord_map", "Set Destination to centre of Map! (%s)");
        add("message.tardis.beacon_sent", "Distress Signal sent to all nearby Timeships!");
        add("message.tardis.item_not_attuned", "This item cannot be used, it is not attuned to a Timeship!");
        add("message.tardis.wrong_item", "You can only access this block using a %s");
        add("message.tardis.use.outside_tardis", "You can only use this inside a Tardis!");
        add("message.tardis.not_enough_artron", "Not enough Artron! You need %s more!");
        add("message.tardis.use.in_dim", "You cannot use this in the current dimension!");
        
        add("message.tardis.telepathic.success", "INFO: Loaded Coordinates into Nav-Com!");
        add("message.tardis.telepathic.not_found", "ERROR: Failed to locate %s in range! (%s)");
        
        add("message.data_crystal.transfer_to_bank", "Transferred Waypoints to Waypoint Bank!");
        add("message.data_crystal.extract_from_bank", "Extracted Waypoints from Waypoint Bank!");
        
        add("message.tardis.not_enough_charge", "Not enough charge! You need \u00A7d%s \u00A7rmore!");
        add("message.sonic.tardis_dest_set", "Timeship Destination set to: \u00A7d%s");
        add("message.sonic.tardis_dest_fail", "Cannot set Timeship Destination in this Dimension!");
        
        add("message.tardis.no_tardis_found", "No TARDIS Found with the specified ID: \n%s");
        
        add("message.monitor_remote.use", "You can only use this on a Tardis Monitor!");
        
        add("message.tardis.snap.denied", "This Timeship is not loyal enough to open as commanded!");
        
        add("message.tardis.interior_cooldown", "Interior Cooldown (%s seconds!)");
        
        add("message.tardis.vm.function_locked", "\u00A7cLocked: WIP");
        add("message.tardis.waypoint.saved", "INFO: Saved Waypoint %s");
        add("message.tardis.waypoint.save_fail", "ERROR: Failed to save waypoint %s");
        add("message.tardis.waypoint.save_fail_dimension", "ERROR: Cannot to save waypoint %s for blacklisted dimension!");
        add("message.waypoint.loaded", "INFO: Loaded Waypoint %s");
        add("message.tardis.waypoints.delete", "INFO: Deleted Waypoint %s");
        
        add("message.tardis.ars.selected_piece", "Selected ARS Structure: ");
//        add("message.tardis.invalid_ars_spawn", "ARS structures can only be spawned on an %s Block!");
        add("message.tardis.ars.structure_null", "You have not selected a valid ARS Structure with the ARS Tablet!");
        add("message.tardis.ars.room.delete.concurrent_mod.denied", "ERROR: You cannot create a new room whilst deleting an existing room!");
        add("message.tardis.ars.structure_not_found", "\u00A7cERROR: \u00A77Could not load structure:\u00A7r %s");
        add("message.tardis.ars.room.delete_countdown", "\u00A7e\u00A7lTardis Room Deletion in: \u00A7d\u00A7l%s \u00A7e\u00A7lseconds!");
        add("message.tardis.ars.room.deleted", "\u00A7aTardis Room Deletion Complete!\n\u00A7rYou have been teleported to the Console Room!");
        
        add("message.tardis.computer.downloaded", "Downloaded Data Fragments!");
        add("message.tardis.no_subsystem","WARNING: %s doesn't have enough health or is not activated!");
        
        add("message.tardis.telepathic.not_connected", "You feel the connection fade");
        add("message.tardis.telepathic.connected", "You feel connected to this Timeship");
        
        add("message.tardis.transduction.set_code", "Set Landing Code to: %s");
        
        add("message.tardis.not_admin", "ERROR: This Tardis is not loyal enough to you to perform this action");
        
        add("message.tardis.door.deadlocked", "Access denied! This Timeship is Deadlocked!");
        add("message.tardis.door.interior_change", "Entry Forbidden! Interior Reconfiguration in Progress! (%s seconds remaining)");
        add("message.tardis.change_interior.started", "Interior Reconfiguration to %s started! All occupants, please exit this Timeship to start the process");
        add("message.tardis.change_interior.cancelled","Interior Reconfiguration Process Cancelled!");
        add("message.tardis.change_interior.complete","Interior Reconfiguration Process Completed for Timeship %s!");
        add("message.tardis.change_interior.interrupted","Interior Reconfiguration Process Interrupted for Timeship %s! Relocated player to the exterior to resume the process!");
        
        add("message.tardis.anti_gravs.true", "Activated Anti-Gravity Protocols");
        add("message.tardis.anti_gravs.false", "Deactivated Anti-Gravity Protocols");
        
        add("message.tardis.subsystem.not_activated.shield_generator", "ERROR: Shield Generator is not Activated!");
        
        add("message.tardis.corridor.blocked", "ERROR: Cannot place structure due to existing blocks");
        add("message.tardis.corridor.blocked_positions", "ERROR: Please ensure the area between %s and %s are clear of any blocks. Conflicting position at: %s");
        
        add(TardisConstants.Translations.UNLOCKED_INTERIOR, "Unlocked interior %s!");
        add(TardisConstants.Translations.UNLOCKED_EXTERIOR, "Unlocked exterior %s!");
        add(TardisConstants.Translations.UNLOCKED_CONSOLE, "Unlocked console %s!");
        add(TardisConstants.Translations.ALREADY_UNLOCKED, "Object %s has already been unlocked!");
        
        add("message.tardis.time_link.linked", "Time Link Upgrade now linked to: ");
        add("message.tardis.roundel_tap.push_to_tardis", "Tap is now pushing energy to Tardis");
        add("message.tardis.roundel_tap.push_to_blocks", "Tap is now pushing energy to nearby blocks");
        
        add("message.laser_gun.set_power_tier", "Set Power Tier to: ");
        
        //Trait Messages
        add("trait.tardis.dislike_location", "The timeship doesn't like this location");
        add("trait.tardis.like_location", "The timeship likes this location");
        add("trait.tardis.generic_like", "The timeship likes that");
        add("trait.tardis.generic_dislike", "The timeship dislikes that");
        
        //Tooltips
        add("tooltip.item.info.shift", "\u00A77Hold \u00A78[\u00A7eLeft Shift\u00A78]");
        add("tooltip.item.info.shift_control", "\u00A77Hold \u00A78[\u00A7eLeft Shift\u00A78] \u00A77and \u00A78[\u00A7eLeft Control\u00A78]");
        add("tooltip.item.info.control", "\u00A77Hold \u00A78[\u00A7eLeft Control\u00A78]");
        add("tooltip.item.redstone.required", "This requires a Redstone Signal to activate!");
        add("tooltip.item.artron_battery_required","Requires Artron Batteries");
        add("tooltip.item.info.disabled", "This feature has been disabled");
        
        add(TardisConstants.Strings.EMPTY_FLUID, "Empty");
        add(TardisConstants.Strings.FLUID, "Fluid: ");
        add(TardisConstants.Strings.PROGRESS, "Progress: ");
        add(TardisConstants.Strings.ETA_TIME, "ETA: ");
        add(TardisConstants.Strings.MILLIBUCKETS, " MB");
        add(TardisConstants.Strings.SECONDS, " s");
        add(TardisConstants.Strings.MERCURY_FLUID, "Mercury");
        add("tooltip.item.description_prefix", "Description: ");
        
        add("tooltip.laser_gun.charge.none", "No Charge!");
        add("tooltip.laser_gun.charge", "Charge: ");
        add("tooltip.laser_gun.shots_left", "Estimated Shots Left: ");
        add("tooltip.laser_gun.num_batteries", "Battery Count: ");
        add("tooltip.laser_gun.tier", "Power Tier: ");
        add("tooltip.laser_gun.description", "Add or remove batteries with %s.\nUse Sneak and Right Click to change power tiers.");
        
        add("tooltip.monitor_remote.use", "Adjusts camera angle of Monitor Scanner Mode \nAdjusts arm extension length on Rotating Monitor Block\nSneak and right click on a Tardis Monitor");
        
        add("tooltip.item.attuned.owner", "Attuned to: ");
        add("tooltip.item.not_attuned", "\u00A78[\u00A7cNot Attuned\u00A78]");
        add("tooltip.tardis.diagnostic.owner", "Tracking Timeship: ");
        add("tooltip.tardis.diagnostic.use", "Hold this and look around until you hear a beeping noise to locate your Timeship Exterior or Console.");
        add("tooltip.tardis.diagnostic.use2", "Hover over the console block to view system dashboard");
        
        add("tooltip.stat_remote.use", "Right click on a block to summon your Timeship");
        add("tooltip.stat_remote.tardis_owner", "Tracking Timeship: ");
        add("tooltip.stat_remote.exterior_dim", " Dimension: ");
        add("tooltip.stat_remote.exterior_pos", " Position: ");
        add("tooltip.stat_remote.in_flight", "In Flight: ");
        add("tooltip.stat_remote.journey", "Journey: ");
        add("tooltip.stat_remote.fuel", "Fuel: ");
        
        add("tooltip.ars_tablet.piece", "Selected ARS Piece: ");
        add("tooltip.ars_tablet.spawn", "Room Creation: \u00A77Right Click on an \u00A72Corridor Spawn Block");
        add("tooltip.ars_tablet.remove", "Room Removal: \u00A77Right Click on an \u00A7cCorridor Kill Block");
        add("tooltip.ars_structure.placement_offset", "Place %s block(s) above a solid block to generate ARS structures properly");
        
        add("tooltip.plasmic_shell.use1", "Right click on a block to set start position, then another block to set end position");
        add("tooltip.plasmic_shell.use2", "Sneak and Right click on an Atrium Frame block to create disguise json file. \nThe block below the Atrium Frame block will not be included in the json");
        add("tooltip.plasmic_shell.use3", "Sneak and Right click in the air to clear positions");
        
        add("tooltip.artron_battery.creative_setup", "\u00A7cRight click to gain infinite charge");
        add("tooltip.artron_battery.max_charge", "Max Capacity: ");
        add("tooltip.artron_battery.charge", "Artron: ");
        add("tooltip.artron_battery.discharge_multiplier", "Discharge Multiplier: ");
        add("tooltip.artron_battery.charge_multiplier", "Charge Multiplier: ");
        add("tooltip.artron_battery.howto", "Used to fuel various devices\nRecharging - in Artron Collector or Charging Panel of the Tardis Engine!");
        
        add("tooltip.vm.total_charge", "Total Charge: ");
        add("tooltip.vm.battery_count", "Artron Batteries: ");
        
        add("tooltip.backdoor.description", "Allows for emergency access to a linked Timeship. Attune this item, then use this on the top half of a door block to spawn an invisible portal.");
        
        add("tooltip.artron_capacitor.max_charge", "Total Capacity: ");
        add("tooltip.artron_capacitor.recharge_multiplier", "Recharge Multiplier: ");
        add("tooltip.artron_capacitor.info", "Increases Tardis Artron Bank Capacity and Refuel Rate");
        add("tooltip.artron_capacitor.howto", "Place in Tardis Engine (Artron Banks Panel) to allow Tardis to refuel!");
        
        add("tooltip.part.type", "Type: ");
        add("tooltip.part.flight.required", "Required for Flight: ");
        add("tooltip.part.repair.required", "Requires Repair: ");
        add("tooltip.part.engine_panel", "Engine Install Location: ");
        add("tooltip.part.dependency", "Required Dependency: ");
        
        add("tooltip.part.subsystem/dematerialisation_circuit.description", "Used for flight. \nTakes 1 damage every 5-10 seconds");
//        add("tooltip.part.subsystem/thermocoupling.description", "Non Functional at the moment");
        add("tooltip.part.subsystem/fluid_link.description", "Used for flight with Dematerialisation Circuit. Required for refuelling. \nTakes 1 damage on takeoff and 1 on landing");
        add("tooltip.part.subsystem/chameleon_circuit.description", "Allows for Exterior to be changed.\nTakes 1 damage per tick and chance to take 50% more");
        add("tooltip.part.subsystem/interstitial_antenna.description", "Allows for detection of Distress Signals. \nTakes 1 damage per tick and 2 more if a signal is detected");
        add("tooltip.part.subsystem/temporal_grace.description", "Negates damage to pilot when they are hurt inside the interior. \nTakes 1 damage everytime the pilot is hurt in the interior");
        add("tooltip.part.subsystem/shield_generator.description", "Prevents other subsystems from taking damage when exterior is hit. \nTakes 10 damage for explosions and 4 for projectiles");
        add("tooltip.part.subsystem/stabilizer.description", "Allows your Tardis to switch between Stabilized and Unstabilized Flight Mode. \nTakes 1 damage every 10 seconds when being used");
        add("tooltip.part.subsystem/nav_com.description", "Allows your Tardis to be able to set its destination position. \nWithout it, the destination coordinates will be randomised");
        
        add("tooltip.part.blank_upgrade.description", "Template to craft other Upgrades.");
        add("tooltip.part.upgrades/atrium.description", "Takes blocks with the exterior. \nConsult the Tardis Manual for more information.");
        add("tooltip.part.upgrades/electro_convert.description", "Allows all entities within a 16 block radius around the exterior to breath underwater when exterior is submerged");
        add("tooltip.part.upgrades/structure.description", "Allows the Telepathic Circuits to locate valid Structures within 500 blocks of the exterior");
        add("tooltip.part.upgrades/key_fob.description", "Allows you to open the Tardis doors using the Tardis Key");
        add("tooltip.part.upgrades/time_link.description", "Allows you to tow another Tardis using your own.");
        add("tooltip.part.upgrades/zero_room.description", "Removes Poison Effects and heals players under their max health. \nTakes 1 damage for every heart healed");
        
        add("tooltip.tape_measure.howto", "Right click two blocks to calculate the difference between them");
        add("tooltip.tape_measure.howto_2", "Sneak + Right Click on a Block to clear your selection");
        add("tooltip.tardis.watch.line1", "Tells the localtime of the dimension your exterior is in");
        add("tooltip.tardis.watch.line2", "May go crazy near time or spacial anomalies");
        
        add("tooltip.roundel_tap.purpose", "Allows energy to be taken from the Tardis Forge Energy Buffer");
        add("tooltip.roundel_tap.use", "Connect other mod cables to push/pull energy to and from the Tardis' FE Buffer");
        
        add("tooltip.item_access_panel.description_one", "Allows items to be piped throughout the Tardis via this block.");
        add("tooltip.item_access_panel.description_two", "Connect hoppers and similar objects to the sides of this block.");
        
        add("tooltip.tardis.mood_change", "CREATIVE ONLY: Changes the timeship's mood.");
        
        add("tooltip.waypoint_bank.desc", "Adds 5 more waypoints to the Tardis.\nWhen broken, deletes waypoints from the Tardis!");
        
        add("tooltip.gui.vm.select", "Activate Function");
        add("tooltip.gui.vm.close", "Close GUI");
        add("tooltip.gui.vm.dec_func", "Previous Function");
        add("tooltip.gui.vm.inc_func", "Next Function");
        add("tooltip.gui.vm.inc_subfunc", "Next Sub Function");
        add("tooltip.gui.vm.dec_subfunc", "Previous Sub Function");

        add("tooltip.gui.monitor_remote.update_arm_length", "Updates the Arm length for Rotating Monitor Block");
        add("tooltip.gui.monitor_remote.monitor_mode", "Set the View Mode of the Monitor");
        
        add("tooltip.tardis.data_crystal", "Saved Waypoint: %s");
        add("tooltip.tardis.data_crystal_use", "Extracts Waypoints from Waypoint Banks if crystal doesn't have waypoints. \nAdds waypoints to Waypoint Banks if this contains waypoints.");
        add("tooltip.tardis.data_crystal.empty", "No Waypoint Stored!");
        add("tooltip.tardis.waypoint_bank.bank_input", "Waypoints in Bank ");
        add("tooltip.tardis.waypoint_bank.crystal_output", "Waypoints in Crystal");
        add("tooltip.tardis.waypoint_bank.copy_from_crystal", "Copy to/Paste from Data Crystal");
        add("tooltip.tardis.waypoint_bank.copy_from_bank", "Copy to/Paste from Waypoint Bank");
        add("tooltip.tardis.waypoint_bank.delete_from_crystal", "Delete selected Waypoint in Crystal");
        add("tooltip.tardis.waypoint_bank.delete_from_bank", "Delete selected Waypoint in Bank");
        
        add("tooltip.sonic.parts", "Casing Types: ");
        
        add("tooltip.tardis.timelink", "Linked to: ");
        
        add("tooltip.gui.monitor_remote.set_arm_length", "Set a number");
        
        add("tooltip.tardis.transduction_barrier.line1", "Blocks Tardises from landing in a 32 block radius if their Landing Code don't match the ones stored in this block.");
        add("tooltip.tardis.transduction_barrier.line2", "Requires FE and Redstone to be powered.");
        add("tooltip.tardis.transduction_barrier.line3", "Uses FE when blocking Tardises and 1 FE per second when not in use");
        
        add(TardisConstants.Translations.COUNT_TRANSLATION_KEY, " (x%s)");
        
        add("tooltip.tardis.manual.next_page", "Next Page");
        add("tooltip.tardis.manual.previous_page", "Previous Page");
        add("tooltip.tardis.manual.next_chapter", "Next Chapter");
        add("tooltip.tardis.manual.previous_chapter", "Previous Chapter");
        add("tooltip.tardis.manual.last_page", "Last Page");
        add("tooltip.tardis.manual.first_page", "First Page");
        
        //GUI elements
        add("gui.quantiscope.mode.weld", "Fabricate");
        add("gui.quantiscope.mode.sonic", "Reconfig");

        add("gui.tardis.previous", "> Previous");
        add("gui.tardis.next", "> Next");
        add("gui.tardis.select", "> Select");
        add("gui.tardis.confirm", "> Confirm");
        add("gui.tardis.save", "> Save");
        add("gui.tardis.cancel", "> Cancel");
        add("gui.tardis.button.back", "Back");

        add("gui.vm.selected_func", "Selected Function:");
        add("gui.vm.teleport.tp_type", "Teleport Type:");
        add("gui.vm.teleport.setting.top", "Top Block");
        add("gui.vm.teleport.setting.precise", "Precise");
        add("gui.vm.teleport.tp", "Teleport");
        add("gui.vm.teleport.title", "Teleport Player");
        add("gui.vm.distress.title", "Distress Signal");
        add(TardisConstants.Strings.GUI_VM + "teleport.precise_warning", "Warning! This setting may be dangerous!");
        add("gui.vm.distress.select_tardis", "Select Timeship:");
        add("gui.vm.distress.message", "Optional Message: ");
        add("gui.vm.distress.send_signal", "Send Signal");
        add("gui.vm.distress.check_name", "Check Name");
        add("gui.vm.distress.tardis_found", "Timeship Found!");
        add("gui.vm.distress.tardis_invalid", "Invalid Timeship:");
        add("gui.tardis.protocol.int_hum", "Interior Hum");
        add("gui.tardis.protocol.security", "Security Submenu");
        add("gui.tardis.protocol.console", "Change Console Unit");
        add("gui.tardis.protocol.interior", "Change Interior");
        add("gui.tardis.protocol.interior_properties", "Interior Properties Submenu");
        add("gui.tardis.protocol.interior_properties.interior_light_level", "Interior Light Level");
        add("gui.tardis.protocol.interior_properties.interior_light_level.slider", "Interior Light Level");
        add("gui.tardis.protocol.interior_properties.hum", "Interior Hum");
        add("gui.tardis.protocol.interior_properties.console_variant", "> Console Variant: ");
        add("gui.tardis.protocol.interior_properties.sound_scheme", "> Flight Sound Scheme");
        add("gui.tardis.protocol.exterior", "Change Exterior");
        add("gui.tardis.protocol.waypoints", "Waypoint Screen");
        add("gui.tardis.protocol.exterior_properties", "Exterior Properties");
        add("gui.tardis.protocol.exterior_properties.land_code", "> Set Landing Code");
        add("gui.tardis.protocol.exterior_properties.exterior_var", "> Exterior Variant: ");
        add("gui.tardis.protocol.info", "Timeship Flight Information");
        add("gui.tardis.info.title", "Timeship Flight Information");
        add("gui.tardis.info.location", "Location: ");
        add("gui.tardis.info.dimension", "Dimension: ");
        add("gui.tardis.info.facing", "Facing: ");
        add("gui.tardis.info.target_location", "Target: ");
        add("gui.tardis.info.target_dimension", "Target Dim: ");
        add("gui.tardis.info.artron", "Artron Units: ");
        add("gui.tardis.info.journey", "Journey: ");

        add("gui.tardis.interior.change.warning_message", "Any existing bed positons, paintings and placed blocks in the Tardis will be removed!\nWaypoints will be saved inside a Data Crystal Item inside the Reclamation Unit.\nAny blocks in the 'reclamation_blacklist' Block Tag will NOT have their contents saved.");
        add("gui.tardis.interior.change.confirm_message", "Are you sure you wish to change interiors?"); 

        add("gui.tardis.interior.change_cancel.warning_message", "The Interior Reconfiguration Process will be cancelled.");
        add("gui.tardis.interior.cancel_confirm.confirm_message","Are you sure you wish to continue with the cancellation?");
        
        add("gui.tardis.sound_scheme.title", "Sound Scheme Selection");
        add("gui.tardis.sound_scheme.desc", "Set Flight sounds for this Timeship");
        add("gui.tardis.sound_scheme.tv", "TV");
        add("gui.tardis.sound_scheme.basic", "Default");
        add("gui.tardis.sound_scheme.master", "Master");
        add("gui.tardis.sound_scheme.junk", "Junk");
        add("gui.tardis.waypoint.title", "Waypoints");
        add("gui.tardis.waypoint.delete", "Delete Current Waypoint");
        add("gui.tardis.waypoint.new", "Create New Waypoint");
        add("gui.tardis.distress_signal.message_desc", "Enter Distress Message:");
        add("gui.tardis.distress_signal.select_ship", "Send signal to:");
        add("gui.tardis.ars_tablet.gui_title", "ARS Structure Selection");
        add("gui.tardis.ars_tablet.gui_info", "Select Structure Type to Generate:");
        add("gui.tardis.ars_tablet.kill.title", "Please confirm room deletion");
        add("gui.tardis.ars_tablet.kill.confirm", "> Confirm");
        add("gui.tardis.ars_tablet.kill.close", "> Close");
        add("gui.tardis.land_code.title", "Timeship Landing Codes");
        add("gui.tardis.land_code.desc", "Set Private Landing Code: ");
        add("gui.tardis.land_code.set", "> Set Code");
        add("gui.tardis.land_code.suggestion.default", "Enter Land Code:");
        add("gui.tardis.land_code.suggestion.current_value", "Current Code: ");
        add("gui.tardis.waypoint.new.title", "Waypoint Creation");
        add("gui.tardis.waypoint.new.desc", "Set New Waypoint Name: ");
        add("gui.tardis.waypoint.new.suggestion", "Enter Waypoint Name");
        add("gui.tardis.waypoint.no_banks", "No Waypoint Banks Found!");
        add("gui.tardis.waypoint.no_saved", "No Saved Waypoints!");

        add("gui.tardis.waypoint.delete.confirm_title", "Confirm Waypoint Delete");
        add("gui.tardis.waypoint.delete.confirm_message", "Are you sure you wish to delete this waypoint?");
        add("gui.tardis.communicator.load", "> Load in Nav-Com");
        add("gui.tardis.communicator.ignore", "> Ignore");
        add("gui.tardis.communicator.ignore_all", "> Ignore All");
        add("gui.tardis.communicator.message_title", "Distress Signal Detected!");
        add("gui.tardis.communicator.message_subtitle", "Message Contents:");

        add("gui.tardis.remote.view.north_west", "NW");
        add("gui.tardis.remote.view.south_east", "SE");
        add("gui.tardis.remote.view.orbit", "Orbit");
        add("gui.tardis.transduction.title", "Transduction Barrier");
        add("gui.tardis.transduction.land_code.update", "Update Land Code");
        add("gui.tardis.transduction.land_code.desc", "Set Private Landing Code:");
        add("gui.tardis.transduction.land_code.suggestion", "Enter Landing Code");
        
        add("gui.tardis.diagnostic.menu.artron_use", " > Artron Uses");
        add("gui.tardis.diagnostic.artron_use", "Artron Uses:");
        add("gui.tardis.diagnostic.menu.subsystem", " > Subsystems");
        add("gui.tardis.diagnostic.menu.location", " > Timeship Location");
        add("gui.tardis.diagnostic.title.location", "Timeship Location");
        add("gui.tardis.diagnostic.location", "Located at: %s in %s");
        add("gui.tardis.diagnostic.menu.forge_energy", " > Forge Energy");
        add("gui.tardis.diagnostic.title.forge_energy", "Forge Energy Buffer");
        add("gui.tardis.diagnostic.forge_energy", "%s/%s FE");
        
        add("gui.tardis.monitor_remote.scanner_title","Scanner View Mode");
        add("gui.tardis.monitor_remote.arm_length_title", "Rotatable Monitor Arm Extension Length");
        add("gui.tardis.monitor_remote.arm_length_button", "Update Arm Length");
        //World Text
        add("text.tardis.plaque.line1", "TT Type 40, MK. III");
        add("text.tardis.plaque.line2", "Kasterborous Blackhole Shipyards");
        add("text.tardis.plaque.line3", "Designation: %s");
        add("text.tardis.transduction.line1", "");
        add("text.tardis.transduction.line2", "WARNING:");
        add("text.tardis.transduction.line3", "Redirection device detected!");
        add("text.tardis.fall_below_world.line1", "");
        add("text.tardis.fall_below_world.line2", "WARNING: ");
        add("text.tardis.fall_below_world.line3", "Exterior has fallen below world threshold, redirecting!");
        
        //Overlays
        add("overlay.tardis.system_title", "==-Timeship Systems Readout-==");
        add("overlay.tardis.fe_power", "Timeship Forge Energy: %s FE");
        
        
        //Advancements
        add("advancements.tardis.title.tardis_mod", "New TARDIS Mod");
        add("advancements.tardis.desc.tardis_mod", "An Adventure in Space and Time!");
        add("advancements.tardis.title.obtained_tardis", "11D Euclidean Geometry");
        add("advancements.tardis.desc.obtained_tardis", "Find and access a broken down TARDIS!");
        add("advancements.tardis.title.vortex", "Forward to the Past...");
        add("advancements.tardis.desc.vortex","Travel through the Time Vortex!");
        add("advancements.tardis.title.moon", "One Small Step...");
        add("advancements.tardis.desc.moon", "Step on the Moon");
        add("advancements.tardis.title.space","The Final Frontier...");
        add("advancements.tardis.desc.space", "Enter the endless Space.");
        add("advancements.tardis.title.space_suit", "Spaaaaace!");
        add("advancements.tardis.desc.space_suit", "Obtain a Spacesuit!");
        add("advancements.tardis.title.mission.drone_station.complete", "Timely Rescue!");
        add("advancements.tardis.desc.mission.drone_station.complete", "Rescue a space station crew from rogue drones!");
//        add("advancements.tardis.interior.root", "Not too Shabby");
//        add("advancements.tardis.interior.root.desc", "Recover Interior Schematics for your TARDIS");
//        add("advancements.tardis.interior.nemo", "Down Where it's Wetter");
//        add("advancements.tardis.interior.nemo.desc", "Unlock the Nautilus Interior for your TARDIS");
//        add("advancements.tardis.interior.panamax", "A Tight Squeeze");
//        add("advancements.tardis.interior.panamax.desc", "Unlock the Panamax Interior for your TARDIS");
//        add("advancements.tardis.exterior.root", "Not Just a Box...");
//        add("advancements.tardis.exterior.root.desc", "Recover Exterior Schematics for your TARDIS");
//        add("advancements.tardis.exterior.fortune", "Answer Unclear, Try Again Later");
//        add("advancements.tardis.exterior.fortune.desc", "Unlock the Fortune Teller Exterior for your TARDIS");
//        add("advancements.tardis.exterior.safe", "Safe Space");
//        add("advancements.tardis.exterior.safe.desc", "Unlock the Safe Exterior for your TARDIS");
//        add("advancements.tardis.exterior.trunk", "All Hands Lost...");
//        add("advancements.tardis.exterior.trunk.desc", "Unlock the Trunk Exterior for your TARDIS");
//        add("advancements.tardis.interior.envoy", "Trusted Ambassador!");
//        add("advancements.tardis.interior.envoy.desc", "Unlock the Envoy Interior for your TARDIS");
//        add("advancements.tardis.interior.traveler", "Around the World!");
//        add("advancements.tardis.interior.traveler.desc", "Unlock the Traveler Interior for your TARDIS");
        
        //Command Feedback
        add("command.tardis.setup.success", "Successfully setup Tardis Components, Artron and Key for tardis \u00A7d%s");

        add("command.tardis.unlock.exterior_all", "Unlocked All Exteriors for tardis %s");
        add("command.tardis.unlock.exterior", "Unlocked Exterior %s for tardis %s");
        add("command.tardis.unlock.interior_all", "Unlocked All Interiors for tardis %s");
        add("command.tardis.unlock.interior", "Unlocked Interior %s for tardis %s");
        add("command.tardis.unlock.console","Unlocked Console %s for tardis %s");
        add("command.tardis.unlock.console_all", "Unlocked All Consoles for tardis %s");
        
        add("command.tardis.matching_dim", "Matched Dimension: %s");
        add("command.tardis.matching_name", "Matched Name: %s");
        
        add("command.tardis.loyalty.success", "%s's loyalty to player %s has been updated to %s");
        add("command.tardis.loyalty.no_player_found", "Player %s has not interacted with this Tardis");
        add("command.tardis.loyalty.check", "Loyalties found for Tardis %s\n%s");

        add("command.tardis.refuel.check", "Fuel Values for Tardis %s: \nCurrent Fuel: %s\nMax Capacity: %s\n\nArtron Uses: \n%s");
        add("command.tardis.refuel.success", "Set fuel for Tardis %s to: %s AU");

        add("command.tardis.artron.check", "Artron value: %s");
        add("command.tardis.artron.add", "Requested adding %s charge, %s Artron charge added, value is now %s.");
        add("command.tardis.artron.invalid_item","Error: Item %s is not a valid Artron Battery!");
        
        add("command.tardis.traits.check", "Traits found on %s\n%s");
        add("command.tardis.traits.regenerate", "Traits regenerated on %s\nNew Traits: \n%s");
        
        add("command.tardis.attune.success", "Attuned Item %s to tardis: \n%s");
        add("command.tardis.attune.invalid", "Error: Item %s is not attunable");
        
        add("command.tardis.interior.success","Teleported %s to interior of tardis %s");
        
        add("command.tardis.mission.spawn.success", "Succesfully spawned MissionType: %s at %s");
        add("command.tardis.mission.spawn.fail", "Failed to spawn MissionType: %s at %s");
        
        add("command.tardis.mission.objective_update.success","Successfully set objective for stage %s of mission %s at %s to %s");
        
        add("command.tardis.mission.stage_update.success","Successfully set stage of mission %s at %s to %s");
        add("command.tardis.mission.objective_get.success", "Mission %s at %s is currently at Objective %s for stage %s");
        add("command.tardis.mission.stage_get.success", "Mission %s at %s is currently at Stage %s");
        
        add("command.tardis.mission.no_mission_found_at_pos", "No Mission found at %s");
        add("command.tardis.mission.no_stage_found_for_mission", "Error: No Stage found for Mission %s at %s");
        
        add("command.tardis.mission.invalid_mission", "Error: MissionType not found!");
        add("command.tardis.mission.invalid_mission_world", "World %s does not support spawning of missions!");
        
        add("command.tardis.disguise.spawn.success","Successfully placed blocks of Disguise %s at %s");
        add("command.tardis.disguise.invalid_disguise", "Error: Disguise not found, or the Disguise blocks are empty");
        
        add("command.tardis.rift.find.success", "Found Rifts within %s chunks from %s: \n%s");
        add("command.tardis.rift.find.error", "Could not find Rift at %s");
        add("command.tardis.rift.get_energy.success", "Rift at %s has %s AU");
        add("command.tardis.rift.add_energy.success", "Added %s AU to Rift at %s. Rift now has %s AU");
        add("command.tardis.rift.create_rift.success", "Created Rift with %s AU at %s");
        add("command.tardis.rift.remove_rift.success", "Removed Rift of %s AU at %s");
        
        add("command.tardis.create.success","Created Tardis %s of Exterior %s, Console Room %s, Console Unit %s");
        
        add("command.suggestion.tardis.no_connections", "Error_No_Connections_to_Any_Tardises");
        add("command.suggestion.tardis.no_connections_tooltip", "This player has not interacted with any Tardises");

        add("argument.tardis.console_room.invalid", "Invalid Console Room: %s");
        add("argument.tardis.exterior.invalid", "Invalid Exterior: %s");
        add("argument.tardis.console_unit.invalid","Invalid Console Unit: %s");
        add("argument.tardis.mission_type.invalid","Invalid Mission Type: %s");
        add("argument.tardis.disguise.invalid", "Invalid Disguise: %s");
        
        //Sound subtitles
        add(getSoundTranslationKey(TSounds.ALARM_LOW.get()), "Tardis Cloister Bell");
        add(getSoundTranslationKey(TSounds.AMBIENT_CREAKS.get()), "Tardis creaks");
        add(getSoundTranslationKey(TSounds.BESSIE_DRIVE.get()), "Vrooom");
        add(getSoundTranslationKey(TSounds.BESSIE_HORN.get()), "Honk");
        add(getSoundTranslationKey(TSounds.CANT_START.get()), "Tardis Struggles to Start");
        add(getSoundTranslationKey(TSounds.CAR_LOCK.get()), "Beep Beep");
        add(getSoundTranslationKey(TSounds.COMMUNICATOR_BEEP.get()), "Incoming Transmission");
        add(getSoundTranslationKey(TSounds.COMMUNICATOR_PHONE_PICKUP.get()), "Telephone Rings");
        add(getSoundTranslationKey(TSounds.COMMUNICATOR_RING.get()), "Telephone Rings");
        add(getSoundTranslationKey(TSounds.COMMUNICATOR_STEAM.get()), "Transmission Static");
        add(getSoundTranslationKey(TSounds.DIMENSION.get()), "Destination Dimension Changed");
        add(getSoundTranslationKey(TSounds.DIRECTION.get()), "Tardis Door Facing Modified");
        add("subtitle.tardis.generic_control", "Controls beep");
        add("subtitle.tardis.control_landing_type", "Landing Type Modified");
        add(getSoundTranslationKey(TSounds.REFUEL_START.get()), "Refuelling Started");
        add(getSoundTranslationKey(TSounds.REFUEL_STOP.get()), "Refuelling Stopped");
        add(getSoundTranslationKey(TSounds.STABILIZER_OFF.get()), "Stabilizers Deactivated");
        add(getSoundTranslationKey(TSounds.STABILIZER_ON.get()), "Stabilizers Activated");
        add(getSoundTranslationKey(TSounds.DALEK_ARM.get()), "Dalek arm rotates");
        add(getSoundTranslationKey(TSounds.DALEK_DEATH.get()), "Argghrgwwrhg");
        add(getSoundTranslationKey(TSounds.DALEK_EXTERMINATE.get()), "Exterminate!");
        add(getSoundTranslationKey(TSounds.DALEK_FIRE.get()), "Energy beam fires");
        add(getSoundTranslationKey(TSounds.DALEK_HOVER.get()), "Gravity Projectors Hum");
        add(getSoundTranslationKey(TSounds.DALEK_MOVES.get()), "Mechanical Whirring");
        add(getSoundTranslationKey(TSounds.DALEK_SW_AIM.get()), "Servo rotates");
        add(getSoundTranslationKey(TSounds.DALEK_SW_FIRE.get()), "Energy bolt discharges");
        add(getSoundTranslationKey(TSounds.DALEK_SW_HIT_EXPLODE.get()), "Explosion");
        add(getSoundTranslationKey(TSounds.DOOR_CLOSE.get()), "Tardis Door closes");
        add(getSoundTranslationKey(TSounds.DOOR_KNOCK.get()), "Knock Knock Knock Knock Knock");
        add(getSoundTranslationKey(TSounds.DOOR_LOCK.get()), "Tardis Door locked");
        add(getSoundTranslationKey(TSounds.DOOR_OPEN.get()), "Tardis Door opens");
        add(getSoundTranslationKey(TSounds.DOOR_UNLOCK.get()), "Tardis Door unlocked");
        add(getSoundTranslationKey(TSounds.ELECTRIC_ARC.get()), "Electricity Arcs");
        add(getSoundTranslationKey(TSounds.ELECTRIC_SPARK.get()), "Circuits Spark");
        add(getSoundTranslationKey(TSounds.EYE_MONITOR_INTERACT.get()), "Monitor activates");
        add(getSoundTranslationKey(TSounds.GADGET_MENU_OPEN.get()), "Gadget Interface Opens");
        add(getSoundTranslationKey(TSounds.HANDBRAKE_ENGAGE.get()), "Handbrake engaged");
        add(getSoundTranslationKey(TSounds.HANDBRAKE_RELEASE.get()), "Handbrake released");
        add(getSoundTranslationKey(TSounds.LASER_GUN_FIRE.get()), "Pew");
        add(getSoundTranslationKey(TSounds.PAPER_DROP.get()), "Paper scatters");
        add(getSoundTranslationKey(TSounds.POWER_DOWN.get()), "Tardis powers down");
        add(getSoundTranslationKey(TSounds.RANDOMISER.get()), "Randomiser wobbles");
        add(getSoundTranslationKey(TSounds.REACHED_DESTINATION.get()), "Tardis Landed");
        add(getSoundTranslationKey(TSounds.REMOTE_ACCEPT.get()), "Device Beeps");
        add(getSoundTranslationKey(TSounds.ROTOR_END.get()), "Time Rotor Shutting Down");
        add(getSoundTranslationKey(TSounds.ROTOR_START.get()), "Time Rotor Starts");
        add(getSoundTranslationKey(TSounds.ROTOR_TICK.get()), "Time Rotor Ticks");
        add(getSoundTranslationKey(TSounds.SCREEN_BEEP_SINGLE.get()), "Buttons Beep");
        add(getSoundTranslationKey(TSounds.SHIELD_HUM.get()), "Shield Projectors Hum");
        add(getSoundTranslationKey(TSounds.SINGLE_CLOISTER.get()), "Tardis Cloister Bell");
        add(getSoundTranslationKey(TSounds.SNAP.get()), "Fingers Snap");
        add(getSoundTranslationKey(TSounds.SONIC_BROKEN.get()), "Sonic Screwdriver destroyed");
        add(getSoundTranslationKey(TSounds.SONIC_FAIL.get()), "Sonic Screwdriver Activation Fail");
        add(getSoundTranslationKey(TSounds.SONIC_GENERIC.get()), "Sonic Screwdriver Buzzes");
        add(getSoundTranslationKey(TSounds.SONIC_MODE_CHANGE.get()), "Sonic Screwdriver Mode Change");
        add(getSoundTranslationKey(TSounds.SONIC_TUNING.get()), "Sonic Screwdriver Tuning");
        add(getSoundTranslationKey(TSounds.SPACE_AMBIENT_LOOP.get()), "Space Ambience");
        add(getSoundTranslationKey(TSounds.STEAM_HISS.get()), "Steam Hiss");
        add(getSoundTranslationKey(TSounds.STEAMPUNK_MONITOR_INTERACT.get()), "Monitor Activated");
        add(getSoundTranslationKey(TSounds.SUBSYSTEMS_OFF.get()), "Subsystem Deactivated");
        add(getSoundTranslationKey(TSounds.SUBSYSTEMS_ON.get()), "Subsystem Activated");
        add(getSoundTranslationKey(TSounds.TARDIS_FLY_LOOP.get()), "Vworp");
        add("subtitle.tardis.tardis_hum", "Tardis Hums");
        add(getSoundTranslationKey(TSounds.TARDIS_LAND.get()), "Tardis Rematerialises");
        add(getSoundTranslationKey(TSounds.TARDIS_LAND_NOTIFICATION.get()), "Tardis Landing Notification");
        add(getSoundTranslationKey(TSounds.TARDIS_POWER_UP.get()), "Tardis Powers Up");
        add(getSoundTranslationKey(TSounds.TARDIS_TAKEOFF.get()), "Tardis Dematerialising");
        add(getSoundTranslationKey(TSounds.TELEPATHIC_CIRCUIT.get()), "Telepathic Circuits Pulse");
        add(getSoundTranslationKey(TSounds.TARDIS_FIRST_ENTRANCE.get()), "Timeship Welcome");
        add(getSoundTranslationKey(TSounds.TARDIS_SHUT_DOWN.get()), "Tardis Shuts Down");
        add(getSoundTranslationKey(TSounds.THROTTLE.get()), "Throttle clicks");
        add(getSoundTranslationKey(TSounds.VM_BUTTON.get()), "Beep");
        add(getSoundTranslationKey(TSounds.VM_TELEPORT.get()), "Vortex Manipulator Teleportation");
        add(getSoundTranslationKey(TSounds.VORTEX_AMBIENT_LOOP.get()), "Vortex Ambience");
        add(getSoundTranslationKey(TSounds.WATCH_MALFUNCTION.get()), "Watch Malfunctions");
        add(getSoundTranslationKey(TSounds.WATCH_TICK.get()), "Tick Tick");

        add("biome.tardis.tardis", "TARDIS Biome");
        
        //KeyBindings
        add(TardisConstants.Strings.KEYBINDING_CATEGORY, "TARDIS Mod");
        add("key.tardis.snap", "Tardis Door Snap");
        add("key.tardis.reload_laser_gun", "Reload Laser Gun");
        
        //Statistics
        add("stat.tardis.subsystems_broken", "Tardis Subsystems Broken");
        
        //Compatibility translations
        
        //JEI Recipe titles
        add("compat.tardis.jei.alembic_title", "Alembic");
        add("compat.tardis.jei.quantiscope_title", "Quantiscope Welding");
        add("compat.tardis.jei.spectrometer_title", "Spectrometer");
        add("compat.tardis.jei.ars_title", "ARS System");
        add("compat.tardis.jei.attunement_title", "Attunable");

    }
    
    public Path getSoundPath(Path path, String modid) {
        return path.resolve("data/" + modid + "/sounds/" + "sounds.json");
    }

    public String fixCapitalisations(String text) {
        String original = text.trim().replace("    ", "").replace("_", " ").replace("/", ".");
        String output = Arrays.stream(original.split("\\s+"))
                .map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
                .collect(Collectors.joining(" "));
        return output;
    }
    
    public String getControlTranslationKeyFromEntry(ControlEntry entry) {
        ResourceLocation loc = entry.getRegistryName();
        return "control" + "." + loc.getNamespace() + "." + loc.getPath();
    }
    
    public String getSoundTranslationKey(SoundEvent sound) {
    	String subtitleTranslationKey = "";
		if (subtitleTranslationKey.isEmpty() || subtitleTranslationKey == null) {
			subtitleTranslationKey = Util.makeTranslationKey("subtitle", sound.getRegistryName());
		}
		return subtitleTranslationKey;
	}
    
    

}
