package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.function.Supplier;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.Tags;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.items.TItems;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.recipe.SpectrometerRecipe;
import net.tardis.mod.schematics.Schematic;
import net.tardis.mod.schematics.Schematics;
import net.tardis.mod.tags.TardisItemTags;

public class SpectrometerRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public SpectrometerRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        //Register core interiors for data gen
        if (ConsoleRoom.getRegistry().isEmpty())
            ConsoleRoom.registerCoreConsoleRooms();

        //TODO: Make recipes for all consoles, exteriors, interiors
        //Consoles
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.PRISMARINE), () -> Schematics.Consoles.NEMO);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.FURNACE), () -> Schematics.Consoles.STEAM);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.IRON_INGOT), () -> Schematics.Consoles.GALVANIC);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromTag(TardisItemTags.DEAD_CORAL), () -> Schematics.Consoles.CORAL);
        //createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.HARTNELL); //In crashed Ships
        //createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Consoles.TOYOTA);//In crashed Ships
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(TBlocks.xion_crystal.get()), () -> Schematics.Consoles.XION);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.ENDER_EYE), () -> Schematics.Consoles.NEUTRON);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.POTION), () -> Schematics.Consoles.POLYMEDICAL);
        
        //Exteriors
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.BLAZE_POWDER), () -> Schematics.Exteriors.STEAMPUNK);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.CHEST), () -> Schematics.Exteriors.TRUNK);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.BELL), () -> Schematics.Exteriors.TELEPHONE);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.CAULDRON), () -> Schematics.Exteriors.FORTUNE);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.IRON_DOOR), () -> Schematics.Exteriors.SAFE);
        //createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.TT_CAPSULE); //Default unlocked
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.CLOCK), () -> Schematics.Exteriors.CLOCK);
        //createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.TT_2020); //This one is found in space stations
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.BAMBOO), () -> Schematics.Exteriors.JAPAN);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Blocks.BREWING_STAND), () -> Schematics.Exteriors.APERTURE);
        //createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.POLICE_BOX); //From crashed ships
        //createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.POLICE_BOX_MODERN); //From crashed ships
        //createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(), () -> Schematics.Exteriors.DISGUISE); Always Unlocked
        
        //Interiors
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.BLAST_FURNACE), () -> Schematics.Interiors.STEAM);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.ACACIA_LOG), () -> Schematics.Interiors.JADE);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.NAUTILUS_SHELL), () -> Schematics.Interiors.NAUTILUS);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.QUARTZ_PILLAR), () -> Schematics.Interiors.OMEGA);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.POLISHED_DIORITE), () -> Schematics.Interiors.ALABASTER);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromTag(TardisItemTags.CONCRETE), () -> Schematics.Interiors.ARCHITECT);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromTag(TardisItemTags.CORAL), () -> Schematics.Interiors.CORAL);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromTag(ItemTags.BOATS), () -> Schematics.Interiors.PANAMAX);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.LEATHER_BOOTS), () -> Schematics.Interiors.TRAVELER);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.EMERALD), () -> Schematics.Interiors.ENVOY);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.PURPUR_BLOCK), () -> Schematics.Interiors.AMETHYST);
        createSpectrometerRecipe(path, cache, 200, Ingredient.fromItems(Items.POLISHED_BLACKSTONE), () -> Schematics.Interiors.IMPERIAL);
        
    }

    @Override
    public String getName() {
        return "TARDIS Spectrometer Recipe Generator";
    }
    
    public void createSpectrometerRecipe(Path path, DirectoryCache cache, int progressTicks, Ingredient ingredient, Supplier<Schematic> output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(progressTicks, ingredient, output.get()), getPath(path, output.get()));
    }

    public void createSpectrometerRecipe(Path path, DirectoryCache cache, Item item, Supplier<Schematic> output) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(200, Ingredient.fromItems(item), output.get()), getPath(path, output.get()));
    }
    
    public static Path getPath(Path path, Schematic output) {
        ResourceLocation key = output.getId();
        return path.resolve("data/" + key.getNamespace() + "/recipes/spectrometer/" + key.getPath() + ".json");
    }
    
    public JsonObject createRecipe(int processTicks, Ingredient ingredient, Schematic output) {
    	SpectrometerRecipe recipe = new SpectrometerRecipe(processTicks, ingredient, output.getId());
    	JsonObject recipeJson = recipe.serialise();
        return recipeJson;
    }
}