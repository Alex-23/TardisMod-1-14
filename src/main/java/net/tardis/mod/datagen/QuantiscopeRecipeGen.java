package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mojang.serialization.JsonOps;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.items.TItems;
import net.tardis.mod.recipe.TardisRecipeSerialisers;
import net.tardis.mod.recipe.WeldRecipe;
/**
 * Generate recipes and repair recipes for Quantiscope
 */
public class QuantiscopeRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public QuantiscopeRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        createNormalRecipe(path, cache, TItems.KEY_FOB_UPGRADE.get(), Ingredient.fromItems(TItems.BLANK_UPGRADE.get(), TItems.CIRCUITS.get(), Items.IRON_DOOR, Items.TRIPWIRE_HOOK, Items.GOLD_INGOT));
        createNormalRecipe(path, cache, TItems.TIME_LINK_UPGRADE.get(), Ingredient.fromItems(TItems.BLANK_UPGRADE.get(), Items.REDSTONE, Items.ENDER_EYE, TItems.CIRCUITS.get()));
        createNormalRecipe(path, cache, TItems.ZERO_ROOM_UPGRADE.get(), Ingredient.fromItems(TItems.BLANK_UPGRADE.get(), Items.GHAST_TEAR, Items.BLAZE_ROD, TItems.CIRCUITS.get()));
    }

    @Override
    public String getName() {
        return "TARDIS Quantiscope Recipe Generator";
    }
    
    public static Path getPath(Path base, Item output) {
        ResourceLocation key = output.getRegistryName();
        return base.resolve("data/" + key.getNamespace() + "/recipes/quantiscope/" + key.getPath() + ".json");
    }
    
    public static Path getPathRepair(Path base, Item output) {
        ResourceLocation key = output.getRegistryName();
        return base.resolve("data/" + key.getNamespace() + "/recipes/quantiscope/" + key.getPath() + "_repair" + ".json");
    }

    /**
     * Create a non-repair recipe. Make sure it is a unique recipe
     * @param output
     * @param ingredients - items that are used to craft the output item. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @throws IOException 
     */
    public void createNormalRecipe(Path base, DirectoryCache cache, Item output, Ingredient ingredients) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(output, ingredients), getPath(base, output));
    }
    
    public void createNormalRecipe(Path base, DirectoryCache cache, Item output, Ingredient ingredients, int progressTicks) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipeWithProcessTicks(output, ingredients, progressTicks), getPath(base, output));
    }
    
    /**
     * Create a repair recipe. Make sure it is a unique recipe
     * @param itemToRepair - this is the item that needs repairing
     * @param repairItems - items that are used to repair itemToRepair. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @throws IOException 
     */
    public void createRepairRecipe(Path base, DirectoryCache cache, Item itemToRepair, Ingredient repairItems) throws IOException {
        IDataProvider.save(GSON, cache, this.createRepairRecipe(itemToRepair, repairItems), getPathRepair(base, itemToRepair));
    }
    
    /**
     * Creates a non-repair recipe. Make sure it is a unique recipe
     * @param output - this is the item that is output at the end
     * @param ingredients - the items that are used to craft output. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @return {@link JsonObject}
     */
    public JsonObject createRecipe(Item output, Ingredient ingredients) {
        WeldRecipe recipe = new WeldRecipe(false, Optional.empty(), ingredients, Optional.of(new WeldRecipe.RecipeResult(output)));
        return WeldRecipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
                .ifLeft(element -> {
            	    JsonObject json = element.getAsJsonObject();
            	    json.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
                }).ifRight(right -> {
            	    Tardis.LOGGER.error(right.message());
                }).orThrow().getAsJsonObject();
    }
    
    public JsonObject createRecipeWithProcessTicks(Item output, Ingredient ingredients, int progressTicks) {
    	WeldRecipe recipe = new WeldRecipe(false, Optional.empty(), Optional.of(progressTicks), ingredients, Optional.of(new WeldRecipe.RecipeResult(output)));
        return WeldRecipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
                .ifLeft(element -> {
            	    JsonObject json = element.getAsJsonObject();
            	    json.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
                }).ifRight(right -> {
            	    Tardis.LOGGER.error(right.message());
                }).orThrow().getAsJsonObject();
    }
    
    /**
     * Creates a repair recipe
     * @param itemToRepair - this is the item that needs repairing
     * @param repairItems - the items that are used to repair the itemToRepair. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @return {@link JsonObject}
     */
    public JsonObject createRepairRecipe(Item itemToRepair, Ingredient repairItems) {
    	WeldRecipe recipe = new WeldRecipe(false, Optional.of(itemToRepair), Optional.empty(), repairItems, Optional.of(new WeldRecipe.RecipeResult(itemToRepair)));
        return WeldRecipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
                .ifLeft(element -> {
            	    JsonObject json = element.getAsJsonObject();
            	    json.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
                }).ifRight(right -> {
            	    Tardis.LOGGER.error(right.message());
                }).orThrow().getAsJsonObject();
    }
    
    public JsonObject createRepairRecipeWithProgress(Item itemToRepair, Ingredient repairItems, int progressTicks) {
    	WeldRecipe recipe = new WeldRecipe(false, Optional.of(itemToRepair), Optional.of(progressTicks), repairItems, Optional.of(new WeldRecipe.RecipeResult(itemToRepair)));
        return WeldRecipe.getCodec().encodeStart(JsonOps.INSTANCE, recipe).get()
                .ifLeft(element -> {
            	    JsonObject json = element.getAsJsonObject();
            	    json.addProperty("type", TardisRecipeSerialisers.WELD_RECIPE_TYPE_LOC.toString());
                }).ifRight(right -> {
            	    Tardis.LOGGER.error(right.message());
                }).orThrow().getAsJsonObject();
    }

}
