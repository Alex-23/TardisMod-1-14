package net.tardis.mod.datagen.compat;

import net.minecraft.block.Block;
import net.minecraft.data.BlockTagsProvider;
import net.minecraft.data.DataGenerator;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.BrokenExteriorBlock;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.blocks.QuantiscopeBlock;
import net.tardis.mod.blocks.exteriors.ExteriorBlock;
import net.tardis.mod.tags.TardisBlockTags;
/** Data Generator to prevent our blocks being moved by mods like Create, which can cause a duplication exploit*/
public class MovableBlockBlacklistGen extends BlockTagsProvider {

    public static final ITag.INamedTag<Block> CREATE_NON_MOVEABLE_PROOF = TardisBlockTags.makeBlock(new ResourceLocation("create", "non_movable"));

    public MovableBlockBlacklistGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
        super(generatorIn, Tardis.MODID, existingFileHelper);
    }

    @Override
    protected void registerTags() {
        for (Block block : ForgeRegistries.BLOCKS) {
            if(block instanceof ConsoleBlock || block instanceof BrokenExteriorBlock || block instanceof ExteriorBlock || block instanceof QuantiscopeBlock){
                add(CREATE_NON_MOVEABLE_PROOF, block);
            }
        }
    }

    public void add(ITag.INamedTag< Block > branch, Block block) {
        this.getOrCreateBuilder(branch).add(block);
    }

    public void add(ITag.INamedTag< Block > branch, Block... block) {
        this.getOrCreateBuilder(branch).add(block);
    }
}