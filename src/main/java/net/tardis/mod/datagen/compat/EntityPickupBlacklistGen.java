package net.tardis.mod.datagen.compat;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.EntityTypeTagsProvider;
import net.minecraft.entity.EntityType;
import net.minecraft.tags.ITag;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.tags.TardisEntityTypeTags;
/** Data Generator to prevent our entities being moved by mods like Carry On, which can cause a game crash*/
public class EntityPickupBlacklistGen extends EntityTypeTagsProvider{
	
	public static final ITag.INamedTag<EntityType<?>> CARRY_ON_PROOF = TardisEntityTypeTags.makeEntity(new ResourceLocation("carryon", "entity_blacklist"));

	public EntityPickupBlacklistGen(DataGenerator generator, ExistingFileHelper existingFileHelper) {
		super(generator, Tardis.MODID, existingFileHelper);
	}

	@Override
	protected void registerTags() {
		for (EntityType<?> type : ForgeRegistries.ENTITIES) {
			if (type == TEntities.DISPLAY_TARDIS.get() || type == TEntities.TARDIS.get() || type == TEntities.CONTROL.get()) {
				add(CARRY_ON_PROOF, type);
			}
		}
	}
	
	public void add(ITag.INamedTag<EntityType<?>> branch, EntityType<?> type) {
        this.getOrCreateBuilder(branch).add(type);
    }

    public void add(ITag.INamedTag<EntityType<?>> branch, EntityType<?>... type) {
        this.getOrCreateBuilder(branch).add(type);
    }

}
